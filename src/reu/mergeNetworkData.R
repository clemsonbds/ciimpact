rm(list=ls()) # clean up everything
load(file=file.path("d:","work","R","ciimpact","data","ncses","clean","longform.RData"))

data.dir <- file.path("d:","work","r","ciimpact","data","ncses","clean") 
setwd(data.dir)

nsf.2005 <- read.csv("nsf_facilities_2005.txt",sep="\t",header=TRUE,stringsAsFactors=FALSE)
nsf.2007 <- read.csv("nsf_facilities_2007.txt",sep="\t",header=TRUE,stringsAsFactors=FALSE)
nsf.2009 <- read.csv("nsf_facilities_2009.txt",sep="\t",header=TRUE,stringsAsFactors=FALSE)
nsf.2011 <- read.csv("nsf_facilities_2011.txt",sep="\t",header=TRUE,stringsAsFactors=FALSE)

faculty.data <- read.csv("Faculty-VHR-HR_2000_2012.txt",sep="\t",header=TRUE,stringsAsFactors=FALSE)
publication.data <- read.csv("publication_vhr_hr_2000_2009.txt",sep="\t",header=TRUE,stringsAsFactors=FALSE)
top500 <- read.csv("top500_20032011.txt",sep="\t",header=TRUE,stringsAsFactors=FALSE)


new.data <- subset(longform.ncses.table,longform.ncses.table$Year %in% seq(2005,2009))



new.data <- subset(new.data, new.data$FICE %in% faculty.data$FICE)
new.data <- subset(new.data, new.data$FICE %in% publication.data$FICE)
new.data <- subset(new.data, new.data$CC %in% c(15,16))

common.network <- numeric(length(new.data$FICE))
common.faculty <- numeric(length(new.data$FICE))
common.publication <- numeric(length(new.data$FICE))
common.top500.app <- numeric(length(new.data$FICE))
common.top500.drank <- numeric(length(new.data$FICE))


for (i in 1:146){
  index <- 5 * (i - 1) + 1
  
  val.2005 <- as.numeric(as.vector(subset(nsf.2005$P2Q1_1,nsf.2005$INST_ID == new.data$FICE[index])))
  val.2006 <- as.numeric(as.vector(subset(nsf.2005$P2Q1_2,nsf.2005$INST_ID == new.data$FICE[index])))
  val.2007 <- as.numeric(as.vector(subset(nsf.2007$P2Q1_1,nsf.2007$INST_ID == new.data$FICE[index])))
  val.2008 <- as.numeric(as.vector(subset(nsf.2007$P2Q1_2,nsf.2007$INST_ID == new.data$FICE[index])))
  val.2009 <- as.numeric(as.vector(subset(nsf.2009$P2Q1_1,nsf.2009$INST_ID == new.data$FICE[index])))
  
  if (length(val.2005) == 1) {
    common.network[index] <- val.2005
  }
  common.network[index + 1] <- ifelse(length(val.2006) == 1,val.2006, common.network[index])
  common.network[index + 2] <- ifelse(length(val.2007) == 1,val.2007, common.network[index + 1])
  common.network[index + 3] <- ifelse(length(val.2008) == 1,val.2008, common.network[index + 2])
  common.network[index + 4] <- ifelse(length(val.2009) == 1,val.2009, common.network[index + 3])
  
  tmp.faculty <- subset(faculty.data,faculty.data$FICE == new.data$FICE[index])
  common.faculty[index:(index + 4)] <- t(tmp.faculty[,7:11])
  tmp.publication <- subset(publication.data,publication.data$FICE == new.data$FICE[index])
  common.publication[index:(index + 4)] <- t(tmp.publication[,8:12])
  
  for (j in 2005:2009){
    #entry.top500 <- subset(top500,top500$FICE == new.data$FICE[index] & top500$Year %in% seq(j,j-2))
    entry.top500 <- subset(top500,top500$FICE == new.data$FICE[index] & top500$Year %in% seq(j,2003))    
    common.top500.app[index + j - 2005] <- length(entry.top500$Rank)
    common.top500.drank[index + j - 2005] <- 501 * length(entry.top500$Rank) - sum(entry.top500$Rank)
  }  
}

new.data$network <- common.network
new.data$faculty <- common.faculty
new.data$publication <- common.publication
new.data$app <- common.top500.app
new.data$drank <- common.top500.drank

#new.data <- subset(new.data, new.data$FICE != 2077)

require(googleVis)
my.chart <- gvisMotionChart(new.data, idvar="Institution", timevar="Year")
plot(my.chart)

save(new.data,file="longform.allData.RData")


## analysis

lag.list <- list()
lag.top500.list <- list()
corr.list <- list()
corr.top500.list <- list()
unique.fice <- unique(new.data$FICE)
institution.list <- list()

corr.mean <- matrix(0, ncol=8,nrow=8)
corr.stdev <- matrix(0, ncol=8,nrow=8)

corr.top500.mean <- matrix(0,ncol=10,nrow=10)
corr.top500.stdev <- matrix(0, ncol=10,nrow=10)


lag.list.mean <- list()
lag.list.stdev <- list()
lag.list.top500.mean <- list()
lag.list.top500.stdev <- list()

for (i in 1:8){
  lag.list.mean[[i]] <- matrix(0,ncol=8,nrow=5)
  lag.list.stdev[[i]] <- matrix(0, ncol=8,nrow=5)  
}

for (i in 1:10){
  lag.list.top500.mean[[i]] <- matrix(0,ncol=10,nrow=5)
  lag.list.top500.stdev[[i]] <- matrix(0, ncol=10,nrow=5)  
}

cnt.all <- 1
cnt.top500 <- 1
for (i in 1:length(unique.fice)){
  tmp.table <- subset(new.data, new.data$FICE == unique.fice[i])
  tmp.table.top500 <- c()
  if (sum(tmp.table$app) > 0){
    tmp.table.top500 <- tmp.table
  }
  column.list <- c(5:10,23:24)
  lag.list[[i]] <- acf(tmp.table[,column.list],lag.max = 4, plot = FALSE)  
  corr.list[[i]] <- cor(tmp.table[,column.list])
  names(corr.list)[i] <- tmp.table$Institution[1]
  names(lag.list)[i] <- tmp.table$Institution[1]
  
  if (length(tmp.table.top500$Institution) > 0){
    column.list <- c(5:10,23:26)
    lag.top500.list[[cnt.top500]] <- acf(tmp.table.top500[,column.list],lag.max = 4, plot = FALSE)  
    corr.top500.list[[cnt.top500]] <- cor(tmp.table.top500[,column.list])
    names(corr.top500.list)[cnt.top500] <- tmp.table.top500$Institution[1]
    names(lag.top500.list)[cnt.top500] <- tmp.table.top500$Institution[1]
    cnt.top500 <- cnt.top500 + 1
  }   
}

for (i in 1:8){
  for (j in 1:8){
    corr.vector <- rep(-2,length(corr.list))
    for (k in 1:length(corr.vector)){
      if (!is.na(corr.list[[k]][i,j])){
        corr.vector[k] <- corr.list[[k]][i,j]
      }
    }
    corr.vector <- subset(corr.vector, corr.vector != -2)
    corr.mean[i,j] <- mean(corr.vector)
    corr.stdev[i,j] <- sd(corr.vector)    
  }
}
colnames(corr.mean) <- colnames(corr.list[[1]])
rownames(corr.mean) <- rownames(corr.list[[1]])
colnames(corr.stdev) <- colnames(corr.list[[1]])
rownames(corr.stdev) <- rownames(corr.list[[1]])


for (i in 1:5){
  for (j in 1:8){    
    for (m in 1:8){
      corr.vector <- rep(-2, length(lag.list))
      for (k in 1:length(corr.vector)){
        if (i <= length(lag.list[[k]]$acf[,,m][,j])){
          if (!is.na(lag.list[[k]]$acf[,,m][i,j])){
            corr.vector[k] <- lag.list[[k]]$acf[,,m][i,j]
          }
        }
      }
      corr.vector <- subset(corr.vector, corr.vector != -2)
      lag.list.mean[[m]][i,j] <- mean(corr.vector)
      lag.list.stdev[[m]][i,j] <- sd(corr.vector)
    }
  }
}

names(lag.list.mean) <- c("f1","g1","g3","h1","h3","s1","faculty","publication")

for (i in 1:10){
  for (j in 1:10){
    corr.vector <- rep(-2,length(corr.top500.list))
    for (k in 1:length(corr.vector)){
      if (!is.na(corr.top500.list[[k]][i,j])){
        corr.vector[k] <- corr.top500.list[[k]][i,j]
      }
    }
    corr.vector <- subset(corr.vector, corr.vector != -2)
    corr.top500.mean[i,j] <- mean(corr.vector)
    corr.top500.stdev[i,j] <- sd(corr.vector)
  }
}
colnames(corr.top500.mean) <- colnames(corr.top500.list[[1]])
rownames(corr.top500.mean) <- rownames(corr.top500.list[[1]])
colnames(corr.top500.stdev) <- colnames(corr.top500.list[[1]])
rownames(corr.top500.stdev) <- rownames(corr.top500.list[[1]])

for (i in 1:5){
  for (j in 1:10){
    for (m in 1:10){
      corr.vector <- rep(-2, length(lag.top500.list))
      for (k in 1:length(corr.vector)){
        if (i <= length(lag.top500.list[[k]]$acf[,,m][,j])){
          if (!is.na(lag.top500.list[[k]]$acf[,,m][i,j])){
            
            corr.vector[k] <- lag.top500.list[[k]]$acf[,,m][i,j]
          }
        }
      }
      corr.vector <- subset(corr.vector, corr.vector != -2)
      lag.list.top500.mean[[m]][i,j] <- mean(corr.vector)
      lag.list.top500.stdev[[m]][i,j] <- sd(corr.vector)
    }
  }
}


# 
# corr.df.google <- matrix(0,nc=29,nr=101)
# corr.df.google[,1] <- t(seq(-1,1,0.02))
# df.colnames <- c("xVal")
# df.counter <- 2
# for (i in 1:7){
#   lineLegend <- c()
#   colorList <- rainbow(8 - i + 1)
#   counter <- 1
#   for (j in (i+1):8){
#     corr.vector <- rep(-2,length(corr.list))
#     for (k in 1:length(corr.vector)){
#       if (!is.na(corr.list[[k]][i,j])){
#         corr.vector[k] <- corr.list[[k]][i,j]
#       }
#     }
#     corr.vector <- subset(corr.vector, corr.vector != -2)
#     
#     tmp.ecdf <- ecdf(corr.vector)
#     for (k in 1:101){
#       corr.df.google[k,df.counter] <- tmp.ecdf(corr.df.google[k,1])
#     }
#     df.colnames <- c(df.colnames,paste(rownames(corr.list[[1]])[i],colnames(corr.list[[1]])[j],
#                                        sep="--"))
#     df.counter <- df.counter + 1
#     
#     lineLegend <- c(lineLegend,paste(rownames(corr.list[[1]])[i],colnames(corr.list[[1]])[j],
#                         sep=" - "))
#     if (j == (i + 1)){
#       plot(ecdf(corr.vector),verticals=TRUE,do.points=FALSE,lwd=1.5,main="",lty=1,
#            col=colorList[counter],ylab="CDF",xlab="Correlation")
#     }
#     else {
#       lines(ecdf(corr.vector),verticals=TRUE,
#             lwd=1.5,lty=1,col=colorList[counter],do.points=FALSE)
#     }
#     counter <- counter + 1
#   }
#   legend("topleft", legend=lineLegend, cex=0.8, lwd=rep(1.5,28),
#          col=colorList, bty="n")
# }
# colnames(corr.df.google) <- df.colnames
# require(googleVis)
# my.chart <- gvisLineChart(as.data.frame(corr.df.google), xvar="xVal", 
#                           yvar=df.colnames[2:29],
#                           options=list(focusTarget="datum",
#                                        lineWidth=1))
# plot(my.chart)






googlevis.dir <- file.path("d:","Work","Dropbox","R","ciimpact","output","googleVis","ncses","all")
setwd(googlevis.dir)
require(googleVis)

colorMatrix <- matrix(0,nc=2,nr=length(corr.list))
colnames(colorMatrix) <- c("Institution","Color")
colorMatrix <- as.data.frame(colorMatrix)
for (i in 1:length(corr.list)) { colorMatrix$Institution[i] <- names(corr.list[i])}
colorMatrix <- colorMatrix[order(colorMatrix$Institution),]
rbPal <- colorRampPalette(c('red','blue','yellow','cyan'))
colorMatrix$Color <- rbPal(length(corr.list))

html.header <- "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\"\n        \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n<head>\n  <title>faculty_publication</title>\n  <meta http-equiv=\"content-type\" content=\"text/html;charset=utf-8\" />\n  <style type=\"text/css\">\n    body {\n          color: #444444;\n          font-family: Arial,Helvetica,sans-serif;\n          font-size: 75%;\n    }\n    a {\n          color: #4D87C7;\n          text-decoration: none;\n    }\n  </style>\n</head>\n<body>\n"
html.footer <- "\n<!-- htmlFooter -->\n<span> \nR version 2.15.2 (2012-10-26) &#8226; <a href=\"http://code.google.com/p/google-motion-charts-with-r/\">googleVis-0.4.3</a>\n&#8226; <a href=\"https://developers.google.com/terms/\">Google Terms of Use</a> &#8226; <a href=\"https://google-developers.appspot.com/chart/interactive/docs/gallery/linechart.html#Data_Policy\">Data Policy</a>\n</span></div>\n</body>\n</html>\n"

cat(html.header, file="all.html")
for (i in 1:7){
  for (j in (i+1):8){
    corr.df.google <- matrix(-2,nc=2,nr=length(corr.list))
    colnames(corr.df.google) <- c("Institution","Correlation")
    corr.df.google <- as.data.frame(corr.df.google)
    corr.df.google$xVal <- rep(-2,length(corr.list))    
    corr.vector <- rep(-2,length(corr.list))
    colorSet <- "["
    for (k in 1:length(corr.vector)){
      if (!is.na(corr.list[[k]][i,j])){
        corr.vector[k] <- corr.list[[k]][i,j]
        corr.df.google$Correlation[k] <- corr.list[[k]][i,j]
        corr.df.google$Institution[k] <- names(corr.list)[k]
        separator <- ifelse(k == 1,"",",")
        colorSet <- paste(colorSet,
                          paste("{color:\'",subset(colorMatrix$Color,colorMatrix$Institution == corr.df.google$Institution[k]),"\'}",sep=""),
                          sep=separator)
      }
    }
    colorSet <- paste(colorSet,"]",sep="")
    corr.vector <- subset(corr.vector, corr.vector != -2)
    corr.df.google <- subset(corr.df.google,corr.df.google$Correlation != -2)    
    corr.df.google <- corr.df.google[order(corr.df.google$Correlation),]    
    chartID <- paste(rownames(corr.list[[1]])[i],colnames(corr.list[[1]])[j],sep="_")
    my.chart <- gvisLineChart(as.data.frame(corr.df.google), xvar="Institution", 
                              yvar=c("Correlation"),
                              options=list(
                                curveType="function",
                                chartArea="{left:50,top:50,width:600,height:400}",
                                fontSize=12,lineWidth=0,pointSize=3,
                                hAxis = "{ textPosition : \'none\'}",
                                colors=colorSet,
                                width=600,
                                height=500,
                                title=chartID
                                ),
                              chartid=chartID)    
    cat(my.chart$html$chart, file="all.html", append=TRUE)
  }
}
cat(html.footer, file="all.html", append=TRUE)   




cat(html.header, file="allColumn.html")
for (i in 1:7){
  for (j in (i+1):8){
    corr.list <- subset(corr.list, !is.na(corr.list))
    corr.df.google <- matrix(-2,nc=(length(corr.list)+1),nr=1)
    colorSet <- "["
    for (k in 1:length(corr.list)){      
        corr.df.google[1,k+1] <- corr.list[[k]][i,j]
        separator <- ifelse(k == 1,"",",")
        colorSet <- paste(colorSet,
                          paste("{color:\'",subset(colorMatrix$Color,colorMatrix$Institution == names(corr.list)[k]),"\'}",sep=""),
                          sep=separator)      
    }
    colorSet <- paste(colorSet,"]",sep="")
    corr.df.google <- as.data.frame(t(corr.df.google[,order(corr.df.google[1,])]))
    
    colnames(corr.df.google) <- c("Institution",names(corr.list))
    corr.df.google$Institution[1] <- "Type"
    chartID <- paste(rownames(corr.list[[1]])[i],colnames(corr.list[[1]])[j],sep="_")
    my.chart <- gvisColumnChart(corr.df.google, xvar="Institution",yvar=colnames(corr.df.google)[2:length(corr.df.google)],
                              options=list(chartArea="{left:50,top:50,width:1000,height:400}",
                                fontSize=12,lineWidth=0,pointSize=3,
                                bar = "{groupWidth: \'100%\'}",
                               # hAxis = "{ }",
                                colors=colorSet,
                                width=1000,
                                height=500,
                                title=chartID
                              ),
                              chartid=chartID)    
    cat(my.chart$html$chart, file="allColumn.html", append=TRUE)
  }
}
cat(html.footer, file="allColumn.html", append=TRUE)   



cat(html.header, file="allColumn500.html")
for (i in 1:9){
  for (j in (i+1):10){
    corr.top500.list <- subset(corr.top500.list, !is.na(corr.top500.list))
    corr.df.google <- matrix(-2,nc=(length(corr.top500.list)+1),nr=1)
    colorSet <- "["
    for (k in 1:length(corr.top500.list)){      
      corr.df.google[1,k+1] <- corr.top500.list[[k]][i,j]
      separator <- ifelse(k == 1,"",",")
      colorSet <- paste(colorSet,
                        paste("{color:\'",subset(colorMatrix$Color,colorMatrix$Institution == names(corr.top500.list)[k]),"\'}",sep=""),
                        sep=separator)      
    }
    colorSet <- paste(colorSet,"]",sep="")
    corr.df.google <- as.data.frame(t(corr.df.google[,order(corr.df.google[1,])]))
    
    colnames(corr.df.google) <- c("Institution",names(corr.top500.list))
    corr.df.google$Institution[1] <- "Type"
    chartID <- paste(rownames(corr.top500.list[[1]])[i],colnames(corr.top500.list[[1]])[j],sep="_")
    my.chart <- gvisColumnChart(corr.df.google, xvar="Institution",yvar=colnames(corr.df.google)[2:length(corr.df.google)],
                                options=list(chartArea="{left:50,top:50,width:1000,height:400}",
                                             fontSize=12,lineWidth=0,pointSize=3,
                                             bar = "{groupWidth: \'100%\'}",
                                             # hAxis = "{ }",
                                             colors=colorSet,
                                             width=1000,
                                             height=500,
                                             title=chartID
                                ),
                                chartid=chartID)    
    cat(my.chart$html$chart, file="allColumn500.html", append=TRUE)
  }
}
cat(html.footer, file="allColumn500.html", append=TRUE)









cat(html.header, file="top500.html")
for (i in 1:9){
  for (j in (i+1):10){
    corr.df.google <- matrix(-2,nc=2,nr=length(corr.top500.list))
    colnames(corr.df.google) <- c("Institution","Correlation")
    corr.df.google <- as.data.frame(corr.df.google)
    corr.df.google$xVal <- rep(-2,length(corr.top500.list))    
    corr.vector <- rep(-2,length(corr.top500.list))
    for (k in 1:length(corr.vector)){
      if (!is.na(corr.top500.list[[k]][i,j])){
        corr.vector[k] <- corr.top500.list[[k]][i,j]
        corr.df.google$Correlation[k] <- corr.top500.list[[k]][i,j]
        corr.df.google$Institution[k] <- names(corr.top500.list)[k]
      }
    }
    corr.vector <- subset(corr.vector, corr.vector != -2)
    corr.df.google <- subset(corr.df.google,corr.df.google$Correlation != -2)
    
    corr.df.google <- corr.df.google[order(corr.df.google$Correlation),]
    chartID <- paste(rownames(corr.top500.list[[1]])[i],colnames(corr.top500.list[[1]])[j],sep="_")
    my.chart <- gvisLineChart(as.data.frame(corr.df.google), xvar="Institution", 
                              yvar=c("Correlation"),
                              options=list(
                                curveType="function",
                                chartArea="{left:50,top:50,width:600,height:400}",
                                fontSize=12,
                                lineWidth=0,
                                pointSize=2,
                                hAxis = "{ textPosition : \'none\'}",
                                width=600,
                                height=500,
                                title=chartID
                              ),
                              chartid=chartID)    
    cat(my.chart$html$chart, file="top500.html", append=TRUE)
  }
}
cat(html.footer, file="top500.html", append=TRUE)   

# for (i in 1:9){
#   lineLegend <- c()
#   colorList <- rainbow(10 - i + 1)
#   counter <- 1
#   for (j in (i+1):10){
#     corr.vector <- rep(-2,length(corr.top500.list))
#     for (k in 1:length(corr.vector)){
#       if (!is.na(corr.top500.list[[k]][i,j])){
#         corr.vector[k] <- corr.top500.list[[k]][i,j]
#       }
#     }
#     corr.vector <- subset(corr.vector, corr.vector != -2)
#     lineLegend <- c(lineLegend,paste(rownames(corr.top500.list[[1]])[i],
#                                      colnames(corr.top500.list[[1]])[j],
#                                      sep=" - "))
#     if (j == (i + 1)){
#       plot(ecdf(corr.vector),verticals=TRUE,do.points=FALSE,lwd=1.5,main="",lty=1,
#            col=colorList[counter],ylab="CDF",xlab="Correlation")
#     }
#     else {
#       lines(ecdf(corr.vector),verticals=TRUE,
#             lwd=1.5,lty=1,col=colorList[counter],do.points=FALSE)
#     }
#     counter <- counter + 1
#   }
#   legend("topleft", legend=lineLegend, cex=0.8, lwd=rep(1.5,28),
#          col=colorList, bty="n")
# }
# 



#   institution.list[[i]] <- as.list(tmp.table)
#   institution.list[[i]]$FICE <- unique(institution.list[[i]]$FICE)
#   institution.list[[i]]$IPEDS <- unique(institution.list[[i]]$IPEDS)
#   institution.list[[i]]$Institution <- unique(institution.list[[i]]$Institution)
#   institution.list[[i]]$City <- unique(institution.list[[i]]$City)
#   institution.list[[i]]$State <- unique(institution.list[[i]]$State)
#   institution.list[[i]]$Lat <- unique(institution.list[[i]]$Lat)
#   institution.list[[i]]$Long <- unique(institution.list[[i]]$Long)
#   
#   reduce.size <- object.size(tmp.table[,5:26]) + object.size(unique(tmp.table[,1])) +
#     object.size(unique(tmp.table[,2])) +object.size(unique(tmp.table[,3])) + object.size(unique(tmp.table[,4]))

# 
# print(object.size(new.data))
# print(object.size(new.data[,c(4:17,22:26)]) + object.size(unique(new.data[,1])) +
#         object.size(unique(new.data[,2])) +object.size(unique(new.data[,3]))
#       + object.size(unique(new.data[18])) + object.size(unique(new.data[,19])) +
#         object.size(unique(new.data[,20])) +
#         object.size(unique(new.data[,3])))
# print(reduce.size)
# 
# n <- 100
# x <- matrix(0,ncol=n,nrow=n)
# y <- list()
# for (i in 1:n){
#   tmp <- as.vector(x[,i])
#   y[[i]] <- as.list(as.data.frame(tmp))
# }
# print(object.size(x))
# print(object.size(y))



