require(xlsx)

data.dir <- file.path("/common","ciimpact","data","ncses","clean") 
setwd(data.dir)
# read input file from the command line argument
# surrounded with microbenchmark's syntax
input.file <- read.table("g4.txt", header = TRUE, sep = "\t", stringsAsFactors = TRUE)

fice.ipeds.mapping <- read.xlsx("2000_edition_data.xlsx",sheetIndex=1,header=TRUE,stringsAsFactors=FALSE)
fice.ipeds.mapping <- subset(fice.ipeds.mapping, !is.na(fice.ipeds.mapping$FICE))
institution.control <- read.xlsx("Institution-Control.xlsx",sheetIndex=1,header=TRUE,stringsAsFactors=FALSE)
latlong <- read.xlsx("university_latlong.xlsx",sheetIndex=1,header=TRUE,stringsAsFactors=FALSE)

common.fice <- unique(input.file$FICE)
common.institution <- character(length(common.fice))
common.ipeds <- numeric(length(common.fice))
common.epscor <- numeric(length(common.fice))
common.type <- character(length(common.fice))
common.CC <- character(length(common.fice))
common.latlong <- character(length(common.fice))
common.lat <- numeric(length(common.fice))
common.long <- numeric(length(common.fice))

for (i in 1:length(common.fice)){
  print(i)
  
  for (j in 1:length(input.file$Institution)){
    if (common.fice[i] == input.file$FICE[j]){
      common.institution[i] <- as.character(input.file$Institution[j])
      print(common.institution[i])
      break
    }
  }
  
  for (j in 1:length(institution.control$FICE)){
    if (common.fice[i] == institution.control$FICE[j]){
      common.CC[i] <- institution.control$CC[j]
      common.epscor[i] <- institution.control$EPSCOR[j]
      common.type[i] <- institution.control$Type[j]      
      break
    }
  }
  
  for (j in 1:length(fice.ipeds.mapping$FICE)){
    if (common.fice[i] == fice.ipeds.mapping$FICE[j]){
      common.ipeds[i] <- fice.ipeds.mapping$UNITID[j]    
      for (k in 1:length(latlong$UNITID)){
        if (fice.ipeds.mapping$UNITID[j] == latlong$UNITID[k]){
          common.lat[i] <- latlong$LATITUDE[k]
          common.long[i] <-latlong$LONGITUD[k]
          common.latlong[i] <- paste(common.lat[i],common.long[i],sep=":")          
          break
        }        
      }
      break
    }
  }
}

g4.table <- data.frame(matrix(0,nrow=length(common.fice),ncol=16))
colnames(g4.table) <- c("FICE","IPEDS","Institution","2003","2004","2005","2006",
                        "2007","2008","2009","CC","EPSCoR","Type","Lat","Long","LatLong")

g4.table$FICE <- common.fice
g4.table$IPEDS <- common.ipeds
g4.table$Institution <- common.institution
g4.table$CC <- common.CC
g4.table$EPSCoR <- common.epscor
g4.table$Type <- common.type
g4.table$Lat <- common.lat
g4.table$Long <- common.long
g4.table$LatLong <- common.latlong

g4.table <- subset(g4.table, g4.table$CC != "")

# g4: columns 10 back to 4
for (i in 7:13){
  tmp <- numeric(length(g4.table$FICE))
  for (j in 1:length(g4.table$FICE)){
    tmp[j] <- sum(subset(input.file[,i],input.file$FICE == g4.table$FICE[j]))
  }
  tmp.quantile <- quantile(tmp)
  print(tmp.quantile)
  for (j in 1:length(g4.table$FICE)){
    if (tmp[j] < tmp.quantile[2]){
      g4.table[j,17-i] <- 1
    }
    else if (tmp[j] < tmp.quantile[3]){
      g4.table[j,17-i] <- 2
    }
    else if (tmp[j] < tmp.quantile[4]){
      g4.table[j,17-i] <- 3
    }
    else {
      g4.table[j,17-i] <- 4
    }
  }
}