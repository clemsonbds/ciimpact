rm(list=ls()) # clean up everything
require(xlsx)

data.dir <- file.path("d:","work","r","ciimpact","data","ncses","clean") 
setwd(data.dir)
# read input file from the command line argument
# surrounded with microbenchmark's syntax
input.file <- read.table("f1.txt", header = TRUE, sep = "\t", stringsAsFactors = TRUE)

#fice.ipeds.mapping <- read.xlsx("2000_edition_data.xlsx",sheetIndex=1,header=TRUE,stringsAsFactors=FALSE)
#fice.ipeds.mapping <- subset(fice.ipeds.mapping, !is.na(fice.ipeds.mapping$FICE))
#institution.control <- read.xlsx("Institution_Control_Full.xlsx",sheetIndex=1,header=TRUE,stringsAsFactors=FALSE)
#epscor.state <- read.xlsx("EpscorStateList.xlsx",sheetIndex=1,header=TRUE,stringsAsFactors=FALSE)
#latlong <- read.xlsx("university_latlong.xlsx",sheetIndex=1,header=TRUE,stringsAsFactors=FALSE)

fice.ipeds.mapping <- read.csv("2000_edition_data.txt",sep="\t",header=TRUE,stringsAsFactors=FALSE)
fice.ipeds.mapping <- subset(fice.ipeds.mapping, !is.na(fice.ipeds.mapping$FICE))
institution.control <- read.csv("Institution_Control_Full.txt",sep="\t",header=TRUE,stringsAsFactors=FALSE)
epscor.state <- read.csv("EpscorStateList.txt",sep="\t",header=TRUE,stringsAsFactors=FALSE)
latlong <- read.csv("university_latlong.txt",sep="\t",header=TRUE,stringsAsFactors=FALSE)

common.fice <- unique(input.file$FICE)
common.institution <- character(length(common.fice))
common.ipeds <- numeric(length(common.fice))
common.CC <- numeric(length(common.fice))
common.lat <- numeric(length(common.fice))
common.long <- numeric(length(common.fice))
common.city <- character(length(common.fice))
common.state <- character(length(common.fice))
common.gradprofile <- numeric(length(common.fice))
common.epscor <- numeric(length(common.fice))
common.type <- numeric(length(common.fice))
common.locale <- numeric(length(common.fice))
common.landgrant <- numeric(length(common.fice))
common.medical <- numeric(length(common.fice))

for (i in 1:length(common.fice)){
  print(i)
  
  for (j in 1:length(input.file$Institution)){
    if (common.fice[i] == input.file$FICE[j]){
      common.institution[i] <- as.character(input.file$Institution[j])
      print(common.institution[i])
      break
    }
  }
  
  for (j in 1:length(fice.ipeds.mapping$FICE)){
    if (common.fice[i] == fice.ipeds.mapping$FICE[j]){
      common.ipeds[i] <- fice.ipeds.mapping$UNITID[j]
      common.city[i] <- as.character(fice.ipeds.mapping$CITY[j])
      common.state[i] <- as.character(fice.ipeds.mapping$ST[j])
      
      if (common.state[i] %in% epscor.state$StateCode){
        common.epscor[i] <- 1
      }
      
      for (k in 1:length(institution.control$UNITID)){
        if (fice.ipeds.mapping$UNITID[j] == institution.control$UNITID[k]){
          common.CC[i] <- institution.control$BASIC2010[k]
          common.gradprofile[i] <- institution.control$IPGRAD2010[k]
          common.locale[i] <- institution.control$LOCALE[k]
          common.type[i] <- institution.control$CONTROL[k]
          common.landgrant[i] <- institution.control$LANDGRNT[k]
          common.medical[i] <- institution.control$MEDICAL[k]
          break
        }
      }
      
      for (k in 1:length(latlong$UNITID)){
        if (fice.ipeds.mapping$UNITID[j] == latlong$UNITID[k]){
          common.lat[i] <- latlong$LATITUDE[k]
          common.long[i] <-latlong$LONGITUD[k]
          break
        }        
      }
      break
    }
  }
}

f1.table <- data.frame(matrix(0,nrow=length(common.fice),ncol=21))
colnames(f1.table) <- c("FICE","IPEDS","Institution","2003","2004","2005","2006",
                        "2007","2008","2009","CC","GradProfile","EPSCoR","Control","Locale",
                        "LandGrant","Medical","City","State","Lat","Long")

f1.table$FICE <- common.fice
f1.table$IPEDS <- common.ipeds
f1.table$Institution <- common.institution
f1.table$CC <- common.CC
f1.table$GradProfile <- common.gradprofile
f1.table$EPSCoR <- common.epscor
f1.table$Control <- common.type
f1.table$Locale <- common.locale
f1.table$LandGrant <- common.landgrant
f1.table$Medical <- common.medical
f1.table$City <- common.city
f1.table$State <- common.state
f1.table$Lat <- common.lat
f1.table$Long <- common.long

f1.table <- subset(f1.table, f1.table$CC != "")

# f1: columns 10 back to 4
for (i in 4:10){
  tmp <- numeric(length(f1.table$FICE))
  for (j in 1:length(f1.table$FICE)){
    tmp[j] <- sum(subset(input.file[,i],input.file$FICE == f1.table$FICE[j]))
  }
  
  f1.table[,14-i] <- tmp  
}

save(f1.table,file="f1.full.RData")

