#! C:\\Program Files\\R\\R-2.14.2\\bin\\Rscript.exe
rm(list=ls()) # clean up everything
options(digits=2)
#### load libraries and set up directory paths #################################
library(FEAR)

work.dir <- file.path("D:","Work","R","ciimpact")
#work.dir <- file.path("/home","lngo","Dropbox","R","ciimpact")
setwd(work.dir)

lib.dir <- file.path(work.dir,"lib")
data.dir <- file.path(work.dir,"data")
graph.dir <- file.path(work.dir,"output","epscor")
tex.dir <- file.path(work.dir,"tex")

#### library function for ESPCOR study #########################################
################################################################################
################################################################################

mean.diff.dea <- function(input.data, input.set, output.set, orientation, rts){
  # split data into NON-EPSCOR-HPC and Non-EPCSCOR-NONHPC
  input.hpc <- subset(input.data,input.data$APP[] > 0 & input.data$epscor.2006 == 0)
  input.nonhpc <- subset(input.data,input.data$APP[] == 0 & input.data$epscor.2006 == 0)
  
  output.all <- get.dea(input.data,input.set,output.set,orientation, rts)
  
  n.hpc <- length(input.hpc[,1])
  output.hpc <- get.dea(input.hpc,input.set,output.set,orientation, rts)
  output.hpc.1 <- get.dea(input.hpc[1:(n.hpc%/%2),],input.set,output.set,orientation, rts)
  output.hpc.2 <- get.dea(input.hpc[(n.hpc%/%2 + 1):n.hpc,],input.set,output.set,orientation, rts)
  
  n.nonhpc <- length(input.nonhpc[,1])
  output.nonhpc <- get.dea(input.nonhpc,input.set,output.set,orientation, rts)
  output.nonhpc.1 <- get.dea(input.nonhpc[1:(n.nonhpc%/%2),],input.set,output.set,orientation, rts)
  output.nonhpc.2 <- get.dea(input.nonhpc[(n.nonhpc%/%2 + 1):n.nonhpc,],input.set,output.set,orientation, rts)
  
  if (orientation != 2){
    output.hpc$DEA <- 1/output.hpc$DEA
    output.hpc.1$DEA <- 1/output.hpc.1$DEA
    output.hpc.2$DEA <- 1/output.hpc.2$DEA
    output.nonhpc$DEA <- 1/output.nonhpc$DEA
    output.nonhpc.1$DEA <- 1/output.nonhpc.1$DEA
    output.nonhpc.2$DEA <- 1/output.nonhpc.2$DEA
    output.all$DEA <- 1/output.all$DEA
  }
  
  mu.hpc <- sum(output.hpc$DEA) / n.hpc
  mu.hpc.half <- 0.5 * (sum(output.hpc.1$DEA) / (n.hpc%/%2) + sum(output.hpc.2$DEA) / (n.hpc - n.hpc%/%2))
  
  mu.nonhpc <- sum(output.nonhpc$DEA) / n.nonhpc
  mu.nonhpc.half <- 0.5 * (sum(output.nonhpc.1$DEA) / (n.nonhpc%/%2) + sum(output.nonhpc.2$DEA) / (n.nonhpc - n.nonhpc%/%2))
  
  if (rts == 1){
    # variable return to scale
    k.rate <- 2 / (length(input.set) + length(output.set) + 1)
  } else if (rts == 3){ 
    # constant return to scale
    k.rate <- 2 / (length(input.set) + length(output.set))
  }
    
  beta.hpc <- (mu.hpc.half - mu.hpc) / (2^k.rate - 1)
  beta.nonhpc <- (mu.nonhpc.half - mu.nonhpc) / (2^k.rate - 1)
  
  mu.all <- sum(output.all$DEA) / length(output.all$DEA)
  
  phi.hpc <- sum((output.hpc$DEA - mu.all)^2) / n.hpc
  phi.nonhpc <- sum((output.nonhpc$DEA - mu.all)^2) / n.nonhpc
  
  T.hat <- ((mu.hpc - mu.nonhpc) - (beta.hpc - beta.nonhpc)) / ((((phi.hpc)^2)/n.hpc + ((phi.nonhpc)^2)/n.nonhpc)^0.5) 
  
  tmp1 <- pnorm(T.hat,lower.tail=FALSE,log.p=TRUE)
  tmp2 <- tmp1 / log(10)
  rho <- 10^tmp2
  rho
}



mean.diff.fdh <- function(input.data, input.set, output.set, orientation){
  # split data into NON-EPSCOR-HPC and NON-EPSCOR-NonHPC
  input.hpc <- subset(input.data,input.data$APP[] > 0 & input.data$epscor.2006 == 0)
  input.nonhpc <- subset(input.data,input.data$APP[] == 0 & input.data$epscor.2006 == 0)
    
  output.all <- get.fdh(input.data,input.set,output.set,orientation)
  
  n.hpc <- length(input.hpc[,1])
  output.hpc <- get.fdh(input.hpc,input.set,output.set,orientation)
  output.hpc.1 <- get.fdh(input.hpc[1:(n.hpc%/%2),],input.set,output.set,orientation)
  output.hpc.2 <- get.fdh(input.hpc[(n.hpc%/%2 + 1):n.hpc,],input.set,output.set,orientation)
  
  n.nonhpc <- length(input.nonhpc[,1])
  output.nonhpc <- get.fdh(input.nonhpc,input.set,output.set,orientation)
  output.nonhpc.1 <- get.fdh(input.nonhpc[1:(n.nonhpc%/%2),],input.set,output.set,orientation)
  output.nonhpc.2 <- get.fdh(input.nonhpc[(n.nonhpc%/%2 + 1):n.nonhpc,],input.set,output.set,orientation)
  
  if (orientation != 2){
    output.hpc$DEA <- 1/output.hpc$DEA
    output.hpc.1$DEA <- 1/output.hpc.1$DEA
    output.hpc.2$DEA <- 1/output.hpc.2$DEA
    output.nonhpc$DEA <- 1/output.nonhpc$DEA
    output.nonhpc.1$DEA <- 1/output.nonhpc.1$DEA
    output.nonhpc.2$DEA <- 1/output.nonhpc.2$DEA
    output.all$DEA <- 1/output.all$DEA
  }
  
  mu.hpc <- sum(output.hpc$DEA) / n.hpc
  mu.hpc.half <- 0.5 * (sum(output.hpc.1$DEA) / (n.hpc%/%2) + sum(output.hpc.2$DEA) / (n.hpc - n.hpc%/%2))
  
  mu.nonhpc <- sum(output.nonhpc$DEA) / n.nonhpc
  mu.nonhpc.half <- 0.5 * (sum(output.nonhpc.1$DEA) / (n.nonhpc%/%2) + sum(output.nonhpc.2$DEA) / (n.nonhpc - n.nonhpc%/%2))
  
  k.rate <- 1 / (length(input.set) + length(output.set) + 1)
  
  beta.hpc <- (mu.hpc.half - mu.hpc) / (2^k.rate - 1)
  beta.nonhpc <- (mu.nonhpc.half - mu.nonhpc) / (2^k.rate - 1)
  
  mu.all <- sum(output.all$DEA) / length(output.all$DEA)
  
  phi.hpc <- sum((output.hpc$DEA - mu.all)^2) / n.hpc
  phi.nonhpc <- sum((output.nonhpc$DEA - mu.all)^2) / n.nonhpc
    
  # new FDH elements
  n.hpc.fdh <- floor((length(input.hpc[,1]))^(2*k.rate))  
  output.hpc.fdh <- get.fdh(input.hpc,input.set,output.set,orientation)
  
  n.nonhpc.fdh <- floor((length(input.nonhpc[,1]))^(2*k.rate))
  output.nonhpc.fdh <- get.fdh(input.nonhpc,input.set,output.set,orientation)
  
  mu.hpc.fdh <- sum(sample(output.hpc.fdh$DEA,n.hpc.fdh)) / n.hpc.fdh
  mu.nonhpc.fdh <- sum(sample(output.nonhpc.fdh$DEA,n.hpc.fdh)) / n.nonhpc.fdh
    
  T.hat <- ((mu.hpc.fdh - mu.nonhpc.fdh) - (beta.hpc - beta.nonhpc)) / 
    ((((phi.hpc)^2)/n.hpc.fdh + ((phi.nonhpc)^2)/n.nonhpc.fdh)^0.5) 
  
  tmp1 <- pnorm(T.hat,lower.tail=FALSE,log.p=TRUE)
  tmp2 <- tmp1 / log(10)
  rho <- 10^tmp2
  rho
}

################################################################################
################################################################################
################################################################################
################################################################################



#### load data files ###########################################################
load(file.path(data.dir,"nas","nas.RData"))
load(file.path(data.dir,"nas","fieldList.RData"))
load(file.path(data.dir,"top500","top500.RData"))
epscor.state <- read.csv(file.path(data.dir,"nas","EpscorStateList.txt"),
                         header=TRUE,sep="\t",stringsAsFactors=FALSE)
institution.state <- read.csv(file.path(data.dir,"nas","InstitutionState.txt"),
                              header=TRUE,sep="\t",stringsAsFactors=FALSE)

#### load soure files ##########################################################
source(file.path(lib.dir,"calculateDEA.R"))
source(file.path(lib.dir,"genGraph.R"))
source(file.path(lib.dir,"modifiedDensity.R"))
source(file.path(lib.dir,"cleanDEAparam.R"))
source(file.path(lib.dir,"rtsTest.R"))
source(file.path(lib.dir,"convTest.R"))
source(file.path(lib.dir,"textOutput.R"))

ks.list <- vector(mode = "list", length = 10)
list.counter <- 1
graph.counter <- 1
#### set up parameters for the analysis ########################################
# [1] "Program.ID" [2] "Broad.Field" [3] "Field"  [4] "Institution.Name."                                    
# [5] "Control"    [6] "Program.Size.Quartile"  [7] "Avg.Publications.Allocated.Faculty"                   
# [8] "Avg.Citations.Publication"         [9] "X.Percent.Faculty.with.Grants"                        
# [10] "X.Awards.Allocated.Faculty" [11] "Percent.First.Year.Students.Full.Support"             
# [12] "Avg.CompletionPercentage"   [13] "Median.Time.to.Degree"                                
# [14] "Percent.with.Academic.Plans"    [15] "Avg.PhD.Graduated"                                    
# [16] "Percent.of.Interdisciplinary.Faculty"     [17] "Avg.GRE.Scores"                                       
# [18] "Percent.First.Year.Students.with.External.Fellowships"
# [19] "Total.Faculty"  [20] "Total.Allocated.Faculty"                              
# [21] "Percent.Assistant.Professors"         [22] "Percent.Tenured.Faculty"                              
# [23] "X.Number.of.Students.Enrolled
# [24] "Avg.Annual.First.Year.Enrollment.2002.2006"           
# [25] "Percent.Students.RA"  [26] "Percent.Students.TA"                                  
# [27] "Percent.First.Year.Institutional.Fellowships.Alone."  
# [28] "State"  [29] "IPEDS.ID" [30] "CC.2005" [31] "epscor.2006"
# [32] "Total.Publications.Allocated.Faculty"                 
# [33] "Total.Citations"                                      
# [34] "Total.Faculty.with.Grants"                            
# [35] "Total.Awards.Allocated.Faculty"                       
# [36] "Total.First.Year.Students.Full.Support"               
# [37] "Total.with.Academic.Plans.PhD"                        
# [38] "Total.Interdisciplinary.Faculty"                      
# [39] "Total.First.Year.Students.with.External.Fellowships"  
# [40] "Total.Assistant.Professors"                           
# [41] "Total.Tenured.Faculty"                                
# [42] "Total.Students.RA"                                    
# [43] "Total.Students.TA"                                    
# [44] "Total.First.Year.Institutional.Fellowships.Alone"  

input.set <- c(20,17)
output.set <- c(32,15)

orientation <- 2

#### function: get mean, std, min, max #########################################
getStats <- function(x){
  output <- c(round(mean(x),digits=2),round(sd(x),digits=2),
              round(min(x),digits=2),round(max(x),digits=2))
  output
}



#### print summary table to LaTEX###############################################
print(paste("ORIENTATION: ",orientation,sep=""))

#### initilize global variables ################################################
ks.list <- vector(mode = "list", length = 8)
list.counter <- 1
file.flag <- TRUE


#### Preping data frame for all institutions' efficiency scores
allInstitutions <- data.frame(Institution.Name.= unique(input.file$Institution.Name.))
allAPP <- data.frame(Institution.Name. = aggTop500$Institution.Name., APP = aggTop500$APP)
allInstitutions <- merge(x=allInstitutions,by="Institution.Name.",y=allAPP,all.x=TRUE)

##### begin main code body #####################################################
num.BroadField <- length(field.list[[1]])
for (i in 1:(num.BroadField+1)){
  num.field <- length(field.list[[i]])
  for (j in 1:num.field){
    if (field.list[[i]][j] == "Biology/Integrated Biology/Integrated Biomedical Sciences (Note: Use this field only if the degree field is not specialized.)"){
      field.list[[i]][j] <- "Biology/Integrated Biology/Integrated Biomedical Sciences"
    }
    # keep only the institutions in the selected academic field/BroadField of
    # the loop
    rawData <- subset(input.file,input.file$Broad.Field == field.list[[1]][i-1])
    rawData <- subset(rawData,rawData$Field == field.list[[i]][j])
    
    # skip if the field contains less than 50 institutions
    if (length(rawData[,1]) < 50){
      next
    }
    rawData <- rawData[order(rawData$Institution.Name.),]
    tmpData <- rawData
    
    # merge NRC/NAS data with EPSCOR data and Top500 data
    rawData <- merge(x=rawData,by="Institution.Name.",y=institution.state,all.x=TRUE)
    rawData <- merge(x=rawData,by="Institution.Name.",y=aggTop500,all.x=TRUE)
    input.data <- clean.NAS(rawData,input.set,output.set)
    
    # fixed institutions with NA for APP
    for (k in 1:length(input.data[,1])){
      if (is.na(input.data$APP[k])){
        input.data$APP[k] <- 0
      } 
    }

    # check for EPSCOR/HPC combinations and validate observation count
    #input.data <- subset(input.data,input.data$epscor.2006 == 1)
    nonepscor.hpc <- subset(input.data,input.data$APP[] > 0 & input.data$epscor.2006 == 0)
    nonepscor.nonhpc <- subset(input.data,input.data$APP[] == 0 & input.data$epscor.2006 == 0)
    if (length(nonepscor.hpc$Institution.Name.) < 17 | length(nonepscor.nonhpc$Institution.Name.) < 20){
      print(paste(field.list[[i]][j]," has too few observations: ",length(nonepscor.hpc$Institution.Name.),
                  " non-epscor-have and ",length(nonepscor.nonhpc$Institution.Name.), " non-epscor-have-not",sep=""))
      next
    }
    
    legend.stat <- c()
    
    # the first test is the convexity test
    print(paste("Department:",field.list[[i]][j],": ",sep=""))
    pval.convex <- get.dea.convex(input.data,input.set,output.set)$pval
    print(paste("Convexity Test:",pval.convex,sep=""))
    legend.stat <- c(legend.stat,paste("Convexity Test:",pval.convex,sep=""))
    if (pval.convex <= 0.10){
      # if we reject convexity, we go straight to mean difference test and then
      # stochastic dominance test, both using fdh          
      rho <- mean.diff.fdh (input.data, input.set, output.set, orientation)
      legend.stat <- c(legend.stat,paste("Mean Diff Test:",rho,sep=""))
      
      # Efficiency estimation is done using FDH
      output.all <- get.fdh(input.data,input.set,output.set,orientation)     
      output.hpc <- subset(output.all, output.all$APP[] > 0 & output.all$epscor.2006 == 0)    
      output.nonhpc <- subset(output.all, output.all$APP[] == 0 & output.all$epscor.2006 == 0)    
      
      if (orientation != 2){
        output.hpc$DEA <- 1/output.hpc$DEA        
        output.nonhpc$DEA <- 1/output.nonhpc$DEA        
        output.all$DEA <- 1/output.all$DEA
      }
    }
    else {
      # if we don't reject convexity, we now test for return-to-scale
      pval.rts <- get.dea.rts(input.data,input.set,output.set)$pval
      print(paste("RTS Test:",pval.rts,sep=""))
      legend.stat <- c(legend.stat,paste("RTS Test:",pval.rts,sep=""))
      
      if (pval.rts <= 0.10){
        # if we reject the null hypothesis that this is constant return to scale, 
        # we set the rts value to be 1 (variable return to scale)
        rts <- 1
      }
      else {
        # if we do not reject the null hypothesis that this is constant return to scale, 
        # we set the rts value to be 3 (constant return to scale)
        rts <- 3
      }      
      rho <- mean.diff.dea (input.data, input.set, output.set, orientation, rts)
      legend.stat <- c(legend.stat,paste("Mean Diff Test:",rho,sep=""))
      
      # Efficiency estimation is done using FDH
      output.all <- get.dea(input.data,input.set,output.set,orientation,rts)     
      output.hpc <- subset(output.all, output.all$APP[] > 0 & output.all$epscor.2006 == 0)    
      output.nonhpc <- subset(output.all, output.all$APP[] == 0 & output.all$epscor.2006 == 0)          
      
      if (orientation != 2){
        output.hpc$DEA <- 1/output.hpc$DEA        
        output.nonhpc$DEA <- 1/output.nonhpc$DEA        
        output.all$DEA <- 1/output.all$DEA
      }
    }

    # add to overall data.frame
    tmpDEA <- data.frame(Institution.Name. = output.all$Institution.Name.,DEA=output.all$DEA)
    colnames(tmpDEA)[length(tmpDEA)] <- field.list[[i]][j]
    allInstitutions <- merge(x=allInstitutions,by="Institution.Name.",y=tmpDEA,all.x=TRUE)
  
    densHPC <- get.density(output.hpc$DEA)
    densNonHPC <- get.density(output.nonhpc$DEA)
    
    inputLabel <- genParamLabel(input.data, input.set, "Input Set:")
    outputLabel <- genParamLabel(input.data, output.set, "Output Set:")
    graphName <- paste(field.list[[i]][j],inputLabel,outputLabel,sep="\n")
    firstLegend = "NONEPSCOR-HPC"
    secondLegend = "NONEPSCOR-NonHPC"  
    
    
    ks.list[[list.counter]] <- ks.test(output.hpc$DEA, output.nonhpc$DEA,alternative="less")
    print(paste("Number of institutions   : ",length(output.all$DEA),sep=""))
    print(paste("Institution with HPC     : ",length(output.hpc$DEA),sep=""))  
    print(paste("Institution without HPC  : ",length(output.nonhpc$DEA),sep=""))
    
    print(paste("Mean Difference Test     : ",rho,sep=""))
    if (rho <= 0.10){
      print(paste("Stochastic Dominance Test: ",ks.list[[list.counter]]$p.value,sep=""))
      legend.stat <- c(legend.stat,paste("Stochastic Dominance Test:",ks.list[[list.counter]]$p.value,sep=""))      
    } 
    
    category.count <- paste(firstLegend,": ",length(output.hpc$DEA),"   -----   ",
                            secondLegend,": ", length(output.nonhpc$DEA),sep="")
    for (k in 1:max(length(output.hpc$Institution.Name.), 
                    length(output.nonhpc$Institution.Name.))){
      x1 <- format("",width=100)
      x2 <- format("",width=70)
      if (k <= length(output.hpc$Institution.Name.)) x1 <- paste(format(gsub("^\\s+|\\s+$", "",output.hpc$Institution.Name.[k]),width=100),":",
                                                                 format(output.hpc$DEA[k],digits=2,width=4),sep="")
      if (k <= length(output.nonhpc$Institution.Name.)) x2 <- paste(format(gsub("^\\s+|\\s+$", "",output.nonhpc$Institution.Name.[k]),width=70),":",
                                                                    format(output.nonhpc$DEA[k],digits=2,width=4),sep="")
      tmpLines <- paste(x1,x2,sep="   -----   ")
      #print(tmpLines)
      category.count <- c(category.count,tmpLines)
    }
    
    
    if (file.flag){
      #      gen.plot.file(graph.dir,densHPC,densNonHPC,2.0,field.list[[i]][j],firstLegend,secondLegend)
      main.title <- paste(firstLegend,"-",secondLegend,field.list[[i]][j])
      gen.plot.file.vis(graph.dir,densHPC,densNonHPC,2.0,main.title,firstLegend,secondLegend,
                        legend.stat,category.count)
      
    }
    else {
      gen.plot(densHPC,densNonHPC,6.0,graphName,firstLegend,secondLegend)   
    }

    print("-----------------------------------------------")
    list.counter <- list.counter + 1  
  }
}
