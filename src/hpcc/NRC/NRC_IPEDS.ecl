﻿Layout := RECORD

	STRING institutionName;
	STRING stabbr;
	STRING unitid;
	STRING ccbasic2005;
	STRING ccbasic2010;

END;

EXPORT NRC_IPEDS := DATASET('~ciimpact::institutionstate',Layout,CSV(HEADING(1)));