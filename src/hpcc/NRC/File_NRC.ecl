﻿EXPORT File_NRC := MODULE

	EXPORT Layout := RECORD

		UNSIGNED programID;
		STRING broadField;
		STRING field;
		STRING institutionName;
		STRING programName;
		STRING programWebsite;
		STRING control;
		UNSIGNED regionalCode;
		UNSIGNED programSizeQuartile;
		UNSIGNED rRankings5thPercentile;
		UNSIGNED rRankings95thPercentile;
		UNSIGNED sRankings5thPercentile;
		UNSIGNED sRankings95thPercentile;
		UNSIGNED researchActivity5thPercentile;
		UNSIGNED researchActivity95thPercentile;
		UNSIGNED studentSupportOutcomes5thPercentile;
		UNSIGNED studentSupportOutcomes95thPercentile;
		UNSIGNED diversity5thPercentile;
		UNSIGNED diversity95thPercentile;
		REAL avgPubsPerAllocFaculty2006;
		REAL avgrageCitationsPerPub;
		REAL percFacultywGrants2006;
		REAL awardsPerAllocFaculty2006;
		REAL percFirstYearStuFullFinanSuppFall05;
		REAL avgCompletionPerc;
		REAL medianTimeToDegree06;
		REAL percwAcademPlans;
		INTEGER collectsPostGradEmpData;
		REAL nonAsianMinorityFacultyPerc06;
		REAL femaleFacultyPerc06;
		REAL nonAsianMinorityStudentsPercFall05;
		REAL femaleStudentsPercFall20;
		REAL internationalStudentsPercFall05;
		REAL avgNumPhDGrad0206;
		REAL percInterdisciplinaryFaculty06;
		REAL avgGRE0406;
		REAL perc1stYearStudentsExternFellow05;
		INTEGER studentWorkSpace;
		INTEGER healthInsurance;
		REAL numStudentActivities;
		UNSIGNED totalFaculty06;
		REAL numAllocFaculty06;
		REAL assistProfPerc06;
		REAL tenuredFacultyPerc06;
		UNSIGNED numCoreAndNewFaculty06;
		UNSIGNED numStudentsEnrolledFall05;
		REAL avgAnnualFirstYrEnroll0206;
		REAL percStudentsResearchAssistFall05;
		REAL percStudentsTeachingAssistFall05;
		REAL percFirstYrStudentsInstFellowAlone;
		REAL percFirstYrStudentsComboFellowTrainee;
		REAL percFirstYrStudentsInternalFellowsAssist;
		REAL percFirstYrStudentsMultiInternAssist;
		UNSIGNED orientationNewGrad;
		UNSIGNED internationalStudentOrientation;
		UNSIGNED langScreenSupportPrior2Teaching;
		UNSIGNED instructionInWriting;
		UNSIGNED instructionInStats;
		UNSIGNED prizesAwardTeachResearch;
		UNSIGNED assistTrainPropWrit;
		UNSIGNED onCampGradResearchConf;
		UNSIGNED formTrainAcademInteg;
		UNSIGNED activeGradStudentAssoc;
		UNSIGNED staffAssignedGradStudentAssoc;
		UNSIGNED finanSuppGradStudentAssoc;
		UNSIGNED postAcademGrievProc;
		UNSIGNED dispResProc;
		UNSIGNED regGradProgDirCoordMeet;
		UNSIGNED annualReviewAllEnrolDocStudents;
		UNSIGNED orgTrainStudentsTeachingSkills;
		UNSIGNED travelSupportProfMeet;
		UNSIGNED countStudentActivities;

	END;

	EXPORT File := DATASET('~mpayne3::nrcdata::mastertable',Layout,CSV(Heading(1)));

END;