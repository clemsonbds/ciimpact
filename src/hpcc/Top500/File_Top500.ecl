﻿EXPORT File_Top500 := MODULE

	EXPORT Layout := RECORD

		STRING institution;
		UNSIGNED year;
		UNSIGNED app;
		UNSIGNED dRankSum;
		REAL rMaxSum;
		
	END;

	EXPORT File := DATASET('~mpayne3::top500',Layout,CSV(Heading(1)));

END;