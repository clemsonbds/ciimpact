﻿IMPORT NSF.File_NSF as NSF;
IMPORT Publications.ExposedPubs as Pubs;
IMPORT NRC.File_NRC AS NRC;
IMPORT IPEDS.File_Institutional_Characteristics_2012 AS IPEDS;
IMPORT IPEDS.CarnegieClassification_2012 as CC;
IMPORT Top500.File_Top500 AS Top500;
IMPORT EPSCOR.File_EPSCOR AS EPSCOR;
IMPORT STD;

// srtNRC := SORT(NRC.File,institutionname);
// ddNRC  := DEDUP(srtNRC,institutionname);
// OUTPUT(ddNRC,{institutionname},'~mpayne3::peekdata::uniqueinstnrc',OVERWRITE);
// COUNT(ddnrc);

// srtNSF := SORT(NSF.Institutions,name);
// ddNSF  := DEDUP(srtNSF,name);
// OUTPUT(ddNSF,{name},'~mpayne3::peekdata::uniqueinstnnsf',OVERWRITE);
// COUNT(ddnsf);

IPEDScc := CC.Doctoral + CC.High + CC.VeryHigh;

/*

NRC - NSF

*/
NrcNsfRec := RECORD

	STRING NrcName;
	STRING NsfName;

END;

NrcNsfRec IniNrcNsf(NRC.File Le, NSF.Institutions Ri) := TRANSFORM

	SELF.NrcName := Le.institutionname;
	SELF.NsfName := std.Str.toUpperCase((STRING)Ri.name);
	
END;

nnmatches := JOIN(NRC.File,NSF.Institutions, LEFT.institutionname = std.Str.toUpperCase((STRING)RIGHT.name), IniNrcNsf(LEFT,RIGHT));
srtnnmatches := SORT(nnmatches,nrcname,nsfname);
ddnnmatches := DEDUP(srtnnmatches,nrcname,nsfname);

OUTPUT(ddnnmatches,,'~mpayne3::peekdata::nrcnnsfmatches',OVERWRITE);
COUNT(ddnnmatches);

/*

NRC - IPEDS

*/
NrcIPEDSRec := RECORD

	STRING NrcName;
	STRING IpedsName;

END;

NrcIPEDSRec IniNrcIPEDS(NRC.File Le, IPEDScc Ri) := TRANSFORM

	SELF.NrcName := Le.institutionname;
	SELF.IpedsName := std.Str.toUpperCase(Ri.instnm);
	
END;

nrcipedsmatches := JOIN(NRC.File,IPEDScc, LEFT.institutionname = std.Str.toUpperCase(RIGHT.instnm), IniNrcIPEDS(LEFT,RIGHT));
srtnrcipedsmatches := SORT(nrcipedsmatches,nrcname,ipedsname);
ddnrcipedsmatches := DEDUP(srtnrcipedsmatches,nrcname,ipedsname);

OUTPUT(ddnrcipedsmatches,,'~mpayne3::peekdata::nrcnipdesmatches',OVERWRITE);
COUNT(ddnrcipedsmatches);


/*

NSF - IPEDS

*/
NsfIPEDSRec := RECORD

	STRING NsfName;
	STRING IpedsName;
	STRING NsfZip;
	STRING IpedsZip;

END;

NsfIPEDSRec IniNsfIPEDS(NSF.Institutions Le, IPEDScc Ri) := TRANSFORM

	SELF.NsfName := std.Str.toUpperCase((STRING)Le.name);
	SELF.IpedsName := std.Str.toUpperCase(Ri.instnm);
	SELF.NsfZip := (STRING)Le.zipcode;
	SELF.IpedsZip	:= REGEXREPLACE('-',Ri.zip,'');
	
END;

nsfipedsmatches := JOIN(NSF.Institutions,IPEDScc, (std.Str.toUpperCase((STRING)LEFT.name) = std.Str.toUpperCase(RIGHT.instnm))// OR
																										 //((STRING)LEFT.zipcode = REGEXREPLACE('-',RIGHT.zip,''))
																										 , IniNsfIPEDS(LEFT,RIGHT));
srtnsfipedsmatches := SORT(nsfipedsmatches,nsfname,ipedsname);
ddnsfipedsmatches := DEDUP(srtnsfipedsmatches,nsfname,ipedsname);

nsfipedsmatcheszip := JOIN(NSF.Institutions(zipcode != ''),IPEDScc(zip != ''), //((STRING)LEFT.zipcode[..5] = REGEXREPLACE('-',RIGHT.zip,'')[..5]) OR
																										 ((STRING)LEFT.zipcode = REGEXREPLACE('-',RIGHT.zip,''))
																										 , IniNsfIPEDS(LEFT,RIGHT));
srtnsfipedsmatcheszip := SORT(nsfipedsmatcheszip,nsfname,ipedsname);
ddnsfipedsmatcheszip := DEDUP(srtnsfipedsmatcheszip,nsfname,ipedsname);

nsfipedsmatcheszipshort := JOIN(NSF.Institutions(zipcode != ''),IPEDScc(zip != ''), ((STRING)LEFT.zipcode[..5] = REGEXREPLACE('-',RIGHT.zip,'')[..5])// OR
																										 //((STRING)LEFT.zipcode = REGEXREPLACE('-',RIGHT.zip,''))
																										 , IniNsfIPEDS(LEFT,RIGHT));
srtnsfipedsmatcheszipshort := SORT(nsfipedsmatcheszipshort,nsfname,ipedsname);
ddnsfipedsmatcheszipshort := DEDUP(srtnsfipedsmatcheszipshort,nsfname,ipedsname);

allnsfipeds := ddnsfipedsmatches + ddnsfipedsmatcheszip + ddnsfipedsmatcheszipshort;
srtallnsfipeds := SORT(allnsfipeds,nsfname,ipedsname);
ddallnsfipeds := DEDUP(srtallnsfipeds,nsfname,ipedsname);

OUTPUT(ddallnsfipeds,,'~mpayne3::peekdata::nsfipdesmatches',OVERWRITE);
COUNT(ddallnsfipeds);

// OUTPUT(EPSCOR.File,NAMED('EPSCOR'));
// OUTPUT(NRC.File,NAMED('NRC'));
// OUTPUT(IPEDS.File,NAMED('IPEDS'));
// OUTPUT(Top500.File,NAMED('Top500'));
// OUTPUT(Pubs.Articles,NAMED('Publications'));
// OUTPUT(NSF.Institutions,NAMED('NSF_Awards'));