﻿IMPORT NSF.File_NSF as NSF;
IMPORT Publications.ExposedPubs as Pubs;
IMPORT NRC.File_NRC AS NRC;
IMPORT IPEDS.File_Institutional_Characteristics_2012 AS IPEDS;
IMPORT Top500.File_Top500 AS Top500;
IMPORT EPSCOR.File_EPSCOR AS EPSCOR;

srtNRC := SORT(NRC.File,institutionname);
ddNRC  := DEDUP(srtNRC,institutionname);
OUTPUT(ddNRC,{institutionname},'~mpayne3::peekdata::uniqueinstnrc',OVERWRITE);
COUNT(ddnrc);

OUTPUT(EPSCOR.File,NAMED('EPSCOR'));
OUTPUT(NRC.File,NAMED('NRC'));
OUTPUT(IPEDS.File,NAMED('IPEDS'));
OUTPUT(Top500.File,NAMED('Top500'));
OUTPUT(Pubs.Articles,NAMED('Publications'));
OUTPUT(NSF.Awards,NAMED('NSF_Awards'));