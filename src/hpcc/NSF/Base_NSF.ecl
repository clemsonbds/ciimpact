﻿lAwardInstrumentXML:= RECORD
	UNICODE value {XPATH('Value')};
END;

lLongNameXML:= RECORD
	UNICODE longName {XPATH('LongName')};
END;

lOrganizationXML:= RECORD
	UNICODE code {XPATH('Code')};
	DATASET(lLongNameXML) Directorate {XPATH('Directorate')};
	DATASET(lLongNameXML) Division {XPATH('Division')};
END;

lProgramOfficerXML:= RECORD
	UNICODE name {XPATH('SignBlockName')};
END;

lPIsXML := RECORD

	UNICODE firstName {XPATH('FirstName')};
	UNICODE lastName {XPATH('LastName')};
	UNICODE emailAddress {XPATH('EmailAddress')};
	UNICODE startDate {XPATH('StartDate')};
	UNICODE endDate {XPATH('EndDate')};
	UNICODE roleCode {XPATH('RoleCode')};

END;

lInstitutionsXML := RECORD

	UNICODE name {XPATH('Name')};
	UNICODE cityName {XPATH('CityName')};
	UNICODE zipCode {XPATH('ZipCode')};
	UNICODE phoneNumber {XPATH('PhoneNumber')};
	UNICODE startDate {XPATH('StreetAddress')};
	UNICODE endDate {XPATH('CountryName')};
	UNICODE stateName {XPATH('StateName')};
	UNICODE stateCode {XPATH('StateCode')};

END;

lFoaInformationXML := RECORD

	UNICODE code {XPATH('Code')};	
	UNICODE name {XPATH('Name')};

END;

lProgramElementXML := RECORD
	UNICODE code {XPATH('Code')};
	UNICODE text {XPATH('Text')};
END;

lProgramReferenceXML := RECORD

	UNICODE code {XPATH('Code')};
	UNICODE text {XPATH('Text')};

END;


lAwardXML:= RECORD

	UNICODE awardTitle																{XPATH('AwardTitle')};
	UNICODE awardEffectiveDate												{XPATH('AwardEffectiveDate')};
	UNICODE awardExpirationDate 											{XPATH('AwardExpirationDate')};
	UNICODE awardAmount																{XPATH('AwardAmount')};
	UNICODE awardInstruments 													{XPATH('AwardInstrument/Value')};
	UNICODE organizationCode													{XPATH('Organization/Code')};
	UNICODE directorate																{XPATH('Organization/Directorate/LongName')};
	UNICODE division																	{XPATH('Organization/Division/LongName')};
	UNICODE programOfficer					{XPATH('ProgramOfficer/SignBlockName')};
	UNICODE abstractNarration 												{XPATH('AbstractNarration')};
	UNICODE minAmdLetterDate 													{XPATH('MinAmdLetterDate')};
	UNICODE maxAmdLetterDate 													{XPATH('MaxAmdLetterDate')};
	UNICODE arraAmount 																{XPATH('ARRAAmount')};
	UNICODE awardID																		{XPATH('AwardID')};
	DATASET (lPIsXML) investigators										{XPATH('Investigator')};
	DATASET (lInstitutionsXML) institutions 					{XPATH('Institution')};
	DATASET (lFoaInformationXML) foaInformation 			{XPATH('FoalInformation')};
	DATASET (lProgramElementXML) programElements 			{XPATH('ProgramElement')};
	DATASET (lProgramReferenceXML) programReferences 	{XPATH('ProgramReference')};
	
END;




nsfxml := DATASET('~mpayne3::nsfdata::allnsf',lAwardXML,XML('rootTag/Award',NOROOT));
//Nsfxml;
lAwardInstrument:= RECORD
	UNICODE value;
END;

lLongName:= RECORD
	UNICODE longName;
END;

lOrganization:= RECORD
	UNICODE code;
	DATASET(lLongName) Directorate;
	DATASET(lLongName) Division;
END;

lProgramOfficer:= RECORD
	UNICODE name;
END;

lPIs := RECORD

	UNICODE firstName;
	UNICODE lastName;
	UNICODE emailAddress;
	UNICODE startDate;
	UNICODE endDate;
	UNICODE roleCode;

END;

lInstitutions := RECORD

	UNICODE name;
	UNICODE cityName;
	UNICODE zipCode;
	UNICODE phoneNumber;
	UNICODE startDate;
	UNICODE endDate;
	UNICODE stateName;
	UNICODE stateCode;

END;

lFoaInformation := RECORD

	UNICODE code;	
	UNICODE name;

END;

lProgramElement := RECORD
	UNICODE code;
	UNICODE text;
END;

lProgramReference := RECORD

	UNICODE code;
	UNICODE text;

END;


lAward:= RECORD

	UNICODE awardTitle;
	UNICODE awardEffectiveDate;
	UNICODE awardExpirationDate;
	UNICODE awardAmount;
	UNICODE awardInstrument;
	UNICODE organizationCode;
	UNICODE directorate;
	UNICODE division;
	UNICODE programOfficer;
	UNICODE abstractNarration;
	UNICODE minAmdLetterDate;
	UNICODE maxAmdLetterDate;
	UNICODE arraAmount;
	UNICODE awardID;
	DATASET (lPIs) investigators;
	DATASET (lInstitutions) institutions;
	DATASET (lFoaInformation) foaInformation;
	DATASET (lProgramElement) programElements;
	DATASET (lProgramReference) programReferences;
	
END;
// OUTPUT(PROJECT(nsfxml,TRANSFORM(lAward,SELF:=LEFT;SELF:=[];)));
EXPORT Base_NSF := OUTPUT(PROJECT(nsfxml,TRANSFORM(lAward,SELF:=LEFT;SELF:=[];)),,'~mpayne3::nsfdata::cleannsf',OVERWRITE);