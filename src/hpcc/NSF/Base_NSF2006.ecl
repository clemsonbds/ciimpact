﻿lProgramsXML:= RECORD
	UNICODE program {XPATH('')};
END;
lElemCodesXML := RECORD
	UNICODE elemCode {XPATH('')};
END;

lRefCodesXML := RECORD

	UNICODE refCode {XPATH('')};

END;

lPIsXML := RECORD

	UNICODE principalInvestigator {XPATH('')};

END;

lPiEmailsXML := RECORD

	UNICODE pi_email {XPATH('')};

END;

lCoPIsXML := RECORD

	UNICODE co_pi {XPATH('')};

END;

lAwardXML:= RECORD
	UNICODE awardnumber																				{XPATH('AwardNumber')};
	UNICODE awardedAmountToDate																{XPATH('AwardedAmountToDate')};
	UNICODE title																							{XPATH('Title')};
	UNICODE awardinstrument 																	{XPATH('AwardInstrument')};
	UNICODE startDate																					{XPATH('StartDate')};
	UNICODE expirationDate 																		{XPATH('ExpirationDate')};
	UNICODE lastAmendmentDate 																{XPATH('LastAmendmentDate')};
	UNICODE directorate 																			{XPATH('NSFDirectorate')};
	UNICODE nsfOrganization 																	{XPATH('NsfOrganization')};
	DATASET (lProgramsXML) programs 													{XPATH('Program')};
	DATASET (lElemCodesXML) elemCodes 												{XPATH('ProgramElementCode')};
	DATASET (lRefCodesXML) refCodes 													{XPATH('ProgramReferenceCode')};
	DATASET (lPIsXML) PIs																			{XPATH('PrincipalInvestigator')};
	DATASET (lPiEmailsXML) piEmails														{XPATH('PIEmailAddress')};
	DATASET (lCoPIsXML) co_pis																{XPATH('Co-PIName')};
	UNICODE state 																						{XPATH('State')};
	UNICODE organization 																			{XPATH('Organization')};
	UNICODE organizationStreet 																{XPATH('OrganizationStreet')};
	UNICODE organizationState																	{XPATH('OrganizationState')};
	UNICODE organizationZip																		{XPATH('OrganizationZip')};
	UNICODE organizationPhone																	{XPATH('OrganizationPhone')};
	UNICODE programManager 																		{XPATH('ProgramManager')};
	UNICODE arraAmount 																				{XPATH('ARRAAmount')};
	UNICODE abstract 																					{XPATH('Abstract')};
	
END;




nsfxml := DATASET('~mpayne3::nsfdata::nsf2006xml',lAwardXML,XML('AwardsList/Award',NOROOT));
//Nsfxml;
lPrograms:= RECORD
	UNICODE program;
END;
lElemCodes := RECORD
	UNICODE elemCode;
END;

lRefCodes := RECORD

	UNICODE refCode;

END;

lCoPIs := RECORD

	UNICODE co_pi;

END;

lPIs := RECORD

	UNICODE PrincipalInvestigator;

END;

lPiEmails := RECORD

	UNICODE pi_email;

END;

lAward:= RECORD
	UNICODE awardnumber;
	UNICODE awardedAmountToDate;
	UNICODE title;
	UNICODE awardinstrument;
	UNICODE startDate;
	UNICODE expirationDate;
	UNICODE lastAmendmentDate;
	UNICODE directorate;
	UNICODE nsfOrganization;
	DATASET(lPrograms) programs;
	DATASET(lElemCodes) ElemCodes;
	DATASET(lRefCodes) RefCodes;
	DATASET(lPIs) PIs;
	DATASET (lPiEmails) piEmails;
	DATASET(lCoPIs) co_pis;
	UNICODE state;
	UNICODE organization;
	UNICODE organizationStreet;
	UNICODE organizationState;
	UNICODE organizationZip;
	UNICODE organizationPhone;
	UNICODE programManager;
	UNICODE arraAmount;
	UNICODE abstract;
	
END;
//OUTPUT(PROJECT(nsfxml,TRANSFORM(lAward,SELF:=LEFT;SELF:=[];)));
EXPORT Base_NSF := OUTPUT(PROJECT(nsfxml,TRANSFORM(lAward,SELF:=LEFT;SELF:=[];)),,'~mpayne3::nsfdata::clean2006nsf',OVERWRITE);