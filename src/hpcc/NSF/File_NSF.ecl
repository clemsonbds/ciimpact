﻿IMPORT $;
EXPORT File_NSF := MODULE;
	
	lAwardInstrument:= RECORD
		UNICODE value;
	END;

	lLongName:= RECORD
		UNICODE longName;
	END;

	lOrganization:= RECORD
		UNICODE code;
		DATASET(lLongName) Directorate;
		DATASET(lLongName) Division;
	END;

	lProgramOfficer:= RECORD
		UNICODE name;
	END;

	lPIs := RECORD

		UNICODE firstName;
		UNICODE lastName;
		UNICODE emailAddress;
		UNICODE startDate;
		UNICODE endDate;
		UNICODE roleCode;

	END;

	lInstitutions := RECORD

		UNICODE name;
		UNICODE cityName;
		UNICODE zipCode;
		UNICODE phoneNumber;
		UNICODE startDate;
		UNICODE endDate;
		UNICODE stateName;
		UNICODE stateCode;

	END;

	lFoaInformation := RECORD

		UNICODE code;	
		UNICODE name;

	END;

	lProgramElement := RECORD
		UNICODE code;
		UNICODE text;
	END;

	lProgramReference := RECORD

		UNICODE code;
		UNICODE text;

	END;

	lAward:= RECORD

		UNICODE awardTitle;
		UNICODE awardEffectiveDate;
		UNICODE awardExpirationDate;
		UNICODE awardAmount;
		UNICODE awardInstrument;
		UNICODE organizationCode;
		UNICODE directorate;
		UNICODE division;
		UNICODE programOfficer;
		UNICODE abstractNarration;
		UNICODE minAmdLetterDate;
		UNICODE maxAmdLetterDate;
		UNICODE arraAmount;
		UNICODE awardID;
		DATASET (lPIs) investigators;
		DATASET (lInstitutions) institutions;
		DATASET (lFoaInformation) foaInformation;
		DATASET (lProgramElement) programElements;
		DATASET (lProgramReference) programReferences;
		
	END;

	EXPORT Awards := DATASET('~mpayne3::nsfdata::cleannsf',lAward,THOR);
	EXPORT Investigators := Awards.Investigators;
	EXPORT Institutions := Awards.Institutions;
	EXPORT Foa := Awards.FoaInformation;
	EXPORT ProgramElements := Awards.ProgramElements;
	EXPORT ProgramReferences := Awards.ProgramReferences;

END;