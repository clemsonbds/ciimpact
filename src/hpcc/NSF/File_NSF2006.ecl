﻿IMPORT $;
EXPORT File_NSF2006 := MODULE;
	lAward:= RECORD
		UNICODE awardnumber;
		UNICODE awardedAmountToDate;
		UNICODE title;
		UNICODE awardinstrument;
		UNICODE startDate;
		UNICODE expirationDate;
		UNICODE lastAmendmentDate;
		UNICODE directorate;
		UNICODE nsfOrganization;
		DATASET(lPrograms) programs;
		DATASET(lProgramElementCodes) ElemCodes;
		DATASET(lProgramReferenceCodes) RefCodes;
		DATASET(lPIs) PIs;
		DATASET (lPiEmails) piEmails;
		DATASET(lCoPIs) co_pis;
		UNICODE state;
		UNICODE organization;
		UNICODE organizationStreet;
		UNICODE organizationState;
		UNICODE organizationZip;
		UNICODE organizationPhone;
		UNICODE programManager;
		UNICODE arraAmount;
		UNICODE abstract;
		
	END;

	EXPORT Awards := DATASET('~mpayne3::nsfdata::clean2006nsf',lAward,THOR);
	EXPORT Programs := Awards.Programs;
	EXPORT ElemCodes := Awards.ElemCodes;
	EXPORT RefCodes := Awards.RefCodes;
	EXPORT PIs := Awards.PIs;
	EXPORT PiEmails := Awards.PiEmails;
	EXPORT CoPIs := Awards.Co_PIs;

END;