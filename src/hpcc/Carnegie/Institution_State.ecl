﻿EXPORT Institution_State := MODULE

EXPORT Layout := RECORD

	STRING institutionName;
	STRING stabbr;
	STRING unitid;
	STRING ccbasic2005;
	STRING ccbasic2010;

END;

EXPORT FILE := DATASET('~ciimpact::institutionstate',Layout,CSV(HEADING(1)));


END;