﻿EXPORT File_EPSCOR := MODULE

	EXPORT Layout := RECORD

		STRING state;
		STRING stateCode;
		UNSIGNED yearEPSCOR;
		
	END;

	EXPORT File := DATASET('~ciimpact::nas::epscorstatelist',Layout,CSV(Heading(1)));

END;