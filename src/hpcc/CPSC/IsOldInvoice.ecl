﻿IMPORT $;

LOCAL IsInvoice := $.Accounts.TradeType = 'I';
LOCAL IsBefore1995 := $.Accounts.ReportDate < '1995';
LOCAL IsActiveBalance := $.Accounts.Balance > 0;


EXPORT IsOldInvoice := IsInvoice AND IsBefore1995 AND IsActiveBalance;