﻿IMPORT $;

MySort := SORT($.Accounts, AccountNumber, -Balance); 

//EXPORT DedupAccounts := COUNT(DEDUP(MySort, AccountNumber));
//EXPORT DedupAccounts := COUNT($.Accounts) - COUNT(DEDUP(MySort, AccountNumber));
EXPORT DedupAccounts := DEDUP(MySort, AccountNumber) : PERSIST('~CLASS::MEP::PERSIST::DedupAccount');