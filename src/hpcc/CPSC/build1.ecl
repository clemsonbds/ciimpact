﻿IMPORT TrainingMike as X;
	MyTable := TABLE(X.File_Persons.file, {X.File_Persons.file.BureauCode});
	MyTableDist := DISTRIBUTE(MyTable,HASH32(BureauCode));
	MyTableSort := SORT(MyTableDist,BureauCode,LOCAL);
	MyTableSortDedup := DEDUP(MyTableSort,LOCAL);
	Count(MyTableSortDedup);