﻿IMPORT $;

LOCAL IsFloridian := $.Persons.State = 'FL';
LOCAL IsMale := $.Persons.Gender = 'M';
LOCAL IsBorn80 := NOT $.Persons.BIRTHDATE = '' AND  $.Persons.BIRTHDATE[1..4] > '1979';
EXPORT IsYoungMaleFloridian := IsFloridian and IsMale and IsBorn80;