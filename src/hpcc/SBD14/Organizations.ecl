﻿IMPORT $;
IMPORT Publications.ExposedPubs as Pubs;
IMPORT Publications.ArticleClass as ArticleClass;
IMPORT STD;
//IMPORT $.ArticleYear;

year := (UNICODE)2006;

MyArticles := Pubs.Articles(pub_year = year);
MyChemArticles := MyArticles(ArticleClass.Chemistry > 0);
//MyChemArticles := MyChemArticle($.OrgUniv > 0);

orgRec := RECORD

	UNICODE org;

END;

//OUTPUT(COUNT(),NAMED('ChemArticlesCount'));
//OUTPUT(MyArticles(ArticleClass.Chemistry > 0),NAMED('ChemArticles'));
//authorGrpSrt := SORT(Pubs.AuthorGroupOrg,organization);
//uniqueGrp := DEDUP(authorGrpSrt, organization);

Pubs.AuthorGroup getAuthorGrps(Pubs.AuthorGroup R) := TRANSFORM

	SELF := R;

END;

Pubs.AuthorGroupOrg getOrganizations(Pubs.AuthorGroupOrg R) := TRANSFORM

	//str := (STRING)R.organization;
	upper := std.Uni.ToUpperCase(R.organization);
	noperiod := std.Uni.FindReplace(upper,'.','');
	nodash := std.Uni.FindReplace(noperiod,'-',' ');
	nocomma := std.Uni.FindReplace(nodash,',','');
	nolp := std.Uni.FindReplace(nocomma,'(','');
	norp := std.Uni.FindReplace(nolp,')','');
	//nopossess := std.Uni.FindReplace(norp,'\'s','');
	noapos := std.Uni.FindReplace(norp,'\'','');
	noslash := std.Uni.FindReplace(noapos,'/',' ');
	//unistr := (UNICODE)nopossess;
	SELF.organization := std.Uni.CleanAccents(noslash);

END;

authorGrps := NORMALIZE(MyChemArticles,LEFT.author_groups, getAuthorGrps(RIGHT));
usaAuthors := authorGrps(author_group_country = 'usa');
orgGrps := NORMALIZE(usaAuthors,LEFT.author_group_org, getOrganizations(RIGHT));
//OUTPUT(usaAuthors(author_group_city = ''),NAMED('NoCityInfo'));
//OUTPUT(COUNT(usaAuthors(author_group_city = '')),NAMED('NoCityInfoCount'));
SET OF UNICODE numbers := ['1','2','3','4','5','6','7','8','9','0'];

//univall := orgGrps(std.str.contains((STRING)organization, 'University', FALSE));
univall := orgGrps('UNIVERSITY' IN std.str.SplitWords((STRING)organization, ' ', FALSE) OR
									 'UNIVERSTITY' IN std.str.SplitWords((STRING)organization, ' ', FALSE) OR
									 'UUNIVERSITY' IN std.str.SplitWords((STRING)organization, ' ', FALSE) OR
									 'UNVERSITY' IN std.str.SplitWords((STRING)organization, ' ', FALSE) OR
									 'UNIVERSITV' IN std.str.SplitWords((STRING)organization, ' ', FALSE) OR
									 'UNIVOF' IN std.str.SplitWords((STRING)organization, ' ', FALSE) OR
									 'UNIVERSITYOF' IN std.str.SplitWords((STRING)organization, ' ', FALSE) OR
									 'UNIVESITY' IN std.str.SplitWords((STRING)organization, ' ', FALSE) OR
									 'UNIVERSITYZ' IN std.str.SplitWords((STRING)organization, ' ', FALSE) OR
									 'UNIVERSITE' IN std.str.SplitWords((STRING)organization, ' ', FALSE) OR
									 'UNIVERSITAT' IN std.str.SplitWords((STRING)organization, ' ', FALSE) OR
									 'UNIVERSIDADE' IN std.str.SplitWords((STRING)organization, ' ', FALSE) OR
									 'UNIVERISTY' IN std.str.SplitWords((STRING)organization, ' ', FALSE) OR
									 'UNIVERSITYS' IN std.str.SplitWords((STRING)organization, ' ', FALSE) OR
									 'UNIVERSITIES' IN std.str.SplitWords((STRING)organization, ' ', FALSE) OR
									 'VANDERBILT' IN std.str.SplitWords((STRING)organization, ' ', FALSE) OR
									 'UNIV' IN std.str.SplitWords((STRING)organization, ' ', FALSE) OR
									 'UNLV' IN std.str.SplitWords((STRING)organization, ' ', FALSE) OR
									 'UNM' IN std.str.SplitWords((STRING)organization, ' ', FALSE) OR
									 'UPENN' IN std.str.SplitWords((STRING)organization, ' ', FALSE) OR
									 'UT' IN std.str.SplitWords((STRING)organization, ' ', FALSE) OR
									 'UTA' IN std.str.SplitWords((STRING)organization, ' ', FALSE) OR
									 'UW' IN std.str.SplitWords((STRING)organization, ' ', FALSE) OR
									 'UC' IN std.str.SplitWords((STRING)organization, ' ', FALSE) OR
									 'UCDAVIS' IN std.str.SplitWords((STRING)organization, ' ', FALSE) OR
									 'UCD' IN std.str.SplitWords((STRING)organization, ' ', FALSE) OR
									 'UALR' IN std.str.SplitWords((STRING)organization, ' ', FALSE) OR
									 'UCLA' IN std.str.SplitWords((STRING)organization, ' ', FALSE) OR
									 'UCSD' IN std.str.SplitWords((STRING)organization, ' ', FALSE) OR
									 'UCSF' IN std.str.SplitWords((STRING)organization, ' ', FALSE) OR
									 'UDRI' IN std.str.SplitWords((STRING)organization, ' ', FALSE) OR
									 'UF' IN std.str.SplitWords((STRING)organization, ' ', FALSE) OR
									 'UCF' IN std.str.SplitWords((STRING)organization, ' ', FALSE) OR
									 'ULCA' IN std.str.SplitWords((STRING)organization, ' ', FALSE) OR
									 'UMASS' IN std.str.SplitWords((STRING)organization, ' ', FALSE) OR
									 'TAMU' IN std.str.SplitWords((STRING)organization, ' ', FALSE) OR
									 'UNC' IN std.str.SplitWords((STRING)organization, ' ', FALSE) OR
									 'CITADEL' IN std.str.SplitWords((STRING)organization, ' ', FALSE) OR
									 'HARVARD' IN std.str.SplitWords((STRING)organization, ' ', FALSE) OR
									 'SUNY' IN std.str.SplitWords((STRING)organization, ' ', FALSE) OR
									 'CUNY' IN std.str.SplitWords((STRING)organization, ' ', FALSE) OR
									 'NDSU' IN std.str.SplitWords((STRING)organization, ' ', FALSE) OR
									 'NCSU' IN std.str.SplitWords((STRING)organization, ' ', FALSE) OR
									 'DUKE' IN std.str.SplitWords((STRING)organization, ' ', FALSE) OR
									 'LSU' IN std.str.SplitWords((STRING)organization, ' ', FALSE) OR
									 'FSU' IN std.str.SplitWords((STRING)organization, ' ', FALSE) OR
									 'FAMU' IN std.str.SplitWords((STRING)organization, ' ', FALSE) OR
									 'CALTECH' IN std.str.SplitWords((STRING)organization, ' ', FALSE) OR
									 'GEORGIATECH' IN std.str.SplitWords((STRING)organization, ' ', FALSE) OR
									 'MIT' IN std.str.SplitWords((STRING)organization, ' ', FALSE) OR
									 'POLYTECHNIC' IN std.str.SplitWords((STRING)organization, ' ', FALSE) OR
									 'POLYTECHNIQUE' IN std.str.SplitWords((STRING)organization, ' ', FALSE) OR
									 'TECH' IN std.str.SplitWords((STRING)organization, ' ', FALSE) OR
									 //'University' IN std.str.SplitWords((STRING)organization, ',', FALSE) OR
                   //'University' IN std.str.SplitWords((STRING)organization, '\'', FALSE) OR
									 ('A' IN std.str.SplitWords((STRING)organization, ' ', FALSE) AND 'AND' IN std.str.SplitWords((STRING)organization, ' ', FALSE) AND 'M' IN std.str.SplitWords((STRING)organization, ' ', FALSE)) OR
									 ('U' IN std.str.SplitWords((STRING)organization, ' ', FALSE) AND 'OF' IN std.str.SplitWords((STRING)organization, ' ', FALSE)) OR
									 ('UNIVER' IN std.str.SplitWords((STRING)organization, ' ', FALSE) AND 'SITY' IN std.str.SplitWords((STRING)organization, ' ', FALSE)) OR
									 ('U' IN std.str.SplitWords((STRING)organization, ' ', FALSE) AND 'CALIFORNIA' IN std.str.SplitWords((STRING)organization, ' ', FALSE)) OR
									 ('U' IN std.str.SplitWords((STRING)organization, ' ', FALSE) AND 'WISCONSIN' IN std.str.SplitWords((STRING)organization, ' ', FALSE)) OR
									 ('ILLINOIS' IN std.str.SplitWords((STRING)organization, ' ', FALSE) AND 'INSTITUTE' IN std.str.SplitWords((STRING)organization, ' ', FALSE)) OR
									 ('ILLINIOS' IN std.str.SplitWords((STRING)organization, ' ', FALSE) AND 'INSTITUTE' IN std.str.SplitWords((STRING)organization, ' ', FALSE)) OR
									 ('GEORGIA' IN std.str.SplitWords((STRING)organization, ' ', FALSE) AND 'TECH' IN std.str.SplitWords((STRING)organization, ' ', FALSE)) OR
									 ('GEORGIA' IN std.str.SplitWords((STRING)organization, ' ', FALSE) AND 'INSTITUTE' IN std.str.SplitWords((STRING)organization, ' ', FALSE)) OR
									 ('GEORGIA' IN std.str.SplitWords((STRING)organization, ' ', FALSE) AND 'INSITUTE' IN std.str.SplitWords((STRING)organization, ' ', FALSE)) OR
									 ('FLORIDA' IN std.str.SplitWords((STRING)organization, ' ', FALSE) AND 'INSTITUTE' IN std.str.SplitWords((STRING)organization, ' ', FALSE)) OR
									 ('MASSACHUSETTS' IN std.str.SplitWords((STRING)organization, ' ', FALSE) AND 'INSTITUTE' IN std.str.SplitWords((STRING)organization, ' ', FALSE)) OR
									 ('MASSACHUSETTS' IN std.str.SplitWords((STRING)organization, ' ', FALSE) AND 'INSTITUDE' IN std.str.SplitWords((STRING)organization, ' ', FALSE)) OR
									 ('MASSACHUSSETTS' IN std.str.SplitWords((STRING)organization, ' ', FALSE) AND 'INSTITUTE' IN std.str.SplitWords((STRING)organization, ' ', FALSE)) OR
									 ('CALIFORNIA' IN std.str.SplitWords((STRING)organization, ' ', FALSE) AND 'INSTITUTE' IN std.str.SplitWords((STRING)organization, ' ', FALSE) AND 'TECHNOL' IN std.str.SplitWords((STRING)organization, ' ', FALSE)) OR
									 ('CALIFORNIA' IN std.str.SplitWords((STRING)organization, ' ', FALSE) AND 'INSTITUTE' IN std.str.SplitWords((STRING)organization, ' ', FALSE) AND 'TECHNOLOGY' IN std.str.SplitWords((STRING)organization, ' ', FALSE)) OR
									 ('JOHNS' IN std.str.SplitWords((STRING)organization, ' ', FALSE) AND 'HOPKINS' IN std.str.SplitWords((STRING)organization, ' ', FALSE)) OR
									 ('COLLEGE' IN std.str.SplitWords((STRING)organization, ' ', FALSE) AND 'WILLIAM' IN std.str.SplitWords((STRING)organization, ' ', FALSE)) OR
									 ('COLLEGE' IN std.str.SplitWords((STRING)organization, ' ', FALSE) AND 'JERSEY' IN std.str.SplitWords((STRING)organization, ' ', FALSE)) OR
									 ('COLLEGE' IN std.str.SplitWords((STRING)organization, ' ', FALSE) AND 'BENEDICT' IN std.str.SplitWords((STRING)organization, ' ', FALSE)) OR
									 ('COLLEGE' IN std.str.SplitWords((STRING)organization, ' ', FALSE) AND 'STATIN' IN std.str.SplitWords((STRING)organization, ' ', FALSE)) OR
									 ('COLLEGE' IN std.str.SplitWords((STRING)organization, ' ', FALSE) AND 'SUNY' IN std.str.SplitWords((STRING)organization, ' ', FALSE)) OR
									 ('COLLEGE' IN std.str.SplitWords((STRING)organization, ' ', FALSE) AND 'STOLEN' IN std.str.SplitWords((STRING)organization, ' ', FALSE)) OR
									 ('COLLEGE' IN std.str.SplitWords((STRING)organization, ' ', FALSE) AND 'HOLY' IN std.str.SplitWords((STRING)organization, ' ', FALSE)) OR
									 ('COLLEGE' IN std.str.SplitWords((STRING)organization, ' ', FALSE) AND 'VIRGINIA' IN std.str.SplitWords((STRING)organization, ' ', FALSE)) OR
									 ('COLLEGE' IN std.str.SplitWords((STRING)organization, ' ', FALSE) AND 'YORK' IN std.str.SplitWords((STRING)organization, ' ', FALSE)) OR
									 ('COLLEGE' IN std.str.SplitWords((STRING)organization, ' ', FALSE) AND 'CHARLESTON' IN std.str.SplitWords((STRING)organization, ' ', FALSE)) OR
									 ('COLLEGE' IN std.str.SplitWords((STRING)organization, ' ', FALSE) AND 'VASSAR' IN std.str.SplitWords((STRING)organization, ' ', FALSE)) OR
									 ('COLLEGE' IN std.str.SplitWords((STRING)organization, ' ', FALSE) AND 'WOOSTER' IN std.str.SplitWords((STRING)organization, ' ', FALSE)) OR
									 ('COLLEGE' IN std.str.SplitWords((STRING)organization, ' ', FALSE) AND 'UTAH' IN std.str.SplitWords((STRING)organization, ' ', FALSE)) OR
									 ('COLLEGE' IN std.str.SplitWords((STRING)organization, ' ', FALSE) AND 'HUNTER' IN std.str.SplitWords((STRING)organization, ' ', FALSE)) OR
									 ('COLLEGE' IN std.str.SplitWords((STRING)organization, ' ', FALSE) AND 'NEWARK' IN std.str.SplitWords((STRING)organization, ' ', FALSE)) OR
									 ('COLLEGE' IN std.str.SplitWords((STRING)organization, ' ', FALSE) AND 'MARIN' IN std.str.SplitWords((STRING)organization, ' ', FALSE)));//AND 'William' IN std.str.SplitWords((STRING)organization, ' ', FALSE)));
univallnum := univall(organization[1] NOT IN numbers);
univallSrt := SORT(univallnum,organization);
univalldd := DEDUP(univallSrt, organization);


//collegeall := orgGrps(std.str.contains((STRING)organization, 'College', FALSE));
collegeall := orgGrps(('COLLEGE' IN std.str.SplitWords((STRING)organization, ' ', FALSE) OR 
											'COLLEGES' IN std.str.SplitWords((STRING)organization, ' ', FALSE)) AND
                       NOT ('COLLEGE' IN std.str.SplitWords((STRING)organization, ' ', FALSE) AND 'OF' IN std.str.SplitWords((STRING)organization, ' ', FALSE)));
collegeallnum := collegeall(organization[1] NOT IN numbers);
collegeallSrt := SORT(collegeallnum,organization);
collegealldd := DEDUP(collegeallSrt, organization);

univall2 := univalldd + collegealldd;						 
univall3 := univall2(organization[1] NOT IN numbers);
univSrt := SORT(univall3,organization);
univ := DEDUP(univSrt, organization);

//OUTPUT(univ,NAMED('Universities'));
OUTPUT(univalldd,NAMED('univalldd'));
OUTPUT(collegealldd,NAMED('collegealldd'));
OUTPUT(univ,,'~mpayne3::scopusdata::univlist',OVERWRITE);
OUTPUT(COUNT(univ),NAMED('UniversityCount'));

nonunivall := orgGrps(NOT('UNIVERSITY' IN std.str.SplitWords((STRING)organization, ' ', FALSE)) AND
											NOT('UNIVERSITYS' IN std.str.SplitWords((STRING)organization, ' ', FALSE)) AND
											NOT('UNIVERSTITY' IN std.str.SplitWords((STRING)organization, ' ', FALSE)) AND
											NOT('UUNIVERSITY' IN std.str.SplitWords((STRING)organization, ' ', FALSE)) AND
									 NOT('UNVERSITY' IN std.str.SplitWords((STRING)organization, ' ', FALSE)) AND
									 NOT('UNIVERSITV' IN std.str.SplitWords((STRING)organization, ' ', FALSE)) AND
									 NOT('UNIVOF' IN std.str.SplitWords((STRING)organization, ' ', FALSE)) AND
									 NOT('UNIVERSITYOF' IN std.str.SplitWords((STRING)organization, ' ', FALSE)) AND
									 NOT('UNIVESITY' IN std.str.SplitWords((STRING)organization, ' ', FALSE)) AND
									 NOT('UNIVERSITYZ' IN std.str.SplitWords((STRING)organization, ' ', FALSE)) AND
									 NOT('UNIVERSITE' IN std.str.SplitWords((STRING)organization, ' ', FALSE)) AND
									 NOT('UNIVERSITAT' IN std.str.SplitWords((STRING)organization, ' ', FALSE)) AND
									 NOT('UNIVERSIDADE' IN std.str.SplitWords((STRING)organization, ' ', FALSE)) AND
									 NOT('UNIVERISTY' IN std.str.SplitWords((STRING)organization, ' ', FALSE)) AND
									 NOT('UNIVERSITYS' IN std.str.SplitWords((STRING)organization, ' ', FALSE)) AND
									 NOT('UNIVERSITIES' IN std.str.SplitWords((STRING)organization, ' ', FALSE)) AND
									 NOT('VANDERBILT' IN std.str.SplitWords((STRING)organization, ' ', FALSE)) AND
									 NOT('UNIV' IN std.str.SplitWords((STRING)organization, ' ', FALSE)) AND
									 NOT('UNLV' IN std.str.SplitWords((STRING)organization, ' ', FALSE)) AND
									 NOT('UNM' IN std.str.SplitWords((STRING)organization, ' ', FALSE)) AND
									 NOT('UPENN' IN std.str.SplitWords((STRING)organization, ' ', FALSE)) AND
									 NOT('UT' IN std.str.SplitWords((STRING)organization, ' ', FALSE)) AND
									 NOT('UTA' IN std.str.SplitWords((STRING)organization, ' ', FALSE)) AND
									 NOT('UW' IN std.str.SplitWords((STRING)organization, ' ', FALSE)) AND
									 NOT('UC' IN std.str.SplitWords((STRING)organization, ' ', FALSE)) AND
									 NOT('UCDAVIS' IN std.str.SplitWords((STRING)organization, ' ', FALSE)) AND
									 NOT('UCD' IN std.str.SplitWords((STRING)organization, ' ', FALSE)) AND
									 NOT('UALR' IN std.str.SplitWords((STRING)organization, ' ', FALSE)) AND
									 NOT('UCLA' IN std.str.SplitWords((STRING)organization, ' ', FALSE)) AND
									 NOT('UCSD' IN std.str.SplitWords((STRING)organization, ' ', FALSE)) AND
									 NOT('UCSF' IN std.str.SplitWords((STRING)organization, ' ', FALSE)) AND
									 NOT('UDRI' IN std.str.SplitWords((STRING)organization, ' ', FALSE)) AND
									 NOT('UF' IN std.str.SplitWords((STRING)organization, ' ', FALSE)) AND
									 NOT('UCF' IN std.str.SplitWords((STRING)organization, ' ', FALSE)) AND
									 NOT('ULCA' IN std.str.SplitWords((STRING)organization, ' ', FALSE)) AND
									 NOT('UMASS' IN std.str.SplitWords((STRING)organization, ' ', FALSE)) AND
									 NOT('TAMU' IN std.str.SplitWords((STRING)organization, ' ', FALSE)) AND
									 NOT('UNC' IN std.str.SplitWords((STRING)organization, ' ', FALSE)) AND
									 NOT('CITADEL' IN std.str.SplitWords((STRING)organization, ' ', FALSE)) AND
									 NOT('HARVARD' IN std.str.SplitWords((STRING)organization, ' ', FALSE)) AND
									 NOT('SUNY' IN std.str.SplitWords((STRING)organization, ' ', FALSE)) AND
									 NOT('CUNY' IN std.str.SplitWords((STRING)organization, ' ', FALSE)) AND
									 NOT('NDSU' IN std.str.SplitWords((STRING)organization, ' ', FALSE)) AND
									 NOT('NCSU' IN std.str.SplitWords((STRING)organization, ' ', FALSE)) AND
									 NOT('DUKE' IN std.str.SplitWords((STRING)organization, ' ', FALSE)) AND
									 NOT('LSU' IN std.str.SplitWords((STRING)organization, ' ', FALSE)) AND
									 NOT('FSU' IN std.str.SplitWords((STRING)organization, ' ', FALSE)) AND
									 NOT('FAMU' IN std.str.SplitWords((STRING)organization, ' ', FALSE)) AND
									 NOT('CALTECH' IN std.str.SplitWords((STRING)organization, ' ', FALSE)) AND
									 NOT('MIT' IN std.str.SplitWords((STRING)organization, ' ', FALSE)) AND
									 NOT('A' IN std.str.SplitWords((STRING)organization, ' ', FALSE) AND 'AND' IN std.str.SplitWords((STRING)organization, ' ', FALSE) AND 'M' IN std.str.SplitWords((STRING)organization, ' ', FALSE)) AND
									 NOT('UNIVER' IN std.str.SplitWords((STRING)organization, ' ', FALSE) AND 'SITY' IN std.str.SplitWords((STRING)organization, ' ', FALSE)) AND
											NOT('COLLEGES' IN std.str.SplitWords((STRING)organization, ' ', FALSE)) AND
											NOT('TECH' IN std.str.SplitWords((STRING)organization, ' ', FALSE)) AND
											NOT('POLYTECHNIC' IN std.str.SplitWords((STRING)organization, ' ', FALSE)) AND
											NOT('POLYTECHNIQUE' IN std.str.SplitWords((STRING)organization, ' ', FALSE)) AND
											//NOT('University,' IN std.str.SplitWords((STRING)organization, ',', FALSE)) AND
											NOT('U' IN std.str.SplitWords((STRING)organization, ' ', FALSE) AND 'OF' IN std.str.SplitWords((STRING)organization, ' ', FALSE)) AND
									    NOT('U' IN std.str.SplitWords((STRING)organization, ' ', FALSE) AND 'CALIFORNIA' IN std.str.SplitWords((STRING)organization, ' ', FALSE)) AND
									    NOT('U' IN std.str.SplitWords((STRING)organization, ' ', FALSE) AND 'WISCONSIN' IN std.str.SplitWords((STRING)organization, ' ', FALSE)) AND
											NOT('ILLINOIS' IN std.str.SplitWords((STRING)organization, ' ', FALSE) AND 'INSTITUTE' IN std.str.SplitWords((STRING)organization, ' ', FALSE)) AND
											NOT('ILLINIOS' IN std.str.SplitWords((STRING)organization, ' ', FALSE) AND 'INSTITUTE' IN std.str.SplitWords((STRING)organization, ' ', FALSE)) AND
											NOT('GEORGIA' IN std.str.SplitWords((STRING)organization, ' ', FALSE) AND 'TECH' IN std.str.SplitWords((STRING)organization, ' ', FALSE)) AND
											NOT('GEORGIA' IN std.str.SplitWords((STRING)organization, ' ', FALSE) AND 'INSITUTE' IN std.str.SplitWords((STRING)organization, ' ', FALSE)) AND
											NOT('GEORGIA' IN std.str.SplitWords((STRING)organization, ' ', FALSE) AND 'INSTITUTE' IN std.str.SplitWords((STRING)organization, ' ', FALSE)) AND
											NOT('GEORGIA' IN std.str.SplitWords((STRING)organization, ' ', FALSE) AND 'TECH' IN std.str.SplitWords((STRING)organization, ' ', FALSE)) AND
											NOT('MASSACHUSETTS' IN std.str.SplitWords((STRING)organization, ' ', FALSE) AND 'INSTITUTE' IN std.str.SplitWords((STRING)organization, ' ', FALSE)) AND
											NOT('MASSACHUSETTS' IN std.str.SplitWords((STRING)organization, ' ', FALSE) AND 'INSTITUDE' IN std.str.SplitWords((STRING)organization, ' ', FALSE)) AND
											NOT('MASSACHUSSETTS' IN std.str.SplitWords((STRING)organization, ' ', FALSE) AND 'INSTITUTE' IN std.str.SplitWords((STRING)organization, ' ', FALSE)) AND
											NOT('CALIFORNIA' IN std.str.SplitWords((STRING)organization, ' ', FALSE) AND 'INSTITUTE' IN std.str.SplitWords((STRING)organization, ' ', FALSE) AND 'TECHNOL' IN std.str.SplitWords((STRING)organization, ' ', FALSE)) AND
											NOT('CALIFORNIA' IN std.str.SplitWords((STRING)organization, ' ', FALSE) AND 'INSTITUTE' IN std.str.SplitWords((STRING)organization, ' ', FALSE) AND 'TECHNOLOGY' IN std.str.SplitWords((STRING)organization, ' ', FALSE)) AND
											NOT('JOHNS' IN std.str.SplitWords((STRING)organization, ' ', FALSE) AND 'HOPKINS' IN std.str.SplitWords((STRING)organization, ' ', FALSE)) AND
									    NOT('COLLEGE' IN std.str.SplitWords((STRING)organization, ' ', FALSE)));
									 
nonunivall2 := nonunivall(organization[1] NOT IN numbers);
nonunivdepartment := nonunivall2(NOT ('DEPARTMENT' IN std.str.SplitWords((STRING)organization, ' ', FALSE)) AND
																 NOT ('DEPT' IN std.str.SplitWords((STRING)organization, ' ', FALSE)) AND
																 NOT ('DEPARTMENTS' IN std.str.SplitWords((STRING)organization, ' ', FALSE)) AND
																 //NOT ('Dept' IN std.str.SplitWords((STRING)organization, ' ', FALSE)) AND
																 NOT ('DOW' IN std.str.SplitWords((STRING)organization, ' ', FALSE)) AND
																 NOT ('USA' IN std.str.SplitWords((STRING)organization, ' ', FALSE)) AND
																 NOT ('US' IN std.str.SplitWords((STRING)organization, ' ', FALSE)) AND
																 NOT ('USDA' IN std.str.SplitWords((STRING)organization, ' ', FALSE)) AND
																 NOT ('USAF' IN std.str.SplitWords((STRING)organization, ' ', FALSE)) AND
																 NOT ('MAIL' IN std.str.SplitWords((STRING)organization, ' ', FALSE)) AND
																 NOT ('PROGRAM' IN std.str.SplitWords((STRING)organization, ' ', FALSE)) AND
																 NOT ('COMPANY' IN std.str.SplitWords((STRING)organization, ' ', FALSE)) AND
																 NOT ('HOSPITAL' IN std.str.SplitWords((STRING)organization, ' ', FALSE)) AND
																 NOT ('GENERAL' IN std.str.SplitWords((STRING)organization, ' ', FALSE)) AND
																 NOT ('GE' IN std.str.SplitWords((STRING)organization, ' ', FALSE)) AND
																 NOT ('DIRECTORATE' IN std.str.SplitWords((STRING)organization, ' ', FALSE)) AND
																 NOT ('PRODUCT' IN std.str.SplitWords((STRING)organization, ' ', FALSE)) AND
																 NOT ('PRODUCTS' IN std.str.SplitWords((STRING)organization, ' ', FALSE)) AND
																 NOT ('COMMISSION' IN std.str.SplitWords((STRING)organization, ' ', FALSE)) AND
																 NOT ('ADMINISTRATION' IN std.str.SplitWords((STRING)organization, ' ', FALSE)) AND
																 NOT ('ADMINISTRATIONS' IN std.str.SplitWords((STRING)organization, ' ', FALSE)) AND
																 NOT ('FACULTY' IN std.str.SplitWords((STRING)organization, ' ', FALSE)) AND
																 NOT ('FEDERAL' IN std.str.SplitWords((STRING)organization, ' ', FALSE)) AND
																 NOT ('FACILITY' IN std.str.SplitWords((STRING)organization, ' ', FALSE)) AND
																 NOT ('GROUP' IN std.str.SplitWords((STRING)organization, ' ', FALSE)) AND
																 NOT ('BRANCH' IN std.str.SplitWords((STRING)organization, ' ', FALSE)) AND
																 NOT ('FACILITIES' IN std.str.SplitWords((STRING)organization, ' ', FALSE)) AND
																 NOT ('DISCOVERIES' IN std.str.SplitWords((STRING)organization, ' ', FALSE)) AND
																 NOT ('DIV' IN std.str.SplitWords((STRING)organization, ' ', FALSE)) AND
																 //NOT ('Div' IN std.str.SplitWords((STRING)organization, ' ', FALSE)) AND
																 NOT ('DIVISIONS' IN std.str.SplitWords((STRING)organization, ' ', FALSE)) AND
																 NOT ('DIVISION' IN std.str.SplitWords((STRING)organization, ' ', FALSE)) AND
																 NOT ('DISCOVERY' IN std.str.SplitWords((STRING)organization, ' ', FALSE)) AND
																 NOT ('FOUNDATION' IN std.str.SplitWords((STRING)organization, ' ', FALSE)) AND
																 NOT ('HALL' IN std.str.SplitWords((STRING)organization, ' ', FALSE)) AND
																 NOT ('INC' IN std.str.SplitWords((STRING)organization, ' ', FALSE)) AND
																 //NOT ('Inc' IN std.str.SplitWords((STRING)organization, ' ', FALSE)) AND
																 NOT ('ACADEMIC' IN std.str.SplitWords((STRING)organization, ' ', FALSE)) AND
																 NOT ('ACADEMY' IN std.str.SplitWords((STRING)organization, ' ', FALSE)) AND
																 NOT ('ARMY' IN std.str.SplitWords((STRING)organization, ' ', FALSE)) AND
																 NOT ('NAVAL' IN std.str.SplitWords((STRING)organization, ' ', FALSE)) AND
																 NOT ('AIR' IN std.str.SplitWords((STRING)organization, ' ', FALSE) AND 'FORCE' IN std.str.SplitWords((STRING)organization, ' ', FALSE)) AND
																 NOT ('CENTER' IN std.str.SplitWords((STRING)organization, ' ', FALSE) AND 'FOR' IN std.str.SplitWords((STRING)organization, ' ', FALSE)) AND
																 NOT ('INSTITUTE' IN std.str.SplitWords((STRING)organization, ' ', FALSE) AND 'FOR' IN std.str.SplitWords((STRING)organization, ' ', FALSE)) AND
																 NOT ('LABAORATORY' IN std.str.SplitWords((STRING)organization, ' ', FALSE) AND 'FOR' IN std.str.SplitWords((STRING)organization, ' ', FALSE)) AND
																 NOT ('NATIONAL' IN std.str.SplitWords((STRING)organization, ' ', FALSE) AND 'LABORATORY' IN std.str.SplitWords((STRING)organization, ' ', FALSE)) AND
																 NOT ('NATIONAL' IN std.str.SplitWords((STRING)organization, ' ', FALSE) AND 'LABORATORIES' IN std.str.SplitWords((STRING)organization, ' ', FALSE)) AND
																 NOT ('NATIONAL' IN std.str.SplitWords((STRING)organization, ' ', FALSE) AND 'LAB' IN std.str.SplitWords((STRING)organization, ' ', FALSE)) AND
																 NOT ('NATIONAL' IN std.str.SplitWords((STRING)organization, ' ', FALSE) AND 'LABS' IN std.str.SplitWords((STRING)organization, ' ', FALSE)) AND
																 NOT ('HIGH' IN std.str.SplitWords((STRING)organization, ' ', FALSE) AND 'SCHOOL' IN std.str.SplitWords((STRING)organization, ' ', FALSE)) AND
																 NOT ('SOCIETY' IN std.str.SplitWords((STRING)organization, ' ', FALSE)) AND
																 NOT ('CHILDREN' IN std.str.SplitWords((STRING)organization, ' ', FALSE)) AND
																 NOT ('AMES' IN std.str.SplitWords((STRING)organization, ' ', FALSE)) AND
																 NOT ('COUNCIL' IN std.str.SplitWords((STRING)organization, ' ', FALSE)) AND
																 NOT ('ASSOCIATION' IN std.str.SplitWords((STRING)organization, ' ', FALSE)) AND
																 NOT ('CORPORATION' IN std.str.SplitWords((STRING)organization, ' ', FALSE)) AND
																 NOT ('INCORPORATED' IN std.str.SplitWords((STRING)organization, ' ', FALSE)) AND
																 NOT ('LLC' IN std.str.SplitWords((STRING)organization, ' ', FALSE)) AND
																 //NOT ('L.L.C.' IN std.str.SplitWords((STRING)organization, ' ', FALSE)) AND
																 //NOT ('LLP' IN std.str.SplitWords((STRING)organization, ' ', FALSE)) AND
																 //NOT ('plc.' IN std.str.SplitWords((STRING)organization, ' ', FALSE)) AND
																 //NOT ('plc.' IN std.str.SplitWords((STRING)organization, ' ', FALSE)) AND
																 NOT ('PLC' IN std.str.SplitWords((STRING)organization, ' ', FALSE)) AND
																 NOT ('CORP' IN std.str.SplitWords((STRING)organization, ' ', FALSE)) AND
																 //NOT ('Corp.' IN std.str.SplitWords((STRING)organization, ' ', FALSE)) AND
																 NOT ('BLDG' IN std.str.SplitWords((STRING)organization, ' ', FALSE)) AND
																 //NOT ('Bldg.' IN std.str.SplitWords((STRING)organization, ' ', FALSE)) AND
																 NOT ('BUILDING' IN std.str.SplitWords((STRING)organization, ' ', FALSE)) AND
																 //NOT ('Division' IN std.str.SplitWords((STRING)organization, ' ', FALSE)) AND
																 NOT ('TECHNOLOGIES' IN std.str.SplitWords((STRING)organization, ' ', FALSE)));
nonunivSrt := SORT(nonunivdepartment,organization);
nonuniv := DEDUP(nonunivSrt, organization);

//OUTPUT(nonuniv,NAMED('NonUniversities'));
OUTPUT(nonuniv,,'~mpayne3::scopusdata::nonunivlist',OVERWRITE);
OUTPUT(COUNT(nonuniv),NAMED('NonUniversityCount'));

lAuthorGroupAuthor:=RECORD
    UNICODE group_author_seq;
    UNICODE group_author_auid;
    UNICODE group_author_type;
    UNICODE group_author_initials;
    UNICODE group_author_indexed_name;
    UNICODE group_author_surname;
    UNICODE group_author_given_name;
    UNICODE group_author_pref_initials;
    UNICODE group_author_pref_indexed_name;
    UNICODE group_author_pref_surname;
    UNICODE group_author_pref_given_name;
    UNICODE group_author_e_address_type;
    UNICODE group_author_e_address;
  END;
	
	lAffOrg:=RECORD
    UNICODE organization;
  END;
	
listedLocation := usaAuthors(author_group_city != '');

expandedLocation := RECORD

	DATASET(lAuthorGroupAuthor) author_group_author;
  UNICODE author_group_afid;
  UNICODE author_group_dptid;
  UNICODE author_group_country;
  DATASET(lAffOrg) author_group_org;
  UNICODE author_group_city;
	UNICODE author_group_state;
	UNICODE author_group_zip;
	//SET cityPrep := Std.Str.SplitWords()
	//SELF.author_group_city;

END;
//DATASET(lAffOrg) author_group_org;
expandedLocation splitLocation(Pubs.AuthorGroup Le) := TRANSFORM

	SET OF STRING cityPrep := Std.Str.SplitWords((STRING)Le.author_group_city, ',');
	SELF.author_group_city := (UNICODE)cityPrep[1];
	stateZip := TRIM(cityPrep[2]);
	SET OF STRING zipPrep := Std.Str.SplitWords(stateZip, ' ');
	SELF.author_group_state := (UNICODE)zipPrep[1];
	SELF.author_group_zip := (UNICODE)zipPrep[2][..5];
	SELF.author_group_author := Le.author_group_author;
  SELF.author_group_afid := Le.author_group_afid;
  SELF.author_group_dptid := Le.author_group_dptid;
  SELF.author_group_country := Le.author_group_country;
  SELF.author_group_org := Le.author_group_org;
  //SELF.author_group_city := Le.author_group_city;
	//SELF.author_group_state := Le.author_group_state;
	//SELF.author_group_zip := Le.author_group_zip;
	SELF := Le;
	//SELF :=[];
	
END;

//OUTPUT(usaAuthors(author_group_city != ''),NAMED('CityInfo'));
//OUTPUT(COUNT(usaAuthors(author_group_city != '')),NAMED('CityInfoCount'));

expandedCity := PROJECT(usaAuthors, splitLocation(LEFT));

// univs := usaAuthors($.OrgUniv > 0);
// nonuniv := usaAuthors($.OrgUniv = 0);


// OUTPUT(univs,NAMED('Universities'));
// OUTPUT(COUNT(univs),NAMED('UniversityCount'));

// OUTPUT(nonuniv,NAMED('NonUniversities'));
// OUTPUT(COUNT(nonuniv),NAMED('NonUniversityCount'));

// OUTPUT(expandedCity(author_group_zip = ''),NAMED('NoZipInfo'));
// OUTPUT(COUNT(expandedCity(author_group_zip = '')),NAMED('NoZipInfoCount'));

//OUTPUT(expandedCity,NAMED('CityInfoExpanded'));
//OUTPUT(COUNT(expandedCity),NAMED('CityInfoExpandedCount'));

//EXPORT Organizations := 'todo';