﻿IMPORT IPEDS;
IMPORT IPEDS.CarnegieClassification_2012 as Class;

StudySchools := Class.Doctoral + Class.High + Class.VeryHigh;

SrtStudySchools := SORT(StudySchools, -ccbasic, unitid);

slimSchoolRec := RECORD

	SrtStudySchools.unitid;
	SrtStudySchools.instnm;
	SrtStudySchools.addr;
	SrtStudySchools.city;
	SrtStudySchools.stabbr;
	SrtStudySchools.zip;
	STRING class;

END;

slimSchoolRec shrink(IPEDS.File_Institutional_Characteristics_2012.Layout L) := TRANSFORM

	SELF.zip := L.zip[..5];
	SELF.class := L.ccbasic;
	SELF := L;

END;

EXPORT Institutions := PROJECT(SrtStudySchools, shrink(LEFT));