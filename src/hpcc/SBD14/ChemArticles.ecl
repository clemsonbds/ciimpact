﻿IMPORT Publications.ExposedPubs as Pubs;
IMPORT Publications.ArticleClass as ArticleClass;
IMPORT SBD14;
//IMPORT $.ArticleYear;

year := (UNICODE)2006;

MyArticles := Pubs.Articles(pub_year = year);
MyChemArticles := MyArticles(ArticleClass.Chemistry > 0);

//MyChemDHV := MyChemArticles(SBD14.DG_HR_VHR > 0);
//OUTPUT(COUNT(MyChemDHV),NAMED('ChemArticlesCount'));
//OUTPUT(MyChemDHV,NAMED('ChemArticles'));

EXPORT ChemArticles := MyChemArticles(SBD14.DG_HR_VHR > 0):PERSIST('~mpayne3::temp::chemarticles');