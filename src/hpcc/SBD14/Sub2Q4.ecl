﻿IMPORT $;
IMPORT STD;
IMPORT Publications.ExposedPubs as Pubs;
IMPORT Top500.File_Top500 as Top500;
IMPORT NRC;
IMPORT NSF.File_NSF as NSF;
IMPORT Publications.ArticleClass as ArticleClass;
//#OPTION('outputlimit',2000);
//IMPORT $.ArticleClass as ArticleClass;
//EXPORT VHR_Chem(INTEGER cc) := FUNCTION



year := (UNICODE)2006;
//year;
MyArticles := Pubs.Articles(pub_year = year);
MyChemArticles := MyArticles(ArticleClass.Chemistry > 0);

slimpubrec := RECORD

	{Pubs.AuthorGroup} author_groups;
	//MyChemArticle.author_groups.author_group_author;

END;



// slimpubrec makeSlim(MyChemArticle Le) := TRANSFORM
	
	// SELF.author_groups := NORMALIZE(Le,LEFT.author_groups, getAuthorGrps(RIGHT));

// END;

// MyChemArticles :=  PROJECT(MyChemArticle,makeslim(LEFT));



// $.Sub1Q4 rollthem($.Sub1Q4 Le, $.Sub1Q4 Ri, INTEGER C) := TRANSFORM
	
	// SELF.cnt := IF(Le.unitid = Ri.unitid,Le.cnt + COUNT(MyChemArticles($.mycnt(Ri.organization)>0)), COUNT(MyChemArticles($.mycnt(Ri.organization)>0))) ;
	// SELF := Ri;

// END;
// pubcounts :=ITERATE($.Sub1Q4,rollThem(LEFT,RIGHT,COUNTER));

$.Sub1Q4 rollthem($.Sub1Q4 Le) := TRANSFORM
	
	SELF.cnt := COUNT(MyChemArticles($.mycnt(Le.organization)>0));
	SELF := Le;

END;
pubcounts := Project($.Sub1Q4,rollThem(LEFT));

pubcounts myRoll(pubcounts Le, pubcounts Ri) := TRANSFORM
	SELF.cnt := Le.cnt+Ri.cnt;
	SELF := Ri;
END;
EXPORT Sub2Q4 := ROLLUP(pubcounts,LEFT.unitid=RIGHT.unitid,myroll(LEFT,RIGHT));
//EXPORT Sub2Q4 := ITERATE($.Sub1Q4,rollThem(LEFT,RIGHT,COUNTER))(cnt > 50);
//OUTPUT(ITERATE($.Sub1Q4,rollThem(LEFT,RIGHT,COUNTER))(cnt > 50),,'mpayne3::chemover50');