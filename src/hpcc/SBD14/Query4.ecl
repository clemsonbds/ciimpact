﻿IMPORT Publications.ExposedPubs as Pubs;
IMPORT Publications.ArticleClass as ArticleClass;
IMPORT SBD14;
IMPORT Top500.File_Top500 as Top500;
IMPORT NRC;
//IMPORT $.ArticleYear;

topvalid:=Top500.File(year <= 2006);
stop := SORT(topvalid,institution);
top := DEDUP(stop,institution);

uidRec := RECORD

	NRC.NRC_IPEDS.unitid;

END;

uidRec getID(top Le, NRC.NRC_IPEDS Ri) := TRANSFORM

	SELF := Ri;

END;
insts := JOIN(top, NRC.NRC_IPEDS(ccbasic2005 = '15'), LEFT.institution = RIGHT.institutionname);
year := (UNICODE)2006;
year;
MyArticles := Pubs.Articles(pub_year = year);
MyChemArticles := MyArticles(ArticleClass.Chemistry > 0);
docs := MyChemArticles(SBD14.VHR_Chem(15) > 0);
COUNT(docs)

//EXPORT Query4 := 'todo';