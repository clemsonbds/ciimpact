﻿IMPORT $;
IMPORT STD;
IMPORT Publications.ExposedPubs as Pubs;
//IMPORT $.ArticleClass as ArticleClass;

OrgInstDCT := DICTIONARY($.Org_Inst_Lookup,{organization => $.Org_Inst_Lookup});
//CodeColorDCT := DICTIONARY(ColorCodes,{Code => Color});

Sanitize(UNICODE org) := FUNCTION

	//str := (STRING)R.organization;
	upper := std.Uni.ToUpperCase(org);
	noperiod := std.Uni.FindReplace(upper,'.','');
	nodash := std.Uni.FindReplace(noperiod,'-',' ');
	nocomma := std.Uni.FindReplace(nodash,',','');
	nolp := std.Uni.FindReplace(nocomma,'(','');
	norp := std.Uni.FindReplace(nolp,')','');
	//nopossess := std.Uni.FindReplace(norp,'\'s','');
	noapos := std.Uni.FindReplace(norp,'\'','');
	noslash := std.Uni.FindReplace(noapos,'/',' ');
	//unistr := (UNICODE)nopossess;
	//SELF.organization := std.Uni.CleanAccents(noslash);
	//SELF := R;//std.Uni.CleanAccents(noslash);
	RETURN noslash;

END;

//EXPORT Discipline := TABLE(Pubs.Articles(ArticleClass.Chemistry > 0 OR ArticleClass.ComputerScience > 0 OR ArticleClass.Economics > 0), r, pub_year);

EXPORT DG_HR_VHR := COUNT(Pubs.AuthorGroupOrg((STRING)Sanitize(organization) IN OrgInstDCT));