﻿IMPORT Publications.ExposedPubs as Pubs;
IMPORT Publications.ArticleClass as ArticleClass;
IMPORT SBD14;
IMPORT IPEDS;
IMPORT NRC;

year := (UNICODE)2008;

MyArticles := Pubs.Articles(pub_year = year);
MyCSArticles := MyArticles(ArticleClass.ComputerScience > 0);

CSArticles := MyCSArticles(SBD14.DG_HR_VHR_CS(16,1) > 0);
SET regions := [COUNT(MyCSArticles(SBD14.DG_HR_VHR_CS(16,1) > 0)),
								COUNT(MyCSArticles(SBD14.DG_HR_VHR_CS(16,2) > 0)),
								COUNT(MyCSArticles(SBD14.DG_HR_VHR_CS(16,3) > 0)),
								COUNT(MyCSArticles(SBD14.DG_HR_VHR_CS(16,4) > 0)),
								COUNT(MyCSArticles(SBD14.DG_HR_VHR_CS(16,5) > 0))];
//MAX(regions);
MAX(regions);

// idcode := RECORD

	// STRING unitid;
	// UNSIGNED regionalCode;

// END;

// idcode getCode(NRC.NRC_IPEDS Le, NRC.File_NRC.File Ri) := TRANSFORM
	// SELF.unitid := Le.unitid;
	// SELF.nrcName := Le.nrcname;
	// SELF.ipedsName := Le.ipedsname;
	// SELF.regionalcode := Ri.regionalcode;
	
// END;

// hrRegions := JOIN(NRC.NRC_IPEDS(ccbasic2005='16'), NRC.File_NRC.File, LEFT.institutionname = RIGHT.institutionname, getCode(LEFT,RIGHT), LEFT OUTER);
// singleRegions := DEDUP(hrRegions, unitid);
// regions := RECORD

	// CSArticles;
	// UNSIGNED region;

// END;

// regions addRegion(CSArticles Le, SingleRegions Ri ) := TRANSFORM

	// SELF.region := Ri.regionalcode;
	// SELF := Le;

// END;

//JOIN(CSArticles, SingleRegions , LEFT.unitid = RIGHT.unitid, getCode(LEFT,RIGHT), LEFT OUTER);

//EXPORT Query3 := 'todo';