﻿EXPORT SET_Sample_Journals := ['Accounts of Chemical Research','Advanced Synthesis and Catalysis','Advances in Catalysis','Aldrichimica Acta',
                               'Analytical Chemistry','Annual Review of Physical Chemistry','Biosensors and Bioelectronics','Chemical Reviews',
															 'Chemical Society Reviews','Coordination Chemistry Reviews','Current Medicinal Chemistry','Journal of Combinatorial Chemistry',
															 'Medicinal Research Reviews','Natural Product Reports','Nature Materials','TrAC - Trends in Analytical Chemistry'];