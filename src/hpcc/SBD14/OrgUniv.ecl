﻿IMPORT Publications.ExposedPubs as Pubs;
IMPORT Publications.ArticleClass as ArticleClass;
IMPORT STD;

EXPORT OrgUniv := FUNCTION

	compRec := RECORD

		STRING listedOrg;
		STRING containsComp;
		BOOLEAN result;
	
	END;
	comp := DATASET([{(STRING)Pubs.AuthorGroupOrg.organization, 'university',std.str.contains((STRING)Pubs.AuthorGroupOrg.organization, 'university', true)}],compRec);
	k := OUTPUT(comp, NAMED('Compare'),EXTEND);
	RETURN COUNT(Pubs.AuthorGroupOrg(std.str.contains((STRING)organization, 'university', true)));

END;


//EXPORT OrgUniv := 'todo';