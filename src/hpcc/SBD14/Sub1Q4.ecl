﻿IMPORT $;
IMPORT STD;
IMPORT Publications.ExposedPubs as Pubs;
IMPORT Top500.File_Top500 as Top500;
IMPORT NRC;
IMPORT NSF.File_NSF as NSF;
IMPORT Publications.ArticleClass as ArticleClass;
//IMPORT $.ArticleClass as ArticleClass;
//EXPORT VHR_Chem(INTEGER cc) := FUNCTION
OrgInstDCT := DICTIONARY($.VHR_Chem,{organization => $.VHR_Chem});
//CodeColorDCT := DICTIONARY(ColorCodes,{Code => Color});

Sanitize(UNICODE org) := FUNCTION

	//str := (STRING)R.organization;
	upper := std.Uni.ToUpperCase(org);
	noperiod := std.Uni.FindReplace(upper,'.','');
	nodash := std.Uni.FindReplace(noperiod,'-',' ');
	nocomma := std.Uni.FindReplace(nodash,',','');
	nolp := std.Uni.FindReplace(nocomma,'(','');
	norp := std.Uni.FindReplace(nolp,')','');
	//nopossess := std.Uni.FindReplace(norp,'\'s','');
	noapos := std.Uni.FindReplace(norp,'\'','');
	noslash := std.Uni.FindReplace(noapos,'/',' ');
	//unistr := (UNICODE)nopossess;
	//SELF.organization := std.Uni.CleanAccents(noslash);
	//SELF := R;//std.Uni.CleanAccents(noslash);
	RETURN noslash;

END;


artRec := RECORD

	$.VHR_Chem.organization;
	$.VHR_Chem.unitid;
	INTEGER cnt;

END;

artRec addCol($.VHR_Chem Le) := TRANSFORM

	SELF.cnt := 0;
	SELF := Le;

END;

EXPORT Sub1Q4 := PROJECT($.VHR_Chem, addCol(LEFT));