﻿IMPORT $;
IMPORT NSF.File_NSF as NSF;
IMPORT NRC;
//IMPORT NRC.File_NRC as NRC;
IMPORT IPEDS;
IMPORT STD;

awardsprep := NSF.Awards(awardeffectivedate[7..] = '2005' OR
														awardeffectivedate[7..] = '2006' OR
														awardeffectivedate[7..] = '2007' OR
														awardeffectivedate[7..] = '2008');
														
awards := awardsprep(division = 'Division of Social and Economic Sciences');
//awards;
//COUNT(awards);
//EXPORT Query1 := 'todo';

NrcNsfRec := RECORD
	//STRING assisttrainpropwrit;
	STRING NrcName;
	STRING NsfName;

END;

NrcNsfRec IniNrcNsf(NRC.NRC_IPEDS Le, NSF.Institutions Ri) := TRANSFORM

	SELF.NrcName := Le.institutionname;
	SELF.NsfName := std.Str.toUpperCase((STRING)Ri.name);
	
END;

nnmatches := JOIN(NRC.NRC_IPEDS(ccbasic2005 = '16'),Awards.Institutions, LEFT.institutionname = std.Str.toUpperCase((STRING)RIGHT.name), IniNrcNsf(LEFT,RIGHT));
srtnnmatches := SORT(nnmatches,nrcname,nsfname);
distnnmat := DISTRIBUTE(srtnnmatches,HASH32(nrcname,nsfname));
ddnnmatches := DEDUP(distnnmat,nrcname,nsfname, LOCAL);
//ddnnmatches := DEDUP(srtnnmatches,nrcname,nsfname);
//ddnnmatches;

assistRec := RECORD

		STRING name;
		UNSIGNED assist;
	
END;


assistRec appendNum(NrcNsfRec Le, NRC.File_NRC.File Ri) := TRANSFORM

	SELF.name := Le.nrcname;
	SELF.assist := Ri.assisttrainpropwrit;

END; 	
nrcAssistNames :=	JOIN(ddnnmatches, NRC.File_NRC.File((assisttrainpropwrit=2 OR assisttrainpropwrit=3) AND field = 'Economics'), LEFT.NrcName = RIGHT.institutionname, appendNum(LEFT,RIGHT));
//nrcAssistNames;
getGrants(UNSIGNED assistcode) := FUNCTION
	dict := nrcAssistNames(assist = assistcode);
	myDCT := DICTIONARY(dict,{name => assistcode});
	RETURN COUNT(NSF.Institutions(std.Str.toUpperCase((STRING)name) IN myDCT));
END;
a := AVE(awards(getgrants(2)>0),(unsigned)awardamount);
b :=  AVE(awards(getgrants(3)>0),(unsigned)awardamount);
//a;
//b;
difference := a - b;
difference;
//MAX(awards(getgrant(2)),awards(getgrant(4)))