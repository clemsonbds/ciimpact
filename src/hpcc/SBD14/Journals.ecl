﻿IMPORT Publications.ExposedPubs as Pubs;
IMPORT Publications.ArticleClass as ArticleClass;
IMPORT SBD14;
//IMPORT $.ArticleYear;

year := (UNICODE)2006;

//MyArticles := Pubs.Articles(sort_year = year);
MyChemArticles := SBD14.ChemArticles;//MyArticles(ArticleClass.Chemistry > 0);
MyChemDHV := MyChemArticles(source_title IN SBD14.SET_Sample_Journals);
//MyChemDHV := MyChemArticles(SBD14.DG_HR_VHR > 0 AND source_title IN SBD14.SET_Sample_Journals);
OUTPUT(COUNT(MyChemDHV),NAMED('ChemArticlesCount'));
OUTPUT(MyChemDHV,NAMED('ChemArticles'));

journalRec := RECORD

	Pubs.Articles.source_title;

END;

// journalsmany := PROJECT(MyChemDHV, TRANSFORM(journalRec, SELF:=LEFT));
// srtjournals := SORT(journalsmany,source_title);
// journals := DEDUP(srtjournals,source_title);

//OUTPUT(journals,NAMED('Journals'));
// OUTPUT(journals,,'~mpayne3::scopusdata::chemjournals2006',OVERWRITE);
// OUTPUT(journals,NAMED('JournalsCount'));

//EXPORT Journals := 'todo';