﻿IMPORT $;
IMPORT STD;
IMPORT Publications.ExposedPubs as Pubs;
IMPORT Top500.File_Top500 as Top500;
IMPORT NRC;
IMPORT NSF.File_NSF as NSF;
IMPORT Publications.ArticleClass as ArticleClass;
//IMPORT $.ArticleClass as ArticleClass;
//EXPORT VHR_Chem(INTEGER cc) := FUNCTION
//#OPTION('outputlimit',2000);
cc := 15;
topvalid:=Top500.File(year <= 2006);
stop := SORT(topvalid,institution);
top := DEDUP(stop,institution);
//top;
uidRec := RECORD

	NRC.NRC_IPEDS.unitid;

END;

uidRec getID(top Le, NRC.NRC_IPEDS Ri) := TRANSFORM

	SELF := Ri;

END;
insts := JOIN(top, NRC.NRC_IPEDS(ccbasic2005 = '15'), LEFT.institution = RIGHT.institutionname);
topdict := DICTIONARY(insts, {unitid=>insts});
//topdict;
mypersist := '~temp::cleanorgsclasschem' + (STRING)cc;
prepdict := $.Org_Inst_Lookup_Chem(class = (STRING)cc):PERSIST(mypersist);
//DEDUP(prepdict,unitid);
 EXPORT VHR_Chem := JOIN(prepdict, insts, LEFT.unitid = RIGHT.unitid, TRANSFORM(LEFT), LOOKUP);
//EXPORT VHR_Chem := prepdict(unitid IN topdict);//dict := prepdict(unitid IN topdict);