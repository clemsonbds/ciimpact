﻿IMPORT Publications.ExposedPubs as Pubs;
IMPORT STD;
EXPORT MyCnt(STRING org) := FUNCTION
	Sanitize(UNICODE org) := FUNCTION

	//str := (STRING)R.organization;
	upper := std.Uni.ToUpperCase(org);
	noperiod := std.Uni.FindReplace(upper,'.','');
	nodash := std.Uni.FindReplace(noperiod,'-',' ');
	nocomma := std.Uni.FindReplace(nodash,',','');
	nolp := std.Uni.FindReplace(nocomma,'(','');
	norp := std.Uni.FindReplace(nolp,')','');
	//nopossess := std.Uni.FindReplace(norp,'\'s','');
	noapos := std.Uni.FindReplace(norp,'\'','');
	noslash := std.Uni.FindReplace(noapos,'/',' ');
	//unistr := (UNICODE)nopossess;
	//SELF.organization := std.Uni.CleanAccents(noslash);
	//SELF := R;//std.Uni.CleanAccents(noslash);
	RETURN noslash;

END;
	RETURN COUNT(Pubs.AuthorGroupOrg((STRING)Sanitize(organization) = org));

END;