﻿IMPORT Publications.ExposedPubs as Pubs;
IMPORT Publications.ArticleClass as ArticleClass;
IMPORT SBD14;
IMPORT IPEDS;
IMPORT NRC;

//IMPORT $.ArticleYear;
High := NRC.NRC_IPEDS(ccbasic2005 = '16');

dualNames := RECORD
	STRING unitid;
	STRING nrcName;
	STRING ipedsName;
	//UNSIGNED regionalCode;

END;

codeNames := RECORD
	STRING unitid;
	STRING nrcName;
	STRING ipedsName;
	UNSIGNED regionalCode;

END;


dualNames getNames(NRC.NRC_IPEDS Le, IPEDS.File_Institutional_Characteristics_2012.File Ri) := TRANSFORM
	SELF.unitid := Le.unitid;
	SELF.nrcName := Le.institutionName;
	SELF.ipedsName := Ri.INSTNM;
	
END;

codeNames getCode(dualnames Le, NRC.File_NRC.File Ri) := TRANSFORM
	SELF.unitid := Le.unitid;
	SELF.nrcName := Le.nrcname;
	SELF.ipedsName := Le.ipedsname;
	SELF.regionalcode := Ri.regionalcode;
	
END;

STRING getNRCid(STRING name) := FUNCTION
	
	foundinst := NRC.NRC_IPEDS(institutionname = name);
	return foundinst[1].unitid;

END;

//getNRCid(NRC.File_NRC.File[1].institutionName);
searchNames := JOIN(NRC.NRC_IPEDS(ccbasic2005 = '16'),IPEDS.File_Institutional_Characteristics_2012.File, 
										LEFT.unitid = RIGHT.unitid, getNames(LEFT,RIGHT));
										
searchcode := JOIN(searchNames,NRC.File_NRC.File, 
										LEFT.nrcName = RIGHT.institutionName, getCode(LEFT,RIGHT));
										
searchcodeSort := SORT(searchcode, unitid);
searchcodeDD := DEDUP(searchcodeSort,unitid);
searchcodeDD;





year := (UNICODE)2008;

//MyArticles := Pubs.Articles(pub_year = year);
//MyCompArticles := MyArticles(ArticleClass.ComputerScience > 0);
//MyCompArticles;
//COUNT(MyCompArticles);





//MyChemDHV := MyChemArticles(SBD14.DG_HR_VHR > 0);
//OUTPUT(COUNT(MyChemDHV),NAMED('ChemArticlesCount'));
//OUTPUT(MyChemDHV,NAMED('ChemArticles'));

//EXPORT CompSciArticles := MyCompArticles(SBD14.DG_HR_VHR > 0);//:PERSIST('~mpayne3::temp::chemarticles');

//EXPORT Query3Prep := 'todo';