﻿IMPORT $.File_Institutional_Characteristics_2012 as X;
IMPORT * FROM STD;

CleanForTokens(STRING s):=FUNCTION
		sRestrictChars:=' '+REGEXREPLACE('[^- A-Z0-9\']',Str.ToUpperCase(s),' ')+' ';
		sStripPunctEnds:=REGEXREPLACE('( -)|(- )|( \')|(\' )',sRestrictChars,' ');
		sRemoveNumberOnly:=REGEXREPLACE(' [-0-9\']+(?=[ ])',sStripPunctEnds,'');
		//sRemoveSingleChars:=REGEXREPLACE(' [B-H,J-Z](?=[ ])',sRemoveNumberOnly,'');
		//sCompressSpaces:=REGEXREPLACE('[ ]+',sRemoveSingleChars,' ');
		sCompressSpaces:=REGEXREPLACE('[ ]+',sRemoveNumberOnly,' ');
		sNormalizePosessives:=REGEXREPLACE('\'S ',sCompressSpaces,' ');
		sSplitContraction01:=REGEXREPLACE('\'RE ',sNormalizePosessives,' ARE ');
		sSplitContraction02:=REGEXREPLACE('\'LL ',sSplitContraction01,' WILL ');
		sSplitContraction03:=REGEXREPLACE(' I\'M ',sSplitContraction02,' I AM ');
		RETURN sSplitContraction03;
END;


X.Layout Standardize(X.Layout Le) := TRANSFORM

	//SELF.UNITID:= 	std.Str.toUpperCase(Le.UNITID);
	SELF.INSTNM:= 	CleanForTokens(Le.INSTNM);
	SELF.ADDR:= 		CleanForTokens(Le.ADDR);
	SELF.CITY:= 		CleanForTokens(Le.CITY);
	SELF.STABBR:= 	CleanForTokens(Le.STABBR);
	SELF.ZIP:=		 	Le.ZIP[..5];
	// SELF.FIPS:= 		std.Str.toUpperCase(Le.;
	// SELF.OBEREG:= 	std.Str.toUpperCase(Le.;
	// SELF.CHFNM:= 		std.Str.toUpperCase(Le.;
	// SELF.CHFTITLE:= std.Str.toUpperCase(Le.;
	// SELF.GENTELE:= 	std.Str.toUpperCase(Le.;
	// SELF.EIN:= 			std.Str.toUpperCase(Le.;
	// SELF.OPEID:= 		std.Str.toUpperCase(Le.;
	// SELF.OPEFLAG:= 	std.Str.toUpperCase(Le.;
	// SELF.WEBADDR:= 	std.Str.toUpperCase(Le.;
	// SELF.ADMINURL:= std.Str.toUpperCase(Le.;
	// SELF.FAIDURL:= 	std.Str.toUpperCase(Le.;
	// SELF.APPLURL:= 	std.Str.toUpperCase(Le.;
	// SELF.NPRICURL:= std.Str.toUpperCase(Le.;
	// SELF.SECTOR:= 	std.Str.toUpperCase(Le.;
	// SELF.ICLEVEL:= 	std.Str.toUpperCase(Le.;
	// SELF.CONTROL:= 	std.Str.toUpperCase(Le.;
	// SELF.HLOFFER:= 	std.Str.toUpperCase(Le.;
	// SELF.UGOFFER:= 	std.Str.toUpperCase(Le.;
	// SELF.GROFFER:= 	std.Str.toUpperCase(Le.;
	// SELF.HDEGOFR1:= std.Str.toUpperCase(Le.;
	// SELF.DEGGRANT:= std.Str.toUpperCase(Le.;
	// SELF.HBCU:= 		std.Str.toUpperCase(Le.;
	// SELF.HOSPITAL:= std.Str.toUpperCase(Le.;
	// SELF.MEDICAL:= 	std.Str.toUpperCase(Le.;
	// SELF.TRIBAL:= 	std.Str.toUpperCase(Le.;
	// SELF.LOCALE:= 	std.Str.toUpperCase(Le.; //Extra E added to end because LOCALE is a reserve word
	// SELF.OPENPUBL:= std.Str.toUpperCase(Le.;
	// SELF.ACT:= 			std.Str.toUpperCase(Le.;
	// SELF.NEWID:= 		std.Str.toUpperCase(Le.;
	// SELF.DEATHYR:= 	std.Str.toUpperCase(Le.;
	// SELF.CLOSEDAT:= std.Str.toUpperCase(Le.;
	// SELF.CYACTIVE:= std.Str.toUpperCase(Le.;
	// SELF.POSTSEC:= 	std.Str.toUpperCase(Le.;
	// SELF.PSEFLAG:= 	std.Str.toUpperCase(Le.;
	// SELF.PSET4FLG:= std.Str.toUpperCase(Le.;
	// SELF.RPTMTH:= 	std.Str.toUpperCase(Le.;
	// SELF.IALIAS:= 	std.Str.toUpperCase(Le.;
	// SELF.INSTCAT:= 	std.Str.toUpperCase(Le.;
	// SELF.CCBASIC:= 	std.Str.toUpperCase(Le.;
	// SELF.CCIPUG:= 	std.Str.toUpperCase(Le.;
	// SELF.CCIPGRAD:= std.Str.toUpperCase(Le.;
	// SELF.CCUGPROF:= std.Str.toUpperCase(Le.;
	// SELF.CCENRPRF:= std.Str.toUpperCase(Le.;
	// SELF.CCSIZSET:= std.Str.toUpperCase(Le.;
	// SELF.CARNEGIE:= std.Str.toUpperCase(Le.;
	// SELF.LANDGRNT	:=	std.Str.toUpperCase(Le.;
	// SELF.INSTSIZE	:=	std.Str.toUpperCase(Le.;
	// SELF.CBSA:= 		std.Str.toUpperCase(Le.;
	// SELF.CBSATYPE:= std.Str.toUpperCase(Le.;
	// SELF.CSA:= 			std.Str.toUpperCase(Le.;
	// SELF.NECTA:= 		std.Str.toUpperCase(Le.;
	SELF := Le;

END;

Standardized_IPEDS := PROJECT(X.File, Standardize(LEFT));
OUTPUT(X.File, {unitid, instnm,addr,city,stabbr}, NAMED('Original'));
OUTPUT(Standardized_IPEDS, {unitid, instnm,addr,city,stabbr}, NAMED('Standardized'));