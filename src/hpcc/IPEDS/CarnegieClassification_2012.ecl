﻿IMPORT $;

EXPORT CarnegieClassification_2012 := MODULE 

	EXPORT Doctoral := $.File_Institutional_Characteristics_2012.File(ccbasic = '17');
	
	EXPORT High := $.File_Institutional_Characteristics_2012.File(ccbasic = '16');
	
	EXPORT VeryHigh := $.File_Institutional_Characteristics_2012.File(ccbasic = '15');

END;