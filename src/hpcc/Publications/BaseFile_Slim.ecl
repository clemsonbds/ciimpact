﻿IMPORT STD;
 sFilename:='~mpayne3::scopusdata::pubxml';

 
// XML layouts for ingesting the data
   lAuthorsXML:=RECORD
    UNICODE initials                            {XPATH('cto:auth-initials')};
    UNICODE indexed_name                        {XPATH('cto:auth-indexed-name')};
    UNICODE author_id                           {XPATH('cto:auth-id')};
  END;
	
   lAuthorGroupAuthorXML:=RECORD
    UNICODE group_author_seq                    {XPATH('@seq')};
    UNICODE group_author_auid                   {XPATH('@auid')};
    UNICODE group_author_indexed_name           {XPATH('ce:indexed-name')};
  END;
   lAffOrgXML:=RECORD
    UNICODE organization                        {XPATH('')};
  END;
   lAuthorGroupsXML:=RECORD
    DATASET(lAuthorGroupAuthorXML) author_group_author {XPATH('author')};
    UNICODE author_group_country                {XPATH('affiliation/@country')};
    DATASET(lAffOrgXML) author_group_org        {XPATH('affiliation/organization')};
    UNICODE author_group_city                   {XPATH('affiliation/city-group')};
  END;
  
   lDocsXML := RECORD
	  UNICODE eid                                 {XPATH('xocs:meta/xocs:eid')};
	  DATASET(lAuthorGroupsXML) author_groups     {XPATH('xocs:item/item/bibrecord/head/author-group')};
  END;
   dFileXML:=DATASET(sFilename,lDocsXML,XML('xocs:doc',NOROOT));


// Standard ECL layouts for a cleaned version of the XML data.
   lAuthors:=RECORD
    UNICODE initials;
    UNICODE indexed_name;
    UNICODE author_id;
  END;
   lAuthorGroupAuthor:=RECORD
    UNICODE group_author_seq;
    UNICODE group_author_auid;
    UNICODE group_author_indexed_name;
  END;
   lAffOrg:=RECORD
    UNICODE organization;
  END;
   lAuthorGroups:=RECORD
    DATASET(lAuthorGroupAuthor) author_group_author;
    UNICODE author_group_country;
    DATASET(lAffOrg) author_group_org;
    UNICODE author_group_city;
  END;
  
   lDocs := RECORD
    UNICODE eid;
		DATASET(lAuthorGroups) author_groups;
  END;

 //dFile:=PROJECT(dFileXML,TRANSFORM(lDocs,SELF:=LEFT;SELF:=[];));

EXPORT BaseFile_Slim := OUTPUT(PROJECT(dFileXML,TRANSFORM(lDocs,SELF:=LEFT;SELF:=[];)),,'~mpayne3::scopusdata::cleanpubsslim',OVERWRITE);