﻿IMPORT $.ExposedPubs as EP;

EXPORT ArticleClass := MODULE
	
	ChemCodes := ['1600','1601','1602','1603','1604','1605','1606','1607'];
	EXPORT Chemistry := COUNT(EP.Classifications(classification IN ChemCodes));
	
	CompSciCodes := ['1700','1701','1702','1703','1704','1705','1706','1707','1708','1709','1710','1711','1712'];
	EXPORT ComputerScience := COUNT(EP.Classifications(classification IN CompSciCodes));
	
	EconCodes := ['2000','2001','2002','2003'];
	EXPORT Economics := COUNT(EP.Classifications(classification IN EconCodes));

END;