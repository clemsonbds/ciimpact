﻿IMPORT $.ExposedPubs as EP;

EXPORT ArticleYear(UNICODE year) := FUNCTION

	RETURN COUNT(EP.Articles(pub_year = year));

END;
