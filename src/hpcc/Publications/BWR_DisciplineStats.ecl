﻿IMPORT $;
IMPORT $.ExposedPubs as Pubs;
IMPORT $.ArticleClass as ArticleClass;

Articles := Pubs.Articles(pub_year != '');

r := RECORD

	Pubs.Articles.pub_year;
	INTEGER cnt := COUNT(GROUP);

END;


Chemistry := TABLE(Articles(ArticleClass.Chemistry > 0), r, pub_year);
ComputerScience := TABLE(Articles(ArticleClass.ComputerScience > 0), r, pub_year);
Economics := TABLE(Articles(ArticleClass.Economics > 0), r, pub_year);

OUTPUT(Chemistry,NAMED('ChemStats'));
OUTPUT(ComputerScience,NAMED('CompSciStats'));
OUTPUT(Economics,NAMED('EconStats'));