﻿IMPORT STD;
 sFilename:='~mpayne3::scopusdata::pubxml';

 
// XML layouts for ingesting the data
   lRefIDsXML:={UNICODE ref_id {XPATH('')};};
   lAuthorsXML:=RECORD
    UNICODE initials                            {XPATH('cto:auth-initials')};
    UNICODE indexed_name                        {XPATH('cto:auth-indexed-name')};
    UNICODE surname                             {XPATH('cto:auth-surname')};
    UNICODE author_id                           {XPATH('cto:auth-id')};
  END;
   lCopyrightXML:=RECORD
    UNICODE copyright_type                      {XPATH('@type')};
    UNICODE copyright                           {XPATH('')};
  END;
   lItemIDsXML:=RECORD
    UNICODE item_id_type                        {XPATH('@idtype')};
    UNICODE item_id                             {XPATH('')};
  END;
   lCollectionsXML:=RECORD
    UNICODE collection                          {XPATH('')};
  END;
   lCitationTypesXML:=RECORD
    UNICODE citation_type                       {XPATH('@code')};
  END;
   lCitationLangXML:=RECORD
    UNICODE citation_language                   {XPATH('@xml:lang')};
  END;
   lAbstractLangXML:=RECORD
    UNICODE abstract_language                   {XPATH('@xml:lang')};
  END;
   lPatentRegXML:={UNICODE patent_registration {XPATH('')};};
   lPatentAppXML:={UNICODE patent_application {XPATH('')};};
   lPatentPrioAppXML:={UNICODE patent_prioapplication {XPATH('')};};
   lPatentAssigneeXML:={UNICODE patent_assignee {XPATH('')};};
   lPatentCountryXML:={UNICODE patent_country {XPATH('')};};
   lPatentIPCXML:={UNICODE patent_IPC_code {XPATH('')};};
   lPatentInventorXML:={UNICODE patent_inventor {XPATH('')};};
   lKeywordsXML:={UNICODE keyword {XPATH('')};};
   lCitationTitleXML:=RECORD
    UNICODE citation_title_language             {XPATH('@xml:lang')};
    UNICODE citation_title_orig                 {XPATH('@original')};
    UNICODE citation_title                      {XPATH('')};
  END;
   lAuthorGroupAuthorXML:=RECORD
    UNICODE group_author_seq                    {XPATH('@seq')};
    UNICODE group_author_auid                   {XPATH('@auid')};
    UNICODE group_author_type                   {XPATH('@type')};
    UNICODE group_author_initials               {XPATH('ce:initials')};
    UNICODE group_author_indexed_name           {XPATH('ce:indexed-name')};
    UNICODE group_author_surname                {XPATH('ce:surname')};
    UNICODE group_author_given_name             {XPATH('ce:given-name')};
    UNICODE group_author_pref_initials          {XPATH('preferred-name/ce:initials')};
    UNICODE group_author_pref_indexed_name      {XPATH('preferred-name/ce:indexed-name')};
    UNICODE group_author_pref_surname           {XPATH('preferred-name/ce:surname')};
    UNICODE group_author_pref_given_name        {XPATH('preferred-name/ce:given-name')};
    UNICODE group_author_e_address_type         {XPATH('ce:e-address@type')};
    UNICODE group_author_e_address              {XPATH('ce:e-address')};
  END;
   lAffOrgXML:=RECORD
    UNICODE organization                        {XPATH('')};
  END;
   lAuthorGroupsXML:=RECORD
    DATASET(lAuthorGroupAuthorXML) author_group_author {XPATH('author')};
    UNICODE author_group_afid                   {XPATH('affiliation/@afid')};
    UNICODE author_group_dptid                  {XPATH('affiliation/@dptid')};
    UNICODE author_group_country                {XPATH('affiliation/@country')};
    DATASET(lAffOrgXML) author_group_org        {XPATH('affiliation/organization')};
    UNICODE author_group_city                   {XPATH('affiliation/city-group')};
		UNICODE author_group_cetext  								{XPATH('affiliation/ce:text')};
  END;
   lGrantsXML:=RECORD
    UNICODE grant_id                            {XPATH('grant-id')};
    UNICODE grant_acronym                       {XPATH('grant-acronym')};
    UNICODE grant_agency                        {XPATH('grant-agency')};
  END;
   lAbstractXML:=RECORD
    UNICODE abstract_original                   {XPATH('@original')};
    UNICODE abstract_language                   {XPATH('@xml:lang')};
    UNICODE abstract_perspective                {XPATH('@perspective')};
    UNICODE abstract_source                     {XPATH('@source')};
    UNICODE abstract_publisher_copyright        {XPATH('publishercopyright')};
    UNICODE abstract                            {XPATH('<>')};
  END;
   lSourceIssnXML:=RECORD
    UNICODE source_issn_type                    {XPATH('@type')};
    UNICODE source_issn                         {XPATH('')};
  END;
   lSourceIsbnXML:=RECORD
    UNICODE source_isbn_type                    {XPATH('@type')};
    UNICODE source_isbn_lengh                   {XPATH('@length')};
    UNICODE source_isbn_level                   {XPATH('@level')};
    UNICODE source_isbn                         {XPATH('')};
  END;
   lSourceWebsiteXML:=RECORD
    UNICODE source_website_type                 {XPATH('@type')};
    UNICODE source_website                      {XPATH('ce:e-address')};
    UNICODE source_website_name                 {XPATH('websitename')};
  END;
   lSourceEditorsXML:=RECORD
    UNICODE editor_initials                     {XPATH('ce:initials')};
    UNICODE editor_indexed_name                 {XPATH('ce:indexed-name')};
    UNICODE editor_surname                      {XPATH('ce:surname')};
    UNICODE editor_given_name                   {XPATH('ce:given-name')};
    UNICODE editor_role                         {XPATH('@role')};
    UNICODE editor_type                         {XPATH('@type')};
  END;
   lPublisherXML:=RECORD
    UNICODE publisher_name                      {XPATH('publishername')};
    UNICODE publisher_address                   {XPATH('publisheraddress')};
    UNICODE publisher_affiliation               {XPATH('affiliation')};
  END;
   lConfNameXML:={UNICODE conference_event_name {XPATH('')};};
   lConfNumberXML:={UNICODE conference_event_number {XPATH('')};};
   lConfThemeXML:={UNICODE conference_event_theme {XPATH('')};};
   lConfTitleXML:={UNICODE conference_event_title {XPATH('')};};
   lConfOrgXML:={UNICODE conference_organization {XPATH('')};};
   lConfSponsorXML:={UNICODE conference_sponsor {XPATH('')};};
   lConfEdOrgXML:={UNICODE conference_editor_organization {XPATH('')};};
   lEnhDescriptorsXML:=RECORD
    UNICODE descriptor_mainterm_weight          {XPATH('mainterm@weight')};
    UNICODE descriptor_mainterm_candidate       {XPATH('mainterm@candidate')};
    UNICODE descriptor_mainterm_sort_pos        {XPATH('mainterm@sort-pos')};
    UNICODE descriptor_mainterm_code            {XPATH('mainterm@code')};
    UNICODE descriptor                          {XPATH('mainterm')};
  END;
   lEnhDescXML:=RECORD
    UNICODE descriptor_type                     {XPATH('@type')};
    UNICODE descriptor_controlled               {XPATH('@controlled')};
    UNICODE descriptor_name                     {XPATH('@name')};
    DATASET(lEnhDescriptorsXML) descriptor      {XPATH('descriptor')};
  END;
   lEnhClassesXML:=RECORD
    UNICODE classification                      {XPATH('')};
    UNICODE classification_code                 {XPATH('classification-code')};
    UNICODE classification_description          {XPATH('classification-description')};
  END;
   lEnhClassXML:=RECORD
    UNICODE classification_type                 {XPATH('@type')};
    DATASET(lEnhClassesXML) classifications     {XPATH('classification')};
  END;

   lAPITermTypeXML:=RECORD
    UNICODE major_term_indicator                {XPATH('@major-term-indicator')};
    UNICODE enh_API_ATM_role                    {XPATH('@role')};
    UNICODE enh_API_ATM_group_indicator         {XPATH('@group-indicator')};
    UNICODE enh_API_ATM_sort_pos                {XPATH('@sort-pos')};
  END;
   lAPIAutoTermTypeXML:=RECORD
    UNICODE prefix                              {XPATH('@prefix')};
    UNICODE postfix                             {XPATH('@postfix')};
    UNICODE CAS_nr                              {XPATH('@CAS-nr')};
  END;
   lEnhAPICCXML:=RECORD
    UNICODE classification                      {XPATH('')};
  END;
   lEnhAPILTMXML:=RECORD
    DATASET(lAPIAutoTermTypeXML) enh_API_LTM_auto {XPATH('autoposting-term')};
  END;

   lVGroupTermGrpXML:=RECORD
    DATASET(lAPITermTypeXML) vgroup_term_grp    {XPATH('API-term')};
  END;
   lSGroupTermGrpXML:=RECORD
    DATASET(lAPITermTypeXML) sgroup_term_grp    {XPATH('API-term')};
  END;
   lEnhAPIVgroupXML:=RECORD
    DATASET(lAPITermTypeXML) vgroup_term        {XPATH('API-term')};
    DATASET(lVGroupTermGrpXML) vgroup_termgroup {XPATH('API-termgroup')};
  END;
   lEnhAPISgroupXML:=RECORD
    DATASET(lAPITermTypeXML) sgroup_term        {XPATH('API-term')};
    DATASET(lSGroupTermGrpXML) sgroup_termgroup {XPATH('API-termgroup')};
  END;
   lEnhAPIATMXML:=RECORD
    UNICODE enh_API_ATM_template_role           {XPATH('ATM-template@template-role')};
    UNICODE enh_API_ATM_lt_count                {XPATH('LT-count')};
    DATASET(lAPITermTypeXML) enh_API_ATM_term   {XPATH('API-Term')};
    DATASET(lEnhAPIVgroupXML) enh_API_ATM_vgroup{XPATH('ATM-Vgroup')};
    DATASET(lEnhAPISgroupXML) enh_API_ATM_sgroup{XPATH('ATM-Sgroup')};
  END;
   lEnhAPIAMSXML:=RECORD
    DATASET(lAPITermTypeXML) API_AMS_term       {XPATH('API-Term')};
  END;
   lEnhAPIAPCXML:=RECORD
    DATASET(lAPITermTypeXML) API_APC_term       {XPATH('API-Term')};
  END;
   lEnhAPIANCXML:=RECORD
    DATASET(lAPITermTypeXML) API_ANC_term       {XPATH('API-Term')};
  END;
   lEnhAPIATXML:=RECORD
    DATASET(lAPIAutoTermTypeXML) enh_API_AT_auto {XPATH('autoposting-term')};
  END;
   lEnhAPICTXML:=RECORD
    DATASET(lAPIAutoTermTypeXML) enh_API_CT_auto {XPATH('autoposting-term')};
  END;
   lEnhAPILTXML:=RECORD
    DATASET(lAPIAutoTermTypeXML) enh_API_LT_auto {XPATH('autoposting-term')};
  END;
   lEnhAPICRNXML:=RECORD
    DATASET(lAPIAutoTermTypeXML) enh_API_CRN_auto {XPATH('autoposting-term')};
  END;
   lManufacturersXML:=RECORD
    UNICODE manufacturer                        {XPATH('')};
  END;
   lEnhManXML:=RECORD
    UNICODE manufacturer_type                   {XPATH('@type')};
    DATASET(lManufacturersXML) manufacturers    {XPATH('manufacturer')};
  END;
   lTradenameXML:=RECORD
    UNICODE tradename                           {XPATH('')};
  END;
   lEnhTradeXML:=RECORD
    UNICODE tradename_type                      {XPATH('@type')};
    DATASET(lTradenameXML) tradenames           {XPATH('tradename')};
  END;
   lChemRegNumXML:=RECORD
    UNICODE chemical_reg_num                    {XPATH('')};
  END;
   lChemicalsXML:=RECORD
    UNICODE chemical_name                       {XPATH('chemical-name')};
    DATASET(lChemRegNumXML) chemical_reg_nums   {XPATH('cas-registry-number')};
  END;
   lEnhChemXML:=RECORD
    UNICODE chemical_source                     {XPATH('@source')};
    DATASET(lChemicalsXML) chemicals            {XPATH('chemical')};
  END;

   lRefTitleXML:={UNICODE reference_title {XPATH('')};};
   lRefItemIDsXML:=RECORD
    UNICODE ref_item_id_type                    {XPATH('@idtype')};
    UNICODE ref_item_id                         {XPATH('')};
  END;
   lRefAuthorsXML:=RECORD
    UNICODE ref_author_sequence                 {XPATH('@seq')};
    UNICODE ref_author_initials                 {XPATH('ce:initials')};
    UNICODE ref_author_indexed_name             {XPATH('ce:indexed-name')};
    UNICODE ref_author_surname                  {XPATH('ce:surname')};
  END;
   lRefSourceTitleXML:={UNICODE reference_source_title {XPATH('')};};
   lReferencesXML:=RECORD
    UNICODE reference_id                        {XPATH('@id')};
    DATASET(lRefTitleXML) reference_titles      {XPATH('ref-info/ref-title/ref-titletext')};
    DATASET(lRefItemIDsXML) reference_itemids   {XPATH('ref-info/refd-itemidlist/itemid')};
    DATASET(lRefAuthorsXML) reference_authors   {XPATH('ref-info/ref-authors/author')};
    DATASET(lRefSourceTitleXML) reference_source_titles {XPATH('ref-info/ref-sourcetitle')};
    UNICODE reference_pub_year_first            {XPATH('ref-info/ref-publicationyear@first')};
    UNICODE reference_pub_year_last             {XPATH('ref-info/ref-publicationyear@last')};
    UNICODE reference_text                      {XPATH('ref-info/ref-text')};
    UNICODE reference_full_text                 {XPATH('ref-fulltext')};
  END;
  
   lDocsXML := RECORD
	  UNICODE eid                                 {XPATH('xocs:meta/xocs:eid')};
	  UNICODE oeid                                {XPATH('xocs:meta/xocs:oeid')};
    UNICODE content_type                        {XPATH('@content-type')};
    UNICODE dbname                              {XPATH('@dbname')};
    UNICODE online_status                       {XPATH('xocs:meta/xocs:online-status')};
    UNICODE pui                                 {XPATH('xocs:meta/xocs:pui')};
    UNICODE pii                                 {XPATH('xocs:meta/xocs:pii')};
    UNICODE fulltext_eid                        {XPATH('xocs:meta/xocs:fulltext-eid')};
    UNICODE issn                                {XPATH('xocs:meta/xocs:issn')};
    UNICODE volume                              {XPATH('xocs:meta/xocs:volume')};
    UNICODE issue                               {XPATH('xocs:meta/xocs:issue')};
    UNICODE author_surname                      {XPATH('xocs:meta/xocs:author-surname')};
    UNICODE author_first_initial                {XPATH('xocs:meta/xocs:author-fi')};
    UNICODE first_page                          {XPATH('xocs:meta/xocs:firstpage')};
    UNICODE last_page                           {XPATH('xocs:meta/xocs:lastpage')};
    UNICODE sort_year                           {XPATH('xocs:meta/xocs:sort-year')};
    UNICODE pub_year                            {XPATH('xocs:meta/xocs:pub-year')};
    UNICODE timestamp                           {XPATH('xocs:meta/xocs:timestamp')};
    UNICODE group_id                            {XPATH('xocs:meta/cto:group-id')};
    DATASET(lRefIDsXML) ref_ids                 {XPATH('xocs:meta/cto:ref-id')};
    DATASET(lAuthorsXML) authors                {XPATH('xocs:meta/cto:unique-author')};
    UNICODE proc_date_delivered                 {XPATH('xocs:item/item/ait:process-info/ait:date-delivered@timestamp')};
    UNICODE proc_sort_day                       {XPATH('xocs:item/item/ait:process-info/ait:date-sort@day')};
    UNICODE proc_sort_month                     {XPATH('xocs:item/item/ait:process-info/ait:date-sort@month')};
    UNICODE proc_sort_year                      {XPATH('xocs:item/item/ait:process-info/ait:date-sort@year')};
    UNICODE proc_status_stage                   {XPATH('xocs:item/item/ait:process-info/ait:status@stage')};
    UNICODE proc_status_state                   {XPATH('xocs:item/item/ait:process-info/ait:status@state')};
    UNICODE proc_status_type                    {XPATH('xocs:item/item/ait:process-info/ait:status@type')};
    DATASET(lCopyrightXML) copyrights           {XPATH('xocs:item/item/bibrecord/item-info/copyright')};
    UNICODE copyright_pii                       {XPATH('xocs:item/item/bibrecord/item-info/ce:pii')};
    UNICODE copyright_doi                       {XPATH('xocs:item/item/bibrecord/item-info/ce:doi')};
    DATASET(lItemIDsXML) item_ids               {XPATH('xocs:item/item/bibrecord/item-info/itemidlist/itemid')};
    UNICODE history_create_day                  {XPATH('xocs:item/item/bibrecord/item-info/history/date-created@day')};
    UNICODE history_create_month                {XPATH('xocs:item/item/bibrecord/item-info/history/date-created@month')};
    UNICODE history_create_year                 {XPATH('xocs:item/item/bibrecord/item-info/history/date-created@year')};
    UNICODE history_complete_day                {XPATH('xocs:item/item/bibrecord/item-info/history/date-completed@day')};
    UNICODE history_complete_month              {XPATH('xocs:item/item/bibrecord/item-info/history/date-completed@month')};
    UNICODE history_complete_year               {XPATH('xocs:item/item/bibrecord/item-info/history/date-completed@year')};
    UNICODE history_revise_day                  {XPATH('xocs:item/item/bibrecord/item-info/history/date-revised@day')};
    UNICODE history_revise_month                {XPATH('xocs:item/item/bibrecord/item-info/history/date-revised@month')};
    UNICODE history_revise_year                 {XPATH('xocs:item/item/bibrecord/item-info/history/date-revised@year')};
    DATASET(lCollectionsXML) collections        {XPATH('xocs:item/item/bibrecord/item-info/dbcollection')};
    UNICODE external_source                     {XPATH('xocs:item/item/bibrecord/item-info/external-source')};
    DATASET(lCitationTypesXML) citation_types   {XPATH('xocs:item/item/bibrecord/head/citation-info/citation-type')};
    DATASET(lCitationLangXML) citation_languages{XPATH('xocs:item/item/bibrecord/head/citation-info/citation-language')};
    DATASET(lAbstractLangXML) abstract_languages{XPATH('xocs:item/item/bibrecord/head/citation-info/abstract-language')};
    DATASET(lPatentRegXML) patent_registrations {XPATH('xocs:item/item/bibrecord/head/citation-info/patent/registrations/registration')};
    DATASET(lPatentAppXML) patent_applications  {XPATH('xocs:item/item/bibrecord/head/citation-info/patent/localapplications/localapplication')};
    DATASET(lPatentPrioAppXML) patent_prioapp   {XPATH('xocs:item/item/bibrecord/head/citation-info/patent/prioapplications/prioapplication')};
    DATASET(lPatentAssigneeXML) patent_assignees{XPATH('xocs:item/item/bibrecord/head/citation-info/patent/assignees/assignee')};
    DATASET(lPatentCountryXML) patent_countries {XPATH('xocs:item/item/bibrecord/head/citation-info/patent/designatedcountries/country')};
    DATASET(lPatentIPCXML) patent_ipc_codes     {XPATH('xocs:item/item/bibrecord/head/citation-info/patent/ipc-codes/ipc-code')};
    DATASET(lPatentInventorXML) patent_inventors{XPATH('xocs:item/item/bibrecord/head/citation-info/patent/inventors/inventor')};
    DATASET(lKeywordsXML) author_keywords       {XPATH('xocs:item/item/bibrecord/head/citation-info/author-keywords/author-keyword')};
    UNICODE dummy_link_citation_type            {XPATH('xocs:item/item/bibrecord/head/citation-info/dummy_link/gen-citationtype@code')};
    UNICODE dummy_link_itemlink                 {XPATH('xocs:item/item/bibrecord/head/citation-info/dummy_link/itemlink')};
    UNICODE dummy_link_restricted_access        {XPATH('xocs:item/item/bibrecord/head/citation-info/dummy_link@restricted-access')};
    UNICODE figure_information                  {XPATH('xocs:item/item/bibrecord/head/citation-info/figure_information')};
    DATASET(lCitationTitleXML) citation_titles  {XPATH('xocs:item/item/bibrecord/head/citation-title/titletext')};
    DATASET(lAuthorGroupsXML) author_groups     {XPATH('xocs:item/item/bibrecord/head/author-group')};
    UNICODE corresp_initials                    {XPATH('xocs:item/item/bibrecord/head/correspondence/person/ce:initials')};
    UNICODE corresp_indexed_name                {XPATH('xocs:item/item/bibrecord/head/correspondence/person/ce:indexed-name')};
    UNICODE corresp_surname                     {XPATH('xocs:item/item/bibrecord/head/correspondence/person/ce:surname')};
    UNICODE corresp_aff_country                 {XPATH('xocs:item/item/bibrecord/head/correspondence/affiliation@country')};
    DATASET(lAffOrgXML) corresp_aff_org         {XPATH('xocs:item/item/bibrecord/head/correspondence/affiliation/organization')};
    UNICODE corresp_aff_city                    {XPATH('xocs:item/item/bibrecord/head/correspondence/affiliation/city-group')};
    UNICODE corresp_e_address_type              {XPATH('xocs:item/item/bibrecord/head/correspondence/ce:e-address@type')};
    UNICODE corresp_e_address                   {XPATH('xocs:item/item/bibrecord/head/correspondence/ce:e-address')};
    UNICODE grant_complete                      {XPATH('xocs:item/item/bibrecord/head/grantlist@complete')};
    DATASET(lGrantsXML) grants                  {XPATH('xocs:item/item/bibrecord/head/grantlist/grant')};
    DATASET(lAbstractXML) abstracts             {XPATH('xocs:item/item/bibrecord/head/abstracts/abstract')};
    UNICODE source_country                      {XPATH('xocs:item/item/bibrecord/head/source@country')};
    UNICODE source_type                         {XPATH('xocs:item/item/bibrecord/head/source@type')};
    UNICODE source_id                           {XPATH('xocs:item/item/bibrecord/head/source@srcid')};
    UNICODE source_title                        {XPATH('xocs:item/item/bibrecord/head/source/sourcetitle')};
    UNICODE source_title_preferred              {XPATH('xocs:item/item/bibrecord/head/source/preferred-sourcetitle')};
    UNICODE source_title_abbr                   {XPATH('xocs:item/item/bibrecord/head/source/sourcetitle-abbrev')};
    UNICODE source_title_translated             {XPATH('xocs:item/item/bibrecord/head/source/translated-sourcetitle')};
    UNICODE source_title_volume                 {XPATH('xocs:item/item/bibrecord/head/source/volumetitle')};
    UNICODE source_title_issue                  {XPATH('xocs:item/item/bibrecord/head/source/issuetitle')};
    UNICODE source_issue_designation            {XPATH('xocs:item/item/bibrecord/head/source/issue_designation')};
    DATASET(lSourceIssnXML) source_issns        {XPATH('xocs:item/item/bibrecord/head/source/issn')};
    DATASET(lSourceIsbnXML) source_isbns        {XPATH('xocs:item/item/bibrecord/head/source/isbn')};
    UNICODE source_codencode                    {XPATH('xocs:item/item/bibrecord/head/source/codencode')};
    UNICODE source_part                         {XPATH('xocs:item/item/bibrecord/head/source/part')};
    UNICODE source_volisspag_issue              {XPATH('xocs:item/item/bibrecord/head/source/volisspag/voliss@issue')};
    UNICODE source_volisspag_volume             {XPATH('xocs:item/item/bibrecord/head/source/volisspag/voliss@volume')};
    UNICODE source_volisspag_first_page         {XPATH('xocs:item/item/bibrecord/head/source/volisspag/pagerange@first')};
    UNICODE source_volisspag_last_page          {XPATH('xocs:item/item/bibrecord/head/source/volisspag/pagerange@last')};
    UNICODE source_pub_year_first               {XPATH('xocs:item/item/bibrecord/head/source/publicationdate/year@first')};
    UNICODE source_pub_year_last                {XPATH('xocs:item/item/bibrecord/head/source/publicationdate/year@last')};
    DATASET(lSourceWebsiteXML) source_websites  {XPATH('xocs:item/item/bibrecord/head/source/website')};
    DATASET(lSourceEditorsXML) source_editors   {XPATH('xocs:item/item/bibrecord/head/source/editors/editor')};
    DATASET(lPublisherXML) publishers           {XPATH('xocs:item/item/bibrecord/head/source/publisher')};
    UNICODE secondary_journal_title             {XPATH('xocs:item/item/bibrecord/head/source/additional-srcinfo/secondaryjournal/sourcetitle')};
    UNICODE secondary_journal_title_abbrev      {XPATH('xocs:item/item/bibrecord/head/source/additional-srcinfo/secondaryjournal/sourcetitle-abbrev')};
    UNICODE secondary_journal_issn              {XPATH('xocs:item/item/bibrecord/head/source/additional-srcinfo/secondaryjournal/issn')};
    UNICODE secondary_journal_pub_year_first    {XPATH('xocs:item/item/bibrecord/head/source/additional-srcinfo/secondaryjournal/publicationyear@first')};
    UNICODE secondary_journal_pub_year_last     {XPATH('xocs:item/item/bibrecord/head/source/additional-srcinfo/secondaryjournal/publicationyear@last')};
    UNICODE secondary_journal_volume            {XPATH('xocs:item/item/bibrecord/head/source/additional-srcinfo/secondaryjournal/voliss@volume')};
    UNICODE secondary_journal_issue             {XPATH('xocs:item/item/bibrecord/head/source/additional-srcinfo/secondaryjournal/voliss@issue')};
    DATASET(lConfNameXML) conference_names      {XPATH('xocs:item/item/bibrecord/head/source/additional-srcinfo/conferenceinfo/confevent/confname')};
    DATASET(lConfNumberXML) conference_numbers  {XPATH('xocs:item/item/bibrecord/head/source/additional-srcinfo/conferenceinfo/confevent/confnumber')};
    DATASET(lConfThemeXML) conference_themes    {XPATH('xocs:item/item/bibrecord/head/source/additional-srcinfo/conferenceinfo/confevent/conftheme')};
    DATASET(lConfTitleXML) conference_titles    {XPATH('xocs:item/item/bibrecord/head/source/additional-srcinfo/conferenceinfo/confevent/conftitle')};
    UNICODE conference_location_venue           {XPATH('xocs:item/item/bibrecord/head/source/additional-srcinfo/conferenceinfo/confevent/conflocation/venue')};
    UNICODE conference_location_address_part    {XPATH('xocs:item/item/bibrecord/head/source/additional-srcinfo/conferenceinfo/confevent/conflocation/address-part')};
    UNICODE conference_location_city_group      {XPATH('xocs:item/item/bibrecord/head/source/additional-srcinfo/conferenceinfo/confevent/conflocation/city-group')};
    UNICODE conference_location_city            {XPATH('xocs:item/item/bibrecord/head/source/additional-srcinfo/conferenceinfo/confevent/conflocation/city')};
    UNICODE conference_location_state           {XPATH('xocs:item/item/bibrecord/head/source/additional-srcinfo/conferenceinfo/confevent/conflocation/state')};
    UNICODE conference_location_postal_code     {XPATH('xocs:item/item/bibrecord/head/source/additional-srcinfo/conferenceinfo/confevent/conflocation/postal-code')};
    UNICODE conference_location_country         {XPATH('xocs:item/item/bibrecord/head/source/additional-srcinfo/conferenceinfo/confevent/conflocation/country')};
    UNICODE conference_date_start_year          {XPATH('xocs:item/item/bibrecord/head/source/additional-srcinfo/conferenceinfo/confevent/confdate/startdate@year')};
    UNICODE conference_date_start_month         {XPATH('xocs:item/item/bibrecord/head/source/additional-srcinfo/conferenceinfo/confevent/confdate/startdate@month')};
    UNICODE conference_date_start_day           {XPATH('xocs:item/item/bibrecord/head/source/additional-srcinfo/conferenceinfo/confevent/confdate/startdate@day')};
    UNICODE conference_date_end_year            {XPATH('xocs:item/item/bibrecord/head/source/additional-srcinfo/conferenceinfo/confevent/confdate/enddate@year')};
    UNICODE conference_date_end_month           {XPATH('xocs:item/item/bibrecord/head/source/additional-srcinfo/conferenceinfo/confevent/confdate/enddate@month')};
    UNICODE conference_date_end_day             {XPATH('xocs:item/item/bibrecord/head/source/additional-srcinfo/conferenceinfo/confevent/confdate/enddate@day')};
    DATASET(lConfOrgXML) conference_orgs        {XPATH('xocs:item/item/bibrecord/head/source/additional-srcinfo/conferenceinfo/confevent/conforganization')};
    UNICODE conference_url                      {XPATH('xocs:item/item/bibrecord/head/source/additional-srcinfo/conferenceinfo/confevent/confURL')};
    UNICODE conference_catnumber                {XPATH('xocs:item/item/bibrecord/head/source/additional-srcinfo/conferenceinfo/confevent/confcatnumber')};
    UNICODE conference_code                     {XPATH('xocs:item/item/bibrecord/head/source/additional-srcinfo/conferenceinfo/confevent/confcode')};
    UNICODE conference_sponsors_complete        {XPATH('xocs:item/item/bibrecord/head/source/additional-srcinfo/conferenceinfo/confevent/confsponsors@complete')};
    DATASET(lConfSponsorXML) conference_sponsors{XPATH('xocs:item/item/bibrecord/head/source/additional-srcinfo/conferenceinfo/confevent/confsponsors/confsponsor')};
    UNICODE conference_pub_editors              {XPATH('xocs:item/item/bibrecord/head/source/additional-srcinfo/conferenceinfo/confpublication/confeditors/editors')};
    UNICODE conference_pub_address              {XPATH('xocs:item/item/bibrecord/head/source/additional-srcinfo/conferenceinfo/confpublication/confeditors/editoraddress')};
    DATASET(lConfEdOrgXML) conference_Ed_Orgs   {XPATH('xocs:item/item/bibrecord/head/source/additional-srcinfo/conferenceinfo/confpublication/confeditors/editororganization')};
    UNICODE conference_pub_procpartno           {XPATH('xocs:item/item/bibrecord/head/source/additional-srcinfo/conferenceinfo/confpublication/procpartno')};
    UNICODE conference_pub_procpagerange        {XPATH('xocs:item/item/bibrecord/head/source/additional-srcinfo/conferenceinfo/confpublication/procpagerange')};
    UNICODE conference_pub_procpagecount        {XPATH('xocs:item/item/bibrecord/head/source/additional-srcinfo/conferenceinfo/confpublication/procpagecount')};
    UNICODE report_number                       {XPATH('xocs:item/item/bibrecord/head/source/additional-srcinfo/reportinfo/reportnumber')};
    DATASET(lPatentRegXML) enhpat_registrations {XPATH('xocs:item/item/bibrecord/head/enhancement/patent/registrations/registration')};
    DATASET(lPatentAppXML) enhpat_applications  {XPATH('xocs:item/item/bibrecord/head/enhancement/patent/localapplications/localapplication')};
    DATASET(lPatentPrioAppXML) enhpat_prioapp   {XPATH('xocs:item/item/bibrecord/head/enhancement/patent/prioapplications/prioapplication')};
    DATASET(lPatentAssigneeXML) enhpat_assignees{XPATH('xocs:item/item/bibrecord/head/enhancement/patent/assignees/assignee')};
    DATASET(lPatentCountryXML) enhpat_countries {XPATH('xocs:item/item/bibrecord/head/enhancement/patent/designatedcountries/country')};
    DATASET(lPatentIPCXML) enhpat_ipc_codes     {XPATH('xocs:item/item/bibrecord/head/enhancement/patent/ipc-codes/ipc-code')};
    DATASET(lPatentInventorXML) enhpat_inventors{XPATH('xocs:item/item/bibrecord/head/enhancement/patent/inventors/inventor')};
    DATASET(lEnhDescXML) enhancement_descriptors{XPATH('xocs:item/item/bibrecord/head/enhancement/descriptorgroup/descriptors/descriptor')};
    DATASET(lEnhAPICCXML) enh_API_CC            {XPATH('xocs:item/item/bibrecord/head/source/enhancement/API-descriptorgroup/autoposting/API-CC/clasification')};
    DATASET(lEnhAPILTMXML) enh_API_LTM_group    {XPATH('xocs:item/item/bibrecord/head/source/enhancement/API-descriptorgroup/autoposting/API-LTM/API-LTM-group')};
    UNICODE enh_API_ALC_LTM_count               {XPATH('xocs:item/item/bibrecord/head/source/enhancement/API-descriptorgroup/autoposting/API-ALC/LTM-count')};
    UNICODE enh_API_ALC_LT_count                {XPATH('xocs:item/item/bibrecord/head/source/enhancement/API-descriptorgroup/autoposting/API-ALC/LT-count')};
    DATASET(lEnhAPIATMXML) enh_API_ATM_group    {XPATH('xocs:item/item/bibrecord/head/source/enhancement/API-descriptorgroup/autoposting/API-ATM-group/API-ATM')};
    DATASET(lEnhAPIAMSXML) enh_API_AMS          {XPATH('xocs:item/item/bibrecord/head/source/enhancement/API-descriptorgroup/autoposting/API-AMS')};
    DATASET(lEnhAPIAPCXML) enh_API_APC          {XPATH('xocs:item/item/bibrecord/head/source/enhancement/API-descriptorgroup/autoposting/API-APC')};
    DATASET(lEnhAPIANCXML) enh_API_ANC          {XPATH('xocs:item/item/bibrecord/head/source/enhancement/API-descriptorgroup/autoposting/API-ANC')};
    DATASET(lEnhAPIATXML) enh_API_AT            {XPATH('xocs:item/item/bibrecord/head/source/enhancement/API-descriptorgroup/autoposting/API-AT')};
    DATASET(lEnhAPICTXML) enh_API_CT            {XPATH('xocs:item/item/bibrecord/head/source/enhancement/API-descriptorgroup/autoposting/API-CT')};
    DATASET(lEnhAPILTXML) enh_API_LT            {XPATH('xocs:item/item/bibrecord/head/source/enhancement/API-descriptorgroup/autoposting/API-LT')};
    DATASET(lEnhAPICRNXML) enh_API_CRN          {XPATH('xocs:item/item/bibrecord/head/source/enhancement/API-descriptorgroup/autoposting/API-CRN')};
    DATASET(lEnhClassXML) classification_group  {XPATH('xocs:item/item/bibrecord/head/enhancement/classificationgroup/classifications')};
    DATASET(lEnhManXML) manufacturer_group      {XPATH('xocs:item/item/bibrecord/head/enhancement/manufacturergroup/manufacturers')};
    DATASET(lEnhTradeXML) tradename_group       {XPATH('xocs:item/item/bibrecord/head/enhancement/tradenamegroup/tradenames')};
    DATASET(lEnhChemXML) chemical_group         {XPATH('xocs:item/item/bibrecord/head/enhancement/chemicalgroup/chemicals')};
    UNICODE reference_count                     {XPATH('xocs:item/item/bibrecord/tail/bibliography@refcount')};
    DATASET(lReferencesXML) references          {XPATH('xocs:item/item/bibrecord/tail/bibliography/reference')};
  END;
   dFileXML:=DATASET(sFilename,lDocsXML,XML('xocs:doc',NOROOT));


// Standard ECL layouts for a cleaned version of the XML data.
   lRefIDs:={UNICODE ref_id;};
   lAuthors:=RECORD
    UNICODE initials;
    UNICODE indexed_name;
    UNICODE surname;
    UNICODE author_id;
  END;
   lCopyright:=RECORD
    UNICODE copyright_type;
    UNICODE copyright;
  END;
   lItemIDs:=RECORD
    UNICODE item_id_type;
    UNICODE item_id;
  END;
   lCollections:=RECORD
    UNICODE collection;
  END;
   lCitationTypes:=RECORD
    UNICODE citation_type;
  END;
   lCitationLang:=RECORD
    UNICODE citation_language;
  END;
   lAbstractLang:=RECORD
    UNICODE abstract_language;
  END;
   lPatentReg:={UNICODE patent_registration;};
   lPatentApp:={UNICODE patent_application;};
   lPatentPrioApp:={UNICODE patent_prioapplication;};
   lPatentAssignee:={UNICODE patent_assignee;};
   lPatentCountry:={UNICODE patent_country;};
   lPatentIPC:={UNICODE patent_IPC_code;};
   lPatentInventor:={UNICODE patent_inventor;};
   lKeywords:={UNICODE keyword;};
   lCitationTitle:=RECORD
    UNICODE citation_title_language;
    UNICODE citation_title_orig;
    UNICODE citation_title;
  END;
   lAuthorGroupAuthor:=RECORD
    UNICODE group_author_seq;
    UNICODE group_author_auid;
    UNICODE group_author_type;
    UNICODE group_author_initials;
    UNICODE group_author_indexed_name;
    UNICODE group_author_surname;
    UNICODE group_author_given_name;
    UNICODE group_author_pref_initials;
    UNICODE group_author_pref_indexed_name;
    UNICODE group_author_pref_surname;
    UNICODE group_author_pref_given_name;
    UNICODE group_author_e_address_type;
    UNICODE group_author_e_address;
  END;
   lAffOrg:=RECORD
    UNICODE organization;
  END;
   lAuthorGroups:=RECORD
    DATASET(lAuthorGroupAuthor) author_group_author;
    UNICODE author_group_afid;
    UNICODE author_group_dptid;
    UNICODE author_group_country;
    DATASET(lAffOrg) author_group_org;
    UNICODE author_group_city;
		UNICODE author_group_cetext;
  END;
   lGrants:=RECORD
    UNICODE grant_id;
    UNICODE grant_acronym;
    UNICODE grant_agency;
  END;
   lAbstract:=RECORD
    UNICODE abstract_original;
    UNICODE abstract_language;
    UNICODE abstract_perspective;
    UNICODE abstract_source;
    UNICODE abstract_publisher_copyright;
    UNICODE abstract;
  END;
   lSourceIssn:=RECORD
    UNICODE source_issn_type;
    UNICODE source_issn;
  END;
   lSourceIsbn:=RECORD
    UNICODE source_isbn_type;
    UNICODE source_isbn_lengh;
    UNICODE source_isbn_level;
    UNICODE source_isbn;
  END;
   lSourceWebsite:=RECORD
    UNICODE source_website_type;
    UNICODE source_website;
    UNICODE source_website_name;
  END;
   lSourceEditors:=RECORD
    UNICODE editor_initials;
    UNICODE editor_indexed_name;
    UNICODE editor_surname;
    UNICODE editor_given_name;
    UNICODE editor_role;
    UNICODE editor_type;
  END;
   lPublisher:=RECORD
    UNICODE publisher_name;
    UNICODE publisher_address;
    UNICODE publisher_affiliation;
  END;
   lConfName:={UNICODE conference_event_name;};
   lConfNumber:={UNICODE conference_event_number;};
   lConfTheme:={UNICODE conference_event_theme;};
   lConfTitle:={UNICODE conference_event_title;};
   lConfOrg:={UNICODE conference_organization;};
   lConfSponsor:={UNICODE conference_sponsor;};
   lConfEdOrg:={UNICODE conference_editor_organization;};
   lEnhDescriptors:=RECORD
    UNICODE descriptor_mainterm_weight;
    UNICODE descriptor_mainterm_candidate;
    UNICODE descriptor_mainterm_sort_pos;
    UNICODE descriptor_mainterm_code;
    UNICODE descriptor;
  END;
   lEnhDesc:=RECORD
    UNICODE descriptor_type;
    UNICODE descriptor_controlled;
    UNICODE descriptor_name;
    DATASET(lEnhDescriptors) descriptor;
  END;
   lEnhClasses:=RECORD
    UNICODE classification;
    UNICODE classification_code;
    UNICODE classification_description;
  END;
   lEnhClass:=RECORD
    UNICODE classification_type;
    DATASET(lEnhClasses) classifications;
  END;

   lAPITermType:=RECORD
    UNICODE major_term_indicator;
    UNICODE enh_API_ATM_role;
    UNICODE enh_API_ATM_group_indicator;
    UNICODE enh_API_ATM_sort_pos;
  END;
   lAPIAutoTermType:=RECORD
    UNICODE prefix;
    UNICODE postfix;
    UNICODE CAS_nr;
  END;
   lEnhAPICC:=RECORD
    UNICODE classification;
  END;
   lEnhAPILTM:=RECORD
    DATASET(lAPIAutoTermType) enh_API_LTM_auto;
  END;

   lVGroupTermGrp:=RECORD
    DATASET(lAPITermType) vgroup_term_grp;
  END;
   lSGroupTermGrp:=RECORD
    DATASET(lAPITermType) sgroup_term_grp;
  END;
   lEnhAPIVgroup:=RECORD
    DATASET(lAPITermType) vgroup_term;
    DATASET(lVGroupTermGrp) vgroup_termgroup;
  END;
   lEnhAPISgroup:=RECORD
    DATASET(lAPITermType) sgroup_term;
    DATASET(lSGroupTermGrp) sgroup_termgroup;
  END;
   lEnhAPIATM:=RECORD
    UNICODE enh_API_ATM_template_role;
    UNICODE enh_API_ATM_lt_count;
    DATASET(lAPITermType) enh_API_ATM_term;
    DATASET(lEnhAPIVgroup) enh_API_ATM_vgroup;
    DATASET(lEnhAPISgroup) enh_API_ATM_sgroup;
  END;
   lEnhAPIAMS:=RECORD
    DATASET(lAPITermType) API_AMS_term;
  END;
   lEnhAPIAPC:=RECORD
    DATASET(lAPITermType) API_APC_term;
  END;
   lEnhAPIANC:=RECORD
    DATASET(lAPITermType) API_ANC_term;
  END;
   lEnhAPIAT:=RECORD
    DATASET(lAPIAutoTermType) enh_API_AT_auto;
  END;
   lEnhAPICT:=RECORD
    DATASET(lAPIAutoTermType) enh_API_CT_auto;
  END;
   lEnhAPILT:=RECORD
    DATASET(lAPIAutoTermType) enh_API_LT_auto;
  END;
   lEnhAPICRN:=RECORD
    DATASET(lAPIAutoTermType) enh_API_CRN_auto;
  END;
   lManufacturers:=RECORD
    UNICODE manufacturer;
  END;
   lEnhMan:=RECORD
    UNICODE manufacturer_type;
    DATASET(lManufacturers) manufacturers;
  END;
   lTradename:=RECORD
    UNICODE tradename;
  END;
   lEnhTrade:=RECORD
    UNICODE tradename_type;
    DATASET(lTradename) tradenames;
  END;
   lChemRegNum:=RECORD
    UNICODE chemical_reg_num;
  END;
   lChemicals:=RECORD
    UNICODE chemical_name;
    DATASET(lChemRegNum) chemical_reg_nums;
  END;
   lEnhChem:=RECORD
    UNICODE chemical_source;
    DATASET(lChemicals) chemicals;
  END;

   lRefTitle:={UNICODE reference_title;};
   lRefItemIDs:=RECORD
    UNICODE ref_item_id_type;
    UNICODE ref_item_id;
  END;
   lRefAuthors:=RECORD
    UNICODE ref_author_sequence;
    UNICODE ref_author_initials;
    UNICODE ref_author_indexed_name;
    UNICODE ref_author_surname;
  END;
   lRefSourceTitle:={UNICODE reference_source_title;};
   lReferences:=RECORD
    UNICODE reference_id;
    DATASET(lRefTitle) reference_titles;
    DATASET(lRefItemIDs) reference_itemids;
    DATASET(lRefAuthors) reference_authors;
    DATASET(lRefSourceTitle) reference_source_titles;
    UNICODE reference_pub_year_first;
    UNICODE reference_pub_year_last;
    UNICODE reference_text;
    UNICODE reference_full_text;
  END;
  
   lDocs := RECORD
    UNICODE eid;
    UNICODE oeid;
    UNICODE content_type;
    UNICODE dbname;
    UNICODE online_status;
    UNICODE pui;
    UNICODE pii;
    UNICODE fulltext_eid;
    UNICODE issn;
    UNICODE volume;
    UNICODE issue;
    UNICODE author_surname;
    UNICODE author_first_initial;
    UNICODE first_page;
    UNICODE last_page;
    UNICODE sort_year;
    UNICODE pub_year;
    UNICODE timestamp;
    UNICODE group_id;
    DATASET(lRefIDs) ref_ids;
    DATASET(lAuthors) authors;
    UNICODE proc_date_delivered;
    UNICODE proc_sort_day;
    UNICODE proc_sort_month;
    UNICODE proc_sort_year;
    UNICODE proc_status_stage;
    UNICODE proc_status_state;
    UNICODE proc_status_type;
    DATASET(lCopyright) copyrights;
    UNICODE copyright_pii;
    UNICODE copyright_doi;
    DATASET(lItemIDs) item_ids;
    UNICODE history_create_day;
    UNICODE history_create_month;
    UNICODE history_create_year;
    UNICODE history_complete_day;
    UNICODE history_complete_month;
    UNICODE history_complete_year;
    UNICODE history_revise_day;
    UNICODE history_revise_month;
    UNICODE history_revise_year;
    DATASET(lCollections) collections;
    UNICODE external_source;
    DATASET(lCitationTypes) citation_types;
    DATASET(lCitationLang) citation_languages;
    DATASET(lAbstractLang) abstract_languages;
    DATASET(lPatentReg) patent_registrations;
    DATASET(lPatentApp) patent_applications;
    DATASET(lPatentPrioApp) patent_prioapp;
    DATASET(lPatentAssignee) patent_assignees;
    DATASET(lPatentCountry) patent_countries;
    DATASET(lPatentIPC) patent_ipc_codes;
    DATASET(lPatentInventor) patent_inventors;
    DATASET(lKeywords) author_keywords;
    UNICODE dummy_link_citation_type;
    UNICODE dummy_link_itemlink;
    UNICODE dummy_link_restricted_access;
    UNICODE figure_information;
    DATASET(lCitationTitle) citation_titles;
    DATASET(lAuthorGroups) author_groups;
    UNICODE corresp_initials;
    UNICODE corresp_indexed_name;
    UNICODE corresp_surname;
    UNICODE corresp_aff_country;
    DATASET(lAffOrg) corresp_aff_org;
    UNICODE corresp_aff_city;
    UNICODE corresp_e_address_type;
    UNICODE corresp_e_address;
    UNICODE grant_complete;
    DATASET(lGrants) grants;
    DATASET(lAbstract) abstracts;
    UNICODE source_country;
    UNICODE source_type;
    UNICODE source_id;
    UNICODE source_title;
    UNICODE source_title_preferred;
    UNICODE source_title_abbr;
    UNICODE source_title_translated;
    UNICODE source_title_volume;
    UNICODE source_title_issue;
    UNICODE source_issue_designation;
    DATASET(lSourceIssn) source_issns;
    DATASET(lSourceIsbn) source_isbns;
    UNICODE source_codencode;
    UNICODE source_part;
    UNICODE source_volisspag_issue;
    UNICODE source_volisspag_volume;
    UNICODE source_volisspag_first_page;
    UNICODE source_volisspag_last_page;
    UNICODE source_pub_year_first;
    UNICODE source_pub_year_last;
    DATASET(lSourceWebsite) source_websites;
    DATASET(lSourceEditors) source_editors;
    DATASET(lPublisher) publishers;
    UNICODE secondary_journal_title;
    UNICODE secondary_journal_title_abbrev;
    UNICODE secondary_journal_issn;
    UNICODE secondary_journal_pub_year_first;
    UNICODE secondary_journal_pub_year_last;
    UNICODE secondary_journal_volume;
    UNICODE secondary_journal_issue;
    DATASET(lConfName) conference_names;
    DATASET(lConfNumber) conference_numbers;
    DATASET(lConfTheme) conference_themes;
    DATASET(lConfTitle) conference_titles;
    UNICODE conference_location_venue;
    UNICODE conference_location_address_part;
    UNICODE conference_location_city_group;
    UNICODE conference_location_city;
    UNICODE conference_location_state;
    UNICODE conference_location_postal_code;
    UNICODE conference_location_country;
    UNICODE conference_date_start_year;
    UNICODE conference_date_start_month;
    UNICODE conference_date_start_day;
    UNICODE conference_date_end_year;
    UNICODE conference_date_end_month;
    UNICODE conference_date_end_day;
    DATASET(lConfOrg) conference_orgs;
    UNICODE conference_url;
    UNICODE conference_catnumber;
    UNICODE conference_code;
    UNICODE conference_sponsors_complete;
    DATASET(lConfSponsor) conference_sponsors;
    UNICODE conference_pub_editors;
    UNICODE conference_pub_address;
    DATASET(lConfEdOrg) conference_Ed_Orgs;
    UNICODE conference_pub_procpartno;
    UNICODE conference_pub_procpagerange;
    UNICODE conference_pub_procpagecount;
    UNICODE report_number;
    DATASET(lPatentReg) enhpat_registrations;
    DATASET(lPatentApp) enhpat_applications;
    DATASET(lPatentPrioApp) enhpat_prioapp;
    DATASET(lPatentAssignee) enhpat_assignees;
    DATASET(lPatentCountry) enhpat_countries;
    DATASET(lPatentIPC) enhpat_ipc_codes;
    DATASET(lPatentInventor) enhpat_inventors;
    DATASET(lEnhDesc) enhancement_descriptors;
    DATASET(lEnhAPICC) enh_API_CC;
    DATASET(lEnhAPILTM) enh_API_LTM_group;
    UNICODE enh_API_ALC_LTM_count;
    UNICODE enh_API_ALC_LT_count;
    DATASET(lEnhAPIATM) enh_API_ATM_group;
    DATASET(lEnhAPIAMS) enh_API_AMS;
    DATASET(lEnhAPIAPC) enh_API_APC;
    DATASET(lEnhAPIANC) enh_API_ANC;
    DATASET(lEnhAPIAT) enh_API_AT;
    DATASET(lEnhAPICT) enh_API_CT;
    DATASET(lEnhAPILT) enh_API_LT;
    DATASET(lEnhAPICRN) enh_API_CRN;
    DATASET(lEnhClass) classification_group;
    DATASET(lEnhMan) manufacturer_group;
    DATASET(lEnhTrade) tradename_group;
    DATASET(lEnhChem) chemical_group;
    UNICODE reference_count;
    DATASET(lReferences) references;
  END;

 //dFile:=PROJECT(dFileXML,TRANSFORM(lDocs,SELF:=LEFT;SELF:=[];));
 EXPORT BaseFile := OUTPUT(PROJECT(dFileXML,TRANSFORM(lDocs,SELF:=LEFT;SELF:=[];)),,'~mpayne3::scopusdata::cleanpubs',OVERWRITE);
