﻿IMPORT $;
IMPORT $.ExposedPubs as Pubs;
IMPORT $.ArticleClass as ArticleClass;
//IMPORT $.ArticleYear;

year := (UNICODE)2006;

// MyArticles := Pubs.Articles(pub_year = year);
MyArticles := Pubs.Articles(eid = '2-s2.0-0000385804');
OUTPUT(MyArticles,NAMED('SAMPLE'));

//OUTPUT(COUNT(MyArticles(ArticleClass.Chemistry > 0)),NAMED('ChemArticlesCount'));
//OUTPUT(MyArticles(ArticleClass.Chemistry > 0),NAMED('ChemArticles'));

// OUTPUT(COUNT(MyArticles(ArticleClass.ComputerScience > 0)),NAMED('CompSciArticlesCount'));
// OUTPUT(MyArticles(ArticleClass.ComputerScience > 0),NAMED('CompSciArticles'));

// OUTPUT(COUNT(MyArticles(ArticleClass.Economics > 0)),NAMED('EconArticlesCount'));
// OUTPUT(MyArticles(ArticleClass.Economics > 0),NAMED('EconArticles'));

//OUTPUT(COUNT(MyArticles($.ArticleYear(year) > 0)),NAMED('YearArticlesCount'));
//OUTPUT(MyArticles($.ArticleYear(year) > 0),NAMED('YearArticles'));