﻿IMPORT $;
IMPORT ML;

fX2 := $.cleanforcluster.d01 : PERSIST('temp::chemclusdata');
OUTPUT(fX2,,CSV,'mpayne3::chem2006tfidf',OVERWRITE);
//COUNT(fX2);
//fCentroidCandidates := $.cleanforcluster.scent : PERSIST('temp::chemcentdata');

//fX2;
//fCentroidCandidates;

//EXPORT centroids := 'todo';
// Cluster := ML.Cluster;
// Kmeans := Cluster.Kmeans(fX2, fCentroidCandidates, 100);

// KMeans.Allresults;                                       // The table that contains the results of each iteration
// KMeans.Convergence;                                      // The number of iterations it took to converge
// KMeans.Result(50);                                       // The results of iteration 12
// KMeans.Delta(5,15);                                      // The distance every centroid travelled across each axis from iterations 5 to 15
// KMeans.Delta(0);                                         // The total distance the centroids travelled on each axis
// KMeans.DistanceDelta(5,15);                              // The straight-line distance travelled by each centroid from iterations 5 to 15
// KMeans.DistanceDelta(0);                                 // The total straight-line distance each centroid travelled 
// KMeans.DistanceDelta();                                  // The distance travelled by each centroid during the last iteration.
// OUTPUT(KMeans.Allegiances(),,'mpayne3::kmeansoutput',OVERWRITE);
// Kmeans.Allegiance(1);