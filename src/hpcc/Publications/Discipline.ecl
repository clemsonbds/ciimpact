﻿IMPORT $;
IMPORT $.ExposedPubs as Pubs;
IMPORT $.ArticleClass as ArticleClass;

ChemCodes := ['1600','1601','1602','1603','1604','1605','1606','1607'];
	
CompSciCodes := ['1700','1701','1702','1703','1704','1705','1706','1707','1708','1709','1710','1711','1712'];
	
EconCodes := ['2000','2001','2002','2003'];

r := RECORD

	Pubs.Articles.pub_year;
	STRING
	INTEGER cnt := COUNT(GROUP);

END;


EXPORT Discipline := TABLE(Pubs.Articles(ArticleClass.Chemistry > 0 OR ArticleClass.ComputerScience > 0 OR ArticleClass.Economics > 0), r, pub_year);