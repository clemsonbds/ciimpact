﻿IMPORT $;

EXPORT Publications_Slim := MODULE

	 lAuthors:=RECORD
    UNICODE initials;
    UNICODE indexed_name;
    UNICODE surname;
    UNICODE author_id;
  END;

   lAuthorGroupAuthor:=RECORD
    UNICODE group_author_seq;
    UNICODE group_author_auid;
    UNICODE group_author_indexed_name;
  END;
	
   lAffOrg:=RECORD
    UNICODE organization;
  END;
   lAuthorGroups:=RECORD
    DATASET(lAuthorGroupAuthor) author_group_author;
		UNICODE author_group_country;
    DATASET(lAffOrg) author_group_org;
    UNICODE author_group_city;
  END;
	
	lDocs := RECORD
    UNICODE eid;
    DATASET(lAuthorGroups) author_groups;
  END;
	
	EXPORT SlimArticles := DATASET('~mpayne3::scopusdata::cleanpubsslim',lDocs,THOR);
	EXPORT AuthorGroup := SlimArticles.author_groups;
	EXPORT AuthorGroupOrg := SlimArticles.author_groups.author_group_org;
END;