﻿IMPORT $.ExposedPubs as EP;

BlankOrg := [''];

EXPORT NoGrpOrg := COUNT(EP.AuthorGroupOrg(organization NOT IN BlankOrg));