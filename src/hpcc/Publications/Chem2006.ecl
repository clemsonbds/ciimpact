﻿IMPORT $;
IMPORT $.ExposedPubs as Pubs;
IMPORT $.ArticleClass as ArticleClass;

year := (UNICODE)2006;
chempubs := Pubs.Articles(ArticleClass.Chemistry > 0 AND $.ArticleYear(year) > 0);
//chemsort := SORT(chempubs, eid);

EXPORT Chem2006 := chempubs : PERSIST('~mpayne3::temp::chem2006');//DEDUP(chemsort, eid) : PERSIST('~mpayne3::temp::chem2006');