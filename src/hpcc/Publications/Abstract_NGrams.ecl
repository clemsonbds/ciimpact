﻿IMPORT Publications.ExposedPubs as Pubs;
//IMPORT Publications.ArticleClass as ArticleClass;
IMPORT SBD14;
IMPORT ML;
IMPORT STD;

absRec:=RECORD
    UNICODE abstract;
  END;
	
rform := RECORD

	UNICODE abstract := XMLUNICODE('abstract');

END;

absRec clean(Pubs.Abstracts Le):= TRANSFORM

	 //labstag := REGEXREPLACE(u'(<abstract original="y" xml:lang="eng">( )*)',Le.abstract,u'');
	// rabstag := REGEXREPLACE(u'(</abstract>)( )*',labstag,u'');
	// lparatag := REGEXREPLACE(u'(<ce:para>)( )*',rabstag,u'');
	// rparatag := REGEXREPLACE(u'(</ce:para>)( )*',lparatag,u' ');
	abstag := REGEXREPLACE(u'</*abstract.*?>',Le.abstract,u'');
	paratag := REGEXREPLACE(u'</*ce:para.*?>',abstag,u'');
	SELF.abstract:= REGEXREPLACE(u'<.*?>',Le.abstract,u'');//paratag;//:= rparatag;

END;

Pubs.Abstracts getAbstracts(Pubs.Abstracts R) := TRANSFORM

	SELF := R;

END;

abstractset := NORMALIZE(SBD14.ChemArticles,LEFT.abstracts, getAbstracts(RIGHT));
abstag := PROJECT(abstractset,clean(LEFT));
abstag;
//abstracts := PARSE(abstractset,abstract,rform,XML('abstract'));//
//abstracts;
dSentences := PROJECT(abstag,TRANSFORM(ML.Docs.Types.Raw, SELF.id:=COUNTER; SELF.txt:=(STRING)LEFT.abstract));
dSentences;

dCleaned:=ML.Docs.Tokenize.Clean(dSentences);
dCleaned;

dSplit:=ML.Docs.Tokenize.Split(dCleaned);
dSplit;

dLexicon:=ML.Docs.Tokenize.Lexicon(dSplit);

dWords:=ML.Docs.CoLocation.Words(dSentences);
dWords;

dAllNGrams:=ML.Docs.CoLocation.AllNGrams(dWords,,4);
OUTPUT(dAllNGrams,,'mpayne3::ngramtest');
//EXPORT Abstract_NGrams := 'todo';