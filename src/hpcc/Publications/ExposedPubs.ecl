﻿IMPORT $;

EXPORT ExposedPubs := MODULE

	// Standard ECL layouts for a cleaned version of the XML data.
   lRefIDs:={UNICODE ref_id;};
   lAuthors:=RECORD
    UNICODE initials;
    UNICODE indexed_name;
    UNICODE surname;
    UNICODE author_id;
  END;
   lCopyright:=RECORD
    UNICODE copyright_type;
    UNICODE copyright;
  END;
   lItemIDs:=RECORD
    UNICODE item_id_type;
    UNICODE item_id;
  END;
   lCollections:=RECORD
    UNICODE collection;
  END;
   lCitationTypes:=RECORD
    UNICODE citation_type;
  END;
   lCitationLang:=RECORD
    UNICODE citation_language;
  END;
   lAbstractLang:=RECORD
    UNICODE abstract_language;
  END;
   lPatentReg:={UNICODE patent_registration;};
   lPatentApp:={UNICODE patent_application;};
   lPatentPrioApp:={UNICODE patent_prioapplication;};
   lPatentAssignee:={UNICODE patent_assignee;};
   lPatentCountry:={UNICODE patent_country;};
   lPatentIPC:={UNICODE patent_IPC_code;};
   lPatentInventor:={UNICODE patent_inventor;};
   lKeywords:={UNICODE keyword;};
   lCitationTitle:=RECORD
    UNICODE citation_title_language;
    UNICODE citation_title_orig;
    UNICODE citation_title;
  END;
   lAuthorGroupAuthor:=RECORD
    UNICODE group_author_seq;
    UNICODE group_author_auid;
    UNICODE group_author_type;
    UNICODE group_author_initials;
    UNICODE group_author_indexed_name;
    UNICODE group_author_surname;
    UNICODE group_author_given_name;
    UNICODE group_author_pref_initials;
    UNICODE group_author_pref_indexed_name;
    UNICODE group_author_pref_surname;
    UNICODE group_author_pref_given_name;
    UNICODE group_author_e_address_type;
    UNICODE group_author_e_address;
  END;
   lAffOrg:=RECORD
    UNICODE organization;
  END;
   lAuthorGroups:=RECORD
    DATASET(lAuthorGroupAuthor) author_group_author;
    UNICODE author_group_afid;
    UNICODE author_group_dptid;
    UNICODE author_group_country;
    DATASET(lAffOrg) author_group_org;
    UNICODE author_group_city;
		UNICODE author_group_cetext;
  END;
   lGrants:=RECORD
    UNICODE grant_id;
    UNICODE grant_acronym;
    UNICODE grant_agency;
  END;
   lAbstract:=RECORD
    UNICODE abstract_original;
    UNICODE abstract_language;
    UNICODE abstract_perspective;
    UNICODE abstract_source;
    UNICODE abstract_publisher_copyright;
    UNICODE abstract;
  END;
   lSourceIssn:=RECORD
    UNICODE source_issn_type;
    UNICODE source_issn;
  END;
   lSourceIsbn:=RECORD
    UNICODE source_isbn_type;
    UNICODE source_isbn_lengh;
    UNICODE source_isbn_level;
    UNICODE source_isbn;
  END;
   lSourceWebsite:=RECORD
    UNICODE source_website_type;
    UNICODE source_website;
    UNICODE source_website_name;
  END;
   lSourceEditors:=RECORD
    UNICODE editor_initials;
    UNICODE editor_indexed_name;
    UNICODE editor_surname;
    UNICODE editor_given_name;
    UNICODE editor_role;
    UNICODE editor_type;
  END;
   lPublisher:=RECORD
    UNICODE publisher_name;
    UNICODE publisher_address;
    UNICODE publisher_affiliation;
  END;
   lConfName:={UNICODE conference_event_name;};
   lConfNumber:={UNICODE conference_event_number;};
   lConfTheme:={UNICODE conference_event_theme;};
   lConfTitle:={UNICODE conference_event_title;};
   lConfOrg:={UNICODE conference_organization;};
   lConfSponsor:={UNICODE conference_sponsor;};
   lConfEdOrg:={UNICODE conference_editor_organization;};
   lEnhDescriptors:=RECORD
    UNICODE descriptor_mainterm_weight;
    UNICODE descriptor_mainterm_candidate;
    UNICODE descriptor_mainterm_sort_pos;
    UNICODE descriptor_mainterm_code;
    UNICODE descriptor;
  END;
   lEnhDesc:=RECORD
    UNICODE descriptor_type;
    UNICODE descriptor_controlled;
    UNICODE descriptor_name;
    DATASET(lEnhDescriptors) descriptor;
  END;
   lEnhClasses:=RECORD
    UNICODE classification;
    UNICODE classification_code;
    UNICODE classification_description;
  END;
   lEnhClass:=RECORD
    UNICODE classification_type;
    DATASET(lEnhClasses) classifications;
  END;

   lAPITermType:=RECORD
    UNICODE major_term_indicator;
    UNICODE enh_API_ATM_role;
    UNICODE enh_API_ATM_group_indicator;
    UNICODE enh_API_ATM_sort_pos;
  END;
   lAPIAutoTermType:=RECORD
    UNICODE prefix;
    UNICODE postfix;
    UNICODE CAS_nr;
  END;
   lEnhAPICC:=RECORD
    UNICODE classification;
  END;
   lEnhAPILTM:=RECORD
    DATASET(lAPIAutoTermType) enh_API_LTM_auto;
  END;

   lVGroupTermGrp:=RECORD
    DATASET(lAPITermType) vgroup_term_grp;
  END;
   lSGroupTermGrp:=RECORD
    DATASET(lAPITermType) sgroup_term_grp;
  END;
   lEnhAPIVgroup:=RECORD
    DATASET(lAPITermType) vgroup_term;
    DATASET(lVGroupTermGrp) vgroup_termgroup;
  END;
   lEnhAPISgroup:=RECORD
    DATASET(lAPITermType) sgroup_term;
    DATASET(lSGroupTermGrp) sgroup_termgroup;
  END;
   lEnhAPIATM:=RECORD
    UNICODE enh_API_ATM_template_role;
    UNICODE enh_API_ATM_lt_count;
    DATASET(lAPITermType) enh_API_ATM_term;
    DATASET(lEnhAPIVgroup) enh_API_ATM_vgroup;
    DATASET(lEnhAPISgroup) enh_API_ATM_sgroup;
  END;
   lEnhAPIAMS:=RECORD
    DATASET(lAPITermType) API_AMS_term;
  END;
   lEnhAPIAPC:=RECORD
    DATASET(lAPITermType) API_APC_term;
  END;
   lEnhAPIANC:=RECORD
    DATASET(lAPITermType) API_ANC_term;
  END;
   lEnhAPIAT:=RECORD
    DATASET(lAPIAutoTermType) enh_API_AT_auto;
  END;
   lEnhAPICT:=RECORD
    DATASET(lAPIAutoTermType) enh_API_CT_auto;
  END;
   lEnhAPILT:=RECORD
    DATASET(lAPIAutoTermType) enh_API_LT_auto;
  END;
   lEnhAPICRN:=RECORD
    DATASET(lAPIAutoTermType) enh_API_CRN_auto;
  END;
   lManufacturers:=RECORD
    UNICODE manufacturer;
  END;
   lEnhMan:=RECORD
    UNICODE manufacturer_type;
    DATASET(lManufacturers) manufacturers;
  END;
   lTradename:=RECORD
    UNICODE tradename;
  END;
   lEnhTrade:=RECORD
    UNICODE tradename_type;
    DATASET(lTradename) tradenames;
  END;
   lChemRegNum:=RECORD
    UNICODE chemical_reg_num;
  END;
   lChemicals:=RECORD
    UNICODE chemical_name;
    DATASET(lChemRegNum) chemical_reg_nums;
  END;
   lEnhChem:=RECORD
    UNICODE chemical_source;
    DATASET(lChemicals) chemicals;
  END;

   lRefTitle:={UNICODE reference_title;};
   lRefItemIDs:=RECORD
    UNICODE ref_item_id_type;
    UNICODE ref_item_id;
  END;
   lRefAuthors:=RECORD
    UNICODE ref_author_sequence;
    UNICODE ref_author_initials;
    UNICODE ref_author_indexed_name;
    UNICODE ref_author_surname;
  END;
   lRefSourceTitle:={UNICODE reference_source_title;};
   lReferences:=RECORD
    UNICODE reference_id;
    DATASET(lRefTitle) reference_titles;
    DATASET(lRefItemIDs) reference_itemids;
    DATASET(lRefAuthors) reference_authors;
    DATASET(lRefSourceTitle) reference_source_titles;
    UNICODE reference_pub_year_first;
    UNICODE reference_pub_year_last;
    UNICODE reference_text;
    UNICODE reference_full_text;
  END;
  
   lDocs := RECORD
    UNICODE eid;
    UNICODE oeid;
    UNICODE content_type;
    UNICODE dbname;
    UNICODE online_status;
    UNICODE pui;
    UNICODE pii;
    UNICODE fulltext_eid;
    UNICODE issn;
    UNICODE volume;
    UNICODE issue;
    UNICODE author_surname;
    UNICODE author_first_initial;
    UNICODE first_page;
    UNICODE last_page;
    UNICODE sort_year;
    UNICODE pub_year;
    UNICODE timestamp;
    UNICODE group_id;
    DATASET(lRefIDs) ref_ids;
    DATASET(lAuthors) authors;
    UNICODE proc_date_delivered;
    UNICODE proc_sort_day;
    UNICODE proc_sort_month;
    UNICODE proc_sort_year;
    UNICODE proc_status_stage;
    UNICODE proc_status_state;
    UNICODE proc_status_type;
    DATASET(lCopyright) copyrights;
    UNICODE copyright_pii;
    UNICODE copyright_doi;
    DATASET(lItemIDs) item_ids;
    UNICODE history_create_day;
    UNICODE history_create_month;
    UNICODE history_create_year;
    UNICODE history_complete_day;
    UNICODE history_complete_month;
    UNICODE history_complete_year;
    UNICODE history_revise_day;
    UNICODE history_revise_month;
    UNICODE history_revise_year;
    DATASET(lCollections) collections;
    UNICODE external_source;
    DATASET(lCitationTypes) citation_types;
    DATASET(lCitationLang) citation_languages;
    DATASET(lAbstractLang) abstract_languages;
    DATASET(lPatentReg) patent_registrations;
    DATASET(lPatentApp) patent_applications;
    DATASET(lPatentPrioApp) patent_prioapp;
    DATASET(lPatentAssignee) patent_assignees;
    DATASET(lPatentCountry) patent_countries;
    DATASET(lPatentIPC) patent_ipc_codes;
    DATASET(lPatentInventor) patent_inventors;
    DATASET(lKeywords) author_keywords;
    UNICODE dummy_link_citation_type;
    UNICODE dummy_link_itemlink;
    UNICODE dummy_link_restricted_access;
    UNICODE figure_information;
    DATASET(lCitationTitle) citation_titles;
    DATASET(lAuthorGroups) author_groups;
    UNICODE corresp_initials;
    UNICODE corresp_indexed_name;
    UNICODE corresp_surname;
    UNICODE corresp_aff_country;
    DATASET(lAffOrg) corresp_aff_org;
    UNICODE corresp_aff_city;
    UNICODE corresp_e_address_type;
    UNICODE corresp_e_address;
    UNICODE grant_complete;
    DATASET(lGrants) grants;
    DATASET(lAbstract) abstracts;
    UNICODE source_country;
    UNICODE source_type;
    UNICODE source_id;
    UNICODE source_title;
    UNICODE source_title_preferred;
    UNICODE source_title_abbr;
    UNICODE source_title_translated;
    UNICODE source_title_volume;
    UNICODE source_title_issue;
    UNICODE source_issue_designation;
    DATASET(lSourceIssn) source_issns;
    DATASET(lSourceIsbn) source_isbns;
    UNICODE source_codencode;
    UNICODE source_part;
    UNICODE source_volisspag_issue;
    UNICODE source_volisspag_volume;
    UNICODE source_volisspag_first_page;
    UNICODE source_volisspag_last_page;
    UNICODE source_pub_year_first;
    UNICODE source_pub_year_last;
    DATASET(lSourceWebsite) source_websites;
    DATASET(lSourceEditors) source_editors;
    DATASET(lPublisher) publishers;
    UNICODE secondary_journal_title;
    UNICODE secondary_journal_title_abbrev;
    UNICODE secondary_journal_issn;
    UNICODE secondary_journal_pub_year_first;
    UNICODE secondary_journal_pub_year_last;
    UNICODE secondary_journal_volume;
    UNICODE secondary_journal_issue;
    DATASET(lConfName) conference_names;
    DATASET(lConfNumber) conference_numbers;
    DATASET(lConfTheme) conference_themes;
    DATASET(lConfTitle) conference_titles;
    UNICODE conference_location_venue;
    UNICODE conference_location_address_part;
    UNICODE conference_location_city_group;
    UNICODE conference_location_city;
    UNICODE conference_location_state;
    UNICODE conference_location_postal_code;
    UNICODE conference_location_country;
    UNICODE conference_date_start_year;
    UNICODE conference_date_start_month;
    UNICODE conference_date_start_day;
    UNICODE conference_date_end_year;
    UNICODE conference_date_end_month;
    UNICODE conference_date_end_day;
    DATASET(lConfOrg) conference_orgs;
    UNICODE conference_url;
    UNICODE conference_catnumber;
    UNICODE conference_code;
    UNICODE conference_sponsors_complete;
    DATASET(lConfSponsor) conference_sponsors;
    UNICODE conference_pub_editors;
    UNICODE conference_pub_address;
    DATASET(lConfEdOrg) conference_Ed_Orgs;
    UNICODE conference_pub_procpartno;
    UNICODE conference_pub_procpagerange;
    UNICODE conference_pub_procpagecount;
    UNICODE report_number;
    DATASET(lPatentReg) enhpat_registrations;
    DATASET(lPatentApp) enhpat_applications;
    DATASET(lPatentPrioApp) enhpat_prioapp;
    DATASET(lPatentAssignee) enhpat_assignees;
    DATASET(lPatentCountry) enhpat_countries;
    DATASET(lPatentIPC) enhpat_ipc_codes;
    DATASET(lPatentInventor) enhpat_inventors;
    DATASET(lEnhDesc) enhancement_descriptors;
    DATASET(lEnhAPICC) enh_API_CC;
    DATASET(lEnhAPILTM) enh_API_LTM_group;
    UNICODE enh_API_ALC_LTM_count;
    UNICODE enh_API_ALC_LT_count;
    DATASET(lEnhAPIATM) enh_API_ATM_group;
    DATASET(lEnhAPIAMS) enh_API_AMS;
    DATASET(lEnhAPIAPC) enh_API_APC;
    DATASET(lEnhAPIANC) enh_API_ANC;
    DATASET(lEnhAPIAT) enh_API_AT;
    DATASET(lEnhAPICT) enh_API_CT;
    DATASET(lEnhAPILT) enh_API_LT;
    DATASET(lEnhAPICRN) enh_API_CRN;
    DATASET(lEnhClass) classification_group;
    DATASET(lEnhMan) manufacturer_group;
    DATASET(lEnhTrade) tradename_group;
    DATASET(lEnhChem) chemical_group;
    UNICODE reference_count;
    DATASET(lReferences) references;
  END;
	
	EXPORT Articles := DATASET('~mpayne3::scopusdata::cleanpubs',lDocs,THOR);
	EXPORT Authors := Articles.authors;
	EXPORT AuthorGroup := Articles.author_groups;
	EXPORT AuthorGroupOrg := Articles.author_groups.author_group_org;
	EXPORT References := Articles.ref_ids;
	EXPORT Keywords := Articles.author_keywords;
	EXPORT Abstracts := Articles.abstracts;
	EXPORT ClassificationGroup := Articles.classification_group;
	EXPORT Classifications := Articles.classification_group.classifications;
	EXPORT CitationTitles := Articles.citation_titles;
	
END;