﻿IMPORT SBD14 as X;
IMPORT STD;
IMPORT * FROM ML;

EXPORT cleanforcluster := MODULE


EXPORT d01 := FUNCTION
	abstracts := X.ChemArticles.abstracts;

abstractRect := RECORD

	STRING abstract;

END;

Sanitize(UNICODE org) := FUNCTION

	//str := (STRING)R.organization;
	upper := std.Uni.ToUpperCase(org);
	noperiod := std.Uni.FindReplace(upper,'.','');
	nodash := std.Uni.FindReplace(noperiod,'-',' ');
	nocomma := std.Uni.FindReplace(nodash,',','');
	nolp := std.Uni.FindReplace(nocomma,'(','');
	norp := std.Uni.FindReplace(nolp,')','');
	//nopossess := std.Uni.FindReplace(norp,'\'s','');
	noapos := std.Uni.FindReplace(norp,'\'','');
	noslash := std.Uni.FindReplace(noapos,'/','');
	nocolon := std.Uni.FindReplace(noslash,':','');
	//unistr := (UNICODE)nopossess;
	//SELF.organization := std.Uni.CleanAccents(noslash);
	//SELF := R;//std.Uni.CleanAccents(noslash);
	//noxmlspace := REGEXREPLACE(u'<[a-zA-Z0-9=\" \t]+>', nocolon, u'');
	noxml := REGEXREPLACE(u'<[a-zA-Z0-9=\" \t]+>[ ]*', nocolon, u'');
	noexspace := REGEXREPLACE(u'[ ]+', noxml, u' ');
	RETURN noexspace;

END;


ML.Docs.Types.Raw getText(abstracts Le) := TRANSFORM
	
	// cleantxt :=(STRING)Sanitize(Le.abstract);
		
	SELF.txt :=(STRING)Sanitize(Le.abstract); 

END;


abstract := PROJECT(abstracts, getText(LEFT));

	WordsRec := record
		string w;
	end;
	
// Combine the stop words into a single regex pattern
WordsRec CombineStopWords(WordsRec l, WordsRec r) := TRANSFORM
    SELF.w := l.w + IF(l.w != '', '|', '') + r.w;
END;

combinedStopWords := ROLLUP(Docs.stopwords,TRUE,CombineStopWords(LEFT,RIGHT));

stopWordRegex := '\\b(' + combinedStopWords[1].w + ')\\b';

//OUTPUT(stopWordRegex,NAMED('stopWordRegex'));

// Apply the regex against the data
ML.Docs.Types.Raw RemoveStopWords(ML.Docs.Types.Raw l) := TRANSFORM
    SELF.txt := REGEXREPLACE(stopWordRegex,l.txt,'',NOCASE);
    SELF := l;
END;

newDes := PROJECT(abstract,RemoveStopWords(LEFT));


enumabs := Docs.Tokenize.Enumerate(newDes);
//enumabs;
dSplit:= Docs.Tokenize.Split(Docs.Tokenize.Clean(enumabs));

//dSplit;

dLexicon:=ML.Docs.Tokenize.Lexicon(dSplit);

//dLexicon;

dReplaced:=ML.Docs.Tokenize.ToO(dSplit,dLexicon);

//dReplaced;

tfidf := Docs.Trans(dReplaced).TfIdf();
//tfidf;
// sReplaced:= DEDUP(SORT(dReplaced,id,word),id,word);
// sReplaced;

ML.Types.NumericField Representation(tfidf Le):= TRANSFORM

	SELF.id := Le.id;
	SELF.number := Le.word_id;
	SELF.value := Le.tf_idf;

END;

	RETURN PROJECT(tfidf, Representation(LEFT));

END;


//SORT(d01,-value, number);
//maxnum := MAX(d01,number);



EXPORT scent := FUNCTION

TestSize :=MAX(d01,number);
maxval := MAX(d01,value);
//TestSize;
//maxval;

a1 := ML.Distribution.Uniform(0,maxval,10000);
a2 := ML.Distribution.Uniform(0,maxval,10000);
a3 := ML.Distribution.Uniform(0,maxval,10000);
a4 := ML.Distribution.Uniform(0,maxval,10000);
a5 := ML.Distribution.Uniform(0,maxval,10000);
a6 := ML.Distribution.Uniform(0,maxval,10000);
a7 := ML.Distribution.Uniform(0,maxval,10000);
a8 := ML.Distribution.Uniform(0,maxval,10000);
a9 := ML.Distribution.Uniform(0,maxval,10000);
a10 := ML.Distribution.Uniform(0,maxval,10000);
cen01 := ML.Distribution.GenData(TestSize,a1,1); // Field 1 Uniform
cen02 := ML.Distribution.GenData(TestSize,a2,2); // Field 1 Uniform
cen03 := ML.Distribution.GenData(TestSize,a3,3); // Field 1 Uniform
cen04 := ML.Distribution.GenData(TestSize,a4,4); // Field 1 Uniform
cen05 := ML.Distribution.GenData(TestSize,a5,5); // Field 1 Uniform
cen06 := ML.Distribution.GenData(TestSize,a6,6); // Field 1 Uniform
cen07 := ML.Distribution.GenData(TestSize,a7,7); // Field 1 Uniform
cen08 := ML.Distribution.GenData(TestSize,a8,8); // Field 1 Uniform
cen09 := ML.Distribution.GenData(TestSize,a9,9); // Field 1 Uniform
cen10 := ML.Distribution.GenData(TestSize,a10,10); // Field 1 Uniform

cents := cen01+cen02+cen03+cen04+cen05+cen06+cen07+cen08+cen09+cen10;

ML.Types.NumericField makeright(cents Le) := TRANSFORM

	SELF.id := Le.number;
	SELF.number := Le.id;
	SELF := Le;

END;
RETURN PROJECT(cents,makeright(LEFT));
END;
//OUTPUT(scent,,'mpayne3::centroids',OVERWRITE);

//fX3 := ML.Cluster.Kmeans(d01, scent, 10, 0.3);

//fX3.result();
END;