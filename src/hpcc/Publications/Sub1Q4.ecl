﻿IMPORT $;
IMPORT STD;
IMPORT Publications.ExposedPubs as Pubs;
IMPORT Top500.File_Top500 as Top500;
IMPORT NRC;
IMPORT NSF.File_NSF as NSF;
IMPORT Publications.ArticleClass as ArticleClass;
//IMPORT $.ArticleClass as ArticleClass;
//EXPORT VHR_Chem(INTEGER cc) := FUNCTION
OrgInstDCT := DICTIONARY($.VHR_Chem,{organization => $.VHR_Chem});
//CodeColorDCT := DICTIONARY(ColorCodes,{Code => Color});

Sanitize(UNICODE org) := FUNCTION

	//str := (STRING)R.organization;
	upper := std.Uni.ToUpperCase(org);
	noperiod := std.Uni.FindReplace(upper,'.','');
	nodash := std.Uni.FindReplace(noperiod,'-',' ');
	nocomma := std.Uni.FindReplace(nodash,',','');
	nolp := std.Uni.FindReplace(nocomma,'(','');
	norp := std.Uni.FindReplace(nolp,')','');
	//nopossess := std.Uni.FindReplace(norp,'\'s','');
	noapos := std.Uni.FindReplace(norp,'\'','');
	noslash := std.Uni.FindReplace(noapos,'/',' ');
	//unistr := (UNICODE)nopossess;
	//SELF.organization := std.Uni.CleanAccents(noslash);
	//SELF := R;//std.Uni.CleanAccents(noslash);
	RETURN noslash;

END;


artRec := RECORD

	prepdict.organization;
	prepdict.unitid;
	INTEGER cnt;

END;

artRec addCol(dict Le) := TRANSFORM

	SELF.cnt := 0;
	SELF := Le;

END;

forroll := PROJECT(dict, addCol(LEFT));
//forroll;
year := (UNICODE)2006;
//year;
MyArticles := Pubs.Articles(pub_year = year);
MyChemArticles := MyArticles(ArticleClass.Chemistry > 0);

slimpubrec := RECORD

	{Pubs.AuthorGroup} author_groups;
	//MyChemArticle.author_groups.author_group_author;

END;



// slimpubrec makeSlim(MyChemArticle Le) := TRANSFORM
	
	// SELF.author_groups := NORMALIZE(Le,LEFT.author_groups, getAuthorGrps(RIGHT));

// END;

// MyChemArticles :=  PROJECT(MyChemArticle,makeslim(LEFT));


mycnt(STRING org) := FUNCTION

	RETURN COUNT(Pubs.AuthorGroupOrg((STRING)Sanitize(organization) = org));

END;
artRec rollthem(forroll Le, forroll Ri, INTEGER C) := TRANSFORM
	
	SELF.cnt := IF(Le.unitid = Ri.unitid,Le.cnt + COUNT(MyChemArticles(mycnt(Ri.organization)>0)), COUNT(MyChemArticles(mycnt(Ri.organization)>0))) ;
	SELF := Ri;

END;

allcounts := ITERATE(forroll,rollThem(LEFT,RIGHT,COUNTER));
allcounts;
over50all := allcounts(cnt >=50);
srtall := SORT(over50all,unitid,-cnt);
srtall;
over50 := DEDUP(srtall,unitid);
//over50 := ddall(cnt >=50);
over50;

mynrcrec := RECORD

	STRING institutionName;

END;

mynrcrec getnames(over50 Le, NRC.NRC_IPEDS Ri) := TRANSFORM
	SELF := Ri;
END;

awardees := JOIN(over50, NRC.NRC_IPEDS, LEFT.unitid = RIGHT.unitid, getNames(LEFT,RIGHT));
myDCT := DICTIONARY(awardees,{institutionName => awardees});
awards := NSF.Awards((UNSIGNED)awardeffectivedate[7..] = 2006);
//top;
//awards;
//count(top);

slimGrants := FUNCTION
	//dict := nrcAssistNames(assist = assistcode);
	//myDCT := DICTIONARY(dict,{name => assistcode});
	RETURN COUNT(NSF.Institutions(std.Str.toUpperCase((STRING)name) IN myDCT));
	//RETURN COUNT(NSF.Institutions(std.Str.toUpperCase((STRING)name) = ini));
END;

getGrants(STRING ini) := FUNCTION
	//dict := nrcAssistNames(assist = assistcode);
	//myDCT := DICTIONARY(dict,{name => assistcode});
	//RETURN COUNT(NSF.Institutions(std.Str.toUpperCase((STRING)name) IN myDCT));
	RETURN COUNT(NSF.Institutions(std.Str.toUpperCase((STRING)name) = ini));
END;

gethpcc := FUNCTION
	//dict := nrcAssistNames(assist = assistcode);
	//myDCT := DICTIONARY(dict,{name => assistcode});
	//RETURN COUNT(NSF.Institutions(std.Str.toUpperCase((STRING)name) IN myDCT));
	RETURN COUNT(NSF.ProgramReferences(code = 'HPCC'));
END;

slimawards := awards(slimgrants >0);
//COUNT(slimawards);

aveRec := RECORD

		//Top500.File;
		INTEGER funding;

END;
hpcc := slimawards(gethpcc > 0);
//hpcc;

aveRec myFunding(awardees Le) := TRANSFORM
	//myprevious := awards
	//myawards := slimawards(getgrants(Le.institution) >0);
	//myhpccawards := myawards(gethpcc > 0);
	SELF.funding := SUM(slimawards(getgrants(Le.institutionName)>0),(INTEGER)awardamount);
	//SELF := Le;

END;


//EXPORT Query3 := 
numawards := PROJECT(awardees,myFunding(LEFT));
//EXPORT Discipline := TABLE(Pubs.Articles(ArticleClass.Chemistry > 0 OR ArticleClass.ComputerScience > 0 OR ArticleClass.Economics > 0), r, pub_year);
//AVE(numawards,funding);
//RETURN COUNT(Pubs.AuthorGroupOrg((STRING)Sanitize(organization) IN OrgInstDCT));
//COUNT(Pubs.AuthorGroupOrg((STRING)Sanitize(organization) IN OrgInstDCT));
//END;