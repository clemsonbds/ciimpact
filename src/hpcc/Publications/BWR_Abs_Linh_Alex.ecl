﻿IMPORT $,std;
IMPORT $.ExposedPubs as Pubs;
IMPORT $.ArticleClass as ArticleClass;

testrec := RECORD

	Pubs.Articles.eid;
	Pubs.Articles.pub_year;
	//Pubs.Authors.indexed_name;
	//Pubs.Authors.author_id;
	Pubs.Abstracts.abstract;

END;

// testrec ext():= TRANSFORM

	

// END;
gotabstract := TABLE(Pubs.Abstracts,testrec);
chempubs := Pubs.Articles(ArticleClass.Chemistry > 0);
cspubs := Pubs.Articles(ArticleClass.ComputerScience > 0);
econpubs := Pubs.Articles(ArticleClass.Economics > 0);

Sanitize(UNICODE org) := FUNCTION

	//str := (STRING)R.organization;
	upper := std.Uni.ToUpperCase(org);
	noperiod := std.Uni.FindReplace(upper,'.','');
	nodash := std.Uni.FindReplace(noperiod,'-',' ');
	nocomma := std.Uni.FindReplace(nodash,',','');
	nolp := std.Uni.FindReplace(nocomma,'(','');
	norp := std.Uni.FindReplace(nolp,')','');
	//nopossess := std.Uni.FindReplace(norp,'\'s','');
	noapos := std.Uni.FindReplace(norp,'\'','');
	noslash := std.Uni.FindReplace(noapos,'/','');
	nocolon := std.Uni.FindReplace(noslash,':','');
	//unistr := (UNICODE)nopossess;
	//SELF.organization := std.Uni.CleanAccents(noslash);
	//SELF := R;//std.Uni.CleanAccents(noslash);
	//noxmlspace := REGEXREPLACE(u'<[a-zA-Z0-9=\" \t]+>', nocolon, u'');
	noxml := REGEXREPLACE(u'<[a-zA-Z0-9=\" \t]+>[ ]*', nocolon, u'');
	noexspace := REGEXREPLACE(u'[ ]+', noxml, u' ');
	RETURN noexspace;

END;

testrec2 := RECORD

	STRING eid;
	STRING pub_year;
	STRING abstract;
	//Pubs.Authors.indexed_name;
	//Pubs.Authors.author_id;
	//Pubs.AuthorGroupOrg.organization;

END;

testrec2 JoinThem(Pubs.Articles Le, gotabstract Ri):= TRANSFORM

	SELF.eid := (STRING)Le.eid;
	SELF.pub_year := (STRING)Le.pub_year;
	SELF.abstract := (STRING)(Sanitize(Ri.abstract));

END;

ChemAbsInfo := JOIN(chempubs,gotabstract, LEFT.eid=RIGHT.eid AND LEFT.pub_year=RIGHT.pub_year, JoinThem(LEFT,RIGHT));
CSAbsInfo := JOIN(cspubs,gotabstract, LEFT.eid=RIGHT.eid AND LEFT.pub_year=RIGHT.pub_year, JoinThem(LEFT,RIGHT));
EconAbsInfo := JOIN(econpubs,gotabstract, LEFT.eid=RIGHT.eid AND LEFT.pub_year=RIGHT.pub_year, JoinThem(LEFT,RIGHT));

OUTPUT(chemabsinfo,,'~mpayne3::out::ChemistryAbstracts',CSV(SEPARATOR(','),TERMINATOR('\n')),OVERWRITE);
OUTPUT(csabsinfo,,'~mpayne3::out::ComputerScienceAbstracts',CSV(SEPARATOR(','),TERMINATOR('\n')),OVERWRITE);
OUTPUT(econabsinfo,,'~mpayne3::out::EconomicsAbstracts',CSV(SEPARATOR(','),TERMINATOR('\n')),OVERWRITE);

//OUTPUT('test');