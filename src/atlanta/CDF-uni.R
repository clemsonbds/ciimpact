#### preparing environments ####################################################
rm(list=ls(all=TRUE))
set.seed(42)
options(digits=4)
cat("\014")   # This is to clear the console ..



#### load libraries and set up directory paths #################################
require(FEAR)

work.dir <- file.path("D:","Work","Dropbox","R","ciimpact")
#work.dir <- file.path("/home","lngo","Dropbox","R","ciimpact")
setwd(work.dir)

lib.dir <- file.path(work.dir,"lib")
data.dir <- file.path(work.dir,"data","ncses","clean")
graph.dir <- file.path(work.dir,"graphs")

#### load soure files ##########################################################
source(file.path(lib.dir,"calculateDEA.R"))
source(file.path(lib.dir,"genGraph.R"))
source(file.path(lib.dir,"modifiedDensity.R"))
source(file.path(lib.dir,"meanDifferenceTest.R"))
source(file.path(lib.dir,"rtsTest.R"))
source(file.path(lib.dir,"convTest.R"))

setwd(data.dir)
#### read in the input set: Faculty Count and Federal Expenditures #############
faculty.data <- read.csv("Faculty-VHR-HR_2000_2012.txt",header=TRUE,sep="\t",stringsAsFactors=FALSE)
expenditures.data <-  read.csv("h3.txt",header=TRUE,sep="\t",stringsAsFactors=FALSE)

#### read in the output set: PhD Graduated and Publication Count ###############
phd.data <-  read.csv("s1.txt",header=TRUE,sep="\t",stringsAsFactors=FALSE)
publication.data <- read.csv("publication_vhr_hr_2000_2009.txt",header=TRUE,sep="\t",stringsAsFactors=FALSE)


#### read in control data ######################################################
control.data <- read.csv("Institution-Control.txt",header=TRUE,sep="\t",stringsAsFactors=FALSE)

# expenditure data: 2011 - 2003
# faculty data: 2000 - 2012
# phd data: 2009 - 2000
# publication data: 2000 - 2009

common.fice <- as.data.frame(table(c(unique(expenditures.data$FICE),
                                     unique(phd.data$FICE),
                                     faculty.data$FICE,
                                     publication.data$FICE)))

common.fice <- as.vector(subset(common.fice$Var1, common.fice$Freq == 4))
common.fice <- subset(common.fice,common.fice != "2077") # remove John Hopkins as an outlier


dea.expenditures <- subset(expenditures.data, expenditures.data$FICE %in% common.fice)
dea.faculty <- subset(faculty.data, faculty.data$FICE %in% common.fice)
dea.phd <- subset(phd.data, phd.data$FICE %in% common.fice)
dea.publication <- subset(publication.data, publication.data$FICE %in% common.fice)

sum.expenditures <- numeric(length(common.fice))
sum.faculty <- numeric(length(common.fice))
sum.phd <- numeric(length(common.fice))
sum.publication <- numeric(length(common.fice))

input.set <- c(2,3)
output.set <- c(4,5)
orientation <- 2

list.faculty <- list()
list.expenditures <- list()
list.phd <- list()
list.publication <- list()

for (i in 1:length(seq(2003:2009))){
#  print(2002+i)
  input.1 <- dea.faculty[,i+4]    
  input.2 <- numeric(length(common.fice))  
  exp.index <- length(dea.expenditures)
  for (j in 1:length(common.fice)){
    input.2[j] <- sum(as.numeric(as.vector(subset(dea.expenditures[,exp.index-i+1], 
                                                  dea.expenditures$FICE == common.fice[j]))))    
  }
  
  output.1 <- dea.publication[,i+5]
  output.2 <- numeric(length(common.fice))
  phd.index <- length(dea.phd) - 3
  for (j in 1:length(common.fice)){
    output.2[j] <- sum(as.numeric(as.vector(subset(dea.phd[,phd.index-i+1], 
                                                   dea.phd$FICE == common.fice[j]))))    
  }
  
  annual.data <- data.frame(FICE=common.fice,
                            faculty=input.1,
                            expenditures=input.2,
                            publication=output.1,
                            phd=output.2)
  
  annual.data <- merge(x=annual.data,by="FICE",y=control.data,all.x=TRUE)
  annual.data$APP <- numeric(length(annual.data$FICE))
  for (j in 1:length(annual.data$APP)){
    if (annual.data$CC[j] == "Research Universities (very high research activity)"){
      annual.data$APP[j] <- 1
    }
    if (annual.data$CC[j] == "Research Universities (high research activity)"){
      annual.data$APP[j] <- 0
    }
  }
  # removes 0s
  annual.data <- subset(annual.data, annual.data$expenditures !=0)
  annual.data <- subset(annual.data, annual.data$phd != 0)   
  
  list.faculty[[i]] <- annual.data$faculty
  list.expenditures[[i]] <- annual.data$expenditures
  list.phd[[i]] <- annual.data$phd
  list.publication[[i]] <- annual.data$publication
}

gen.cdf <- function(list.data,xlab){
  color <- rainbow(7)
  pch <- seq(19,25)
  lty <- seq(1,7)
  plot(ecdf(list.data[[1]]),main="",
       verticals=TRUE,do.points=FALSE,lwd=1,
       ylab="",xlab=xlab,col="black", lty=lty[1],pch=pch[1])
  index <- c(3,5,7)
  for (i in seq(3,7,2)){
    lines(ecdf(list.data[[i]]),do.points=FALSE,verticals=TRUE,
          lwd=1,col="black", lty=lty[i],pch=pch[i])
  }
  legend("bottomright", legend=seq(2003,2009,2), cex=1, lty=lty,pch=pch, bty="n") 
}
par( mfrow = c(2,2), mar = c(4,6,1,1))
gen.cdf(list.faculty,"Faculty [NCSES]")
gen.cdf(list.expenditures,"Expenditures [NCSES]")
gen.cdf(list.phd,"PhD Graduates[WebCASPAR]")
gen.cdf(list.publication,"Publication Count [ISI]")

