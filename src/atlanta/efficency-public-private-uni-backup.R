#### preparing environments ####################################################
rm(list=ls(all=TRUE))
options(digits=4)


#### load libraries and set up directory paths #################################
require(FEAR)
require(xlsx)

#### library function for ESPCOR study #########################################
################################################################################
################################################################################

mean.diff.dea <- function(input.data, input.set, output.set, orientation, rts){
  # split data into VHR and HR
  input.hpc <- subset(input.data, input.data$Type == "Public")
  input.nonhpc <- subset(input.data, input.data$Type == "Private")
  
  output.all <- get.dea(input.data,input.set,output.set,orientation, rts)
  
  n.hpc <- length(input.hpc[,1])
  output.hpc <- get.dea(input.hpc,input.set,output.set,orientation, rts)
  output.hpc.1 <- get.dea(input.hpc[1:(n.hpc%/%2),],input.set,output.set,orientation, rts)
  output.hpc.2 <- get.dea(input.hpc[(n.hpc%/%2 + 1):n.hpc,],input.set,output.set,orientation, rts)
  
  n.nonhpc <- length(input.nonhpc[,1])
  output.nonhpc <- get.dea(input.nonhpc,input.set,output.set,orientation, rts)
  output.nonhpc.1 <- get.dea(input.nonhpc[1:(n.nonhpc%/%2),],input.set,output.set,orientation, rts)
  output.nonhpc.2 <- get.dea(input.nonhpc[(n.nonhpc%/%2 + 1):n.nonhpc,],input.set,output.set,orientation, rts)
  
  if (orientation != 2){
    output.hpc$DEA <- 1/output.hpc$DEA
    output.hpc.1$DEA <- 1/output.hpc.1$DEA
    output.hpc.2$DEA <- 1/output.hpc.2$DEA
    output.nonhpc$DEA <- 1/output.nonhpc$DEA
    output.nonhpc.1$DEA <- 1/output.nonhpc.1$DEA
    output.nonhpc.2$DEA <- 1/output.nonhpc.2$DEA
    output.all$DEA <- 1/output.all$DEA
  }
  
  mu.hpc <- sum(output.hpc$DEA) / n.hpc
  mu.hpc.half <- 0.5 * (sum(output.hpc.1$DEA) / (n.hpc%/%2) + sum(output.hpc.2$DEA) / (n.hpc - n.hpc%/%2))
  
  mu.nonhpc <- sum(output.nonhpc$DEA) / n.nonhpc
  mu.nonhpc.half <- 0.5 * (sum(output.nonhpc.1$DEA) / (n.nonhpc%/%2) + sum(output.nonhpc.2$DEA) / (n.nonhpc - n.nonhpc%/%2))
  
  if (rts == 1){
    # variable return to scale
    k.rate <- 2 / (length(input.set) + length(output.set) + 1)
  } else if (rts == 3){ 
    # constant return to scale
    k.rate <- 2 / (length(input.set) + length(output.set))
  }
  
  beta.hpc <- (mu.hpc.half - mu.hpc) / (2^k.rate - 1)
  beta.nonhpc <- (mu.nonhpc.half - mu.nonhpc) / (2^k.rate - 1)
  
  mu.all <- sum(output.all$DEA) / length(output.all$DEA)
  
  phi.hpc <- sum((output.hpc$DEA - mu.all)^2) / n.hpc
  phi.nonhpc <- sum((output.nonhpc$DEA - mu.all)^2) / n.nonhpc
  
  T.hat <- ((mu.hpc - mu.nonhpc) - (beta.hpc - beta.nonhpc)) / ((((phi.hpc)^2)/n.hpc + ((phi.nonhpc)^2)/n.nonhpc)^0.5) 
  
  tmp1 <- pnorm(T.hat,lower.tail=FALSE,log.p=TRUE)
  tmp2 <- tmp1 / log(10)
  rho <- 10^tmp2
  rho
}



mean.diff.fdh <- function(input.data, input.set, output.set, orientation){
  # split data into VHR and HR
  input.hpc <- subset(input.data, input.data$Type == "Public")
  input.nonhpc <- subset(input.data, input.data$Type == "Private")
  
  output.all <- get.fdh(input.data,input.set,output.set,orientation)  
  n.hpc <- length(input.hpc[,1])
  output.hpc <- get.fdh(input.hpc,input.set,output.set,orientation)
  output.hpc.1 <- get.fdh(input.hpc[1:(n.hpc%/%2),],input.set,output.set,orientation)
  output.hpc.2 <- get.fdh(input.hpc[(n.hpc%/%2 + 1):n.hpc,],input.set,output.set,orientation)
  
  n.nonhpc <- length(input.nonhpc[,1])
  output.nonhpc <- get.fdh(input.nonhpc,input.set,output.set,orientation)
  output.nonhpc.1 <- get.fdh(input.nonhpc[1:(n.nonhpc%/%2),],input.set,output.set,orientation)
  output.nonhpc.2 <- get.fdh(input.nonhpc[(n.nonhpc%/%2 + 1):n.nonhpc,],input.set,output.set,orientation)
  
  if (orientation != 2){
    output.hpc$DEA <- 1/output.hpc$DEA
    output.hpc.1$DEA <- 1/output.hpc.1$DEA
    output.hpc.2$DEA <- 1/output.hpc.2$DEA
    output.nonhpc$DEA <- 1/output.nonhpc$DEA
    output.nonhpc.1$DEA <- 1/output.nonhpc.1$DEA
    output.nonhpc.2$DEA <- 1/output.nonhpc.2$DEA
    output.all$DEA <- 1/output.all$DEA
  }
  
  mu.hpc <- sum(output.hpc$DEA) / n.hpc
  mu.hpc.half <- 0.5 * (sum(output.hpc.1$DEA) / (n.hpc%/%2) + sum(output.hpc.2$DEA) / (n.hpc - n.hpc%/%2))
  
  mu.nonhpc <- sum(output.nonhpc$DEA) / n.nonhpc
  mu.nonhpc.half <- 0.5 * (sum(output.nonhpc.1$DEA) / (n.nonhpc%/%2) + sum(output.nonhpc.2$DEA) / (n.nonhpc - n.nonhpc%/%2))
  
  k.rate <- 1 / (length(input.set) + length(output.set) + 1)
  
  beta.hpc <- (mu.hpc.half - mu.hpc) / (2^k.rate - 1)
  beta.nonhpc <- (mu.nonhpc.half - mu.nonhpc) / (2^k.rate - 1)
  
  mu.all <- sum(output.all$DEA) / length(output.all$DEA)
  
  phi.hpc <- sum((output.hpc$DEA - mu.all)^2) / n.hpc
  phi.nonhpc <- sum((output.nonhpc$DEA - mu.all)^2) / n.nonhpc
  
  # new FDH elements
  n.hpc.fdh <- floor((length(input.hpc[,1]))^(2*k.rate))  
  output.hpc.fdh <- get.fdh(input.hpc,input.set,output.set,orientation)
  
  n.nonhpc.fdh <- floor((length(input.nonhpc[,1]))^(2*k.rate))
  output.nonhpc.fdh <- get.fdh(input.nonhpc,input.set,output.set,orientation)
  
  mu.hpc.fdh <- sum(sample(output.hpc.fdh$DEA,n.hpc.fdh)) / n.hpc.fdh
  mu.nonhpc.fdh <- sum(sample(output.nonhpc.fdh$DEA,n.hpc.fdh)) / n.nonhpc.fdh
  
  T.hat <- ((mu.hpc.fdh - mu.nonhpc.fdh) - (beta.hpc - beta.nonhpc)) / 
    ((((phi.hpc)^2)/n.hpc.fdh + ((phi.nonhpc)^2)/n.nonhpc.fdh)^0.5) 
  
  tmp1 <- pnorm(T.hat,lower.tail=FALSE,log.p=TRUE)
  tmp2 <- tmp1 / log(10)
  rho <- 10^tmp2
  rho
}

################################################################################
################################################################################
################################################################################
################################################################################


work.dir <- file.path("D:","Work","R","ciimpact")
data.dir <- file.path(work.dir,"data","ncses","clean")
lib.dir <- file.path(work.dir,"lib")

source(file.path(lib.dir,"rtsTest.R"))
source(file.path(lib.dir,"convTest.R"))
source(file.path(lib.dir,"calculateDEA.R"))
source(file.path(lib.dir,"modifiedDensity.R"))
source(file.path(lib.dir,"genGraph.R"))


setwd(data.dir)
#### read in the input set: Faculty Count and Federal Expenditures #############
faculty.data <- read.xlsx("Faculty-VHR-HR_2000_2012.xlsx",sheetIndex=1,header=TRUE,stringsAsFactors=FALSE)
expenditures.data <-  read.csv("h3.txt",header=TRUE,sep="\t",stringsAsFactors=FALSE)

#### read in the output set: PhD Graduated and Publication Count ###############
phd.data <-  read.csv("s1.txt",header=TRUE,sep="\t",stringsAsFactors=FALSE)
publication.data <- read.xlsx("publication_vhr_hr_2000_2009.xlsx",sheetIndex=1,header=TRUE,stringsAsFactors=FALSE)


#### read in control data ######################################################
control.data <- read.xlsx("Institution-Control.xlsx",sheetIndex=1,header=TRUE,stringsAsFactors=FALSE)

# expenditure data: 2011 - 2003
# faculty data: 2000 - 2012
# phd data: 2009 - 2000
# publication data: 2000 - 2009

common.fice <- as.data.frame(table(c(unique(expenditures.data$FICE),
                                     unique(phd.data$FICE),
                                     faculty.data$FICE,
                                     publication.data$FICE)))

common.fice <- as.vector(subset(common.fice$Var1, common.fice$Freq == 4))
common.fice <- subset(common.fice,common.fice != "2077") # remove John Hopkins as an outlier


dea.expenditures <- subset(expenditures.data, expenditures.data$FICE %in% common.fice)
dea.faculty <- subset(faculty.data, faculty.data$FICE %in% common.fice)
dea.phd <- subset(phd.data, phd.data$FICE %in% common.fice)
dea.publication <- subset(publication.data, publication.data$FICE %in% common.fice)

sum.expenditures <- numeric(length(common.fice))
sum.faculty <- numeric(length(common.fice))
sum.phd <- numeric(length(common.fice))
sum.publication <- numeric(length(common.fice))

for (i in 1:length(seq(2003:2009))){
  input.1 <- dea.faculty[,i+4]  
  
  input.2 <- numeric(length(common.fice))  
  exp.index <- length(dea.expenditures)
  for (j in 1:length(common.fice)){
    input.2[j] <- sum(as.numeric(as.vector(subset(dea.expenditures[,exp.index-i+1], dea.expenditures$FICE == common.fice[j]))))    
  }
  
  output.1 <- dea.publication[,i+5]
  output.2 <- numeric(length(common.fice))
  phd.index <- length(dea.phd) - 3
  for (j in 1:length(common.fice)){
    output.2[j] <- sum(as.numeric(as.vector(subset(dea.phd[,phd.index-i+1], dea.phd$FICE == common.fice[j]))))    
  }
  
  sum.faculty <- sum.faculty + input.1
  sum.expenditures <- sum.expenditures + input.2
  
  sum.phd <- sum.phd + output.2
  sum.publication <- sum.publication + output.1
}

color <- rainbow(7)
pch <- seq(19,25)
lty <- seq(1,7)

input.data <- data.frame(FICE=common.fice,
                         faculty=sum.faculty,
                         expenditures=sum.expenditures,
                         publication=sum.publication,
                         phd=sum.phd)

input.data <- merge(x=input.data,by="FICE",y=control.data,all.x=TRUE)
input.set <- c(2,3)
output.set <- c(4,5)
orientation <- 2

legend.stat <- c()
# the first test is the convexity test
pval.convex <- get.dea.convex(input.data,input.set,output.set)$pval
legend.stat <- c(legend.stat,paste("Convexity Test:",format(pval.convex,digits=4),sep=""))

if (pval.convex <= 0.10){
  # if we reject convexity, we go straight to mean difference test and then
  # stochastic dominance test, both using fdh          
  rho <- mean.diff.fdh (input.data, input.set, output.set, orientation)
  legend.stat <- c(legend.stat,paste("Mean Equivalent Test:",format(rho,digits=4),sep=""))
  
  # Efficiency estimation is done using FDH
  output.all <- get.fdh(input.data,input.set,output.set,orientation)     
  output.hpc <- subset(output.all, output.all$Type == "Public")    
  output.nonhpc <- subset(output.all, output.all$Type == "Private")    
} else {
  # if we don't reject convexity, we now test for return-to-scale
  pval.rts <- get.dea.rts(input.data,input.set,output.set)$pval
  print(paste("RTS Test:",pval.rts,sep=""))
  legend.stat <- c(legend.stat,paste("RTS Test:",format(pval.rts,digits=4),sep=""))
  
  if (pval.rts <= 0.10){
    rts <- 1
  } else {
    rts <- 3
  }      
  rho <- mean.diff.dea (input.data, input.set, output.set, orientation, rts)
  legend.stat <- c(legend.stat,paste("Mean Equivalent Test:",format(rho,digits=4),sep=""))
  
  # Efficiency estimation is done using FDH
  output.all <- get.dea(input.data,input.set,output.set,orientation,rts)     
  output.hpc <- subset(output.all, output.all$Type == "Public")    
  output.nonhpc <- subset(output.all, output.all$Type == "Private")     
}

densHPC <- get.density(output.hpc$DEA)
densNonHPC <- get.density(output.nonhpc$DEA)

firstLegend = "Public"
secondLegend = "Private"  
secondLegend = c(secondLegend,legend.stat)


ks.result <- ks.test(output.hpc$DEA, output.nonhpc$DEA,alternative="less")
print(paste("Number of institutions   : ",length(output.all$DEA),sep=""))
print(paste("Public     : ",length(output.hpc$DEA),sep=""))  
print(paste("Private         : ",length(output.nonhpc$DEA),sep=""))

print(paste("Mean Difference Test     : ",rho,sep=""))
if (rho <= 0.10){
  print(paste("Stochastic Dominance Test: ",ks.result$p.value,sep=""))
  legend.stat <- c(legend.stat,paste("Stochastic Dominance Test:",format(ks.result$p.value,digits=4),sep=""))      
} 
graphName=""
gen.plot(densHPC,densNonHPC,2.0,graphName,firstLegend,secondLegend)


plot(ecdf(output.hpc$DEA),verticals=TRUE,do.points=FALSE,lwd=2,main="",lty=1,
     ylab="CDF ",xlab="Efficiency")
lines(ecdf(output.nonhpc$DEA),verticals=TRUE,
      lwd=1,lty=2,do.points=FALSE)
legend("bottom", legend=c(firstLegend,secondLegend), cex=0.8, lty=c(1,2,0,0,0,0), bty="n")
