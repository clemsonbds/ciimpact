#### preparing environments ####################################################
cat("\014")   # This is to clear the console ..
rm(list=ls(all=TRUE))
set.seed(42)
options(digits=4)


#### load libraries and set up directory paths #################################
require(FEAR)
require(xlsx)

work.dir <- file.path("D:","Work","Dropbox","R","ciimpact")
#work.dir <- file.path("/home","lngo","Dropbox","R","ciimpact")
setwd(work.dir)

lib.dir <- file.path(work.dir,"lib")
data.dir <- file.path(work.dir,"data","ncses","clean")
graph.dir <- file.path(work.dir,"graphs")

#### load soure files ##########################################################
source(file.path(lib.dir,"calculateDEA.R"))
source(file.path(lib.dir,"genGraph.R"))
source(file.path(lib.dir,"modifiedDensity.R"))
source(file.path(lib.dir,"meanDifferenceTest.R"))
source(file.path(lib.dir,"rtsTest.R"))
source(file.path(lib.dir,"convTest.R"))

setwd(data.dir)
#### read in the input set: Faculty Count and Federal Expenditures #############
faculty.data <- read.xlsx("Faculty-VHR-HR_2000_2012.xlsx",sheetIndex=1,header=TRUE,stringsAsFactors=FALSE)
expenditures.data <-  read.csv("h3.txt",header=TRUE,sep="\t",stringsAsFactors=FALSE)

#### read in the output set: PhD Graduated and Publication Count ###############
phd.data <-  read.csv("s1.txt",header=TRUE,sep="\t",stringsAsFactors=FALSE)
publication.data <- read.xlsx("publication_vhr_hr_2000_2009.xlsx",sheetIndex=1,header=TRUE,stringsAsFactors=FALSE)


#### read in control data ######################################################
control.data <- read.xlsx("Institution-Control.xlsx",sheetIndex=1,header=TRUE,stringsAsFactors=FALSE)

# expenditure data: 2011 - 2003
# faculty data: 2000 - 2012
# phd data: 2009 - 2000
# publication data: 2000 - 2009

common.fice <- as.data.frame(table(c(unique(expenditures.data$FICE),
                                     unique(phd.data$FICE),
                                     faculty.data$FICE,
                                     publication.data$FICE)))

common.fice <- as.vector(subset(common.fice$Var1, common.fice$Freq == 4))
common.fice <- subset(common.fice,common.fice != "2077") # remove John Hopkins as an outlier


dea.expenditures <- subset(expenditures.data, expenditures.data$FICE %in% common.fice)
dea.faculty <- subset(faculty.data, faculty.data$FICE %in% common.fice)
dea.phd <- subset(phd.data, phd.data$FICE %in% common.fice)
dea.publication <- subset(publication.data, publication.data$FICE %in% common.fice)

sum.expenditures <- numeric(length(common.fice))
sum.faculty <- numeric(length(common.fice))
sum.phd <- numeric(length(common.fice))
sum.publication <- numeric(length(common.fice))

input.set <- c(2,3)
output.set <- c(4,5)
orientation <- 2

for (i in 1:length(seq(2003:2009))){
  print(2002+i)
  input.1 <- dea.faculty[,i+4]    
  input.2 <- numeric(length(common.fice))  
  exp.index <- length(dea.expenditures)
  for (j in 1:length(common.fice)){
    input.2[j] <- sum(as.numeric(as.vector(subset(dea.expenditures[,exp.index-i+1], dea.expenditures$FICE == common.fice[j]))))    
  }
  
  output.1 <- dea.publication[,i+5]
  output.2 <- numeric(length(common.fice))
  phd.index <- length(dea.phd) - 3
  for (j in 1:length(common.fice)){
    output.2[j] <- sum(as.numeric(as.vector(subset(dea.phd[,phd.index-i+1], dea.phd$FICE == common.fice[j]))))    
  }
  
  sum.faculty <- sum.faculty + input.1
  sum.expenditures <- sum.expenditures + input.2
  
  sum.phd <- sum.phd + output.2
  sum.publication <- sum.publication + output.1
  
  annual.data <- data.frame(FICE=common.fice,
                            faculty=input.1,
                            expenditures=input.2,
                            publication=output.1,
                            phd=output.2)
  
  annual.data <- merge(x=annual.data,by="FICE",y=control.data,all.x=TRUE)
  
  # removes 0s
  annual.data <- subset(annual.data, annual.data$expenditures !=0)
  annual.data <- subset(annual.data, annual.data$phd != 0)  
}

input.data <- data.frame(FICE=common.fice,
                         faculty=sum.faculty,
                         expenditures=sum.expenditures,
                         publication=sum.publication,
                         phd=sum.phd)

input.data <- merge(x=input.data,by="FICE",y=control.data,all.x=TRUE)
# removes 0s
input.data <- subset(input.data, input.data$expenditures !=0)
input.data <- subset(input.data, input.data$phd != 0)

## Overall statistics:
header.list <- c("Faculty","Expenditures","Publications","PhDs Granted")
print("Overall")
tmp.data <- input.data
for (i in 2:5)
print(paste(header.list[i-1],"&",mean(tmp.data[,i]),"&",sd(tmp.data[,i]),"&",min(tmp.data[,i]),"&",max(tmp.data[,i]),"\\",sep=" "))

print("EPSCoR")
tmp.data <- subset(input.data, input.data$EPSCOR == 1)
for (i in 2:5)
  print(paste(header.list[i-1],"&",mean(tmp.data[,i]),"&",sd(tmp.data[,i]),"&",min(tmp.data[,i]),"&",max(tmp.data[,i]),"\\",sep=" "))

print("NonEPSCOR")
tmp.data <- subset(input.data, input.data$EPSCOR == 0)
for (i in 2:5)
  print(paste(header.list[i-1],"&",mean(tmp.data[,i]),"&",sd(tmp.data[,i]),"&",min(tmp.data[,i]),"&",max(tmp.data[,i]),"\\",sep=" "))


print("Public")
tmp.data <- subset(input.data, input.data$Type == "Public")
for (i in 2:5)
  print(paste(header.list[i-1],"&",mean(tmp.data[,i]),"&",sd(tmp.data[,i]),"&",min(tmp.data[,i]),"&",max(tmp.data[,i]),"\\",sep=" "))


print("Private")
tmp.data <- subset(input.data, input.data$Type == "Private")
for (i in 2:5)
  print(paste(header.list[i-1],"&",mean(tmp.data[,i]),"&",sd(tmp.data[,i]),"&",min(tmp.data[,i]),"&",max(tmp.data[,i]),"\\",sep=" "))

print("VHR")
tmp.data <- subset(input.data, input.data$CC == "Research Universities (very high research activity)")
for (i in 2:5)
  print(paste(header.list[i-1],"&",mean(tmp.data[,i]),"&",sd(tmp.data[,i]),"&",min(tmp.data[,i]),"&",max(tmp.data[,i]),"\\",sep=" "))

print("HR")
tmp.data <- subset(input.data, input.data$CC == "Research Universities (high research activity)")
for (i in 2:5)
  print(paste(header.list[i-1],"&",mean(tmp.data[,i]),"&",sd(tmp.data[,i]),"&",min(tmp.data[,i]),"&",max(tmp.data[,i]),"\\",sep=" "))


