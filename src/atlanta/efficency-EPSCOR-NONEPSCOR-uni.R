#### preparing environments ####################################################
cat("\014")   # This is to clear the console ..
rm(list=ls(all=TRUE))
set.seed(42)
options(digits=4)


#### load libraries and set up directory paths #################################
require(FEAR)
require(xlsx)

work.dir <- file.path("/Users","lngo","Dropbox","R","ciimpact")
#work.dir <- file.path("/home","lngo","Dropbox","R","ciimpact")
setwd(work.dir)

lib.dir <- file.path(work.dir,"lib")
data.dir <- file.path(work.dir,"data","ncses","clean")
graph.dir <- file.path(work.dir,"graphs")

#### load soure files ##########################################################
source(file.path(lib.dir,"calculateDEA.R"))
source(file.path(lib.dir,"genGraph.R"))
source(file.path(lib.dir,"modifiedDensity.R"))
source(file.path(lib.dir,"meanDifferenceTest.R"))
source(file.path(lib.dir,"rtsTest.R"))
source(file.path(lib.dir,"convTest.R"))

setwd(data.dir)
#### read in the input set: Faculty Count and Federal Expenditures #############
faculty.data <- read.xlsx("Faculty-VHR-HR_2000_2012.xlsx",sheetIndex=1,header=TRUE,stringsAsFactors=FALSE)
expenditures.data <-  read.csv("h3.txt",header=TRUE,sep="\t",stringsAsFactors=FALSE)

#### read in the output set: PhD Graduated and Publication Count ###############
phd.data <-  read.csv("s1.txt",header=TRUE,sep="\t",stringsAsFactors=FALSE)
publication.data <- read.xlsx("publication_vhr_hr_2000_2009.xlsx",sheetIndex=1,header=TRUE,stringsAsFactors=FALSE)


#### read in control data ######################################################
control.data <- read.xlsx("Institution-Control.xlsx",sheetIndex=1,header=TRUE,stringsAsFactors=FALSE)

# expenditure data: 2011 - 2003
# faculty data: 2000 - 2012
# phd data: 2009 - 2000
# publication data: 2000 - 2009

common.fice <- as.data.frame(table(c(unique(expenditures.data$FICE),
                                     unique(phd.data$FICE),
                                     faculty.data$FICE,
                                     publication.data$FICE)))

common.fice <- as.vector(subset(common.fice$Var1, common.fice$Freq == 4))
common.fice <- subset(common.fice,common.fice != "2077") # remove John Hopkins as an outlier


dea.expenditures <- subset(expenditures.data, expenditures.data$FICE %in% common.fice)
dea.faculty <- subset(faculty.data, faculty.data$FICE %in% common.fice)
dea.phd <- subset(phd.data, phd.data$FICE %in% common.fice)
dea.publication <- subset(publication.data, publication.data$FICE %in% common.fice)

sum.expenditures <- numeric(length(common.fice))
sum.faculty <- numeric(length(common.fice))
sum.phd <- numeric(length(common.fice))
sum.publication <- numeric(length(common.fice))

input.set <- c(2,3)
output.set <- c(4,5)
orientation <- 2

for (i in 1:length(seq(2003:2009))){
  print(2002+i)
  input.1 <- dea.faculty[,i+4]    
  input.2 <- numeric(length(common.fice))  
  exp.index <- length(dea.expenditures)
  for (j in 1:length(common.fice)){
    input.2[j] <- sum(as.numeric(as.vector(subset(dea.expenditures[,exp.index-i+1], dea.expenditures$FICE == common.fice[j]))))    
  }
  
  output.1 <- dea.publication[,i+5]
  output.2 <- numeric(length(common.fice))
  phd.index <- length(dea.phd) - 3
  for (j in 1:length(common.fice)){
    output.2[j] <- sum(as.numeric(as.vector(subset(dea.phd[,phd.index-i+1], dea.phd$FICE == common.fice[j]))))    
  }
  
  sum.faculty <- sum.faculty + input.1
  sum.expenditures <- sum.expenditures + input.2
  
  sum.phd <- sum.phd + output.2
  sum.publication <- sum.publication + output.1
  
  annual.data <- data.frame(FICE=common.fice,
                           faculty=input.1,
                           expenditures=input.2,
                           publication=output.1,
                           phd=output.2)
  
  annual.data <- merge(x=annual.data,by="FICE",y=control.data,all.x=TRUE)
  annual.data$APP <- annual.data$EPSCOR
  
  # removes 0s
  annual.data <- subset(annual.data, annual.data$expenditures !=0)
  annual.data <- subset(annual.data, annual.data$phd != 0)

  
  # the first test is the convexity test
  pval.convex <- get.dea.convex(annual.data,input.set,output.set)$pval
#  print(paste("Convexity Test:",pval.convex,sep=""))
  
  if (pval.convex <= 0.10){
    # if we reject convexity, we go straight to mean difference test and then
    # stochastic dominance test, both using fdh          
    rho <- mean.diff.fdh (annual.data, input.set, output.set, orientation)
    
    # Efficiency estimation is done using FDH
    output.all <- get.fdh(annual.data,input.set,output.set,orientation)     
    output.hpc <- subset(output.all, output.all$EPSCOR == 1)    
    output.nonhpc <- subset(output.all, output.all$EPSCOR == 0)    
  } else {
    # if we don't reject convexity, we now test for return-to-scale
    pval.rts <- get.dea.rts(annual.data,input.set,output.set)$pval
#    print(paste("RTS Test:",pval.rts,sep=""))
    
    if (pval.rts <= 0.10){
      rts <- 1
    } else {
      rts <- 3
    }      
#    rho <- mean.diff.dea (annual.data, input.set, output.set, orientation, rts)
    
    # Efficiency estimation is done using FDH
output.all <- get.fdh(annual.data,input.set,output.set,orientation)     

#    output.all <- get.dea(annual.data,input.set,output.set,orientation,rts)     
    output.hpc <- subset(output.all, output.all$EPSCOR == 1)    
    output.nonhpc <- subset(output.all, output.all$EPSCOR == 0)     
  }
  densHPC <- get.density(output.hpc$DEA)
  densNonHPC <- get.density(output.nonhpc$DEA)
#   print(paste("Number of institutions   : ",length(output.all$DEA),sep=""))
#   print(paste("EPSCOR     : ",length(output.hpc$DEA),sep=""))  
#   print(paste("Non-EPSCOR         : ",length(output.nonhpc$DEA),sep=""))
#   print(paste("Mean Difference Test     : ",rho,sep=""))
#   print("=====================================================================")  
}

input.data <- data.frame(FICE=common.fice,
                         faculty=sum.faculty,
                         expenditures=sum.expenditures,
                         publication=sum.publication,
                         phd=sum.phd)

input.data <- merge(x=input.data,by="FICE",y=control.data,all.x=TRUE)
input.data$APP <- input.data$EPSCOR
# removes 0s
input.data <- subset(input.data, input.data$expenditures !=0)
input.data <- subset(input.data, input.data$phd != 0)


legend.stat <- c()
# the first test is the convexity test
pval.convex <- get.dea.convex(input.data,input.set,output.set)$pval
print(paste("Convexity Test:",pval.convex,sep=""))
legend.stat <- c(legend.stat,paste("Convexity Test:",format(pval.convex,digits=4),sep=""))

if (pval.convex <= 0.10){
  # if we reject convexity, we go straight to mean difference test and then
  # stochastic dominance test, both using fdh          
  rho <- mean.diff.fdh (input.data, input.set, output.set, orientation)
  legend.stat <- c(legend.stat,paste("Mean Diff Test:",format(rho,digits=4),sep=""))
  
  # Efficiency estimation is done using FDH
  output.all <- get.fdh(input.data,input.set,output.set,orientation)     
  output.hpc <- subset(output.all, output.all$EPSCOR == 1)    
  output.nonhpc <- subset(output.all, output.all$EPSCOR == 0)    
} else {
  # if we don't reject convexity, we now test for return-to-scale
  pval.rts <- get.dea.rts(input.data,input.set,output.set)$pval
  print(paste("RTS Test:",pval.rts,sep=""))
  legend.stat <- c(legend.stat,paste("RTS Test:",format(pval.rts,digits=4),sep=""))
  
  if (pval.rts <= 0.10){
    rts <- 1
  } else {
    rts <- 3
    #rts <- 1
  }      
  rho <- mean.diff.fdh (input.data, input.set, output.set, orientation)
#  rho <- mean.diff.dea (input.data, input.set, output.set, orientation, rts)
  legend.stat <- c(legend.stat,paste("Mean Diff Test:",format(rho,digits=4),sep=""))
  
  # Efficiency estimation is done using FDH
  output.all <- get.fdh(annual.data,input.set,output.set,orientation)     
  
#  output.all <- get.dea(input.data,input.set,output.set,orientation,rts)     
  output.hpc <- subset(output.all, output.all$EPSCOR == 1)    
  output.nonhpc <- subset(output.all, output.all$EPSCOR == 0)     
}
densHPC <- get.density(output.hpc$DEA)
densNonHPC <- get.density(output.nonhpc$DEA)
print(paste("Number of institutions   : ",length(output.all$DEA),sep=""))
print(paste("EPSCOR     : ",length(output.hpc$DEA),sep=""))  
print(paste("Non-EPSCOR         : ",length(output.nonhpc$DEA),sep=""))
print(paste("Mean Difference Test     : ",rho,sep=""))


color <- rainbow(7)
pch <- seq(19,25)
lty <- seq(1,7)
firstLegend = "EPSCoR"
secondLegend = "non-EPSCoR"  
secondLegend = c(secondLegend,legend.stat)
graphName=""
#gen.plot(densHPC,densNonHPC,2.0,graphName,firstLegend,secondLegend)
#plot(ecdf(output.hpc$DEA), verticals=TRUE, do.points=FALSE,lwd=2,main="",lty=1,ylab="CDF ",xlab="Efficiency")
#lines(ecdf(output.nonhpc$DEA),verticals=TRUE, lwd=1,lty=2,do.points=FALSE)
#legend("bottom", legend=c(firstLegend,secondLegend), cex=0.8, lwd=c(2,1), lty=c(1,2,0,0,0,0), bty="n")
