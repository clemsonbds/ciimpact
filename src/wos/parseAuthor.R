#! C:\\Program Files\\R\\R-2.14.2\\bin\\Rscript.exe
rm(list=ls()) # clean up everything
options(digits=2)

work.dir <- file.path("D:","Work","R","ciimpact")
#work.dir <- file.path("/home","lngo","Dropbox","R","ciimpact")
setwd(work.dir)

data.dir <- file.path(work.dir,"data")
wos.dir <- file.path(data.dir,"WebofScience","CitationCount")


setwd(wos.dir)

dlist <- list.files(wos.dir)
total.file.count <- 0
total.author.amb <- 0
total.author.last <- 0
total.article.count <- 0
article.list <- c()
for (i in 1:length(dlist)){
  curr.institution <- dlist[i]
  tmp.path <- file.path(wos.dir,dlist[i])
  flist <- list.files(file.path(wos.dir,dlist[i]))
  total.file.count <- total.file.count + length(flist)
  print("***************************")
  print(dlist[i])
  if (length(flist) > 0){
    for (j in 1:length(flist)){
      tmp.file <- file.path(wos.dir,dlist[i],flist[j])
      print(tmp.file)
      con <- file(tmp.file, open="r",encoding="UCS-2LE")
      tmp.table <- readLines(con)
      total.article.count <- total.article.count + length(tmp.table)
      article.list <- c(article.list, tmp.table[5:length(tmp.table)])
#       for (k in 5:length(tmp.table)){
#         tmp.split <- strsplit(tmp.table[k],split='\",\"')
#         author.list <- strsplit(tmp.split[[1]][2],split=";")
#         total.author.amb <- total.author.amb + length(author.list[[1]])
#         author.last <- c()
#         for (m in 1:length(author.list[[1]])){
#           tmp.last <- strsplit(author.list[[1]][m],",")
#           author.last <- c(author.last,tmp.last[[]])
#         }
#       }
      close(con)
    }
  }
}  

article.unique <- unique(article.list)
rm(article.list)
author.list <- c()
for ( i in 1:length(article.unique)){
  print(i)
  author.list <- c(author.list, strsplit(article.unique[[1]][2],split=";"))
  
}

