nsf.xml.to.json <- function(filename){
  require(XML)
  require(RJSONIO)
  
  doc <- xmlInternalTreeParse(filename)
  
  tag.AwardTitle <- xpathApply(doc, "//rootTag/Award/AwardTitle", xmlValue)
  tag.AwardEffectiveDate <- xpathApply(doc, "//rootTag/Award/AwardEffectiveDate", xmlValue)
  tag.AwardExpirationDate <- xpathApply(doc, "//rootTag/Award/AwardExpirationDate", xmlValue)  
  tag.AwardAmount <- xpathApply(doc, "//rootTag/Award/AwardAmount", xmlValue)
  tag.AwardEffectiveDate <- xpathApply(doc, "//rootTag/Award/AwardEffectiveDate", xmlValue)
  tag.AwardInstrument <- xpathApply(doc,"//rootTag/Award/AwardInstrument", xmlValue)
  
  #Organization
  tag.Code <- xpathApply(doc,"//rootTag/Award/Organization/Code", xmlValue)
  tag.Directorate <- xpathApply(doc,"//rootTag/Award/Organization/Directorate/LongName", xmlValue)
  tag.Division <- xpathApply(doc,"//rootTag/Award/Organization/Division/LongName", xmlValue)
  
  tag.ProgramOfficer <- xpathApply(doc,"//rootTag/Award/ProgramOfficer/SignBlockName", xmlValue)
  tag.AbstractNarration <- xpathApply(doc,"//rootTag/Award/AbstractNarration", xmlValue)
  tag.MinAmdLetterDate <- xpathApply(doc,"//rootTag/Award/MinAmdLetterDate", xmlValue)
  tag.MaxAmdLetterDate <- xpathApply(doc,"//rootTag/Award/MaxAmdLetterDate", xmlValue)
  tag.ARRAAmount <- xpathApply(doc,"//rootTag/Award/ARRAAmount", xmlValue)
  if (is.na(as.numeric(tag.ARRAAmount[[1]][1]))) tag.ARRAAmount[[1]][1] <- 0
  tag.AwardID <- xpathApply(doc,"//rootTag/Award/AwardID", xmlValue)
  
  #Investigator
  tag.FirstName <- xpathApply(doc,"//rootTag/Award/Investigator/FirstName", xmlValue)
  tag.Investigator <- list()
  if (length(tag.FirstName) != 0){
    tag.LastName <- xpathApply(doc,"//rootTag/Award/Investigator/LastName", xmlValue)
    tag.EmailAddress <- xpathApply(doc,"//rootTag/Award/Investigator/EmailAddress", xmlValue)
    tag.StartDate <- xpathApply(doc,"//rootTag/Award/Investigator/StartDate", xmlValue)
    tag.RoleCode <- xpathApply(doc,"//rootTag/Award/Investigator/RoleCode", xmlValue)    
    for (i in 1:length(tag.FirstName)){
      tmp.Investigator <- c(FirstName = tag.FirstName[[i]][1],
                            LastName = tag.LastName[[i]][1],
                            EmailAddress = tag.EmailAddress[[i]][1],
                            StartDate = tag.StartDate[[i]][1],
                            RoleCode = tag.RoleCode[[i]][1])
      tag.Investigator[[i]] <- tmp.Investigator
    }
  }
  #Institution
  tag.Name <- xpathApply(doc,"//rootTag/Award/Institution/Name", xmlValue)
  tag.CityName <- xpathApply(doc,"//rootTag/Award/Institution/CityName", xmlValue)
  tag.ZipCode <- xpathApply(doc,"//rootTag/Award/Institution/ZipCode", xmlValue)
  tag.PhoneNumber <- xpathApply(doc,"//rootTag/Award/Institution/PhoneNumber", xmlValue)
  tag.StreetAddress <- xpathApply(doc,"//rootTag/Award/Institution/StreetAddress", xmlValue)
  tag.CountryName <- xpathApply(doc,"//rootTag/Award/Institution/CountryName", xmlValue)
  tag.StateName <- xpathApply(doc,"//rootTag/Award/Institution/StateName", xmlValue)
  tag.StateCode <- xpathApply(doc,"//rootTag/Award/Institution/StateCode", xmlValue)
  tag.Institution <- list()
  for (i in 1:length(tag.Name)){
    tmp.Institution <- c(Name = tag.Name[[i]][1],
                         CityName = tag.CityName[[i]][1],
                         ZipCode = tag.ZipCode[[i]][1],
                         PhoneNumber = tag.PhoneNumber[[i]][1],
                         StreetAddress = tag.StreetAddress[[i]][1],
                         CountryName = tag.CountryName[[i]][1],
                         StateName = tag.StateName[[i]][1],
                         StateCode = tag.StateCode[[i]][1])
    tag.Institution[[i]] <- tmp.Institution
  }
  
  # Program Element
  tag.Code.Element <- xpathApply(doc,"//rootTag/Award/ProgramElement/Code", xmlValue)
  tag.Text.Element <- xpathApply(doc,"//rootTag/Award/ProgramElement/Text", xmlValue)
  tag.ProgramElement <- list()
  if (length(tag.Code.Element) > 0){
    for (i in 1:length(tag.Code.Element)){
      tmp.ProgramElement <- c(Code = tag.Code.Element[[i]][1],Text = tag.Text.Element[[i]][1])
      tag.ProgramElement[[i]] <- tmp.ProgramElement
    }
  }
  
  # Program Reference
  tag.Code.Reference <- xpathApply(doc,"//rootTag/Award/ProgramReference/Code", xmlValue)
  tag.Text.Referece <- xpathApply(doc,"//rootTag/Award/ProgramReference/Text", xmlValue)
  tag.ProgramReference <- list()
  if (length(tag.Code.Reference) > 0){    
    for (i in 1:length(tag.Code)){
      tmp.ProgramReference <- c(Code = tag.Code.Referece[[i]][1],Text = tag.Text.Reference[[i]][1])
      tag.ProgramReference[[i]] <- tmp.ProgramReference
    }
  }
  
  tag.Award <- list(AwardTitle = tag.AwardTitle[[1]][1],
                    AwardEffectiveDate = tag.AwardEffectiveDate[[1]][1],
                    AwardExpirationDate = tag.AwardExpirationDate[[1]][1],
                    AwardAmount = tag.AwardAmount[[1]][1],
                    AwardInstrument = list(Value = tag.AwardInstrument[[1]][1]),
                    Organization = list(Code = tag.Code[[1]][1],
                                        Directorate = list(LongName = tag.Directorate[[1]][1]),
                                        Division = list(LongName = tag.Division[[1]][1])),
                    ProgramOfficer = list(SignBlockName = tag.ProgramOfficer[[1]][1]),
                    AbstractNarration = tag.AbstractNarration[[1]][1],
                    MinAmdLetterDate = tag.MinAmdLetterDate[[1]][1],
                    MaxAmdLetterDate = tag.MaxAmdLetterDate[[1]][1],
                    ARRAAmount = tag.ARRAAmount[[1]][1],
                    AwardID = tag.AwardID[[1]][1],
                    Investigator = tag.Investigator,
                    Institution = tag.Institution,
                    ProgramElement = tag.ProgramElement,
                    ProgramReference = tag.ProgramReference)
  tag.Award
}






#nsf.xml.data <- file.path("/home","lngo","ciimpact","data","nsf")
nsf.xml.data <- file.path("D:","Work","R","ciimpact","data","nsf","xml")

year.dirs <- list.files(nsf.xml.data)
dir.count <- length(year.dirs)
library(RMongo)
library(RJSONIO)
con.mongo <- mongoDbConnect("NSF",host="130.127.154.213",port=27017)
for (i in 1:dir.count){
  xml.list <- list.files(file.path(nsf.xml.data,year.dirs[i]))
  file.count <- length(xml.list)
  print(paste(year.dirs[i],":",file.count,"------",sep=""))
  for (j in 1:file.count){
    print(paste(year.dirs[i],": ",xml.list[j],sep=""))
    json.obj <- nsf.xml.to.json(file.path(nsf.xml.data,year.dirs[i],xml.list[j]))
    output <- dbInsertDocument(con.mongo,"NSF_Award",toJSON(json.obj))
    print(output)    
  }
}

dbDisconnect(con.mongo)
