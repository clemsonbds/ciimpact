Creator "igraph version 0.6.5.999-42 Wed Jan 29 11:09:48 2014"
Version 1
graph
[
  directed 0
  node
  [
    id 0
    name "Adelphi University"
  ]
  node
  [
    id 1
    name "Alliant International University"
  ]
  node
  [
    id 2
    name "American University"
  ]
  node
  [
    id 3
    name "Andrews University"
  ]
  node
  [
    id 4
    name "Biola University"
  ]
  node
  [
    id 5
    name "Central Michigan University"
  ]
  node
  [
    id 6
    name "Clark Atlanta University"
  ]
  node
  [
    id 7
    name "DePaul University"
  ]
  node
  [
    id 8
    name "East Carolina University"
  ]
  node
  [
    id 9
    name "East Tennessee State University"
  ]
  node
  [
    id 10
    name "Florida A&M University"
  ]
  node
  [
    id 11
    name "Florida Institute of Technology"
  ]
  node
  [
    id 12
    name "Georgia Southern University"
  ]
  node
  [
    id 13
    name "Hofstra University"
  ]
  node
  [
    id 14
    name "Illinois State University"
  ]
  node
  [
    id 15
    name "Indiana State University"
  ]
  node
  [
    id 16
    name "Indiana University of Pennsylvania"
  ]
  node
  [
    id 17
    name "InterAmerican University of Puerto Rico"
  ]
  node
  [
    id 18
    name "Lamar University"
  ]
  node
  [
    id 19
    name "Marquette University"
  ]
  node
  [
    id 20
    name "Middle Tennessee State University"
  ]
  node
  [
    id 21
    name "National-Louis University"
  ]
  node
  [
    id 22
    name "North Carolina A&T State University"
  ]
  node
  [
    id 23
    name "Oakland University"
  ]
  node
  [
    id 24
    name "Pace University"
  ]
  node
  [
    id 25
    name "Pepperdine University"
  ]
  node
  [
    id 26
    name "Pontifical Catholic University of Puerto Rico"
  ]
  node
  [
    id 27
    name "Saint John's University"
  ]
  node
  [
    id 28
    name "Sam Houston State University"
  ]
  node
  [
    id 29
    name "Seton Hall University"
  ]
  node
  [
    id 30
    name "South Carolina State University"
  ]
  node
  [
    id 31
    name "State University of New York (SUNY) College of Environmental Science & Forestry"
  ]
  node
  [
    id 32
    name "Tennessee State University"
  ]
  node
  [
    id 33
    name "Texas A&M University Commerce"
  ]
  node
  [
    id 34
    name "Texas A&M University Corpus Christi"
  ]
  node
  [
    id 35
    name "Texas A&M University Kingsville"
  ]
  node
  [
    id 36
    name "Texas Christian University"
  ]
  node
  [
    id 37
    name "Texas Southern University"
  ]
  node
  [
    id 38
    name "Texas Womans University"
  ]
  node
  [
    id 39
    name "Union Institute"
  ]
  node
  [
    id 40
    name "University of Arkansas Little Rock"
  ]
  node
  [
    id 41
    name "University of Colorado at Colorado Springs"
  ]
  node
  [
    id 42
    name "University of La Verne"
  ]
  node
  [
    id 43
    name "University of Nebraska Omaha"
  ]
  node
  [
    id 44
    name "University of North Carolina Charlotte"
  ]
  node
  [
    id 45
    name "University of Northern Colorado"
  ]
  node
  [
    id 46
    name "University of Puerto Rico Mayaguez"
  ]
  node
  [
    id 47
    name "University of San Diego"
  ]
  node
  [
    id 48
    name "University of San Francisco"
  ]
  node
  [
    id 49
    name "University of the Pacific"
  ]
  node
  [
    id 50
    name "University of Tulsa"
  ]
  node
  [
    id 51
    name "University of West Florida"
  ]
  node
  [
    id 52
    name "Walden University"
  ]
  node
  [
    id 53
    name "Widener University"
  ]
  node
  [
    id 54
    name "Worcester Polytechnic Institute"
  ]
  edge
  [
    source 49
    target 7
  ]
  edge
  [
    source 36
    target 11
  ]
  edge
  [
    source 44
    target 11
  ]
  edge
  [
    source 46
    target 11
  ]
  edge
  [
    source 31
    target 12
  ]
  edge
  [
    source 46
    target 40
  ]
  edge
  [
    source 49
    target 40
  ]
]
