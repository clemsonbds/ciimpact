"FICE"	"faculty"	"expenditures"	"publication"	"phd"	"State"	"Type"	"CC"	"EPSCOR"	"APP"	"DEA"
"1131"	4659	1764209	19218	1291	"California"	"Private"	"Research Universities (very high research activity)"	0	0	0.628680561408805
"1169"	530	8718	589	759	"California"	"Private"	"Research Universities (high research activity)"	0	0	1
"1305"	1808	3661722	497	4602	"California"	"Private"	"Research Universities (very high research activity)"	0	0	1
"1328"	5221	2355109	8248	3795	"California"	"Private"	"Research Universities (very high research activity)"	0	0	1
"1371"	1517	63099	1356	513	"Colorado"	"Private"	"Research Universities (high research activity)"	0	0	0.526305834104508
"1426"	5450	2411244	4607	2454	"Connecticut"	"Private"	"Research Universities (very high research activity)"	0	0	0.634140368332176
"1437"	4595	113485	2221	552	"District of Columbia"	"Private"	"Research Universities (high research activity)"	0	0	0.361050732990178
"1444"	7141	596812	5473	1570	"District of Columbia"	"Private"	"Research Universities (high research activity)"	0	0	0.744543939624163
"1445"	6124	713730	10400	674	"District of Columbia"	"Private"	"Research Universities (very high research activity)"	0	0	0.325156485823512
"1448"	5916	250427	10863	659	"District of Columbia"	"Private"	"Research Universities (high research activity)"	0	0	0.378177815046861
"1469"	7028	42183	5413	175	"Florida"	"Private"	"Research Universities (high research activity)"	0	0	0.168022396800235
"1536"	11006	1064794	13670	1055	"Florida"	"Private"	"Research Universities (very high research activity)"	0	0	0.441156334134185
"1559"	8880	68814	16117	192	"Georgia"	"Private"	"Research Universities (high research activity)"	0	0	0.476557049162391
"1564"	2504	1864667	1306	1367	"Georgia"	"Private"	"Research Universities (very high research activity)"	0	0	0.466807466963069
"1691"	4158	120385	2436	489	"Illinois"	"Private"	"Research Universities (high research activity)"	0	0	0.333976603000482
"1710"	7914	183933	30611	997	"Illinois"	"Private"	"Research Universities (high research activity)"	0	0	0.887273084265913
"1739"	2487	1740016	2273	2458	"Illinois"	"Private"	"Research Universities (very high research activity)"	0	0	0.877347880689443
"1774"	2840	1777026	2558	2598	"Illinois"	"Private"	"Research Universities (very high research activity)"	0	0	0.897023610427802
"1840"	6322	374863	3042	1110	"Indiana"	"Private"	"Research Universities (very high research activity)"	0	0	0.592288263841565
"2029"	10892	665609	5876	767	"Louisiana"	"Private"	"Research Universities (very high research activity)"	1	0	0.351611082074245
"2128"	3624	146066	4118	955	"Massachusetts"	"Private"	"Research Universities (high research activity)"	0	0	0.681035862305434
"2130"	7299	1612849	50873	2464	"Massachusetts"	"Private"	"Research Universities (very high research activity)"	0	0	1
"2133"	6754	272829	1379	588	"Massachusetts"	"Private"	"Research Universities (very high research activity)"	0	0	0.332909834066489
"2139"	2977	23050	6205	222	"Massachusetts"	"Private"	"Research Universities (high research activity)"	0	0	0.447308234962553
"2155"	7362	2708885	3218	4341	"Massachusetts"	"Private"	"Research Universities (very high research activity)"	0	0	1
"2178"	9818	3221299	11569	4181	"Massachusetts"	"Private"	"Research Universities (very high research activity)"	0	0	1
"2199"	1415	240852	633	662	"Massachusetts"	"Private"	"Research Universities (high research activity)"	0	0	0.583347069724115
"2219"	1336	655432	1215	777	"Massachusetts"	"Private"	"Research Universities (very high research activity)"	0	0	0.506424569955735
"2520"	6620	2769922	22088	1642	"Missouri"	"Private"	"Research Universities (very high research activity)"	1	0	0.554838724680131
"2573"	3806	875485	7727	518	"New Hampshire"	"Private"	"Research Universities (very high research activity)"	1	0	0.312513914485895
"2627"	7346	826482	27341	2204	"New Jersey"	"Private"	"Research Universities (very high research activity)"	0	0	0.979727261872701
"2639"	4021	135976	14705	266	"New Jersey"	"Private"	"Research Universities (high research activity)"	0	0	0.598992519114378
"2699"	7789	49398	9569	181	"New York"	"Private"	"Research Universities (high research activity)"	0	0	0.291461528328564
"2707"	3524	3100368	1866	3330	"New York"	"Private"	"Research Universities (very high research activity)"	0	0	0.777679244429014
"2722"	12980	14911	36043	728	"New York"	"Private"	"Research Universities (high research activity)"	0	0	1
"2785"	11976	1324733	15864	2640	"New York"	"Private"	"Research Universities (very high research activity)"	0	0	0.917291086990469
"2803"	7687	300223	12745	1015	"New York"	"Private"	"Research Universities (very high research activity)"	0	0	0.565397330690699
"2894"	8505	1827600	22724	1559	"New York"	"Private"	"Research Universities (very high research activity)"	0	0	0.549763831716315
"2903"	9202	1024008	17468	534	"New York"	"Private"	"Research Universities (very high research activity)"	0	0	0.385816421832181
"2920"	3179	2794953	4654	2146	"North Carolina"	"Private"	"Research Universities (very high research activity)"	0	0	0.548826474063452
"2978"	5935	941995	3328	343	"North Carolina"	"Private"	"Research Universities (high research activity)"	0	0	0.140972415346406
"3024"	6546	1843711	4440	1373	"Ohio"	"Private"	"Research Universities (very high research activity)"	0	0	0.401191996465424
"3127"	12215	414608	35934	160	"Ohio"	"Private"	"Research Universities (high research activity)"	0	0	0.903943781496618
"3185"	4517	56328	4236	173	"Oklahoma"	"Private"	"Research Universities (high research activity)"	1	0	0.153043072565335
"3242"	7900	1199456	10089	1659	"Pennsylvania"	"Private"	"Research Universities (very high research activity)"	0	0	0.604232971515717
"3256"	6006	434952	4025	813	"Pennsylvania"	"Private"	"Research Universities (high research activity)"	0	0	0.419591746863669
"3289"	4714	138386	5301	629	"Pennsylvania"	"Private"	"Research Universities (high research activity)"	0	0	0.399840515806355
"3378"	9956	3226537	34502	3377	"Pennsylvania"	"Private"	"Research Universities (very high research activity)"	0	0	1
"3401"	5053	637271	2215	1338	"Rhode Island"	"Private"	"Research Universities (very high research activity)"	1	0	0.634798881384355
"3535"	3829	2045459	18618	1744	"Tennessee"	"Private"	"Research Universities (very high research activity)"	1	0	0.794098455194707
"3604"	8479	368986	11534	1098	"Texas"	"Private"	"Research Universities (very high research activity)"	0	0	0.587833506493923
"3863"	5028	45698	4709	462	"Wisconsin"	"Private"	"Research Universities (high research activity)"	0	0	0.309277680726069
"3979"	4991	47419	30858	1528	"New York"	"Private"	"Research Universities (high research activity)"	0	0	1
