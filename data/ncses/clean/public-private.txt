"FICE"	"faculty"	"expenditures"	"publication"	"phd"	"State"	"Type"	"CC"	"EPSCOR"	"APP"	"DEA"
"1009"	14139	0	16574	1344	"Alabama"	"Public"	"Research Universities (high research activity)"	1	1	0.963876359521672
"1051"	8113	173892	7858	1172	"Alabama"	"Public"	"Research Universities (high research activity)"	1	1	0.28655902726197
"1052"	4693	1960497	4171	1014	"Alabama"	"Public"	"Research Universities (very high research activity)"	1	1	0.204338254711964
"1055"	9902	311535	24880	227	"Alabama"	"Public"	"Research Universities (high research activity)"	1	1	0.284568427849152
"1081"	2414	730844	3717	2865	"Arizona"	"Public"	"Research Universities (very high research activity)"	0	1	0.771301392068099
"1082"	4754	91546	15602	299	"Arizona"	"Public"	"Research Universities (high research activity)"	0	1	0.393041346557248
"1083"	2197	1973187	20513	2969	"Arizona"	"Public"	"Research Universities (very high research activity)"	0	1	1
"1108"	5715	223725	10477	973	"Arkansas"	"Public"	"Research Universities (high research activity)"	1	1	0.352494017350167
"1131"	4659	1764209	19218	1291	"California"	"Private"	"Research Universities (very high research activity)"	0	0	0.477391156481251
"1151"	2436	240997	2049	47	"California"	"Public"	"Research Universities (high research activity)"	0	1	0.109207430552489
"1169"	530	8718	589	759	"California"	"Private"	"Research Universities (high research activity)"	0	0	1
"1305"	1808	3661722	497	4602	"California"	"Private"	"Research Universities (very high research activity)"	0	0	0.779603591394206
"1312"	1212	1821989	1121	5903	"California"	"Public"	"Research Universities (very high research activity)"	0	1	1
"1313"	1210	1740332	1743	3231	"California"	"Public"	"Research Universities (very high research activity)"	0	1	0.591529953514009
"1314"	7113	1141684	6844	2290	"California"	"Public"	"Research Universities (very high research activity)"	0	1	0.449703733434862
"1315"	3986	3264364	3232	4916	"California"	"Public"	"Research Universities (very high research activity)"	0	1	0.846364892153962
"1316"	1481	356212	2463	1341	"California"	"Public"	"Research Universities (very high research activity)"	0	1	0.637354349138007
"1317"	7626	3271581	12525	2863	"California"	"Public"	"Research Universities (very high research activity)"	0	1	0.557128497490765
"1320"	9914	728080	44820	2261	"California"	"Public"	"Research Universities (very high research activity)"	0	1	0.807141497081766
"1321"	3466	434131	5914	918	"California"	"Public"	"Research Universities (very high research activity)"	0	1	0.344524392831265
"1328"	5221	2355109	8248	3795	"California"	"Private"	"Research Universities (very high research activity)"	0	0	0.687906177422305
"1348"	7641	132337	39274	327	"Colorado"	"Public"	"Research Universities (high research activity)"	0	1	0.588888582248055
"1350"	5547	1247679	27845	1427	"Colorado"	"Public"	"Research Universities (very high research activity)"	0	1	0.580918762725564
"1370"	5289	1538706	3788	2133	"Colorado"	"Public"	"Research Universities (very high research activity)"	0	1	0.389525901671699
"1371"	1517	63099	1356	513	"Colorado"	"Private"	"Research Universities (high research activity)"	0	0	0.367536543575805
"1426"	5450	2411244	4607	2454	"Connecticut"	"Private"	"Research Universities (very high research activity)"	0	0	0.44028113689099
"1431"	8658	554694	11482	1365	"Delaware"	"Public"	"Research Universities (very high research activity)"	1	1	0.346767341030669
"1437"	4595	113485	2221	552	"District of Columbia"	"Private"	"Research Universities (high research activity)"	0	0	0.175749204289023
"1444"	7141	596812	5473	1570	"District of Columbia"	"Private"	"Research Universities (high research activity)"	0	0	0.32676827738562
"1445"	6124	713730	10400	674	"District of Columbia"	"Private"	"Research Universities (very high research activity)"	0	0	0.229741373786579
"1448"	5916	250427	10863	659	"District of Columbia"	"Private"	"Research Universities (high research activity)"	0	0	0.289243840568299
"1469"	7028	42183	5413	175	"Florida"	"Private"	"Research Universities (high research activity)"	0	0	0.142991660973533
"1481"	10449	118881	92652	527	"Florida"	"Public"	"Research Universities (high research activity)"	0	1	1
"1489"	6519	748650	2821	2318	"Florida"	"Public"	"Research Universities (very high research activity)"	0	1	0.435153382348053
"1535"	2312	1601432	3507	4690	"Florida"	"Public"	"Research Universities (very high research activity)"	0	1	0.824435326309139
"1536"	11006	1064794	13670	1055	"Florida"	"Private"	"Research Universities (very high research activity)"	0	0	0.299631601926506
"1537"	5393	1048627	3907	1572	"Florida"	"Public"	"Research Universities (very high research activity)"	0	1	0.306516670494675
"1559"	8880	68814	16117	192	"Georgia"	"Private"	"Research Universities (high research activity)"	0	0	0.272940391781377
"1564"	2504	1864667	1306	1367	"Georgia"	"Private"	"Research Universities (very high research activity)"	0	0	0.239096287027003
"1574"	6363	173099	7951	1181	"Georgia"	"Public"	"Research Universities (high research activity)"	0	1	0.328426844098736
"1598"	3021	698855	3586	2766	"Georgia"	"Public"	"Research Universities (very high research activity)"	0	1	0.679712173122565
"1610"	8977	1329328	18863	1212	"Hawaii"	"Public"	"Research Universities (very high research activity)"	1	1	0.374139750692703
"1626"	3720	319308	3243	623	"Idaho"	"Public"	"Research Universities (high research activity)"	1	1	0.222265653553287
"1691"	4158	120385	2436	489	"Illinois"	"Private"	"Research Universities (high research activity)"	0	0	0.179251564520792
"1710"	7914	183933	30611	997	"Illinois"	"Private"	"Research Universities (high research activity)"	0	0	0.532020038205832
"1737"	15627	75007	22496	545	"Illinois"	"Public"	"Research Universities (high research activity)"	0	1	0.347020074314034
"1739"	2487	1740016	2273	2458	"Illinois"	"Private"	"Research Universities (very high research activity)"	0	0	0.431903644451522
"1758"	6421	119164	4939	1006	"Illinois"	"Public"	"Research Universities (high research activity)"	0	1	0.257384959871018
"1774"	2840	1777026	2558	2598	"Illinois"	"Private"	"Research Universities (very high research activity)"	0	0	0.455857076011933
"1775"	14492	1905550	27576	4975	"Illinois"	"Public"	"Research Universities (very high research activity)"	0	1	1
"1776"	2716	1365029	728	1996	"Illinois"	"Public"	"Research Universities (very high research activity)"	0	1	0.353967186596917
"1809"	11384	0	16543	2851	"Indiana"	"Public"	"Research Universities (very high research activity)"	0	1	1
"1813"	6000	0	7947	0	"Indiana"	"Public"	"Research Universities (high research activity)"	0	1	0.836387877822062
"1825"	5162	0	2286	4233	"Indiana"	"Public"	"Research Universities (very high research activity)"	0	1	1
"1840"	6322	374863	3042	1110	"Indiana"	"Private"	"Research Universities (very high research activity)"	0	0	0.228350137967461
"1869"	7701	673456	32561	2103	"Iowa"	"Public"	"Research Universities (very high research activity)"	1	1	0.67667338729726
"1892"	4412	1545183	2710	2379	"Iowa"	"Public"	"Research Universities (very high research activity)"	1	1	0.423553311441115
"1928"	4613	379252	11556	1046	"Kansas"	"Public"	"Research Universities (very high research activity)"	1	1	0.419952708182184
"1950"	5701	92118	18064	199	"Kansas"	"Public"	"Research Universities (high research activity)"	1	1	0.371359104538554
"1989"	14915	997091	22466	1826	"Kentucky"	"Public"	"Research Universities (very high research activity)"	1	1	0.506788462847352
"1999"	2810	453945	5793	948	"Kentucky"	"Public"	"Research Universities (high research activity)"	1	1	0.392399252320182
"2010"	3893	488879	7878	1717	"Louisiana"	"Public"	"Research Universities (very high research activity)"	1	1	0.517560016293281
"2029"	10892	665609	5876	767	"Louisiana"	"Private"	"Research Universities (very high research activity)"	1	0	0.185819309301453
"2053"	2889	265933	2053	340	"Maine"	"Public"	"Research Universities (high research activity)"	1	1	0.157822903371185
"2103"	798	1472296	6094	3970	"Maryland"	"Public"	"Research Universities (very high research activity)"	0	1	1
"2105"	4339	313808	1801	590	"Maryland"	"Public"	"Research Universities (high research activity)"	0	1	0.163799908296133
"2128"	3624	146066	4118	955	"Massachusetts"	"Private"	"Research Universities (high research activity)"	0	0	0.368491895257581
"2130"	7299	1612849	50873	2464	"Massachusetts"	"Private"	"Research Universities (very high research activity)"	0	0	0.892550137869175
"2133"	6754	272829	1379	588	"Massachusetts"	"Private"	"Research Universities (very high research activity)"	0	0	0.118683894008456
"2139"	2977	23050	6205	222	"Massachusetts"	"Private"	"Research Universities (high research activity)"	0	0	0.39048779304215
"2155"	7362	2708885	3218	4341	"Massachusetts"	"Private"	"Research Universities (very high research activity)"	0	0	0.749518742039849
"2178"	9818	3221299	11569	4181	"Massachusetts"	"Private"	"Research Universities (very high research activity)"	0	0	0.771890466532395
"2199"	1415	240852	633	662	"Massachusetts"	"Private"	"Research Universities (high research activity)"	0	0	0.341609774414744
"2219"	1336	655432	1215	777	"Massachusetts"	"Private"	"Research Universities (very high research activity)"	0	0	0.285416076960473
"2221"	8779	494261	8579	1868	"Massachusetts"	"Public"	"Research Universities (very high research activity)"	0	1	0.40707561793634
"2290"	8758	1090325	8205	3218	"Michigan"	"Public"	"Research Universities (very high research activity)"	0	1	0.617548773129799
"2292"	4225	159984	14043	373	"Michigan"	"Public"	"Research Universities (high research activity)"	0	1	0.401258540080284
"2329"	4648	803236	8232	1353	"Michigan"	"Public"	"Research Universities (very high research activity)"	0	1	0.344102581458862
"2330"	6038	70574	21227	674	"Michigan"	"Public"	"Research Universities (high research activity)"	0	1	0.48855014573456
"2410"	5796	219190	1816	337	"Mississippi"	"Public"	"Research Universities (high research activity)"	1	1	0.0911796699819258
"2423"	1981	655472	4871	852	"Mississippi"	"Public"	"Research Universities (high research activity)"	1	1	0.362286670858788
"2441"	10400	259948	26676	832	"Mississippi"	"Public"	"Research Universities (high research activity)"	1	1	0.404786795887218
"2516"	5989	707742	5985	2000	"Missouri"	"Public"	"Research Universities (very high research activity)"	1	1	0.406909778387697
"2518"	10358	111946	42054	341	"Missouri"	"Public"	"Research Universities (high research activity)"	1	1	0.477171974778964
"2519"	9668	34990	37155	366	"Missouri"	"Public"	"Research Universities (high research activity)"	1	1	0.899283377503566
"2520"	6620	2769922	22088	1642	"Missouri"	"Private"	"Research Universities (very high research activity)"	1	0	0.472613032285278
"2536"	12432	236063	56591	369	"Montana"	"Public"	"Research Universities (high research activity)"	1	1	0.617452408717753
"2565"	4597	527483	11048	1753	"Nebraska"	"Public"	"Research Universities (very high research activity)"	1	1	0.514653415372817
"2568"	6708	415935	40086	660	"Nevada"	"Public"	"Research Universities (high research activity)"	1	1	0.682842496349351
"2569"	6327	260610	15337	521	"Nevada"	"Public"	"Research Universities (high research activity)"	1	1	0.316723772323368
"2573"	3806	875485	7727	518	"New Hampshire"	"Private"	"Research Universities (very high research activity)"	1	0	0.235743066497668
"2621"	7791	256808	6290	434	"New Jersey"	"Public"	"Research Universities (high research activity)"	0	1	0.142238444604984
"2627"	7346	826482	27341	2204	"New Jersey"	"Private"	"Research Universities (very high research activity)"	0	0	0.63910175510787
"2631"	8212	0	17639	403	"New Jersey"	"Public"	"Research Universities (high research activity)"	0	1	1
"2639"	4021	135976	14705	266	"New Jersey"	"Private"	"Research Universities (high research activity)"	0	0	0.4430140698283
"2663"	3070	867460	1694	1291	"New Mexico"	"Public"	"Research Universities (very high research activity)"	1	1	0.287810394485034
"2699"	7789	49398	9569	181	"New York"	"Private"	"Research Universities (high research activity)"	0	0	0.214649831541061
"2707"	3524	3100368	1866	3330	"New York"	"Private"	"Research Universities (very high research activity)"	0	0	0.57140219444457
"2722"	12980	14911	36043	728	"New York"	"Private"	"Research Universities (high research activity)"	0	0	1
"2785"	11976	1324733	15864	2640	"New York"	"Private"	"Research Universities (very high research activity)"	0	0	0.56863241042318
"2803"	7687	300223	12745	1015	"New York"	"Private"	"Research Universities (very high research activity)"	0	0	0.30749737800341
"2835"	7689	689959	7983	1041	"New York"	"Public"	"Research Universities (very high research activity)"	0	1	0.25642754868838
"2836"	3850	87199	4170	831	"New York"	"Public"	"Research Universities (high research activity)"	0	1	0.332889253324472
"2837"	8590	1035176	21541	2186	"New York"	"Public"	"Research Universities (very high research activity)"	0	1	0.568329577922951
"2851"	15409	52262	27291	139	"New York"	"Public"	"Research Universities (high research activity)"	0	1	0.484057530345582
"2894"	8505	1827600	22724	1559	"New York"	"Private"	"Research Universities (very high research activity)"	0	0	0.459187213509182
"2903"	9202	1024008	17468	534	"New York"	"Private"	"Research Universities (very high research activity)"	0	0	0.254261482086053
"2905"	5656	110207	10214	88	"North Carolina"	"Public"	"Research Universities (high research activity)"	0	1	0.211611041357813
"2920"	3179	2794953	4654	2146	"North Carolina"	"Private"	"Research Universities (very high research activity)"	0	0	0.389395832181941
"2972"	3285	837919	20192	2596	"North Carolina"	"Public"	"Research Universities (very high research activity)"	0	1	0.865290571004663
"2974"	10997	2385998	22475	3380	"North Carolina"	"Public"	"Research Universities (very high research activity)"	0	1	0.71986731323462
"2976"	7912	35192	11986	586	"North Carolina"	"Public"	"Research Universities (high research activity)"	0	1	0.33514534120445
"2978"	5935	941995	3328	343	"North Carolina"	"Private"	"Research Universities (high research activity)"	0	0	0.0907863781328829
"3024"	6546	1843711	4440	1373	"Ohio"	"Private"	"Research Universities (very high research activity)"	0	0	0.262785710057371
"3127"	12215	414608	35934	160	"Ohio"	"Private"	"Research Universities (high research activity)"	0	0	0.387838362906359
"3131"	9040	146604	14691	648	"Ohio"	"Public"	"Research Universities (high research activity)"	0	1	0.259831179381131
"3185"	4517	56328	4236	173	"Oklahoma"	"Private"	"Research Universities (high research activity)"	1	0	0.136479013905238
"3210"	3264	772524	2590	1204	"Oregon"	"Public"	"Research Universities (very high research activity)"	0	1	0.291914547259707
"3223"	3709	336029	2711	1070	"Oregon"	"Public"	"Research Universities (high research activity)"	0	1	0.316371127572425
"3242"	7900	1199456	10089	1659	"Pennsylvania"	"Private"	"Research Universities (very high research activity)"	0	0	0.370649206472298
"3256"	6006	434952	4025	813	"Pennsylvania"	"Private"	"Research Universities (high research activity)"	0	0	0.193040802632513
"3289"	4714	138386	5301	629	"Pennsylvania"	"Private"	"Research Universities (high research activity)"	0	0	0.249587435069029
"3371"	7005	384920	12671	1541	"Pennsylvania"	"Public"	"Research Universities (high research activity)"	0	1	0.400411274241058
"3378"	9956	3226537	34502	3377	"Pennsylvania"	"Private"	"Research Universities (very high research activity)"	0	0	0.832780809241466
"3401"	5053	637271	2215	1338	"Rhode Island"	"Private"	"Research Universities (very high research activity)"	1	0	0.26435888754336
"3414"	5992	349648	3346	532	"Rhode Island"	"Public"	"Research Universities (high research activity)"	1	1	0.140491201876632
"3425"	5850	402262	10340	1065	"South Carolina"	"Public"	"Research Universities (high research activity)"	1	1	0.332634610081369
"3448"	5071	0	6084	1609	"South Carolina"	"Public"	"Research Universities (very high research activity)"	1	1	1
"3471"	7520	113534	46452	153	"South Dakota"	"Public"	"Research Universities (high research activity)"	1	1	0.708879880880627
"3509"	7595	109489	2394	754	"Tennessee"	"Public"	"Research Universities (high research activity)"	1	1	0.159219291291102
"3530"	4588	0	3517	1740	"Tennessee"	"Public"	"Research Universities (very high research activity)"	1	1	1
"3535"	3829	2045459	18618	1744	"Tennessee"	"Private"	"Research Universities (very high research activity)"	1	0	0.57123689400978
"3594"	7819	51643	10039	1132	"Texas"	"Public"	"Research Universities (high research activity)"	0	1	0.314858554676388
"3604"	8479	368986	11534	1098	"Texas"	"Private"	"Research Universities (very high research activity)"	0	0	0.305104360664558
"3644"	10693	166062	25617	1486	"Texas"	"Public"	"Research Universities (high research activity)"	0	1	0.514752522313063
"3652"	4759	266143	2345	1542	"Texas"	"Public"	"Research Universities (high research activity)"	0	1	0.365039801389899
"3656"	10674	119827	13031	816	"Texas"	"Public"	"Research Universities (high research activity)"	0	1	0.273726125904201
"3658"	5346	1917696	3812	5181	"Texas"	"Public"	"Research Universities (very high research activity)"	0	1	0.894384075209514
"3661"	16838	147031	23367	304	"Texas"	"Public"	"Research Universities (high research activity)"	0	1	0.282177381332822
"3675"	3057	1195581	14612	1900	"Utah"	"Public"	"Research Universities (very high research activity)"	1	1	0.56981304983344
"3677"	4353	656139	2010	550	"Utah"	"Public"	"Research Universities (high research activity)"	1	1	0.133392265674055
"3696"	5240	584566	3269	394	"Vermont"	"Public"	"Research Universities (high research activity)"	1	1	0.114931870780652
"3705"	2148	179936	1181	378	"Virginia"	"Public"	"Research Universities (high research activity)"	0	1	0.19189206346893
"3728"	7929	173864	21900	568	"Virginia"	"Public"	"Research Universities (high research activity)"	0	1	0.360818895027539
"3735"	3982	648104	8218	967	"Virginia"	"Public"	"Research Universities (high research activity)"	0	1	0.330640216076863
"3749"	7895	303312	20373	1220	"Virginia"	"Public"	"Research Universities (high research activity)"	0	1	0.421782846132123
"3754"	14592	836433	52078	2607	"Virginia"	"Public"	"Research Universities (very high research activity)"	0	1	0.916982295675074
"3798"	11181	4301328	42104	4098	"Washington"	"Public"	"Research Universities (very high research activity)"	0	1	1
"3800"	6563	579140	4908	1283	"Washington"	"Public"	"Research Universities (very high research activity)"	0	1	0.27263828346706
"3827"	4640	445892	4390	1174	"West Virginia"	"Public"	"Research Universities (high research activity)"	1	1	0.303731914722818
"3863"	5028	45698	4709	462	"Wisconsin"	"Private"	"Research Universities (high research activity)"	0	0	0.199506413023631
"3895"	5375	3251460	25146	5033	"Wisconsin"	"Public"	"Research Universities (very high research activity)"	0	1	1
"3896"	6421	113352	11963	775	"Wisconsin"	"Public"	"Research Universities (high research activity)"	0	1	0.324578675477169
"3932"	9201	184955	8770	427	"Wyoming"	"Public"	"Research Universities (high research activity)"	1	1	0.161639398165455
"3954"	3150	339928	12021	1370	"Florida"	"Public"	"Research Universities (high research activity)"	0	1	0.659445551822592
"3969"	7457	0	10805	4925	"Minnesota"	"Public"	"Research Universities (very high research activity)"	0	1	1
"3979"	4991	47419	30858	1528	"New York"	"Private"	"Research Universities (high research activity)"	0	0	0.975943708622146
"6740"	6865	1508492	16434	633	"Colorado"	"Public"	"Research Universities (very high research activity)"	0	1	0.280349875464696
"6964"	5626	0	6912	2502	"New Jersey"	"Public"	"Research Universities (very high research activity)"	0	1	0.993043908255772
"6968"	6400	0	2330	2467	"Virginia"	"Public"	"Research Universities (very high research activity)"	0	1	0.535572241827998
"7108"	3319	106417	1340	545	"Puerto Rico"	"Public"	"Research Universities (high research activity)"	1	1	0.204324227606043
"9635"	7359	344621	38773	741	"Florida"	"Public"	"Research Universities (high research activity)"	0	1	0.605021705163646
"9741"	2125	134967	2652	907	"Texas"	"Public"	"Research Universities (high research activity)"	0	1	0.479262506003452
