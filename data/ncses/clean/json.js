[
{
 "name": "Auburn University, Main Campus",
"fice": "1009",
"address": "Auburn, Alabama Auburn University AL 36849",
"lat":   32.6,
"long": -85.492,
"expenditures":      0,
"faculty.count":  14139,
"phd.count":   1344,
"publication.count":  16574 
}
,
{
 "name": "University of Alabama",
"fice": "1051",
"address": "739 University Blvd Tuscaloosa AL 35487-0166",
"lat": 33.214,
"long": -87.546,
"expenditures": 1.7389e+05,
"faculty.count":   8113,
"phd.count":   1172,
"publication.count":   7858 
}
,
{
 "name": "University of Alabama at Birmingham",
"fice": "1052",
"address": "Administration Bldg Suite 1070 Birmingham AL 35294-0110",
"lat": 33.502,
"long": -86.809,
"expenditures": 1.9605e+06,
"faculty.count":   4693,
"phd.count":   1014,
"publication.count":   4171 
}
,
{
 "name": "University of Alabama in Huntsville",
"fice": "1055",
"address": "301 Sparkman Dr Huntsville AL 35899",
"lat": 34.723,
"long": -86.638,
"expenditures": 3.1154e+05,
"faculty.count":   9902,
"phd.count":    227,
"publication.count":  24880 
}
,
{
 "name": "Arizona State University Main",
"fice": "1081",
"address": "University Drive and Mill Avenue Tempe AZ 85287",
"lat": 33.422,
"long": -111.94,
"expenditures": 7.3084e+05,
"faculty.count":   2414,
"phd.count":   2865,
"publication.count":   3717 
}
,
{
 "name": "Northern Arizona University",
"fice": "1082",
"address": "Knoles Drive, Babbitt Administrative Center Flagstaff AZ 86011-4132",
"lat":  35.18,
"long": -111.66,
"expenditures":  91546,
"faculty.count":   4754,
"phd.count":    299,
"publication.count":  15602 
}
,
{
 "name": "University of Arizona",
"fice": "1083",
"address": "1401 E University Tucson AZ 85721-0066",
"lat": 32.232,
"long": -110.95,
"expenditures": 1.9732e+06,
"faculty.count":   2197,
"phd.count":   2969,
"publication.count":  20513 
}
,
{
 "name": "University of Arkansas, Main Campus",
"fice": "1108",
"address": "Administration Bldg 425 Fayetteville AR 72701",
"lat": 36.062,
"long": -94.178,
"expenditures": 2.2372e+05,
"faculty.count":   5715,
"phd.count":    973,
"publication.count":  10477 
}
,
{
 "name": "California Institute of Technology",
"fice": "1131",
"address": "1200 E California Blvd Pasadena CA 91125",
"lat": 34.139,
"long": -118.13,
"expenditures": 1.7642e+06,
"faculty.count":   4659,
"phd.count":   1291,
"publication.count":  19218 
}
,
{
 "name": "San Diego State University",
"fice": "1151",
"address": "5500 Campanile Dr San Diego CA 92182",
"lat": 32.775,
"long": -117.07,
"expenditures": 2.41e+05,
"faculty.count":   2436,
"phd.count":     47,
"publication.count":   2049 
}
,
{
 "name": "Claremont Graduate School",
"fice": "1169",
"address": "150 E Tenth St Claremont CA 91711-6160",
"lat": 34.104,
"long": -117.71,
"expenditures":   8718,
"faculty.count":    530,
"phd.count":    759,
"publication.count":    589 
}
,
{
 "name": "Stanford University",
"fice": "1305",
"address": "  Stanford CA 94305",
"lat": 37.432,
"long": -122.18,
"expenditures": 3.6617e+06,
"faculty.count":   1808,
"phd.count":   4602,
"publication.count":    497 
}
,
{
 "name": "University of California-Berkeley",
"fice": "1312",
"address": "200 California Hall Berkeley CA 94720",
"lat": 37.872,
"long": -122.26,
"expenditures": 1.822e+06,
"faculty.count":   1212,
"phd.count":   5903,
"publication.count":   1121 
}
,
{
 "name": "University of California-Davis",
"fice": "1313",
"address": "One Shields Avenue Davis CA 95616-8678",
"lat":  38.54,
"long": -121.75,
"expenditures": 1.7403e+06,
"faculty.count":   1210,
"phd.count":   3231,
"publication.count":   1743 
}
,
{
 "name": "University of California-Irvine",
"fice": "1314",
"address": "501 Aldrich Hall Irvine CA 92697",
"lat": 33.667,
"long": -117.77,
"expenditures": 1.1417e+06,
"faculty.count":   7113,
"phd.count":   2290,
"publication.count":   6844 
}
,
{
 "name": "University of California-Los Angeles",
"fice": "1315",
"address": "405 Hilgard Ave Los Angeles CA 90095-1405",
"lat": 34.069,
"long": -118.44,
"expenditures": 3.2644e+06,
"faculty.count":   3986,
"phd.count":   4916,
"publication.count":   3232 
}
,
{
 "name": "University of California-Riverside",
"fice": "1316",
"address": "900 University Ave Riverside CA 92521",
"lat": 33.976,
"long": -117.33,
"expenditures": 3.5621e+05,
"faculty.count":   1481,
"phd.count":   1341,
"publication.count":   2463 
}
,
{
 "name": "University of California-San Diego",
"fice": "1317",
"address": "9500 Gilman Dr La Jolla CA 92093",
"lat": 32.877,
"long": -117.24,
"expenditures": 3.2716e+06,
"faculty.count":   7626,
"phd.count":   2863,
"publication.count":  12525 
}
,
{
 "name": "University of California-Santa Barbara",
"fice": "1320",
"address": "5221 Cheadle Hall Santa Barbara CA 93106",
"lat": 34.415,
"long": -119.85,
"expenditures": 7.2808e+05,
"faculty.count":   9914,
"phd.count":   2261,
"publication.count":  44820 
}
,
{
 "name": "University of California-Santa Cruz",
"fice": "1321",
"address": "1156 High St Santa Cruz CA 95064-1011",
"lat": 36.997,
"long": -122.07,
"expenditures": 4.3413e+05,
"faculty.count":   3466,
"phd.count":    918,
"publication.count":   5914 
}
,
{
 "name": "University of Southern California",
"fice": "1328",
"address": "University Park Los Angeles CA 90089",
"lat": 34.021,
"long": -118.28,
"expenditures": 2.3551e+06,
"faculty.count":   5221,
"phd.count":   3795,
"publication.count":   8248 
}
,
{
 "name": "Colorado School of Mines",
"fice": "1348",
"address": "1500 Illinois St Golden CO 80401",
"lat": 39.751,
"long": -105.23,
"expenditures": 1.3234e+05,
"faculty.count":   7641,
"phd.count":    327,
"publication.count":  39274 
}
,
{
 "name": "Colorado State University",
"fice": "1350",
"address": "102 Administration Building Fort Collins CO 80523-0100",
"lat": 40.575,
"long": -105.08,
"expenditures": 1.2477e+06,
"faculty.count":   5547,
"phd.count":   1427,
"publication.count":  27845 
}
,
{
 "name": "University of Colorado at Boulder",
"fice": "1370",
"address": "Regent Drive at Broadway Boulder CO 80309-0017",
"lat": 40.004,
"long": -105.27,
"expenditures": 1.5387e+06,
"faculty.count":   5289,
"phd.count":   2133,
"publication.count":   3788 
}
,
{
 "name": "University of Denver",
"fice": "1371",
"address": "2199 S. University Blvd Denver CO 80208",
"lat": 39.696,
"long": -104.96,
"expenditures":  63099,
"faculty.count":   1517,
"phd.count":    513,
"publication.count":   1356 
}
,
{
 "name": "Yale University",
"fice": "1426",
"address": "Woodbridge Hall New Haven CT 6520",
"lat":  41.31,
"long": -72.929,
"expenditures": 2.4112e+06,
"faculty.count":   5450,
"phd.count":   2454,
"publication.count":   4607 
}
,
{
 "name": "University of Delaware",
"fice": "1431",
"address": "104 Hullihen Hall Newark DE 19716",
"lat":  39.68,
"long": -75.754,
"expenditures": 5.5469e+05,
"faculty.count":   8658,
"phd.count":   1365,
"publication.count":  11482 
}
,
{
 "name": "Catholic University of America",
"fice": "1437",
"address": "620 Michigan Avenue, NE Washington DC 20064",
"lat": 38.936,
"long": -76.999,
"expenditures": 1.1348e+05,
"faculty.count":   4595,
"phd.count":    552,
"publication.count":   2221 
}
,
{
 "name": "George Washington University",
"fice": "1444",
"address": "2121 I Street, NW Washington DC 20052",
"lat": 38.899,
"long": -77.048,
"expenditures": 5.9681e+05,
"faculty.count":   7141,
"phd.count":   1570,
"publication.count":   5473 
}
,
{
 "name": "Georgetown University",
"fice": "1445",
"address": "37th and O St NW Washington DC 20057",
"lat": 38.909,
"long": -77.073,
"expenditures": 7.1373e+05,
"faculty.count":   6124,
"phd.count":    674,
"publication.count":  10400 
}
,
{
 "name": "Howard University",
"fice": "1448",
"address": "2400 Sixth St NW Washington DC 20059-0001",
"lat": 38.921,
"long": -77.019,
"expenditures": 2.5043e+05,
"faculty.count":   5916,
"phd.count":    659,
"publication.count":  10863 
}
,
{
 "name": "Florida Institute of Technology",
"fice": "1469",
"address": "150 West University Boulevard Melbourne FL 32901-6975",
"lat": 28.065,
"long": -80.625,
"expenditures":  42183,
"faculty.count":   7028,
"phd.count":    175,
"publication.count":   5413 
}
,
{
 "name": "Florida Atlantic University",
"fice": "1481",
"address": "777 Glades Rd Boca Raton FL 33431-0991",
"lat": 26.373,
"long": -80.102,
"expenditures": 1.1888e+05,
"faculty.count":  10449,
"phd.count":    527,
"publication.count":  92652 
}
,
{
 "name": "Florida State University",
"fice": "1489",
"address": "222 S. Copeland Street Tallahassee FL 32306-1037",
"lat":  30.44,
"long": -84.291,
"expenditures": 7.4865e+05,
"faculty.count":   6519,
"phd.count":   2318,
"publication.count":   2821 
}
,
{
 "name": "University of Florida",
"fice": "1535",
"address": "355 Tigert Hall Gainesville FL 32611-3115",
"lat": 29.638,
"long": -82.361,
"expenditures": 1.6014e+06,
"faculty.count":   2312,
"phd.count":   4690,
"publication.count":   3507 
}
,
{
 "name": "University of Miami",
"fice": "1536",
"address": "University of Miami Coral Gables FL 33124",
"lat":  25.72,
"long": -80.276,
"expenditures": 1.0648e+06,
"faculty.count":  11006,
"phd.count":   1055,
"publication.count":  13670 
}
,
{
 "name": "University of South Florida",
"fice": "1537",
"address": "4202 East Fowler Ave Tampa FL 33620-9951",
"lat": 28.057,
"long": -82.416,
"expenditures": 1.0486e+06,
"faculty.count":   5393,
"phd.count":   1572,
"publication.count":   3907 
}
,
{
 "name": "Clark Atlanta University",
"fice": "1559",
"address": "223 James P Brawley Drive, S.W. Atlanta GA 30314-4391",
"lat": 33.749,
"long": -84.413,
"expenditures":  68814,
"faculty.count":   8880,
"phd.count":    192,
"publication.count":  16117 
}
,
{
 "name": "Emory University",
"fice": "1564",
"address": "408 Administration Building, 201 Dowman Drive Atlanta GA 30322",
"lat": 33.799,
"long": -84.326,
"expenditures": 1.8647e+06,
"faculty.count":   2504,
"phd.count":   1367,
"publication.count":   1306 
}
,
{
 "name": "Georgia State University",
"fice": "1574",
"address": "33 gilmer st Atlanta GA 30303-3083",
"lat": 33.753,
"long": -84.387,
"expenditures": 1.731e+05,
"faculty.count":   6363,
"phd.count":   1181,
"publication.count":   7951 
}
,
{
 "name": "University of Georgia",
"fice": "1598",
"address": "Administration Building Athens GA 30602",
"lat": 33.943,
"long": -83.374,
"expenditures": 6.9885e+05,
"faculty.count":   3021,
"phd.count":   2766,
"publication.count":   3586 
}
,
{
 "name": "University of Hawaii at Manoa",
"fice": "1610",
"address": "2500 Campus Road, Hawaii Hall, Room 001 Honolulu HI 96822-2301",
"lat": 21.293,
"long": -157.82,
"expenditures": 1.3293e+06,
"faculty.count":   8977,
"phd.count":   1212,
"publication.count":  18863 
}
,
{
 "name": "University of Idaho",
"fice": "1626",
"address": "875 Perimeter Drive Moscow ID 83844-2282",
"lat": 46.727,
"long": -117.02,
"expenditures": 3.1931e+05,
"faculty.count":   3720,
"phd.count":    623,
"publication.count":   3243 
}
,
{
 "name": "Illinois Institute of Technology",
"fice": "1691",
"address": "3300 S Federal St Chicago IL 60616",
"lat": 41.834,
"long": -87.628,
"expenditures": 1.2038e+05,
"faculty.count":   4158,
"phd.count":    489,
"publication.count":   2436 
}
,
{
 "name": "Loyola University of Chicago",
"fice": "1710",
"address": "1032 W. Sheridan Rd Chicago IL 60660",
"lat": 41.999,
"long": -87.657,
"expenditures": 1.8393e+05,
"faculty.count":   7914,
"phd.count":    997,
"publication.count":  30611 
}
,
{
 "name": "Northern Illinois University",
"fice": "1737",
"address": "1425 W. Lincoln Hwy. Dekalb IL 60115-2828",
"lat": 41.934,
"long": -88.766,
"expenditures":  75007,
"faculty.count":  15627,
"phd.count":    545,
"publication.count":  22496 
}
,
{
 "name": "Northwestern Univ",
"fice": "1739",
"address": "633 Clark St Evanston IL 60208",
"lat": 42.058,
"long": -87.674,
"expenditures": 1.74e+06,
"faculty.count":   2487,
"phd.count":   2458,
"publication.count":   2273 
}
,
{
 "name": "Southern Illinois University-Carbondale",
"fice": "1758",
"address": "Lincoln Drive Carbondale IL 62901-4512",
"lat": 37.719,
"long": -89.218,
"expenditures": 1.1916e+05,
"faculty.count":   6421,
"phd.count":   1006,
"publication.count":   4939 
}
,
{
 "name": "University of Chicago",
"fice": "1774",
"address": "5801 S Ellis Ave Chicago IL 60637",
"lat": 41.789,
"long": -87.601,
"expenditures": 1.777e+06,
"faculty.count":   2840,
"phd.count":   2598,
"publication.count":   2558 
}
,
{
 "name": "University of Illinois at Urbana-Champaign",
"fice": "1775",
"address": "601 E John Street Champaign IL 61820-5711",
"lat": 40.109,
"long": -88.23,
"expenditures": 1.9056e+06,
"faculty.count":  14492,
"phd.count":   4975,
"publication.count":  27576 
}
,
{
 "name": "University of Illinois at Chicago",
"fice": "1776",
"address": "601 S Morgan Chicago IL 60607",
"lat": 41.873,
"long": -87.651,
"expenditures": 1.365e+06,
"faculty.count":   2716,
"phd.count":   1996,
"publication.count":    728 
}
,
{
 "name": "Indiana University at Bloomington",
"fice": "1809",
"address": "107 South Indiana Ave. Bloomington IN 47405-7000",
"lat": 39.166,
"long": -86.527,
"expenditures":      0,
"faculty.count":  11384,
"phd.count":   2851,
"publication.count":  16543 
}
,
{
 "name": "Indiana University-Purdue Univ at Indianapolis",
"fice": "1813",
"address": "425 University Blvd Indianapolis IN 46202-5143",
"lat": 39.773,
"long": -86.172,
"expenditures":      0,
"faculty.count":   6000,
"phd.count":      0,
"publication.count":   7947 
}
,
{
 "name": "Purdue University, Main Campus",
"fice": "1825",
"address": "Hovde Hall of Administration West Lafayette IN 47907-2040",
"lat": 40.428,
"long": -86.914,
"expenditures":      0,
"faculty.count":   5162,
"phd.count":   4233,
"publication.count":   2286 
}
,
{
 "name": "University of Notre Dame",
"fice": "1840",
"address": "400 Main Building Notre Dame IN 46556",
"lat": 41.617,
"long": -86.29,
"expenditures": 3.7486e+05,
"faculty.count":   6322,
"phd.count":   1110,
"publication.count":   3042 
}
,
{
 "name": "Iowa State University",
"fice": "1869",
"address": "3410 Beardshear Hall Ames IA 50011-2030",
"lat": 42.026,
"long": -93.648,
"expenditures": 6.7346e+05,
"faculty.count":   7701,
"phd.count":   2103,
"publication.count":  32561 
}
,
{
 "name": "University of Iowa",
"fice": "1892",
"address": "101 Jessup Hall Iowa City IA 52242-1316",
"lat": 41.661,
"long": -91.536,
"expenditures": 1.5452e+06,
"faculty.count":   4412,
"phd.count":   2379,
"publication.count":   2710 
}
,
{
 "name": "Kansas State University",
"fice": "1928",
"address": "Anderson Hall Manhattan KS 66506",
"lat": 39.189,
"long": -96.581,
"expenditures": 3.7925e+05,
"faculty.count":   4613,
"phd.count":   1046,
"publication.count":  11556 
}
,
{
 "name": "Wichita State University",
"fice": "1950",
"address": "1845 Fairmount Wichita KS 67260-0124",
"lat": 37.716,
"long": -97.297,
"expenditures":  92118,
"faculty.count":   5701,
"phd.count":    199,
"publication.count":  18064 
}
,
{
 "name": "University of Kentucky",
"fice": "1989",
"address": "South Limestone Lexington KY 40506-0032",
"lat": 38.028,
"long": -84.511,
"expenditures": 9.9709e+05,
"faculty.count":  14915,
"phd.count":   1826,
"publication.count":  22466 
}
,
{
 "name": "University of Louisville",
"fice": "1999",
"address": "2301 S 3rd St Louisville KY 40292-0001",
"lat": 38.216,
"long": -85.759,
"expenditures": 4.5394e+05,
"faculty.count":   2810,
"phd.count":    948,
"publication.count":   5793 
}
,
{
 "name": "Louisiana State Univ & Agric & Mechanical Col",
"fice": "2010",
"address": "156 Thomas Boyd Hall Baton Rouge LA 70803-2750",
"lat": 30.552,
"long": -91.197,
"expenditures": 4.8888e+05,
"faculty.count":   3893,
"phd.count":   1717,
"publication.count":   7878 
}
,
{
 "name": "Tulane University",
"fice": "2029",
"address": "6823 Saint Charles Ave New Orleans LA 70118-5698",
"lat":  29.94,
"long": -90.12,
"expenditures": 6.6561e+05,
"faculty.count":  10892,
"phd.count":    767,
"publication.count":   5876 
}
,
{
 "name": "University of Maine",
"fice": "2053",
"address": "168 College Avenue Orono ME 4469",
"lat": 44.896,
"long": -68.674,
"expenditures": 2.6593e+05,
"faculty.count":   2889,
"phd.count":    340,
"publication.count":   2053 
}
,
{
 "name": "University of Maryland at College Park",
"fice": "2103",
"address": "  College Park MD 20742",
"lat": 38.989,
"long": -76.94,
"expenditures": 1.4723e+06,
"faculty.count":    798,
"phd.count":   3970,
"publication.count":   6094 
}
,
{
 "name": "University of Maryland Baltimore County",
"fice": "2105",
"address": "1000 Hilltop Circle Baltimore MD 21250",
"lat": 39.257,
"long": -76.712,
"expenditures": 3.1381e+05,
"faculty.count":   4339,
"phd.count":    590,
"publication.count":   1801 
}
,
{
 "name": "Boston College",
"fice": "2128",
"address": "140 Commonwealth Avenue Chestnut Hill MA 2467",
"lat": 42.338,
"long": -71.171,
"expenditures": 1.4607e+05,
"faculty.count":   3624,
"phd.count":    955,
"publication.count":   4118 
}
,
{
 "name": "Boston University",
"fice": "2130",
"address": "One Silber Way Boston MA 2215",
"lat":  42.35,
"long":  -71.1,
"expenditures": 1.6128e+06,
"faculty.count":   7299,
"phd.count":   2464,
"publication.count":  50873 
}
,
{
 "name": "Brandeis University",
"fice": "2133",
"address": "415 South St Waltham MA 02454-9110",
"lat": 42.365,
"long": -71.255,
"expenditures": 2.7283e+05,
"faculty.count":   6754,
"phd.count":    588,
"publication.count":   1379 
}
,
{
 "name": "Clark University",
"fice": "2139",
"address": "950 Main St Worcester MA 01610-1477",
"lat": 42.251,
"long": -71.824,
"expenditures":  23050,
"faculty.count":   2977,
"phd.count":    222,
"publication.count":   6205 
}
,
{
 "name": "Harvard University",
"fice": "2155",
"address": "Massachusetts Hall Cambridge MA 2138",
"lat": 42.374,
"long": -71.118,
"expenditures": 2.7089e+06,
"faculty.count":   7362,
"phd.count":   4341,
"publication.count":   3218 
}
,
{
 "name": "Massachusetts Institute of Technology",
"fice": "2178",
"address": "77 Massachusetts Avenue Cambridge MA 02139-4307",
"lat":  42.36,
"long": -71.092,
"expenditures": 3.2213e+06,
"faculty.count":   9818,
"phd.count":   4181,
"publication.count":  11569 
}
,
{
 "name": "Northeastern University",
"fice": "2199",
"address": "360 Huntington Ave Boston MA 02115-5005",
"lat":  42.34,
"long": -71.089,
"expenditures": 2.4085e+05,
"faculty.count":   1415,
"phd.count":    662,
"publication.count":    633 
}
,
{
 "name": "Tufts University",
"fice": "2219",
"address": "  Medford MA 02155-5555",
"lat": 42.406,
"long": -71.121,
"expenditures": 6.5543e+05,
"faculty.count":   1336,
"phd.count":    777,
"publication.count":   1215 
}
,
{
 "name": "University of Massachusetts at Amherst",
"fice": "2221",
"address": "374 Whitmore Building Amherst MA 1003",
"lat": 42.391,
"long": -72.527,
"expenditures": 4.9426e+05,
"faculty.count":   8779,
"phd.count":   1868,
"publication.count":   8579 
}
,
{
 "name": "Michigan State University",
"fice": "2290",
"address": "  East Lansing MI 48824-1046",
"lat": 42.725,
"long": -84.474,
"expenditures": 1.0903e+06,
"faculty.count":   8758,
"phd.count":   3218,
"publication.count":   8205 
}
,
{
 "name": "Michigan Technological University",
"fice": "2292",
"address": "1400 Townsend Drive Houghton MI 49931-1295",
"lat": 47.118,
"long": -88.546,
"expenditures": 1.5998e+05,
"faculty.count":   4225,
"phd.count":    373,
"publication.count":  14043 
}
,
{
 "name": "Wayne State University",
"fice": "2329",
"address": "656 West Kirby Street Detroit MI 48202",
"lat": 42.359,
"long": -83.073,
"expenditures": 8.0324e+05,
"faculty.count":   4648,
"phd.count":   1353,
"publication.count":   8232 
}
,
{
 "name": "Western Michigan University",
"fice": "2330",
"address": "1903 West Michigan Avenue Kalamazoo MI 49008-5200",
"lat": 42.283,
"long": -85.615,
"expenditures":  70574,
"faculty.count":   6038,
"phd.count":    674,
"publication.count":  21227 
}
,
{
 "name": "Jackson State University",
"fice": "2410",
"address": "1440 J R Lynch St Jackson MS 39217",
"lat": 32.297,
"long": -90.207,
"expenditures": 2.1919e+05,
"faculty.count":   5796,
"phd.count":    337,
"publication.count":   1816 
}
,
{
 "name": "Mississippi State University",
"fice": "2423",
"address": "Lee Boulevard Mississippi State MS 39762",
"lat": 33.425,
"long": -88.88,
"expenditures": 6.5547e+05,
"faculty.count":   1981,
"phd.count":    852,
"publication.count":   4871 
}
,
{
 "name": "University of Southern Mississippi",
"fice": "2441",
"address": "118 College Drive # 0001 Hattiesburg MS 39406-0001",
"lat": 31.327,
"long": -89.338,
"expenditures": 2.5995e+05,
"faculty.count":  10400,
"phd.count":    832,
"publication.count":  26676 
}
,
{
 "name": "University of Missouri, Columbia",
"fice": "2516",
"address": "105 Jesse Hall Columbia MO 65211",
"lat": 38.941,
"long": -92.326,
"expenditures": 7.0774e+05,
"faculty.count":   5989,
"phd.count":   2000,
"publication.count":   5985 
}
,
{
 "name": "University of Missouri, Kansas City",
"fice": "2518",
"address": "5100 Rockhill Rd Kansas City MO 64110",
"lat": 39.035,
"long": -94.578,
"expenditures": 1.1195e+05,
"faculty.count":  10358,
"phd.count":    341,
"publication.count":  42054 
}
,
{
 "name": "University of Missouri, St Louis",
"fice": "2519",
"address": "One University Boulevard Saint Louis MO 63121-4400",
"lat": 38.711,
"long": -90.309,
"expenditures":  34990,
"faculty.count":   9668,
"phd.count":    366,
"publication.count":  37155 
}
,
{
 "name": "Washington University",
"fice": "2520",
"address": "One Brookings Drive Saint Louis MO 63130-4899",
"lat": 38.648,
"long": -90.312,
"expenditures": 2.7699e+06,
"faculty.count":   6620,
"phd.count":   1642,
"publication.count":  22088 
}
,
{
 "name": "University of Montana",
"fice": "2536",
"address": "Missoula, Montana Missoula MT 59812",
"lat": 46.861,
"long": -113.98,
"expenditures": 2.3606e+05,
"faculty.count":  12432,
"phd.count":    369,
"publication.count":  56591 
}
,
{
 "name": "University of Nebraska at Lincoln",
"fice": "2565",
"address": "14th and R St Lincoln NE 68588",
"lat": 40.822,
"long": -96.702,
"expenditures": 5.2748e+05,
"faculty.count":   4597,
"phd.count":   1753,
"publication.count":  11048 
}
,
{
 "name": "University of Nevada-Reno",
"fice": "2568",
"address": "North Virginia Street Reno NV 89557",
"lat": 39.553,
"long": -119.83,
"expenditures": 4.1593e+05,
"faculty.count":   6708,
"phd.count":    660,
"publication.count":  40086 
}
,
{
 "name": "University of Nevada-Las Vegas",
"fice": "2569",
"address": "4505 S Maryland Pky Las Vegas NV 89154",
"lat": 36.107,
"long": -115.14,
"expenditures": 2.6061e+05,
"faculty.count":   6327,
"phd.count":    521,
"publication.count":  15337 
}
,
{
 "name": "Dartmouth College",
"fice": "2573",
"address": "207 Parkhurst Hall Hanover NH 03755-3529",
"lat": 43.714,
"long": -72.26,
"expenditures": 8.7548e+05,
"faculty.count":   3806,
"phd.count":    518,
"publication.count":   7727 
}
,
{
 "name": "New Jersey Institute Technology",
"fice": "2621",
"address": "University Heights Newark NJ 7102",
"lat": 40.742,
"long": -74.177,
"expenditures": 2.5681e+05,
"faculty.count":   7791,
"phd.count":    434,
"publication.count":   6290 
}
,
{
 "name": "Princeton University",
"fice": "2627",
"address": "1 Nassau Hall Princeton NJ 08544-0070",
"lat": 40.348,
"long": -74.664,
"expenditures": 8.2648e+05,
"faculty.count":   7346,
"phd.count":   2204,
"publication.count":  27341 
}
,
{
 "name": "Rutgers the State Univ of NJ Newark Campus",
"fice": "2631",
"address": "249 University Avenue, Blumenthal Hall Newark NJ 7102",
"lat": 40.741,
"long": -74.175,
"expenditures":      0,
"faculty.count":   8212,
"phd.count":    403,
"publication.count":  17639 
}
,
{
 "name": "Stevens Institute of Technology",
"fice": "2639",
"address": "Castle Point On Hudson Hoboken NJ 07030-5991",
"lat": 40.745,
"long": -74.025,
"expenditures": 1.3598e+05,
"faculty.count":   4021,
"phd.count":    266,
"publication.count":  14705 
}
,
{
 "name": "University of New Mexico, All Campuses",
"fice": "2663",
"address": "1700 Lomas Blvd NE Albuquerque NM 87106",
"lat": 35.084,
"long": -106.62,
"expenditures": 8.6746e+05,
"faculty.count":   3070,
"phd.count":   1291,
"publication.count":   1694 
}
,
{
 "name": "Clarkson University",
"fice": "2699",
"address": "8 Clarkson Ave Potsdam NY 13699",
"lat": 44.664,
"long":    -75,
"expenditures":  49398,
"faculty.count":   7789,
"phd.count":    181,
"publication.count":   9569 
}
,
{
 "name": "Columbia University in the City of New York",
"fice": "2707",
"address": "West 116 St and Broadway New York NY 10027",
"lat": 40.808,
"long": -73.962,
"expenditures": 3.1004e+06,
"faculty.count":   3524,
"phd.count":   3330,
"publication.count":   1866 
}
,
{
 "name": "Fordham University",
"fice": "2722",
"address": "441 E Fordham Rd Bronx NY 10458",
"lat": 40.861,
"long": -73.89,
"expenditures":  14911,
"faculty.count":  12980,
"phd.count":    728,
"publication.count":  36043 
}
,
{
 "name": "New York University",
"fice": "2785",
"address": "70 Washington Sq South New York NY 10012-1091",
"lat": 40.729,
"long": -73.997,
"expenditures": 1.3247e+06,
"faculty.count":  11976,
"phd.count":   2640,
"publication.count":  15864 
}
,
{
 "name": "Rensselaer Polytechnic Institute",
"fice": "2803",
"address": "110 8th St Troy NY 12180-3590",
"lat": 42.729,
"long": -73.679,
"expenditures": 3.0022e+05,
"faculty.count":   7687,
"phd.count":   1015,
"publication.count":  12745 
}
,
{
 "name": "SUNY at Albany",
"fice": "2835",
"address": "1400 Washington Avenue Albany NY 12222",
"lat": 42.687,
"long": -73.825,
"expenditures": 6.8996e+05,
"faculty.count":   7689,
"phd.count":   1041,
"publication.count":   7983 
}
,
{
 "name": "SUNY at Binghamton",
"fice": "2836",
"address": "4400 Vestal Parkway East Vestal NY 13850-6000",
"lat": 42.088,
"long": -75.971,
"expenditures":  87199,
"faculty.count":   3850,
"phd.count":    831,
"publication.count":   4170 
}
,
{
 "name": "SUNY at Buffalo",
"fice": "2837",
"address": "12 Capen Hall Buffalo NY 14260-1660",
"lat":     43,
"long": -78.789,
"expenditures": 1.0352e+06,
"faculty.count":   8590,
"phd.count":   2186,
"publication.count":  21541 
}
,
{
 "name": "SUNY College of Environmental Sci & Forestry",
"fice": "2851",
"address": "One Forestry Dr. Syracuse NY 13210",
"lat": 43.034,
"long": -76.139,
"expenditures":  52262,
"faculty.count":  15409,
"phd.count":    139,
"publication.count":  27291 
}
,
{
 "name": "University of Rochester",
"fice": "2894",
"address": "Wilson Blvd. - Wallis Hall Rochester NY 14627-0011",
"lat": 43.127,
"long": -77.632,
"expenditures": 1.8276e+06,
"faculty.count":   8505,
"phd.count":   1559,
"publication.count":  22724 
}
,
{
 "name": "Yeshiva University",
"fice": "2903",
"address": "500 W 185th St New York NY 10033-3299",
"lat": 40.851,
"long": -73.929,
"expenditures": 1.024e+06,
"faculty.count":   9202,
"phd.count":    534,
"publication.count":  17468 
}
,
{
 "name": "North Carolina Agricultural & Tech State Univ",
"fice": "2905",
"address": "1601 E Market  St Greensboro NC 27411",
"lat": 36.077,
"long": -79.77,
"expenditures": 1.1021e+05,
"faculty.count":   5656,
"phd.count":     88,
"publication.count":  10214 
}
,
{
 "name": "Duke University",
"fice": "2920",
"address": "103 Allen Bldg Durham NC 27708",
"lat": 36.002,
"long": -78.942,
"expenditures": 2.795e+06,
"faculty.count":   3179,
"phd.count":   2146,
"publication.count":   4654 
}
,
{
 "name": "North Carolina State University at Raleigh",
"fice": "2972",
"address": "2101 Hillsborough Street Raleigh NC 27695-7001",
"lat": 35.785,
"long": -78.675,
"expenditures": 8.3792e+05,
"faculty.count":   3285,
"phd.count":   2596,
"publication.count":  20192 
}
,
{
 "name": "University of North Carolina at Chapel Hill",
"fice": "2974",
"address": "103 South Bldg Cb 9100 Chapel Hill NC 27599",
"lat": 35.912,
"long": -79.051,
"expenditures": 2.386e+06,
"faculty.count":  10997,
"phd.count":   3380,
"publication.count":  22475 
}
,
{
 "name": "University of North Carolina at Greensboro",
"fice": "2976",
"address": "1000 Spring Garden St Greensboro NC 27402-6170",
"lat": 36.068,
"long": -79.81,
"expenditures":  35192,
"faculty.count":   7912,
"phd.count":    586,
"publication.count":  11986 
}
,
{
 "name": "Wake Forest University",
"fice": "2978",
"address": "1834 Wake Forest Road Winston Salem NC 27106",
"lat": 36.135,
"long": -80.279,
"expenditures": 9.42e+05,
"faculty.count":   5935,
"phd.count":    343,
"publication.count":   3328 
}
,
{
 "name": "Case Western Reserve University",
"fice": "3024",
"address": "10900 Euclid Ave Cleveland OH 44106",
"lat": 41.501,
"long": -81.606,
"expenditures": 1.8437e+06,
"faculty.count":   6546,
"phd.count":   1373,
"publication.count":   4440 
}
,
{
 "name": "University of Dayton",
"fice": "3127",
"address": "300 College Park Dayton OH 45469",
"lat": 39.737,
"long": -84.174,
"expenditures": 4.1461e+05,
"faculty.count":  12215,
"phd.count":    160,
"publication.count":  35934 
}
,
{
 "name": "University of Toledo",
"fice": "3131",
"address": "2801 W Bancroft Toledo OH 43606-3390",
"lat": 41.658,
"long": -83.615,
"expenditures": 1.466e+05,
"faculty.count":   9040,
"phd.count":    648,
"publication.count":  14691 
}
,
{
 "name": "University of Tulsa",
"fice": "3185",
"address": "800 South Tucker Drive Tulsa OK 74104-3189",
"lat": 36.152,
"long": -95.945,
"expenditures":  56328,
"faculty.count":   4517,
"phd.count":    173,
"publication.count":   4236 
}
,
{
 "name": "Oregon State University",
"fice": "3210",
"address": "1500 S.W. Jefferson Corvallis OR 97331",
"lat": 44.565,
"long": -123.27,
"expenditures": 7.7252e+05,
"faculty.count":   3264,
"phd.count":   1204,
"publication.count":   2590 
}
,
{
 "name": "University of Oregon",
"fice": "3223",
"address": "110 Johnson Hall Eugene OR 97403",
"lat": 44.045,
"long": -123.07,
"expenditures": 3.3603e+05,
"faculty.count":   3709,
"phd.count":   1070,
"publication.count":   2711 
}
,
{
 "name": "Carnegie Mellon University",
"fice": "3242",
"address": "5000 Forbes Avenue Pittsburgh PA 15213-3890",
"lat": 40.445,
"long": -79.943,
"expenditures": 1.1995e+06,
"faculty.count":   7900,
"phd.count":   1659,
"publication.count":  10089 
}
,
{
 "name": "Drexel University",
"fice": "3256",
"address": "3141 Chestnut St Philadelphia PA 19104",
"lat": 39.955,
"long": -75.189,
"expenditures": 4.3495e+05,
"faculty.count":   6006,
"phd.count":    813,
"publication.count":   4025 
}
,
{
 "name": "Lehigh University",
"fice": "3289",
"address": "27 Memorial Drive West Bethlehem PA 18015",
"lat": 40.607,
"long": -75.379,
"expenditures": 1.3839e+05,
"faculty.count":   4714,
"phd.count":    629,
"publication.count":   5301 
}
,
{
 "name": "Temple University",
"fice": "3371",
"address": "1801 North Broad Street Philadelphia PA 19122-6096",
"lat": 39.981,
"long": -75.158,
"expenditures": 3.8492e+05,
"faculty.count":   7005,
"phd.count":   1541,
"publication.count":  12671 
}
,
{
 "name": "University of Pennsylvania",
"fice": "3378",
"address": "34th and Spruce Streets Philadelphia PA 19104-6303",
"lat": 39.951,
"long": -75.194,
"expenditures": 3.2265e+06,
"faculty.count":   9956,
"phd.count":   3377,
"publication.count":  34502 
}
,
{
 "name": "Brown University",
"fice": "3401",
"address": "One Prospect Street Providence RI 2912",
"lat": 41.828,
"long": -71.404,
"expenditures": 6.3727e+05,
"faculty.count":   5053,
"phd.count":   1338,
"publication.count":   2215 
}
,
{
 "name": "University of Rhode Island",
"fice": "3414",
"address": "Green Hall, 35 Campus Avenue Kingston RI 2881",
"lat": 41.482,
"long": -71.531,
"expenditures": 3.4965e+05,
"faculty.count":   5992,
"phd.count":    532,
"publication.count":   3346 
}
,
{
 "name": "Clemson University",
"fice": "3425",
"address": "201 Sikes Hall Clemson SC 29634",
"lat": 34.677,
"long": -82.834,
"expenditures": 4.0226e+05,
"faculty.count":   5850,
"phd.count":   1065,
"publication.count":  10340 
}
,
{
 "name": "University of South Carolina at Columbia",
"fice": "3448",
"address": "Columbia-Campus Columbia SC 29208",
"lat": 34.006,
"long": -81.038,
"expenditures":      0,
"faculty.count":   5071,
"phd.count":   1609,
"publication.count":   6084 
}
,
{
 "name": "South Dakota State University",
"fice": "3471",
"address": "Administration Lane Brookings SD 57007-1898",
"lat": 44.319,
"long": -96.783,
"expenditures": 1.1353e+05,
"faculty.count":   7520,
"phd.count":    153,
"publication.count":  46452 
}
,
{
 "name": "University of Memphis",
"fice": "3509",
"address": "Southern Avenue Memphis TN 38152",
"lat": 35.116,
"long": -89.994,
"expenditures": 1.0949e+05,
"faculty.count":   7595,
"phd.count":    754,
"publication.count":   2394 
}
,
{
 "name": "University of Tennessee at Knoxville",
"fice": "3530",
"address": "527 Andy Holt Tower Knoxville TN 37996",
"lat": 35.955,
"long": -83.93,
"expenditures":      0,
"faculty.count":   4588,
"phd.count":   1740,
"publication.count":   3517 
}
,
{
 "name": "Vanderbilt University",
"fice": "3535",
"address": "2101 West End Avenue Nashville TN 37240",
"lat": 36.144,
"long": -86.805,
"expenditures": 2.0455e+06,
"faculty.count":   3829,
"phd.count":   1744,
"publication.count":  18618 
}
,
{
 "name": "University of North TX",
"fice": "3594",
"address": "Chestnut Ave. Denton TX 76203-1277",
"lat": 33.211,
"long": -97.151,
"expenditures":  51643,
"faculty.count":   7819,
"phd.count":   1132,
"publication.count":  10039 
}
,
{
 "name": "Rice University",
"fice": "3604",
"address": "6100 S Main Houston TX 77005-1827",
"lat": 29.716,
"long": -95.404,
"expenditures": 3.6899e+05,
"faculty.count":   8479,
"phd.count":   1098,
"publication.count":  11534 
}
,
{
 "name": "Texas Tech University",
"fice": "3644",
"address": "Broadway and University Avenue Lubbock TX 79409-5005",
"lat":  33.58,
"long": -101.88,
"expenditures": 1.6606e+05,
"faculty.count":  10693,
"phd.count":   1486,
"publication.count":  25617 
}
,
{
 "name": "University of Houston",
"fice": "3652",
"address": "212 E. Cullen Building Houston TX 77204-2018",
"lat":  29.72,
"long": -95.345,
"expenditures": 2.6614e+05,
"faculty.count":   4759,
"phd.count":   1542,
"publication.count":   2345 
}
,
{
 "name": "University of Texas at Arlington",
"fice": "3656",
"address": "701 S. Nedderman Dr. Arlington TX 76013",
"lat": 32.728,
"long": -97.115,
"expenditures": 1.1983e+05,
"faculty.count":  10674,
"phd.count":    816,
"publication.count":  13031 
}
,
{
 "name": "University of Texas at Austin",
"fice": "3658",
"address": "2400 Inner Campus Drive Austin TX 78712",
"lat": 30.285,
"long": -97.737,
"expenditures": 1.9177e+06,
"faculty.count":   5346,
"phd.count":   5181,
"publication.count":   3812 
}
,
{
 "name": "University of Texas at El Paso",
"fice": "3661",
"address": "500 W. University Ave El Paso TX 79968-0691",
"lat": 31.772,
"long": -106.5,
"expenditures": 1.4703e+05,
"faculty.count":  16838,
"phd.count":    304,
"publication.count":  23367 
}
,
{
 "name": "University of Utah",
"fice": "3675",
"address": "201 Presidents Circle, ROOM 203 Salt Lake City UT 84112-9008",
"lat": 40.762,
"long": -111.85,
"expenditures": 1.1956e+06,
"faculty.count":   3057,
"phd.count":   1900,
"publication.count":  14612 
}
,
{
 "name": "Utah State University",
"fice": "3677",
"address": "Old Main Hill Logan UT 84322-1400",
"lat": 41.741,
"long": -111.81,
"expenditures": 6.5614e+05,
"faculty.count":   4353,
"phd.count":    550,
"publication.count":   2010 
}
,
{
 "name": "University of Vermont",
"fice": "3696",
"address": "85 S Prospect St Burlington VT 05405-0160",
"lat": 44.479,
"long": -73.198,
"expenditures": 5.8457e+05,
"faculty.count":   5240,
"phd.count":    394,
"publication.count":   3269 
}
,
{
 "name": "College of William and Mary",
"fice": "3705",
"address": "200 Richmond Road Williamsburg VA 23187-8795",
"lat":  37.31,
"long": -76.731,
"expenditures": 1.7994e+05,
"faculty.count":   2148,
"phd.count":    378,
"publication.count":   1181 
}
,
{
 "name": "Old Dominion University",
"fice": "3728",
"address": "5115 Hampton Blvd Norfolk VA 23529",
"lat": 36.885,
"long": -76.31,
"expenditures": 1.7386e+05,
"faculty.count":   7929,
"phd.count":    568,
"publication.count":  21900 
}
,
{
 "name": "Virginia Commonwealth University",
"fice": "3735",
"address": "910 W Franklin St Richmond VA 23284-2512",
"lat": 37.548,
"long": -77.454,
"expenditures": 6.481e+05,
"faculty.count":   3982,
"phd.count":    967,
"publication.count":   8218 
}
,
{
 "name": "George Mason University",
"fice": "3749",
"address": "4400 University Dr Fairfax VA 22030-4444",
"lat": 38.831,
"long": -77.308,
"expenditures": 3.0331e+05,
"faculty.count":   7895,
"phd.count":   1220,
"publication.count":  20373 
}
,
{
 "name": "Virginia Polytechnic Institute and State Univ",
"fice": "3754",
"address": "222 Burruss Hall Blacksburg VA 24061-0131",
"lat": 37.237,
"long": -80.435,
"expenditures": 8.3643e+05,
"faculty.count":  14592,
"phd.count":   2607,
"publication.count":  52078 
}
,
{
 "name": "University of Washington - Seattle",
"fice": "3798",
"address": "1400 NE Campus Parkway Seattle WA 98195-4550",
"lat": 47.656,
"long": -122.31,
"expenditures": 4.3013e+06,
"faculty.count":  11181,
"phd.count":   4098,
"publication.count":  42104 
}
,
{
 "name": "Washington State University",
"fice": "3800",
"address": "French Administration Building Pullman WA 99164-5910",
"lat":  46.73,
"long": -117.16,
"expenditures": 5.7914e+05,
"faculty.count":   6563,
"phd.count":   1283,
"publication.count":   4908 
}
,
{
 "name": "West Virginia University",
"fice": "3827",
"address": "Stewart Hall, 1500 University Avenue Morgantown WV 26506-6201",
"lat": 39.635,
"long": -79.954,
"expenditures": 4.4589e+05,
"faculty.count":   4640,
"phd.count":   1174,
"publication.count":   4390 
}
,
{
 "name": "Marquette University",
"fice": "3863",
"address": "1250 W Wisconsin Avenue Milwaukee WI 53233",
"lat": 43.039,
"long": -87.928,
"expenditures":  45698,
"faculty.count":   5028,
"phd.count":    462,
"publication.count":   4709 
}
,
{
 "name": "University of Wisconsin-Madison",
"fice": "3895",
"address": "500 Lincoln Dr Madison WI 53706-1380",
"lat": 43.074,
"long": -89.405,
"expenditures": 3.2515e+06,
"faculty.count":   5375,
"phd.count":   5033,
"publication.count":  25146 
}
,
{
 "name": "University of Wisconsin-Milwaukee",
"fice": "3896",
"address": "2200 E Kenwood Blvd Milwaukee WI 53201-0413",
"lat": 43.077,
"long": -87.88,
"expenditures": 1.1335e+05,
"faculty.count":   6421,
"phd.count":    775,
"publication.count":  11963 
}
,
{
 "name": "University of Wyoming",
"fice": "3932",
"address": "Corner of Ninth and Ivinson Laramie WY 82071",
"lat": 41.312,
"long": -105.57,
"expenditures": 1.8496e+05,
"faculty.count":   9201,
"phd.count":    427,
"publication.count":   8770 
}
,
{
 "name": "University of Central Florida",
"fice": "3954",
"address": "4000 Central Florida Blvd Orlando FL 32816",
"lat": 28.601,
"long": -81.199,
"expenditures": 3.3993e+05,
"faculty.count":   3150,
"phd.count":   1370,
"publication.count":  12021 
}
,
{
 "name": "University of Minnesota - Twin Cities",
"fice": "3969",
"address": "100 Church Street SE Minneapolis MN 55455-0213",
"lat": 44.978,
"long": -93.235,
"expenditures":      0,
"faculty.count":   7457,
"phd.count":   4925,
"publication.count":  10805 
}
,
{
 "name": "Teachers College, Columbia University",
"fice": "3979",
"address": "525 W 120th St New York NY 10027",
"lat":  40.81,
"long": -73.96,
"expenditures":  47419,
"faculty.count":   4991,
"phd.count":   1528,
"publication.count":  30858 
}
,
{
 "name": "University of Colorado at Denver",
"fice": "6740",
"address": "1380 Lawrence Street, Lawrence Street Center, Suite 1400 Denver CO 80217-3364",
"lat": 39.746,
"long":   -105,
"expenditures": 1.5085e+06,
"faculty.count":   6865,
"phd.count":    633,
"publication.count":  16434 
}
,
{
 "name": "Rutgers the State Univ of NJ New Brunswick",
"fice": "6964",
"address": "83 Somerset St New Brunswick NJ 08901-1281",
"lat": 40.504,
"long": -74.451,
"expenditures":      0,
"faculty.count":   5626,
"phd.count":   2502,
"publication.count":   6912 
}
,
{
 "name": "University of PR Rio Piedras Campus",
"fice": "7108",
"address": "39 Ponce de Leon  Ave San Juan PR 00931-0000",
"lat": 18.403,
"long": -66.046,
"expenditures": 1.0642e+05,
"faculty.count":   3319,
"phd.count":    545,
"publication.count":   1340 
}
,
{
 "name": "Florida International University",
"fice": "9635",
"address": "11200 S. W. 8 Street Miami FL 33199",
"lat": 25.757,
"long": -80.378,
"expenditures": 3.4462e+05,
"faculty.count":   7359,
"phd.count":    741,
"publication.count":  38773 
}
,
{
 "name": "University of Texas at Dallas",
"fice": "9741",
"address": "800 West Campbell  Road Richardson TX 75080-3021",
"lat": 32.989,
"long": -96.748,
"expenditures": 1.3497e+05,
"faculty.count":   2125,
"phd.count":    907,
"publication.count":   2652 
}
]
