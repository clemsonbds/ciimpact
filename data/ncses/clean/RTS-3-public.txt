"FICE"	"faculty"	"expenditures"	"publication"	"phd"	"State"	"Type"	"CC"	"EPSCOR"	"APP"	"DEA"
"1009"	14139	0	16574	1344	"Alabama"	"Public"	"Research Universities (high research activity)"	1	1	0.581152426804766
"1051"	8113	173892	7858	1172	"Alabama"	"Public"	"Research Universities (high research activity)"	1	1	0.2569279389114
"1052"	4693	1960497	4171	1014	"Alabama"	"Public"	"Research Universities (very high research activity)"	1	1	0.150233792923487
"1055"	9902	311535	24880	227	"Alabama"	"Public"	"Research Universities (high research activity)"	1	1	0.283060315306378
"1081"	2414	730844	3717	2865	"Arizona"	"Public"	"Research Universities (very high research activity)"	0	1	0.79022043447828
"1082"	4754	91546	15602	299	"Arizona"	"Public"	"Research Universities (high research activity)"	0	1	0.409787690056427
"1083"	2197	1973187	20513	2969	"Arizona"	"Public"	"Research Universities (very high research activity)"	0	1	1
"1108"	5715	223725	10477	973	"Arkansas"	"Public"	"Research Universities (high research activity)"	1	1	0.355575055807366
"1151"	2436	240997	2049	47	"California"	"Public"	"Research Universities (high research activity)"	0	1	0.0946283070270248
"1312"	1212	1821989	1121	5903	"California"	"Public"	"Research Universities (very high research activity)"	0	1	1
"1313"	1210	1740332	1743	3231	"California"	"Public"	"Research Universities (very high research activity)"	0	1	0.590310008927718
"1314"	7113	1141684	6844	2290	"California"	"Public"	"Research Universities (very high research activity)"	0	1	0.318118552066367
"1315"	3986	3264364	3232	4916	"California"	"Public"	"Research Universities (very high research activity)"	0	1	0.423568610575889
"1316"	1481	356212	2463	1341	"California"	"Public"	"Research Universities (very high research activity)"	0	1	0.700835298919665
"1317"	7626	3271581	12525	2863	"California"	"Public"	"Research Universities (very high research activity)"	0	1	0.261688627627003
"1320"	9914	728080	44820	2261	"California"	"Public"	"Research Universities (very high research activity)"	0	1	0.619173127949381
"1321"	3466	434131	5914	918	"California"	"Public"	"Research Universities (very high research activity)"	0	1	0.354836567264831
"1348"	7641	132337	39274	327	"Colorado"	"Public"	"Research Universities (high research activity)"	0	1	0.584252865506249
"1350"	5547	1247679	27845	1427	"Colorado"	"Public"	"Research Universities (very high research activity)"	0	1	0.564679750608179
"1370"	5289	1538706	3788	2133	"Colorado"	"Public"	"Research Universities (very high research activity)"	0	1	0.284832667968083
"1431"	8658	554694	11482	1365	"Delaware"	"Public"	"Research Universities (very high research activity)"	1	1	0.273650147166307
"1481"	10449	118881	92652	527	"Florida"	"Public"	"Research Universities (high research activity)"	0	1	1
"1489"	6519	748650	2821	2318	"Florida"	"Public"	"Research Universities (very high research activity)"	0	1	0.337605657417932
"1535"	2312	1601432	3507	4690	"Florida"	"Public"	"Research Universities (very high research activity)"	0	1	0.790213018007315
"1537"	5393	1048627	3907	1572	"Florida"	"Public"	"Research Universities (very high research activity)"	0	1	0.258873946399107
"1574"	6363	173099	7951	1181	"Georgia"	"Public"	"Research Universities (high research activity)"	0	1	0.325083064390647
"1598"	3021	698855	3586	2766	"Georgia"	"Public"	"Research Universities (very high research activity)"	0	1	0.689061982952972
"1610"	8977	1329328	18863	1212	"Hawaii"	"Public"	"Research Universities (very high research activity)"	1	1	0.26445996539468
"1626"	3720	319308	3243	623	"Idaho"	"Public"	"Research Universities (high research activity)"	1	1	0.228394958737606
"1737"	15627	75007	22496	545	"Illinois"	"Public"	"Research Universities (high research activity)"	0	1	0.293574659214701
"1758"	6421	119164	4939	1006	"Illinois"	"Public"	"Research Universities (high research activity)"	0	1	0.251755625288026
"1775"	14492	1905550	27576	4975	"Illinois"	"Public"	"Research Universities (very high research activity)"	0	1	0.429471176746494
"1776"	2716	1365029	728	1996	"Illinois"	"Public"	"Research Universities (very high research activity)"	0	1	0.340732840612141
"1809"	11384	0	16543	2851	"Indiana"	"Public"	"Research Universities (very high research activity)"	0	1	0.789226690399791
"1813"	6000	0	7947	0	"Indiana"	"Public"	"Research Universities (high research activity)"	0	1	0.616633255853507
"1825"	5162	0	2286	4233	"Indiana"	"Public"	"Research Universities (very high research activity)"	0	1	1
"1869"	7701	673456	32561	2103	"Iowa"	"Public"	"Research Universities (very high research activity)"	1	1	0.613322725750313
"1892"	4412	1545183	2710	2379	"Iowa"	"Public"	"Research Universities (very high research activity)"	1	1	0.328990156158903
"1928"	4613	379252	11556	1046	"Kansas"	"Public"	"Research Universities (very high research activity)"	1	1	0.427959150637865
"1950"	5701	92118	18064	199	"Kansas"	"Public"	"Research Universities (high research activity)"	1	1	0.370835795403625
"1989"	14915	997091	22466	1826	"Kentucky"	"Public"	"Research Universities (very high research activity)"	1	1	0.254315042509157
"1999"	2810	453945	5793	948	"Kentucky"	"Public"	"Research Universities (high research activity)"	1	1	0.409494716359465
"2010"	3893	488879	7878	1717	"Louisiana"	"Public"	"Research Universities (very high research activity)"	1	1	0.525918364236174
"2053"	2889	265933	2053	340	"Maine"	"Public"	"Research Universities (high research activity)"	1	1	0.166426050435755
"2103"	798	1472296	6094	3970	"Maryland"	"Public"	"Research Universities (very high research activity)"	0	1	1
"2105"	4339	313808	1801	590	"Maryland"	"Public"	"Research Universities (high research activity)"	0	1	0.165620146292958
"2221"	8779	494261	8579	1868	"Massachusetts"	"Public"	"Research Universities (very high research activity)"	0	1	0.300977159984527
"2290"	8758	1090325	8205	3218	"Michigan"	"Public"	"Research Universities (very high research activity)"	0	1	0.379965065044625
"2292"	4225	159984	14043	373	"Michigan"	"Public"	"Research Universities (high research activity)"	0	1	0.418690597639485
"2329"	4648	803236	8232	1353	"Michigan"	"Public"	"Research Universities (very high research activity)"	0	1	0.344640327884342
"2330"	6038	70574	21227	674	"Michigan"	"Public"	"Research Universities (high research activity)"	0	1	0.49859702559859
"2410"	5796	219190	1816	337	"Mississippi"	"Public"	"Research Universities (high research activity)"	1	1	0.0910932076369489
"2423"	1981	655472	4871	852	"Mississippi"	"Public"	"Research Universities (high research activity)"	1	1	0.376344345026966
"2441"	10400	259948	26676	832	"Mississippi"	"Public"	"Research Universities (high research activity)"	1	1	0.348050382596138
"2516"	5989	707742	5985	2000	"Missouri"	"Public"	"Research Universities (very high research activity)"	1	1	0.362223561527683
"2518"	10358	111946	42054	341	"Missouri"	"Public"	"Research Universities (high research activity)"	1	1	0.477125882586759
"2519"	9668	34990	37155	366	"Missouri"	"Public"	"Research Universities (high research activity)"	1	1	0.896800802251328
"2536"	12432	236063	56591	369	"Montana"	"Public"	"Research Universities (high research activity)"	1	1	0.51313172230746
"2565"	4597	527483	11048	1753	"Nebraska"	"Public"	"Research Universities (very high research activity)"	1	1	0.519019711717111
"2568"	6708	415935	40086	660	"Nevada"	"Public"	"Research Universities (high research activity)"	1	1	0.673500168773022
"2569"	6327	260610	15337	521	"Nevada"	"Public"	"Research Universities (high research activity)"	1	1	0.32094689974804
"2621"	7791	256808	6290	434	"New Jersey"	"Public"	"Research Universities (high research activity)"	0	1	0.13911795753466
"2631"	8212	0	17639	403	"New Jersey"	"Public"	"Research Universities (high research activity)"	0	1	1
"2663"	3070	867460	1694	1291	"New Mexico"	"Public"	"Research Universities (very high research activity)"	1	1	0.289795161419838
"2835"	7689	689959	7983	1041	"New York"	"Public"	"Research Universities (very high research activity)"	0	1	0.211505006048981
"2836"	3850	87199	4170	831	"New York"	"Public"	"Research Universities (high research activity)"	0	1	0.345060631363873
"2837"	8590	1035176	21541	2186	"New York"	"Public"	"Research Universities (very high research activity)"	0	1	0.413948060810246
"2851"	15409	52262	27291	139	"New York"	"Public"	"Research Universities (high research activity)"	0	1	0.426672516413211
"2905"	5656	110207	10214	88	"North Carolina"	"Public"	"Research Universities (high research activity)"	0	1	0.204544778503397
"2972"	3285	837919	20192	2596	"North Carolina"	"Public"	"Research Universities (very high research activity)"	0	1	0.882643315307467
"2974"	10997	2385998	22475	3380	"North Carolina"	"Public"	"Research Universities (very high research activity)"	0	1	0.345128793841833
"2976"	7912	35192	11986	586	"North Carolina"	"Public"	"Research Universities (high research activity)"	0	1	0.33107555181204
"3131"	9040	146604	14691	648	"Ohio"	"Public"	"Research Universities (high research activity)"	0	1	0.249500510079065
"3210"	3264	772524	2590	1204	"Oregon"	"Public"	"Research Universities (very high research activity)"	0	1	0.294806199676029
"3223"	3709	336029	2711	1070	"Oregon"	"Public"	"Research Universities (high research activity)"	0	1	0.322773766642625
"3371"	7005	384920	12671	1541	"Pennsylvania"	"Public"	"Research Universities (high research activity)"	0	1	0.387302119697395
"3414"	5992	349648	3346	532	"Rhode Island"	"Public"	"Research Universities (high research activity)"	1	1	0.138919868304593
"3425"	5850	402262	10340	1065	"South Carolina"	"Public"	"Research Universities (high research activity)"	1	1	0.33352334990264
"3448"	5071	0	6084	1609	"South Carolina"	"Public"	"Research Universities (very high research activity)"	1	1	0.708928530781938
"3471"	7520	113534	46452	153	"South Dakota"	"Public"	"Research Universities (high research activity)"	1	1	0.696482148313459
"3509"	7595	109489	2394	754	"Tennessee"	"Public"	"Research Universities (high research activity)"	1	1	0.143367879639823
"3530"	4588	0	3517	1740	"Tennessee"	"Public"	"Research Universities (very high research activity)"	1	1	0.562562375644251
"3594"	7819	51643	10039	1132	"Texas"	"Public"	"Research Universities (high research activity)"	0	1	0.303134912324678
"3644"	10693	166062	25617	1486	"Texas"	"Public"	"Research Universities (high research activity)"	0	1	0.405384011122455
"3652"	4759	266143	2345	1542	"Texas"	"Public"	"Research Universities (high research activity)"	0	1	0.365544999223997
"3656"	10674	119827	13031	816	"Texas"	"Public"	"Research Universities (high research activity)"	0	1	0.215600443340017
"3658"	5346	1917696	3812	5181	"Texas"	"Public"	"Research Universities (very high research activity)"	0	1	0.563666495150924
"3661"	16838	147031	23367	304	"Texas"	"Public"	"Research Universities (high research activity)"	0	1	0.191300806252714
"3675"	3057	1195581	14612	1900	"Utah"	"Public"	"Research Universities (very high research activity)"	1	1	0.568247438527057
"3677"	4353	656139	2010	550	"Utah"	"Public"	"Research Universities (high research activity)"	1	1	0.13374187487759
"3696"	5240	584566	3269	394	"Vermont"	"Public"	"Research Universities (high research activity)"	1	1	0.115323981764988
"3705"	2148	179936	1181	378	"Virginia"	"Public"	"Research Universities (high research activity)"	0	1	0.209312526267886
"3728"	7929	173864	21900	568	"Virginia"	"Public"	"Research Universities (high research activity)"	0	1	0.362442902651468
"3735"	3982	648104	8218	967	"Virginia"	"Public"	"Research Universities (high research activity)"	0	1	0.336222296099523
"3749"	7895	303312	20373	1220	"Virginia"	"Public"	"Research Universities (high research activity)"	0	1	0.413493243630327
"3754"	14592	836433	52078	2607	"Virginia"	"Public"	"Research Universities (very high research activity)"	0	1	0.508523684306317
"3798"	11181	4301328	42104	4098	"Washington"	"Public"	"Research Universities (very high research activity)"	0	1	0.427463887934221
"3800"	6563	579140	4908	1283	"Washington"	"Public"	"Research Universities (very high research activity)"	0	1	0.241762999338806
"3827"	4640	445892	4390	1174	"West Virginia"	"Public"	"Research Universities (high research activity)"	1	1	0.305108155765409
"3895"	5375	3251460	25146	5033	"Wisconsin"	"Public"	"Research Universities (very high research activity)"	0	1	0.563005035656592
"3896"	6421	113352	11963	775	"Wisconsin"	"Public"	"Research Universities (high research activity)"	0	1	0.327127728996162
"3932"	9201	184955	8770	427	"Wyoming"	"Public"	"Research Universities (high research activity)"	1	1	0.149408602713145
"3954"	3150	339928	12021	1370	"Florida"	"Public"	"Research Universities (high research activity)"	0	1	0.689860191025455
"3969"	7457	0	10805	4925	"Minnesota"	"Public"	"Research Universities (very high research activity)"	0	1	1
"6740"	6865	1508492	16434	633	"Colorado"	"Public"	"Research Universities (very high research activity)"	0	1	0.266655110920879
"6964"	5626	0	6912	2502	"New Jersey"	"Public"	"Research Universities (very high research activity)"	0	1	0.78810197131617
"6968"	6400	0	2330	2467	"Virginia"	"Public"	"Research Universities (very high research activity)"	0	1	0.497839287471307
"7108"	3319	106417	1340	545	"Puerto Rico"	"Public"	"Research Universities (high research activity)"	1	1	0.213352885400875
"9635"	7359	344621	38773	741	"Florida"	"Public"	"Research Universities (high research activity)"	0	1	0.607658637419678
"9741"	2125	134967	2652	907	"Texas"	"Public"	"Research Universities (high research activity)"	0	1	0.526889690689187
