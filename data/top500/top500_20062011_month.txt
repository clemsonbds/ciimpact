Rank	Site	Institution	IPEDS.ID	FICE	RMax	RPeak	Architecture	Year	Month
488	Arizona State University High Performance Computing  Center	Arizona State University at the Tempe Campus	104151	1081	2050	2560	Cluster	2006	6
110	Arizona State University High Performance Computing  Center	Arizona State University at the Tempe Campus	104151	1081	12100	18360	Cluster	2007	11
283	Arizona State University High Performance Computing  Center	Arizona State University at the Tempe Campus	104151	1081	12100	18360	Cluster	2008	6
82	Arizona State University HPCI / Translational Genomics Research Institute HPBC	Arizona State University at the Tempe Campus	104151	1081	30110	44688	Cluster	2008	11
160	Arizona State University HPCI / Translational Genomics Research Institute HPBC	Arizona State University at the Tempe Campus	104151	1081	30110	44688	Cluster	2009	6
222	Arizona State University HPCI / Translational Genomics Research Institute HPBC	Arizona State University at the Tempe Campus	104151	1081	30110	44688	Cluster	2009	11
332	Arizona State University HPCI / Translational Genomics Research Institute HPBC	Arizona State University at the Tempe Campus	104151	1081	30110	44688	Cluster	2010	6
98	Boston University	Boston University	164988	2130	4713	5734	MPP	2006	6
139	Boston University	Boston University	164988	2130	4713	5734	MPP	2006	11
374	Boston University	Boston University	164988	2130	4713	5734	MPP	2007	6
468	Bowie State University	Bowie State University	162007	2062	2104	3584	Cluster	2006	6
87	Brigham Young University	Brigham Young University	230038	3670	5439	9072	Cluster	2006	6
45	Brigham Young University	Brigham Young University	230038	3670	12510	26812	Cluster	2006	11
69	Brigham Young University	Brigham Young University	230038	3670	12510	26812	Cluster	2007	6
106	Brigham Young University	Brigham Young University	230038	3670	12510	26812	Cluster	2007	11
273	Brigham Young University	Brigham Young University	230038	3670	12510	26812	Cluster	2008	6
278	Brigham Young University	Brigham Young University	230038	3670	26760	28672	Cluster	2009	11
425	Brigham Young University	Brigham Young University	230038	3670	26760	28672	Cluster	2010	6
52	Caltech	California Institute of Technology	110404	1131	8408	13107.2	Cluster	2006	6
78	Caltech	California Institute of Technology	110404	1131	8408	13107.2	Cluster	2006	11
29	Caltech	California Institute of Technology	110404	1131	22590	37700	Cluster	2007	6
39	Caltech	California Institute of Technology	110404	1131	22590	37700	Cluster	2007	11
77	Caltech	California Institute of Technology	110404	1131	22590	37700	Cluster	2008	6
140	Caltech	California Institute of Technology	110404	1131	22590	37700	Cluster	2008	11
269	Caltech	California Institute of Technology	110404	1131	22590	37700	Cluster	2009	6
436	Caltech	California Institute of Technology	110404	1131	22590	37700	Cluster	2009	11
123	Caltech/JPL	California Institute of Technology	110404	1131	4298	6553.6	Cluster	2006	6
188	Caltech/JPL	California Institute of Technology	110404	1131	4298	6553.6	Cluster	2006	11
460	Caltech/JPL	California Institute of Technology	110404	1131	4298	6553.6	Cluster	2007	6
279	"Center for High-End Computer Systems, Virginia Tech"	Virginia Polytechnic Institute and State Univ	233921	3754	16870	22937	Cluster	2008	11
62	Clemson University	Clemson University	217882	3425	31060	38175	Cluster	2008	6
60	Clemson University	Clemson University	217882	3425	45610	56548.8	Cluster	2008	11
89	Clemson University	Clemson University	217882	3425	45610	56548.8	Cluster	2009	6
79	Clemson University	Clemson University	217882	3425	60670	74704	Cluster	2009	11
85	Clemson University	Clemson University	217882	3425	66180	81475.2	Cluster	2010	6
90	Clemson University	Clemson University	217882	3425	85040	104594	Cluster	2010	11
96	Clemson University	Clemson University	217882	3425	92480	115258	Cluster	2011	6
128	Clemson University	Clemson University	217882	3425	96920	123633	Cluster	2011	11
99	Clemson University Computational Center for Mobility Systems	Clemson University	217882	3425	27370	34400	Cluster	2008	11
194	Clemson University Computational Center for Mobility Systems	Clemson University	217882	3425	27370	34400	Cluster	2009	6
273	Clemson University Computational Center for Mobility Systems	Clemson University	217882	3425	27370	34400	Cluster	2009	11
410	Clemson University Computational Center for Mobility Systems	Clemson University	217882	3425	27370	34400	Cluster	2010	6
133	Colorado School of Mines/Golden Energy Computing Organization	Colorado School of Mines	126775	1348	16700	21680	Cluster	2008	6
285	Colorado School of Mines/Golden Energy Computing Organization	Colorado School of Mines	126775	1348	16700	21680	Cluster	2008	11
124	Columbia University	Columbia University in the City of New York	190150	2707	23856.7	44544	Cluster	2008	11
238	Columbia University	Columbia University in the City of New York	190150	2707	23856.7	44544	Cluster	2009	6
256	Columbia University	Columbia University in the City of New York	190150	2707	23045.1	43008	Cluster	2009	6
382	Columbia University	Columbia University in the City of New York	190150	2707	23856.7	44544	Cluster	2009	11
426	Columbia University	Columbia University in the City of New York	190150	2707	23045.1	43008	Cluster	2009	11
470	Cornell Theory Center	Cornell University-Endowed Colleges	190415	2711	2090	4752	Cluster	2006	6
62	Harvard University	Harvard University	166027	2155	9433	11468.8	MPP	2006	11
93	Harvard University	Harvard University	166027	2155	9433	11468.8	MPP	2007	6
170	Harvard University	Harvard University	166027	2155	9433	11468.8	MPP	2007	11
411	Harvard University	Harvard University	166027	2155	9433	11468.8	MPP	2008	6
60	Harvard University - FAS Research Computing	Harvard University	166027	2155	32410	38175	Cluster	2008	6
80	Harvard University - FAS Research Computing	Harvard University	166027	2155	32410	38175	Cluster	2008	11
135	Harvard University - FAS Research Computing	Harvard University	166027	2155	32410	38175	Cluster	2009	6
183	Harvard University - FAS Research Computing	Harvard University	166027	2155	32410	38175	Cluster	2009	11
264	Harvard University - FAS Research Computing	Harvard University	166027	2155	32410	38175	Cluster	2010	6
432	Harvard University - FAS Research Computing	Harvard University	166027	2155	32410	38175	Cluster	2010	11
43	Holland Computing Center at PKI	University of Nebraska at Lincoln	181464	2565	21500	25782.4	Cluster	2007	11
80	Holland Computing Center at PKI	University of Nebraska at Lincoln	181464	2565	21500	25782.4	Cluster	2008	6
154	Holland Computing Center at PKI	University of Nebraska at Lincoln	181464	2565	21500	25782.4	Cluster	2008	11
296	Holland Computing Center at PKI	University of Nebraska at Lincoln	181464	2565	21500	25782.4	Cluster	2009	6
468	Holland Computing Center at PKI	University of Nebraska at Lincoln	181464	2565	21500	25782.4	Cluster	2009	11
23	Indiana University	Indiana University-Bloomington	151351	1809	15040	20480	Cluster	2006	6
31	Indiana University	Indiana University-Bloomington	151351	1809	15040	20480	Cluster	2006	11
30	Indiana University	Indiana University-Bloomington	151351	1809	21790	30720	Cluster	2007	6
42	Indiana University	Indiana University-Bloomington	151351	1809	21790	30720	Cluster	2007	11
79	Indiana University	Indiana University-Bloomington	151351	1809	21790	30720	Cluster	2008	6
147	Indiana University	Indiana University-Bloomington	151351	1809	21790	30720	Cluster	2008	11
282	Indiana University	Indiana University-Bloomington	151351	1809	21790	30720	Cluster	2009	6
452	Indiana University	Indiana University-Bloomington	151351	1809	21790	30720	Cluster	2009	11
99	Iowa State University	Iowa State University	153603	1869	4713	5734	MPP	2006	6
141	Iowa State University	Iowa State University	153603	1869	4713	5734	MPP	2006	11
376	Iowa State University	Iowa State University	153603	1869	4713	5734	MPP	2007	6
442	Johns Hopkins University	Johns Hopkins University	162928	2077	18376.3	24576	Cluster	2009	6
23	Louisiana Optical Network Initiative	Louisiana State Univ & Ag & Mech & Hebert Laws Ctr	159391	2010	34780	50766.1	Cluster	2007	6
32	Louisiana Optical Network Initiative	Louisiana State Univ & Ag & Mech & Hebert Laws Ctr	159391	2010	34780	50766.1	Cluster	2007	11
54	Louisiana Optical Network Initiative	Louisiana State Univ & Ag & Mech & Hebert Laws Ctr	159391	2010	34780	50766.1	Cluster	2008	6
76	Louisiana Optical Network Initiative	Louisiana State Univ & Ag & Mech & Hebert Laws Ctr	159391	2010	34780	50766.1	Cluster	2008	11
126	Louisiana Optical Network Initiative	Louisiana State Univ & Ag & Mech & Hebert Laws Ctr	159391	2010	34780	50766.1	Cluster	2009	6
163	Louisiana Optical Network Initiative	Louisiana State Univ & Ag & Mech & Hebert Laws Ctr	159391	2010	34780	50766.1	Cluster	2009	11
224	Louisiana Optical Network Initiative	Louisiana State Univ & Ag & Mech & Hebert Laws Ctr	159391	2010	34780	50766.1	Cluster	2010	6
371	Louisiana Optical Network Initiative	Louisiana State Univ & Ag & Mech & Hebert Laws Ctr	159391	2010	34780	50766.1	Cluster	2010	11
176	Louisiana State University	Louisiana State Univ & Ag & Mech & Hebert Laws Ctr	159391	2010	3711.5	4256	MPP	2006	6
451	Louisiana State University	Louisiana State Univ & Ag & Mech & Hebert Laws Ctr	159391	2010	2207	3686.4	Cluster	2006	6
278	Louisiana State University	Louisiana State Univ & Ag & Mech & Hebert Laws Ctr	159391	2010	3711.5	4256	MPP	2006	11
133	Louisiana State University	Louisiana State Univ & Ag & Mech & Hebert Laws Ctr	159391	2010	10601.6	15361.9	Cluster	2007	11
344	Louisiana State University	Louisiana State Univ & Ag & Mech & Hebert Laws Ctr	159391	2010	10601.6	15361.9	Cluster	2008	6
324	Michigan State University	Michigan State University	171100	2290	7417	9556	MPP	2007	11
115	Mississippi State University	Mississippi State University	176080	2423	5857.28	10649.6	Cluster	2006	11
261	Mississippi State University	Mississippi State University	176080	2423	5857.28	10649.6	Cluster	2007	6
331	Mississippi State University	Mississippi State University	176080	2423	30130	34406	Cluster	2010	6
57	National Institute for Computational Sciences/University of Tennessee	"University of Tennessee, The"	221759	3530	32826	38906	MPP	2008	6
15	National Institute for Computational Sciences/University of Tennessee	"University of Tennessee, The"	221759	3530	125128	165195	MPP	2008	11
6	National Institute for Computational Sciences/University of Tennessee	"University of Tennessee, The"	221759	3530	463300	607200	MPP	2009	6
21	National Institute for Computational Sciences/University of Tennessee	"University of Tennessee, The"	221759	3530	125128	165195	MPP	2009	6
3	National Institute for Computational Sciences/University of Tennessee	"University of Tennessee, The"	221759	3530	831700	1028850	MPP	2009	11
30	National Institute for Computational Sciences/University of Tennessee	"University of Tennessee, The"	221759	3530	125128	165195	MPP	2009	11
4	National Institute for Computational Sciences/University of Tennessee	"University of Tennessee, The"	221759	3530	831700	1028850	MPP	2010	6
36	National Institute for Computational Sciences/University of Tennessee	"University of Tennessee, The"	221759	3530	125128	165195	MPP	2010	6
8	National Institute for Computational Sciences/University of Tennessee	"University of Tennessee, The"	221759	3530	831700	1028850	MPP	2010	11
52	National Institute for Computational Sciences/University of Tennessee	"University of Tennessee, The"	221759	3530	125128	165195	MPP	2010	11
11	National Institute for Computational Sciences/University of Tennessee	"University of Tennessee, The"	221759	3530	919100	1173000	MPP	2011	6
66	National Institute for Computational Sciences/University of Tennessee	"University of Tennessee, The"	221759	3530	125128	165195	MPP	2011	6
11	National Institute for Computational Sciences/University of Tennessee	"University of Tennessee, The"	221759	3530	919100	1173000	MPP	2011	11
37	NCSA	University of Illinois at Urbana-Champaign	145637	1775	9819	15300	Cluster	2006	6
62	NCSA	University of Illinois at Urbana-Champaign	145637	1775	7215	10259	Cluster	2006	6
78	NCSA	University of Illinois at Urbana-Champaign	145637	1775	6118	7373	Cluster	2006	6
79	NCSA	University of Illinois at Urbana-Champaign	145637	1775	6117.27	6553	MPP	2006	6
130	NCSA	University of Illinois at Urbana-Champaign	145637	1775	4106	5760	Cluster	2006	6
27	NCSA	University of Illinois at Urbana-Champaign	145637	1775	16500	21300	Cluster	2006	11
59	NCSA	University of Illinois at Urbana-Champaign	145637	1775	9819	15300	Cluster	2006	11
92	NCSA	University of Illinois at Urbana-Champaign	145637	1775	7215	10259	Cluster	2006	11
108	NCSA	University of Illinois at Urbana-Champaign	145637	1775	6118	7373	Cluster	2006	11
109	NCSA	University of Illinois at Urbana-Champaign	145637	1775	6028.23	6553	MPP	2006	11
8	NCSA	University of Illinois at Urbana-Champaign	145637	1775	62680	89587.2	Cluster	2007	6
47	NCSA	University of Illinois at Urbana-Champaign	145637	1775	16680	21300	Cluster	2007	6
90	NCSA	University of Illinois at Urbana-Champaign	145637	1775	9819	15300	Cluster	2007	6
160	NCSA	University of Illinois at Urbana-Champaign	145637	1775	7215	10259	Cluster	2007	6
246	NCSA	University of Illinois at Urbana-Champaign	145637	1775	6028.23	6553	MPP	2007	6
14	NCSA	University of Illinois at Urbana-Champaign	145637	1775	62680	89587.2	Cluster	2007	11
66	NCSA	University of Illinois at Urbana-Champaign	145637	1775	16680	21300	Cluster	2007	11
161	NCSA	University of Illinois at Urbana-Champaign	145637	1775	9819	15300	Cluster	2007	11
351	NCSA	University of Illinois at Urbana-Champaign	145637	1775	7215	10259	Cluster	2007	11
489	NCSA	University of Illinois at Urbana-Champaign	145637	1775	6028.23	6553	MPP	2007	11
23	NCSA	University of Illinois at Urbana-Champaign	145637	1775	68480	89587.2	Cluster	2008	6
134	NCSA	University of Illinois at Urbana-Champaign	145637	1775	16680	21300	Cluster	2008	6
390	NCSA	University of Illinois at Urbana-Champaign	145637	1775	9819	15300	Cluster	2008	6
37	NCSA	University of Illinois at Urbana-Champaign	145637	1775	68480	89587.2	Cluster	2008	11
287	NCSA	University of Illinois at Urbana-Champaign	145637	1775	16680	21300	Cluster	2008	11
56	NCSA	University of Illinois at Urbana-Champaign	145637	1775	68480	89587.2	Cluster	2009	6
73	NCSA	University of Illinois at Urbana-Champaign	145637	1775	68480	89587.2	Cluster	2009	11
82	NCSA	University of Illinois at Urbana-Champaign	145637	1775	68480	89587.2	Cluster	2010	6
109	NCSA	University of Illinois at Urbana-Champaign	145637	1775	68480	89587.2	Cluster	2010	11
403	NCSA	University of Illinois at Urbana-Champaign	145637	1775	33620	68860	Cluster	2010	11
145	NCSA	University of Illinois at Urbana-Champaign	145637	1775	68480	89587.2	Cluster	2011	6
253	NCSA	University of Illinois at Urbana-Champaign	145637	1775	68480	89587.2	Cluster	2011	11
278	New York University	New York University	193900	2785	2994.04	4505.6	Cluster	2006	6
414	New York University	New York University	193900	2785	2994.04	4505.6	Cluster	2006	11
305	Northwestern University	Northwestern University	147767	1739	25679.9	28292.2	Cluster	2009	11
236	Northwestern University	Northwestern University	147767	1739	34274	37288	Cluster	2010	6
390	Northwestern University	Northwestern University	147767	1739	34274	37288	Cluster	2010	11
76	Ohio Supercomputer Center	Ohio State University-Main Campus	204796	3090	15350	21550	Cluster	2007	11
127	Ohio Supercomputer Center	Ohio State University-Main Campus	204796	3090	17100	21550	Cluster	2008	6
274	Ohio Supercomputer Center	Ohio State University-Main Campus	204796	3090	17100	21550	Cluster	2008	11
91	Ohio Supercomputer Center	Ohio State University-Main Campus	204796	3090	43460	68377.6	Cluster	2009	6
107	Ohio Supercomputer Center	Ohio State University-Main Campus	204796	3090	45730	73072	Cluster	2009	11
126	Ohio Supercomputer Center	Ohio State University-Main Campus	204796	3090	45730	73072	Cluster	2010	6
191	Ohio Supercomputer Center	Ohio State University-Main Campus	204796	3090	45730	73072	Cluster	2010	11
379	Ohio Supercomputer Center	Ohio State University-Main Campus	204796	3090	45730	73072	Cluster	2011	6
55	Pittsburgh Supercomputing Center	Carnegie Mellon University	211440	3242	7935.82	9888	MPP	2006	6
116	Pittsburgh Supercomputing Center	Carnegie Mellon University	211440	3242	4463	6032	Cluster	2006	6
85	Pittsburgh Supercomputing Center	Carnegie Mellon University	211440	3242	7935.82	9888	MPP	2006	11
164	Pittsburgh Supercomputing Center	Carnegie Mellon University	211440	3242	4463	6032	Cluster	2006	11
46	Pittsburgh Supercomputing Center	Carnegie Mellon University	211440	3242	17000	21507.2	MPP	2007	6
423	Pittsburgh Supercomputing Center	Carnegie Mellon University	211440	3242	4463	6032	Cluster	2007	6
65	Pittsburgh Supercomputing Center	Carnegie Mellon University	211440	3242	17000	21507.2	MPP	2007	11
129	Pittsburgh Supercomputing Center	Carnegie Mellon University	211440	3242	17000	21507.2	MPP	2008	6
276	Pittsburgh Supercomputing Center	Carnegie Mellon University	211440	3242	17000	21507.2	MPP	2008	11
103	Princeton University	Princeton University	186131	2627	4713	5734	MPP	2006	6
146	Princeton University	Princeton University	186131	2627	4713	5734	MPP	2006	11
382	Princeton University	Princeton University	186131	2627	4713	5734	MPP	2007	6
371	Purdue University	Purdue University-Main Campus	243780	1825	2688	6553.6	Cluster	2006	6
319	Purdue University	Purdue University-Main Campus	243780	1825	7518.87	10240	Cluster	2007	11
104	Purdue University	Purdue University-Main Campus	243780	1825	26800	52248	Cluster	2008	11
196	Purdue University	Purdue University-Main Campus	243780	1825	26800	52248	Cluster	2009	6
277	Purdue University	Purdue University-Main Campus	243780	1825	26800	52248	Cluster	2009	11
102	Purdue University	Purdue University-Main Campus	243780	1825	52192.1	79440	Cluster	2010	6
423	Purdue University	Purdue University-Main Campus	243780	1825	26800	52248	Cluster	2010	6
126	Purdue University	Purdue University-Main Campus	243780	1825	60256.2	74390.4	Cluster	2010	11
147	Purdue University	Purdue University-Main Campus	243780	1825	52192.1	79440	Cluster	2010	11
124	Purdue University	Purdue University-Main Campus	243780	1825	75425.8	95961.6	Cluster	2011	6
277	Purdue University	Purdue University-Main Campus	243780	1825	52192.1	79440	Cluster	2011	6
54	Purdue University	Purdue University-Main Campus	243780	1825	186900	215654.4	Cluster	2011	11
208	Purdue University	Purdue University-Main Campus	243780	1825	75425.8	95961.6	Cluster	2011	11
474	Purdue University	Purdue University-Main Campus	243780	1825	52192.1	79440	Cluster	2011	11
104	Renaissance Computing Institute (RENCI)	University of North Carolina at Chapel Hill	199120	2974	4713	5734	MPP	2006	6
147	Renaissance Computing Institute (RENCI)	University of North Carolina at Chapel Hill	199120	2974	4713	5734	MPP	2006	11
98	Renaissance Computing Institute (RENCI)	University of North Carolina at Chapel Hill	199120	2974	9433	11468.8	MPP	2007	6
175	Renaissance Computing Institute (RENCI)	University of North Carolina at Chapel Hill	199120	2974	9433	11468.8	MPP	2007	11
416	Renaissance Computing Institute (RENCI)	University of North Carolina at Chapel Hill	199120	2974	9433	11468.8	MPP	2008	6
12	"Rensselaer Polytechnic Institute, Computational Center for Nanotechnology Innovations"	Rensselaer Polytechnic Institute	194824	2803	73032	91750	MPP	2007	11
334	"Rensselaer Polytechnic Institute, Computational Center for Nanotechnology Innovations"	Rensselaer Polytechnic Institute	194824	2803	7294	9609.6	Cluster	2007	11
22	"Rensselaer Polytechnic Institute, Computational Center for Nanotechnology Innovations"	Rensselaer Polytechnic Institute	194824	2803	73032	91750	MPP	2008	6
34	"Rensselaer Polytechnic Institute, Computational Center for Nanotechnology Innovations"	Rensselaer Polytechnic Institute	194824	2803	73032	91750	MPP	2008	11
52	"Rensselaer Polytechnic Institute, Computational Center for Nanotechnology Innovations"	Rensselaer Polytechnic Institute	194824	2803	73032	91750	MPP	2009	6
70	"Rensselaer Polytechnic Institute, Computational Center for Nanotechnology Innovations"	Rensselaer Polytechnic Institute	194824	2803	73032	91750	MPP	2009	11
80	"Rensselaer Polytechnic Institute, Computational Center for Nanotechnology Innovations"	Rensselaer Polytechnic Institute	194824	2803	73032	91750	MPP	2010	6
106	"Rensselaer Polytechnic Institute, Computational Center for Nanotechnology Innovations"	Rensselaer Polytechnic Institute	194824	2803	73032	91750	MPP	2010	11
134	"Rensselaer Polytechnic Institute, Computational Center for Nanotechnology Innovations"	Rensselaer Polytechnic Institute	194824	2803	73032	91750	MPP	2011	6
225	"Rensselaer Polytechnic Institute, Computational Center for Nanotechnology Innovations"	Rensselaer Polytechnic Institute	194824	2803	73032	91750	MPP	2011	11
7	"Rensselaer Polytechnic Institute, Computional Center for Nanotechnology Innovations"	Rensselaer Polytechnic Institute	194824	2803	73032	91750	MPP	2007	6
437	Rice University	Rice University	227757	3604	2351	2939	Cluster	2006	6
230	Rice University	Rice University	227757	3604	71440	83558	MPP	2011	11
130	Stanford University High Performance Computing Center	Stanford University	243744	1305	12250	14912	Cluster	2007	11
340	Stanford University High Performance Computing Center	Stanford University	243744	1305	10700	14912	Cluster	2008	6
54	Stanford University/Biomedical Computational Facility	Stanford University	243744	1305	10700	20578	Cluster	2007	6
73	Stanford University/Biomedical Computational Facility	Stanford University	243744	1305	15570	20578	Cluster	2007	11
164	Stanford University/Biomedical Computational Facility	Stanford University	243744	1305	15570	20578	Cluster	2008	6
361	Stanford University/Biomedical Computational Facility	Stanford University	243744	1305	15570	20578	Cluster	2008	11
420	Texas A&M University	Texas A & M University	228723	3632	15570	29030	Cluster	2010	6
65	Texas Advanced Computing Center/Univ. of Texas	"University of Texas at Austin, The"	228778	3658	27140	8320	Cluster	2006	6
126	Texas Advanced Computing Center/Univ. of Texas	"University of Texas at Austin, The"	228778	3658	6989	6338	Cluster	2006	6
460	Texas Advanced Computing Center/Univ. of Texas	"University of Texas at Austin, The"	228778	3658	4152	2560	Cluster	2006	6
12	Texas Advanced Computing Center/Univ. of Texas	"University of Texas at Austin, The"	228778	3658	2141	55473.6	Cluster	2006	11
15	Texas Advanced Computing Center/Univ. of Texas	"University of Texas at Austin, The"	228778	3658	41460	62220	Cluster	2007	6
22	Texas Advanced Computing Center/Univ. of Texas	"University of Texas at Austin, The"	228778	3658	46730	62220	Cluster	2007	11
4	Texas Advanced Computing Center/Univ. of Texas	"University of Texas at Austin, The"	228778	3658	46730	503808	Cluster	2008	6
38	Texas Advanced Computing Center/Univ. of Texas	"University of Texas at Austin, The"	228778	3658	326000	62220	Cluster	2008	6
6	Texas Advanced Computing Center/Univ. of Texas	"University of Texas at Austin, The"	228778	3658	46730	579379	Cluster	2008	11
58	Texas Advanced Computing Center/Univ. of Texas	"University of Texas at Austin, The"	228778	3658	433200	62220	Cluster	2008	11
8	Texas Advanced Computing Center/Univ. of Texas	"University of Texas at Austin, The"	228778	3658	46730	579379	Cluster	2009	6
87	Texas Advanced Computing Center/Univ. of Texas	"University of Texas at Austin, The"	228778	3658	433200	62220	Cluster	2009	6
9	Texas Advanced Computing Center/Univ. of Texas	"University of Texas at Austin, The"	228778	3658	46730	579379	Cluster	2009	11
105	Texas Advanced Computing Center/Univ. of Texas	"University of Texas at Austin, The"	228778	3658	433200	62220	Cluster	2009	11
11	Texas Advanced Computing Center/Univ. of Texas	"University of Texas at Austin, The"	228778	3658	46730	579379	Cluster	2010	6
123	Texas Advanced Computing Center/Univ. of Texas	"University of Texas at Austin, The"	228778	3658	433200	62220	Cluster	2010	6
15	Texas Advanced Computing Center/Univ. of Texas	"University of Texas at Austin, The"	228778	3658	46730	579379	Cluster	2010	11
183	Texas Advanced Computing Center/Univ. of Texas	"University of Texas at Austin, The"	228778	3658	433200	62220	Cluster	2010	11
17	Texas Advanced Computing Center/Univ. of Texas	"University of Texas at Austin, The"	228778	3658	46730	579379	Cluster	2011	6
28	Texas Advanced Computing Center/Univ. of Texas	"University of Texas at Austin, The"	228778	3658	433200	301777	Cluster	2011	6
25	Texas Advanced Computing Center/Univ. of Texas	"University of Texas at Austin, The"	228778	3658	251800	579379.2	Cluster	2011	11
39	Texas Advanced Computing Center/Univ. of Texas	"University of Texas at Austin, The"	228778	3658	433200	301777	Cluster	2011	11
288	Texas Tech University	Texas Tech University	229115	3632	251800	20106	Cluster	2008	11
175	Texas Tech University	Texas Tech University	229115	3632	16680	40028	Cluster	2009	11
247	Texas Tech University	Texas Tech University	229115	3632	33530	40028	Cluster	2010	6
110	Texas Tech University	Texas Tech University	229115	3632	33530	84538	Cluster	2010	11
147	Texas Tech University	Texas Tech University	229115	3632	68270	84538	Cluster	2011	6
254	Texas Tech University	Texas Tech University	229115	3632	68270	84538	Cluster	2011	11
280	The University of Florida High-Performance Computing Center	University of Florida	134130	1535	68270	3660	Cluster	2006	6
419	The University of Florida High-Performance Computing Center	University of Florida	134130	1535	2974	3660	Cluster	2006	11
44	UCSD/San Diego Supercomputer Center	University of California-San Diego	110680	1317	2974	15628	MPP	2006	6
105	UCSD/San Diego Supercomputer Center	University of California-San Diego	110680	1317	9121	5734	MPP	2006	6
246	UCSD/San Diego Supercomputer Center	University of California-San Diego	110680	1317	4713	3993	Cluster	2006	6
42	UCSD/San Diego Supercomputer Center	University of California-San Diego	110680	1317	3152	17203.2	MPP	2006	11
68	UCSD/San Diego Supercomputer Center	University of California-San Diego	110680	1317	13780	15628	MPP	2006	11
372	UCSD/San Diego Supercomputer Center	University of California-San Diego	110680	1317	9121	3993	Cluster	2006	11
63	UCSD/San Diego Supercomputer Center	University of California-San Diego	110680	1317	3152	17203.2	MPP	2007	6
102	UCSD/San Diego Supercomputer Center	University of California-San Diego	110680	1317	13780	15628	MPP	2007	6
93	UCSD/San Diego Supercomputer Center	University of California-San Diego	110680	1317	9121	17203.2	MPP	2007	11
194	UCSD/San Diego Supercomputer Center	University of California-San Diego	110680	1317	13780	15628	MPP	2007	11
217	UCSD/San Diego Supercomputer Center	University of California-San Diego	110680	1317	9121	17203.2	MPP	2008	6
473	UCSD/San Diego Supercomputer Center	University of California-San Diego	110680	1317	13780	15628	MPP	2008	6
443	UCSD/San Diego Supercomputer Center	University of California-San Diego	110680	1317	9121	17203.2	MPP	2008	11
111	UCSD/San Diego Supercomputer Center	University of California-San Diego	110680	1317	13780	99532.8	Cluster	2010	11
151	UCSD/San Diego Supercomputer Center	University of California-San Diego	110680	1317	67320	99532.8	Cluster	2011	6
48	UCSD/San Diego Supercomputer Center	University of California-San Diego	110680	1317	67320	262246	Cluster	2011	11
259	UCSD/San Diego Supercomputer Center	University of California-San Diego	110680	1317	218100	99532.8	Cluster	2011	11
57	"University at Buffalo, SUNY, Center for Computational Res."	SUNY College at Buffalo	196130	2842	67320	9830.4	Cluster	2006	6
87	"University at Buffalo, SUNY, Center for Computational Res."	SUNY College at Buffalo	196130	2842	7737	9830.4	Cluster	2006	11
145	"University at Buffalo, SUNY, Center for Computational Res."	SUNY College at Buffalo	196130	2842	7737	9830.4	Cluster	2007	6
311	"University at Buffalo, SUNY, Center for Computational Res."	SUNY College at Buffalo	196130	2842	7737	9830.4	Cluster	2007	11
383	University of Alabama at Birmingham	University of Alabama at Birmingham	100663	1052	7737	5734	MPP	2007	6
108	University of Alaska - Arctic Region Supercomputing Center	University of Alaska Fairbanks	102614	1063	4713	31795.2	MPP	2008	11
203	University of Alaska - Arctic Region Supercomputing Center	University of Alaska Fairbanks	102614	1063	26310	31795.2	MPP	2009	6
288	University of Alaska - Arctic Region Supercomputing Center	University of Alaska Fairbanks	102614	1063	26310	31795.2	MPP	2009	11
435	University of Alaska - Arctic Region Supercomputing Center	University of Alaska Fairbanks	102614	1063	26310	31795.2	MPP	2010	6
82	University of Alaska - Arctic Region Supercomputing Center	University of Alaska Fairbanks	102614	1063	26310	107162	MPP	2010	11
100	University of Alaska - Arctic Region Supercomputing Center	University of Alaska Fairbanks	102614	1063	88920	107162	MPP	2011	6
142	University of Alaska - Arctic Region Supercomputing Center	University of Alaska Fairbanks	102614	1063	88920	107161.6	MPP	2011	11
399	University of Alaska - ARSC	University of Alaska Fairbanks	102614	1063	88920	4032	MPP	2006	6
205	University of Alaska - ARSC	University of Alaska Fairbanks	102614	1063	2510.27	12022	Cluster	2007	11
237	University of Arizona	University of Arizona	104179	1083	8863	15757	MPP	2008	6
462	University of Arizona	University of Arizona	104179	1083	13480	15757	MPP	2008	11
339	University of Arkansas	University of Arkansas Main Campus	106397	1108	13480	13364	Cluster	2008	6
254	University of California - Santa Cruz	University of California-Santa Cruz	110714	1321	10750	7465.6	Cluster	2007	6
498	University of California - Santa Cruz	University of California-Santa Cruz	110714	1321	5937.33	7465.6	Cluster	2007	11
462	"University of California, Los Angeles"	University of California-Los Angeles	110662	1315	5937.33	4403.2	Cluster	2006	6
148	"University of California, Los Angeles"	University of California-Los Angeles	110662	1315	2135	160577	Cluster	2011	6
235	"University of California, Los Angeles"	University of California-Los Angeles	110662	1315	68100	160577.28	Cluster	2011	11
398	University of Chicago	University of Chicago	144050	1774	70280	4505.6	Cluster	2006	6
65	University of Chicago	University of Chicago	144050	1774	2521.6	149990	MPP	2011	6
92	University of Chicago	University of Chicago	144050	1774	125800	149990	MPP	2011	11
31	University of Colorado	University of Colorado at Boulder	126614	1370	125800	175258	Cluster	2010	6
45	University of Colorado	University of Colorado at Boulder	126614	1370	152200	175258	Cluster	2010	11
52	University of Colorado	University of Colorado at Boulder	126614	1370	152200	175258	Cluster	2011	6
78	University of Colorado	University of Colorado at Boulder	126614	1370	152200	175257.6	Cluster	2011	11
114	University of Illinois	University of Illinois at Urbana-Champaign	145637	1775	152200	8192	Cluster	2006	6
160	University of Illinois	University of Illinois at Urbana-Champaign	145637	1775	4559	8192	Cluster	2006	11
410	University of Illinois	University of Illinois at Urbana-Champaign	145637	1775	4559	8192	Cluster	2007	6
66	University of Kentucky	University of Kentucky	157085	1989	4559	16320	Cluster	2007	6
103	University of Kentucky	University of Kentucky	157085	1989	12832	16320	Cluster	2007	11
263	University of Kentucky	University of Kentucky	157085	1989	12832	16320	Cluster	2008	6
493	University of Kentucky	University of Kentucky	157085	1989	12832	16320	Cluster	2008	11
259	University of Kentucky	University of Kentucky	157085	1989	12832	47114	Cluster	2010	11
498	University of Kentucky	University of Kentucky	157085	1989	40310	47114	Cluster	2011	6
45	University of Minnesota	University of Minnesota-Twin Cities	174066	3969	40310	21791	MPP	2007	6
62	University of Minnesota	University of Minnesota-Twin Cities	174066	3969	17310	21791	MPP	2007	11
124	University of Minnesota	University of Minnesota-Twin Cities	174066	3969	17310	21791	MPP	2008	6
267	University of Minnesota	University of Minnesota-Twin Cities	174066	3969	17310	21791	MPP	2008	11
355	University of Minnesota	University of Minnesota-Twin Cities	174066	3969	17310	21094.4	Cluster	2008	11
490	University of Minnesota	University of Minnesota-Twin Cities	174066	3969	15713	21791	MPP	2009	6
198	University of Minnesota/Supercomputing Institute	University of Minnesota-Twin Cities	174066	3969	17310	5928	Cluster	2006	11
325	University of Minnesota/Supercomputing Institute	University of Minnesota-Twin Cities	174066	3969	4099	5928	Cluster	2007	6
59	University of Minnesota/Supercomputing Institute	University of Minnesota-Twin Cities	174066	3969	5005	90137.6	Cluster	2009	6
67	University of Minnesota/Supercomputing Institute	University of Minnesota-Twin Cities	174066	3969	63997.7	94617.6	Cluster	2009	11
77	University of Minnesota/Supercomputing Institute	University of Minnesota-Twin Cities	174066	3969	74390	94617.6	Cluster	2010	6
103	University of Minnesota/Supercomputing Institute	University of Minnesota-Twin Cities	174066	3969	74390	94617.6	Cluster	2010	11
125	University of Minnesota/Supercomputing Institute	University of Minnesota-Twin Cities	174066	3969	74390	95961.6	Cluster	2011	6
209	University of Minnesota/Supercomputing Institute	University of Minnesota-Twin Cities	174066	3969	75425.8	95961.6	Cluster	2011	11
74	University of North Carolina	University of North Carolina at Chapel Hill	199120	2974	75425.8	7488	Cluster	2006	6
104	University of North Carolina	University of North Carolina at Chapel Hill	199120	2974	6252	7488	Cluster	2006	11
25	University of North Carolina	University of North Carolina at Chapel Hill	199120	2974	6252	38821.1	Cluster	2007	6
36	University of North Carolina	University of North Carolina at Chapel Hill	199120	2974	28770	38821.1	Cluster	2007	11
67	University of North Carolina	University of North Carolina at Chapel Hill	199120	2974	28770	38821.1	Cluster	2008	6
87	University of North Carolina	University of North Carolina at Chapel Hill	199120	2974	28770	38821.1	Cluster	2008	11
175	University of North Carolina	University of North Carolina at Chapel Hill	199120	2974	28770	38821.1	Cluster	2009	6
239	University of North Carolina	University of North Carolina at Chapel Hill	199120	2974	28770	38821.1	Cluster	2009	11
379	University of North Carolina	University of North Carolina at Chapel Hill	199120	2974	28770	38821.1	Cluster	2010	6
178	University of North Carolina	University of North Carolina at Chapel Hill	199120	2974	28770	94228.8	Cluster	2011	11
342	University of Notre Dame	University of Notre Dame	152080	1840	79800	4618	Cluster	2006	6
480	University of Notre Dame	University of Notre Dame	152080	1840	2796	4618	Cluster	2006	11
88	University of Oklahoma	University of Oklahoma Norman Campus	207500	3184	2796	6553.6	Cluster	2006	6
123	University of Oklahoma	University of Oklahoma Norman Campus	207500	3184	5438	6553.6	Cluster	2006	11
289	University of Oklahoma	University of Oklahoma Norman Campus	207500	3184	5438	6553.6	Cluster	2007	6
90	University of Oklahoma	University of Oklahoma Norman Campus	207500	3184	5438	33408	Cluster	2008	11
183	University of Oklahoma	University of Oklahoma Norman Campus	207500	3184	28030	33408	Cluster	2009	6
252	University of Oklahoma	University of Oklahoma Norman Campus	207500	3184	28030	33408	Cluster	2009	11
396	University of Oklahoma	University of Oklahoma Norman Campus	207500	3184	28030	33408	Cluster	2010	6
26	University of Southern California	University of Southern California	123961	1328	28030	21312	Cluster	2006	6
41	University of Southern California	University of Southern California	123961	1328	13810	21312	Cluster	2006	11
52	University of Southern California	University of Southern California	123961	1328	13810	20520	Cluster	2007	6
63	University of Southern California	University of Southern California	123961	1328	15780	23680	Cluster	2007	11
63	University of Southern California	University of Southern California	123961	1328	17110	52428	Cluster	2008	6
126	University of Southern California	University of Southern California	123961	1328	30990	23680	Cluster	2008	6
61	University of Southern California	University of Southern California	123961	1328	17110	56304	Cluster	2008	11
273	University of Southern California	University of Southern California	123961	1328	44190	23680	Cluster	2008	11
76	University of Southern California	University of Southern California	123961	1328	17110	65640	Cluster	2009	6
498	University of Southern California	University of Southern California	123961	1328	51410	23680	Cluster	2009	6
71	University of Southern California	University of Southern California	123961	1328	17110	94208	Cluster	2009	11
65	University of Southern California	University of Southern California	123961	1328	72050	107308	Cluster	2010	6
79	University of Southern California	University of Southern California	123961	1328	83290	119600	Cluster	2010	11
63	University of Southern California	University of Southern California	123961	1328	94220	175990	Cluster	2011	6
79	University of Southern California	University of Southern California	123961	1328	126400	196465	Cluster	2011	11
253	University of Utah	University of Utah	230764	3675	149900	23183	Cluster	2008	11
480	University of Utah	University of Utah	230764	3675	17520	23183	Cluster	2009	6
121	UT SimCenter at Chattanooga	"University of Tennessee-Chattanooga, The"	221740	3529	17520	6464	Cluster	2006	6
186	UT SimCenter at Chattanooga	"University of Tennessee-Chattanooga, The"	221740	3529	4335	6464	Cluster	2006	11
457	UT SimCenter at Chattanooga	"University of Tennessee-Chattanooga, The"	221740	3529	4335	6464	Cluster	2007	6
312	UT SimCenter at Chattanooga	"University of Tennessee-Chattanooga, The"	221740	3529	4335	15552	Cluster	2007	11
140	Vanderbilt University	Vanderbilt University	221999	3535	7705	5808	Cluster	2006	6
235	Vanderbilt University	Vanderbilt University	221999	3535	3842.03	5808	Cluster	2006	11
28	Virginia Tech	Virginia Polytechnic Institute and State Univ	233921	3754	3842.03	20240	Cluster	2006	6
47	Virginia Tech	Virginia Polytechnic Institute and State Univ	233921	3754	12250	20240	Cluster	2006	11
71	Virginia Tech	Virginia Polytechnic Institute and State Univ	233921	3754	12250	20240	Cluster	2007	6
108	Virginia Tech	Virginia Polytechnic Institute and State Univ	233921	3754	12250	20240	Cluster	2007	11
280	Virginia Tech	Virginia Polytechnic Institute and State Univ	233921	3754	12250	20240	Cluster	2008	6
96	Virginia Tech	Virginia Polytechnic Institute and State Univ	233921	3754	120400	238201.6	Cluster	2011	11
275	Yale University	Yale University	130794	1426	52526.3	63078.4	Cluster	2011	6
356	Yale University	Yale University	130794	1426	57800	63078.4	Cluster	2011	11
