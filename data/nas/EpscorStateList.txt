State	StateCode	yearEPSCOR
Alabama	AL	1985
Alaska	AK	2000
Arkansas	AR	1980
Delaware	DE	2003
Guam	GU	2012
Hawaii	HI	2001
Idaho	ID	1987
Iowa	IA	2009
Kansas	KS	1992
Kentucky	KY	1985
Louisiana	LA	1987
Maine	ME	1980
Mississippi	MS	1987
Missouri	MO	2009
Montana	MT	1980
Nebraska	NE	1992
Nevada	NV	1985
New Hampshire	NH	2004
New Mexico	NM	2001
North Dakota	ND	1985
Oklahoma	OK	1985
Puerto Rico	PR	1985
Rhode Island	RI	2004
South Carolina	SC	1980
South Dakota	SD	1987
Tennessee	TN	2004
U.S. Virgin Islands	VI	2002
Utah	UT	2009
Vermont	VT	1985
West Virginia	WV	1980
Wyoming	WY	1985
