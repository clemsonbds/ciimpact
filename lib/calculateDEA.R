# calculate normal dea
get.dea <- function(input.data, input.set, output.set, orientation, rts){
  x <- matrix(nrow=(length(input.set)),ncol=length(input.data[,1]))
  x.names <- c()
  for (k in 1:length(input.set)){
    if (input.set[k] == 22){
      x[k,] <- as.vector(input.data[,input.set[k]]) * as.vector(input.data[,21])  
    }
    else if (input.set[k] == 23){
      x[k,] <- as.vector(input.data[,input.set[k]]) * as.vector(input.data[,21])  
    }
    else if (input.set[k] == 21){
      input.data <- subset(input.data, !is.na(input.data[,22]))
      input.data <- subset(input.data, !is.na(input.data[,23]))      
      x[k,] <- as.vector(input.data[,input.set[k]]) * 
        (as.vector(input.data[,22]) + as.vector(input.data[,23]))
    }
    else {
      x[k,] <- as.vector(input.data[,input.set[k]])
    }
    x.names <- c(x.names,names(input.data)[input.set[k]])
  }
  
  y <- matrix(nrow=(length(output.set)),ncol=length(input.data[,1]))
  y.names <- c()
  for (k in 1:length(output.set)){    
    if (output.set[k] != 8){
      if (output.set[k] == 9){
       # y[k,] <- as.vector(input.data[,output.set[k]]) * as.vector(input.data[,18])
        y[k,] <- as.vector(input.data[,output.set[k]])
        #        input.data[,output.set[k]] <- as.vector(input.data[,output.set[k]]) * as.vector(input.data[,18])
      }      
      else {
        y[k,] <- as.vector(input.data[,output.set[k]])
      }
    } else{
      y[k,] <- max(as.vector(input.data[,output.set[k]])) + 1 - as.vector(input.data[,output.set[k]])
    }
    y.names <- c(y.names,names(input.data)[output.set[k]])
  }
  
  dhat <- dea(XOBS=x,YOBS=y,RTS=rts, ORIENTATION=orientation)
  #  output.dea <- dea.subsample(dhat, XOBS=x, YOBS=y, ORIENTATION=2)
  #  output.dea.ci <- t(output.dea$ci)
  
  #  output.dea <- cbind(input.data,DEA=dhat,dea.lower=output.dea.ci[,2],dea.upper=output.dea.ci[,5])
  output.dea <- cbind(input.data,DEA=dhat)
  output.dea <- data.frame(output.dea,stringsAsFactors=FALSE)
  output.dea
}

#calculate FDH instead of dea
get.fdh <- function(input.data, input.set, output.set, orientation){
  x <- matrix(nrow=(length(input.set)),ncol=length(input.data[,1]))
  x.names <- c()
  for (k in 1:length(input.set)){
    if (input.set[k] == 22){
      x[k,] <- as.vector(input.data[,input.set[k]]) * as.vector(input.data[,21])  
    }
    else if (input.set[k] == 23){
      x[k,] <- as.vector(input.data[,input.set[k]]) * as.vector(input.data[,21])  
    }
    else if (input.set[k] == 21){
      input.data <- subset(input.data, !is.na(input.data[,22]))
      input.data <- subset(input.data, !is.na(input.data[,23]))      
      x[k,] <- as.vector(input.data[,input.set[k]]) * 
        (as.vector(input.data[,22]) + as.vector(input.data[,23]))
    }
    else {
      x[k,] <- as.vector(input.data[,input.set[k]])
    }
    x.names <- c(x.names,names(input.data)[input.set[k]])
  }
  
  y <- matrix(nrow=(length(output.set)),ncol=length(input.data[,1]))
  y.names <- c()
  for (k in 1:length(output.set)){    
    if (output.set[k] != 8){
      if (output.set[k] == 9){
        # y[k,] <- as.vector(input.data[,output.set[k]]) * as.vector(input.data[,18])
        y[k,] <- as.vector(input.data[,output.set[k]])
        #        input.data[,output.set[k]] <- as.vector(input.data[,output.set[k]]) * as.vector(input.data[,18])
      }      
      else {
        y[k,] <- as.vector(input.data[,output.set[k]])
      }
    } else{
      y[k,] <- max(as.vector(input.data[,output.set[k]])) + 1 - as.vector(input.data[,output.set[k]])
    }
    y.names <- c(y.names,names(input.data)[output.set[k]])
  }
  
  dhat <- fdh.eff(XOBS=x,YOBS=y, ORIENTATION=orientation)
  #  output.dea <- dea.subsample(dhat, XOBS=x, YOBS=y, ORIENTATION=2)
  #  output.dea.ci <- t(output.dea$ci)
  
  #  output.dea <- cbind(input.data,DEA=dhat,dea.lower=output.dea.ci[,2],dea.upper=output.dea.ci[,5])
  output.dea <- cbind(input.data,DEA=dhat)
  output.dea <- data.frame(output.dea,stringsAsFactors=FALSE)
  output.dea
}



get.dea.rts <- function(input.data, input.set, output.set){
  x <- matrix(nrow=(length(input.set)),ncol=length(input.data[,1]))
  x.names <- c()
  for (k in 1:length(input.set)){
    if (input.set[k] == 22){
      x[k,] <- as.vector(input.data[,input.set[k]]) * as.vector(input.data[,21])  
    }
    else if (input.set[k] == 23){
      x[k,] <- as.vector(input.data[,input.set[k]]) * as.vector(input.data[,21])  
    }
    else if (input.set[k] == 21){
      input.data <- subset(input.data, !is.na(input.data[,22]))
      input.data <- subset(input.data, !is.na(input.data[,23]))      
      x[k,] <- as.vector(input.data[,input.set[k]]) * 
        (as.vector(input.data[,22]) + as.vector(input.data[,23]))
    }
    else {
      x[k,] <- as.vector(input.data[,input.set[k]])
    }
    x.names <- c(x.names,names(input.data)[input.set[k]])
  }
  
  y <- matrix(nrow=(length(output.set)),ncol=length(input.data[,1]))
  y.names <- c()
  for (k in 1:length(output.set)){    
    if (output.set[k] != 8){
      if (output.set[k] == 9){
        y[k,] <- as.vector(input.data[,output.set[k]]) * as.vector(input.data[,18])
      }      
      else {
        y[k,] <- as.vector(input.data[,output.set[k]])
      }
    } else{
      y[k,] <- max(as.vector(input.data[,output.set[k]])) + 1 - as.vector(input.data[,output.set[k]])
    }
    y.names <- c(y.names,names(input.data)[output.set[k]])
  }
  
 rts.test(x,y)
}

get.dea.convex <- function(input.data, input.set, output.set){
  x <- matrix(nrow=(length(input.set)),ncol=length(input.data[,1]))
  x.names <- c()
  for (k in 1:length(input.set)){
    if (input.set[k] == 22){
      x[k,] <- as.vector(input.data[,input.set[k]]) * as.vector(input.data[,21])  
    }
    else if (input.set[k] == 23){
      x[k,] <- as.vector(input.data[,input.set[k]]) * as.vector(input.data[,21])  
    }
    else if (input.set[k] == 21){
      input.data <- subset(input.data, !is.na(input.data[,22]))
      input.data <- subset(input.data, !is.na(input.data[,23]))      
      x[k,] <- as.vector(input.data[,input.set[k]]) * 
        (as.vector(input.data[,22]) + as.vector(input.data[,23]))
    }
    else {
      x[k,] <- as.vector(input.data[,input.set[k]])
    }
    x.names <- c(x.names,names(input.data)[input.set[k]])
  }
  
  y <- matrix(nrow=(length(output.set)),ncol=length(input.data[,1]))
  y.names <- c()
  for (k in 1:length(output.set)){    
    if (output.set[k] != 8){
      if (output.set[k] == 9){
        y[k,] <- as.vector(input.data[,output.set[k]]) * as.vector(input.data[,18])
      }      
      else {
        y[k,] <- as.vector(input.data[,output.set[k]])
      }
    } else{
      y[k,] <- max(as.vector(input.data[,output.set[k]])) + 1 - as.vector(input.data[,output.set[k]])
    }
    y.names <- c(y.names,names(input.data)[output.set[k]])
  }
  
  convex.test(x,y)
}

get.dea.data <- function(input.data, input.set, output.set){
  x <- matrix(nrow=(length(input.set)),ncol=length(input.data[,1]))
  x.names <- c()
  for (k in 1:length(input.set)){
    if (input.set[k] == 22){
      x[k,] <- as.vector(input.data[,input.set[k]]) * as.vector(input.data[,21])  
    }
    else if (input.set[k] == 23){
      x[k,] <- as.vector(input.data[,input.set[k]]) * as.vector(input.data[,21])  
    }
    else if (input.set[k] == 21){
      input.data <- subset(input.data, !is.na(input.data[,22]))
      input.data <- subset(input.data, !is.na(input.data[,23]))      
      x[k,] <- as.vector(input.data[,input.set[k]]) * 
        (as.vector(input.data[,22]) + as.vector(input.data[,23]))
    }
    else {
      x[k,] <- as.vector(input.data[,input.set[k]])
    }
    x.names <- c(x.names,names(input.data)[input.set[k]])
  }
  
  y <- matrix(nrow=(length(output.set)),ncol=length(input.data[,1]))
  y.names <- c()
  for (k in 1:length(output.set)){    
    if (output.set[k] != 8){
      if (output.set[k] == 9){
        y[k,] <- as.vector(input.data[,output.set[k]]) * as.vector(input.data[,18])
      }      
      else {
        y[k,] <- as.vector(input.data[,output.set[k]])
      }
    } else{
      y[k,] <- max(as.vector(input.data[,output.set[k]])) + 1 - as.vector(input.data[,output.set[k]])
    }
    y.names <- c(y.names,names(input.data)[output.set[k]])
  }
  
  output <- t(rbind(x,y))
}