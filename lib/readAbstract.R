getAbstract <- function(journal.name, journal.dir){
  col.names <- c("PT","AU","BA","BE","GP","AF","BF","CA","TI","SO","SE","LA",
                 "DT","CT","CY","CL","SP","HO","DE","ID","AB","C1","RP","EM",
                 "RI","FU","FX","CR","NR","TC","Z9","PU","PI","PA","SN","BN",
                 "J9","JI","PD","PY","VL","IS","PN","SU","SI","MA","BP","EP",
                 "AR","DI","D2","PG","P2","WC","SC","GA","UT","--")
  hpc.dir <- file.path(journal.dir,journal.name)
  file.list <- dir(hpc.dir)
  file.count <- length(file.list)
  
  abstract.counts <- numeric(file.count)
  for (i in 1:file.count){
    file.name <- file.path(hpc.dir,file.list[i])
    tmp.content <- read.csv(file.name,header=FALSE,sep="\t",col.names=col.names,
                            skip=1,stringsAsFactors=FALSE)
    abstract.counts[i] <- nrow(tmp.content)
  }
  
  abstract.list <- character(sum(abstract.counts))
  bIndex <- 1
  for (i in 1:file.count){
    file.name <- file.path(hpc.dir,file.list[i])
    tmp.content <- read.csv(file.name,header=FALSE,sep="\t",col.names=col.names,
                            skip=1,stringsAsFactors=FALSE)
    eIndex <- bIndex + nrow(tmp.content) - 1
    abstract.list[bIndex:eIndex] <- tmp.content$AB
    bIndex <- eIndex + 1
  }
  abstract.list <- subset(abstract.list,abstract.list[] != "")
  abstract.list <- subset(abstract.list,!is.na(abstract.list[]))
  abstract.list
}

getTitleYear <- function(journal.name, journal.dir){
  col.names <- c("PT","AU","BA","BE","GP","AF","BF","CA","TI","SO","SE","LA",
                 "DT","CT","CY","CL","SP","HO","DE","ID","AB","C1","RP","EM",
                 "RI","FU","FX","CR","NR","TC","Z9","PU","PI","PA","SN","BN",
                 "J9","JI","PD","PY","VL","IS","PN","SU","SI","MA","BP","EP",
                 "AR","DI","D2","PG","P2","WC","SC","GA","UT","--")
  hpc.dir <- file.path(journal.dir,journal.name)
  file.list <- dir(hpc.dir)
  file.count <- length(file.list)
  
  publication.counts <- numeric(file.count)
  for (i in 1:file.count){
    file.name <- file.path(hpc.dir,file.list[i])
    tmp.content <- read.csv(file.name,header=FALSE,sep="\t",col.names=col.names,
                            skip=1,stringsAsFactors=FALSE)
    publication.counts[i] <- nrow(tmp.content)
  }
  
  publication.list <- character(sum(publication.counts))
  publication.year <- character(sum(publication.counts))
  bIndex <- 1
  for (i in 1:file.count){
    file.name <- file.path(hpc.dir,file.list[i])
    tmp.content <- read.csv(file.name,header=FALSE,sep="\t",col.names=col.names,
                            skip=1,stringsAsFactors=FALSE)
    eIndex <- bIndex + nrow(tmp.content) - 1
    publication.list[bIndex:eIndex] <- tmp.content$TI
    publication.year[bIndex:eIndex] <- tmp.content$PY
    
    bIndex <- eIndex + 1
  }
  
  publication.list <- data.frame(Title=publication.list, Year=publication.year)
  publication.list <- subset(publication.list,publication.list$Title != "")
  publication.list <- subset(publication.list,!is.na(publication.list$Title))
  publication.list <- subset(publication.list,!is.na(as.numeric(publication.list$Year)))
  publication.list
}


getAbstractNSF <- function(input.file){
  wd.dir <- file.path("D:","Work","Dropbox","R","ciimpact")
  file.name <- file.path(wd.dir,input.file)
  file.content <- read.csv(file.name,header=TRUE,sep="\t",
                           stringsAsFactors=FALSE)
  file.content
}

#hpc.abstract <- getAbstract("J_COMPUT_CHEM")
#printWordCloud(hpc.abstract,"hpc.png")

# nsf.abstract <- getAbstractNSF("nsf_che.txt")
# year <- seq(1991,2009)
# for (i in 1:length(year)){
#   nsf.abstract.yearly <- subset(nsf.abstract$Abstract,
#                                 nsf.abstract$Year == as.character(year[i]))
#   nsf.abstract.yearly <- subset(nsf.abstract.yearly,
#                                 nsf.abstract.yearly != "")
#   if (length(nsf.abstract.yearly) > 0){
#     print(length(nsf.abstract.yearly))
#     output.file <- paste(year[i],".png",sep="")
#     printWordCloud(nsf.abstract.yearly,output.file)
#   }
# }
