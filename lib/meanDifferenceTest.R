# we assume that orientation is always 2

set.seed(42)

mean.diff.dea <- function(input.data, input.set, output.set, orientation, rts){
  # split data into HPC and NonHPC
  beta.hpc <- 0
  beta.nonhpc <- 0
  
  for (i in 1:100){
    input.data <- input.data[sample(nrow(input.data)),]
    input.hpc <- subset(input.data,input.data$APP[] > 0)
    input.nonhpc <- subset(input.data,input.data$APP[] == 0)
    
    output.all <- get.dea(input.data,input.set,output.set,orientation, rts)
    
    n.hpc <- length(input.hpc[,1])
    output.hpc <- get.dea(input.hpc,input.set,output.set,orientation, rts)
    output.hpc.1 <- get.dea(input.hpc[1:(n.hpc%/%2),],input.set,output.set,orientation, rts)
    output.hpc.2 <- get.dea(input.hpc[(n.hpc%/%2 + 1):n.hpc,],input.set,output.set,orientation, rts)
    
    n.nonhpc <- length(input.nonhpc[,1])
    output.nonhpc <- get.dea(input.nonhpc,input.set,output.set,orientation, rts)
    output.nonhpc.1 <- get.dea(input.nonhpc[1:(n.nonhpc%/%2),],input.set,output.set,orientation, rts)
    output.nonhpc.2 <- get.dea(input.nonhpc[(n.nonhpc%/%2 + 1):n.nonhpc,],input.set,output.set,orientation, rts)
    
    if (orientation != 2){
      output.hpc$DEA <- 1/output.hpc$DEA
      output.hpc.1$DEA <- 1/output.hpc.1$DEA
      output.hpc.2$DEA <- 1/output.hpc.2$DEA
      output.nonhpc$DEA <- 1/output.nonhpc$DEA
      output.nonhpc.1$DEA <- 1/output.nonhpc.1$DEA
      output.nonhpc.2$DEA <- 1/output.nonhpc.2$DEA
      output.all$DEA <- 1/output.all$DEA
    }
    
    mu.hpc <- sum(output.hpc$DEA) / n.hpc
    mu.hpc.half <- 0.5 * (sum(output.hpc.1$DEA) / (n.hpc%/%2) + sum(output.hpc.2$DEA) / (n.hpc - n.hpc%/%2))
    
    mu.nonhpc <- sum(output.nonhpc$DEA) / n.nonhpc
    mu.nonhpc.half <- 0.5 * (sum(output.nonhpc.1$DEA) / (n.nonhpc%/%2) + sum(output.nonhpc.2$DEA) / (n.nonhpc - n.nonhpc%/%2))
    
    if (rts == 1){
      # variable return to scale
      k.rate <- 2 / (length(input.set) + length(output.set) + 1)
    } else if (rts == 3){ 
      # constant return to scale
      k.rate <- 2 / (length(input.set) + length(output.set))
    }        
    
    beta.hpc <- beta.hpc + (mu.hpc.half - mu.hpc) / (2^k.rate - 1)
    beta.nonhpc <- beta.nonhpc + (mu.nonhpc.half - mu.nonhpc) / (2^k.rate - 1)
  }
  
  beta.hpc <- beta.hpc / 100
  beta.nonhpc <- beta.nonhpc / 100
  
  mu.all <- sum(output.all$DEA) / length(output.all$DEA)
  
  phi.hpc <- sum((output.hpc$DEA - mu.all)^2) / n.hpc
  phi.nonhpc <- sum((output.nonhpc$DEA - mu.all)^2) / n.nonhpc
  
  T.hat <- ((mu.hpc - mu.nonhpc) - (beta.hpc - beta.nonhpc)) / ((((phi.hpc)^2)/n.hpc + ((phi.nonhpc)^2)/n.nonhpc)^0.5) 
  
  tmp1 <- pnorm(T.hat,lower.tail=FALSE,log.p=TRUE)
  tmp2 <- tmp1 / log(10)
  tmp3 <- 10^tmp2
  print(paste("test mu have:",mu.hpc,sep=""))
  print(paste("test mu have not:",mu.nonhpc,sep=""))
  print(paste("test phi have:",phi.hpc,sep=""))
  print(paste("test phi have not:",phi.nonhpc,sep=""))
  print(paste("test rho:",tmp3,sep=""))
  
  # calculate the lower and upper bounds:
  c <- 1.959964
  upper.hpc = mu.hpc - beta.hpc + c * phi.hpc * (length(output.hpc$DEA)^(-0.5))
  lower.hpc = mu.hpc - beta.hpc - c * phi.hpc * (length(output.hpc$DEA)^(-0.5))
  upper.nonhpc = mu.nonhpc - beta.nonhpc + c * phi.nonhpc * (length(output.nonhpc$DEA)^(-0.5))
  lower.nonhpc = mu.nonhpc - beta.nonhpc - c * phi.nonhpc * (length(output.nonhpc$DEA)^(-0.5))
  
#   upper.hpc = mu.hpc - beta.hpc + c * sd(output.hpc$DEA) * (length(output.hpc$DEA)^(-0.5))
#   lower.hpc = mu.hpc - beta.hpc - c * sd(output.hpc$DEA) * (length(output.hpc$DEA)^(-0.5))
#   upper.nonhpc = mu.nonhpc - beta.nonhpc + c * sd(output.nonhpc$DEA) * (length(output.nonhpc$DEA)^(-0.5))
#   lower.nonhpc = mu.nonhpc - beta.nonhpc - c * sd(output.nonhpc$DEA) * (length(output.nonhpc$DEA)^(-0.5))
  
  print(paste("Mean have:",mean(output.hpc$DEA),sep=""))
  print(paste("Bias have:",beta.hpc,sep=""))
  print(paste("Upper bound have:",upper.hpc,sep=""))
  print(paste("Lower bound have:",lower.hpc,sep=""))
  print(paste("Mean have not:",mean(output.nonhpc$DEA),sep=""))
  print(paste("Bias have not:",beta.nonhpc,sep=""))
  print(paste("Upper bound have not:",upper.nonhpc,sep=""))
  print(paste("Lower bound have not:",lower.nonhpc,sep=""))
  
  rho <- 1 - pnorm(T.hat)
  rho
}



mean.diff.fdh <- function(input.data, input.set, output.set, orientation){
  beta.hpc <- 0
  beta.nonhpc <- 0
  mu.hpc.fdh <- 0
  mu.nonhpc.fdh <- 0
  
  for (i in 1:100){
    input.data <- input.data[sample(nrow(input.data)),]
    # split data into HPC and NonHPC
    input.hpc <- subset(input.data,input.data$APP[] > 0)
    input.nonhpc <- subset(input.data,input.data$APP[] == 0)    
    output.all <- get.fdh(input.data,input.set,output.set,orientation)
    
    n.hpc <- length(input.hpc[,1])
    output.hpc <- get.fdh(input.hpc,input.set,output.set,orientation)
    output.hpc.1 <- get.fdh(input.hpc[1:(n.hpc%/%2),],input.set,output.set,orientation)
    output.hpc.2 <- get.fdh(input.hpc[(n.hpc%/%2 + 1):n.hpc,],input.set,output.set,orientation)
    
    n.nonhpc <- length(input.nonhpc[,1])
    output.nonhpc <- get.fdh(input.nonhpc,input.set,output.set,orientation)
    output.nonhpc.1 <- get.fdh(input.nonhpc[1:(n.nonhpc%/%2),],input.set,output.set,orientation)
    output.nonhpc.2 <- get.fdh(input.nonhpc[(n.nonhpc%/%2 + 1):n.nonhpc,],input.set,output.set,orientation)
    
    if (orientation != 2){
      output.hpc$DEA <- 1/output.hpc$DEA
      output.hpc.1$DEA <- 1/output.hpc.1$DEA
      output.hpc.2$DEA <- 1/output.hpc.2$DEA
      output.nonhpc$DEA <- 1/output.nonhpc$DEA
      output.nonhpc.1$DEA <- 1/output.nonhpc.1$DEA
      output.nonhpc.2$DEA <- 1/output.nonhpc.2$DEA
      output.all$DEA <- 1/output.all$DEA
    }
    
    mu.hpc <- sum(output.hpc$DEA) / n.hpc
    mu.hpc.half <- 0.5 * (sum(output.hpc.1$DEA) / (n.hpc%/%2) + sum(output.hpc.2$DEA) / (n.hpc - n.hpc%/%2))
    
    mu.nonhpc <- sum(output.nonhpc$DEA) / n.nonhpc
    mu.nonhpc.half <- 0.5 * (sum(output.nonhpc.1$DEA) / (n.nonhpc%/%2) + sum(output.nonhpc.2$DEA) / (n.nonhpc - n.nonhpc%/%2))
    
    k.rate <- 1 / (length(input.set) + length(output.set) + 1)
    
    beta.hpc <- beta.hpc + (mu.hpc.half - mu.hpc) / (2^k.rate - 1)
    beta.nonhpc <- beta.nonhpc + (mu.nonhpc.half - mu.nonhpc) / (2^k.rate - 1)
    
    hpc.subset <- sample(output.hpc$DEA,floor(length(output.hpc$DEA)^(0.5)))
    nonhpc.subset <- sample(output.nonhpc$DEA,floor(length(output.nonhpc$DEA)^(0.5)))
    mu.hpc.fdh <- mu.hpc.fdh + mean(hpc.subset)
    mu.nonhpc.fdh <- mu.nonhpc.fdh + mean(nonhpc.subset)   
  }
  beta.hpc <- beta.hpc/100
  beta.nonhpc <- beta.nonhpc/100
  
  mu.all <- sum(output.all$DEA) / length(output.all$DEA)
  
  phi.hpc <- sum((output.hpc$DEA - mu.all)^2) / n.hpc
  phi.nonhpc <- sum((output.nonhpc$DEA - mu.all)^2) / n.nonhpc
  
  
  # new FDH elements
  n.hpc.fdh <- floor((length(output.hpc[,1]))^(2*k.rate))  
  n.nonhpc.fdh <- floor((length(output.nonhpc[,1]))^(2*k.rate))
  
#   output.hpc.fdh <- get.fdh(input.hpc,input.set,output.set,orientation)  
#   output.nonhpc.fdh <- get.fdh(input.nonhpc,input.set,output.set,orientation)
#   mu.hpc.fdh <- sum(sample(output.hpc.fdh$DEA,n.hpc.fdh)) / n.hpc.fdh
#   mu.nonhpc.fdh <- sum(sample(output.nonhpc.fdh$DEA,n.hpc.fdh)) / n.nonhpc.fdh
  
  mu.hpc.fdh <- mu.hpc.fdh / 100
  mu.nonhpc.fdh <- mu.nonhpc.fdh / 100
    
  T.hat <- ((mu.hpc.fdh - mu.nonhpc.fdh) - (beta.hpc - beta.nonhpc)) / 
    ((((phi.hpc)^2)/n.hpc.fdh + ((phi.nonhpc)^2)/n.nonhpc.fdh)^0.5) 
  
  tmp1 <- pnorm(T.hat,lower.tail=FALSE,log.p=TRUE)
  tmp2 <- tmp1 / log(10)
  tmp3 <- 10^tmp2
  
  print(paste("test mu have fdh:",mu.hpc.fdh,sep=""))
  print(paste("test mu have not fdh:",mu.nonhpc.fdh,sep=""))
  print(paste("test phi have:",phi.hpc,sep=""))
  print(paste("test phi have not:",phi.nonhpc,sep=""))
  print(paste("test rho:",tmp3,sep=""))
  
  # calculate the lower and upper bounds: do we need to sample when we take 1/4 of the data?
  c <- 1.959964
  
  #  for (i in 1:100){
  #  hpc.subset <- sample(output.hpc$DEA,floor(length(output.hpc$DEA)^(0.5)))
  #  nonhpc.subset <- sample(output.nonhpc$DEA,floor(length(output.nonhpc$DEA)^(0.5)))
  
  #  upper.hpc = mean(hpc.subset) - beta.hpc + c * sd(output.hpc$DEA) * (length(output.hpc$DEA)^(-0.25))
  #  lower.hpc = mean(hpc.subset) - beta.hpc - c * sd(output.hpc$DEA) * (length(output.hpc$DEA)^(-0.25))
  #  upper.nonhpc = mean(nonhpc.subset) - beta.nonhpc + c * sd(output.nonhpc$DEA) * (length(output.nonhpc$DEA)^(-0.25))
  #  lower.nonhpc = mean(nonhpc.subset) - beta.nonhpc - c * sd(output.nonhpc$DEA) * (length(output.nonhpc$DEA)^(-0.25))

  upper.hpc = mu.hpc.fdh - beta.hpc + c * phi.hpc * (length(output.hpc$DEA)^(-0.25))
  lower.hpc = mu.hpc.fdh - beta.hpc - c * phi.hpc * (length(output.hpc$DEA)^(-0.25))
  upper.nonhpc = mu.nonhpc.fdh - beta.nonhpc + c * phi.nonhpc * (length(output.nonhpc$DEA)^(-0.25))
  lower.nonhpc = mu.nonhpc.fdh - beta.nonhpc - c * phi.nonhpc * (length(output.nonhpc$DEA)^(-0.25))
  
#   upper.hpc = mu.hpc.fdh - beta.hpc + c * sd(output.hpc$DEA) * (length(output.hpc$DEA)^(-0.25))
#   lower.hpc = mu.hpc.fdh - beta.hpc - c * sd(output.hpc$DEA) * (length(output.hpc$DEA)^(-0.25))
#   upper.nonhpc = mu.nonhpc.fdh - beta.nonhpc + c * sd(output.nonhpc$DEA) * (length(output.nonhpc$DEA)^(-0.25))
#   lower.nonhpc = mu.nonhpc.fdh - beta.nonhpc - c * sd(output.nonhpc$DEA) * (length(output.nonhpc$DEA)^(-0.25))
  #  }
  
  print(paste("Mean have:",mean(output.hpc$DEA),sep=""))
  print(paste("Bias have:",beta.hpc,sep=""))  
  print(paste("Upper bound have:",upper.hpc,sep=""))
  print(paste("Lower bound have:",lower.hpc,sep=""))
  print(paste("Mean have not:",mean(output.nonhpc$DEA),sep=""))
  print(paste("Bias have not:",beta.nonhpc,sep=""))
  print(paste("Upper bound have not:",upper.nonhpc,sep=""))
  print(paste("Lower bound have not:",lower.nonhpc,sep=""))
  
  rho <- 1 - pnorm(T.hat)
  rho
}