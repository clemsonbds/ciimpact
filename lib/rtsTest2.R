rts.test.2 <- function(X,Y) {
  
  n=ncol(X)
  n1=floor(n/2)
  n2=n-n1
  x1=as.matrix(X[,1:n1])
  y1=as.matrix(Y[,1:n1])
  x2=as.matrix(X[,(n1+1):n])
  y2=as.matrix(Y[,(n1+1):n])
  print(x1)
  print(y1)
  print(x2)
  print(y2)
  require(FEAR)
  d1=dea(XOBS=x1,YOBS=y1,RTS=1,ORIENTATION=2)
  print(d1)
  d2=dea(XOBS=x2,YOBS=y2,RTS=3,ORIENTATION=2)
  print(d2)
  tm1=mean(d1)
  tm2=mean(d2)
  sig1.2=var(d1)
  sig2.2=var(d2)
  #
  # bias corrections: 
  n11=floor(n1/2)
  d1a=dea(XOBS=x1[,1:n11],YOBS=y1[,1:n11],RTS=1,ORIENTATION=2)
  print(d1a)
  d1b=dea(XOBS=x1[,(n11+1):n1],YOBS=y1[,(n11+1):n1],RTS=1,ORIENTATION=2)
  print(d1b)
  n21=floor(n2/2)
  d2a=dea(XOBS=x2[,1:n21],YOBS=y2[,1:n21],RTS=1,ORIENTATION=2)
  print(d2a)
  d2b=dea(XOBS=x2[,(n21+1):n2],YOBS=y2[,(n21+1):n2],RTS=1,ORIENTATION=2)
  print(d2b)
  t1=mean(d1a)
  t2=mean(d1b)
  t3=0.5*(t1+t2)
  np=nrow(x1)
  nq=nrow(y1)
  kappa=2/(np+nq)
  fac=1/(2**kappa-1)
  bc1=fac*(t3-tm1)
  t1=mean(d2a)
  t2=mean(d2b)
  t3=0.5*(t1+t2)
  bc2=fac*(t3-tm2)
  if ((np+nq)<=5) {
    t3=sig1.2/n1 + sig2.2/n2
    t4=sqrt(t3)
    t5=tm1-tm2-(bc1-bc2)
    that=t5*t4
  } else {
    n1k=floor(n1**(2*kappa))
    n2k=floor(n2**(2*kappa))
    tmk1=mean(d1[1:n1k])
    tmk2=mean(d2[1:n2k])
    t3=sig1.2/n1k + sig2.2/n2k
    t4=sqrt(t3)
    t5=tmk1-tmk2-(bc1-bc2)
    that=t5*t4
  }
  pval=1-pnorm(that)
  return(list(that=that,pval=pval))
}
