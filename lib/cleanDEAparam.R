clean.NAS <- function(input.data, input.set, output.set){
  num.col <- length(input.data)
  for (k in 7:num.col){
    input.data[,k] <- as.numeric(input.data[,k])
  }
  for (k in 1:length(input.set)){
    input.data <- subset(input.data, !is.na(input.data[,input.set[k]]))
    if (input.set[k] == 21){
      input.data <- subset(input.data, !is.na(input.data[,22]) & !is.na(input.data[,23]))
    }
  }
  for (k in 1:length(output.set)){
    input.data <- subset(input.data, !is.na(input.data[,output.set[k]]))
  }
  input.data
}

clean.NAS.noZero <- function(input.data, input.set, output.set, orientation){
  num.col <- length(input.data)
  for (k in 7:num.col){
    input.data[,k] <- as.numeric(input.data[,k])
  }
  for (k in 1:length(input.set)){
    input.data <- subset(input.data, !is.na(input.data[,input.set[k]]))
    if (input.set[k] == 21){
      input.data <- subset(input.data, !is.na(input.data[,22]) & !is.na(input.data[,23]))
    }
  }
  for (k in 1:length(output.set)){
    input.data <- subset(input.data, !is.na(input.data[,output.set[k]]))
  }
  
  
  
  input.data
}

genParamLabel <- function(input.data, param.set, paramName){
  paramLab <- paramName
  for (k in 1:length(param.set)){
    paramLab <- paste(paramLab,colnames(input.data)[param.set[k]],sep="  ")
  }
  paramLab
}