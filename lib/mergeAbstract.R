# This program should only be run once, and it is responsible for combining the text files together. 
work.dir <- file.path("D:","Work","Dropbox","R","ciimpact")
#work.dir <- file.path("/home","lngo","Dropbox","R","ciimpact")
setwd(work.dir)

lib.dir <- file.path(work.dir,"lib")
data.dir <- file.path(work.dir,"data")

#### load data files ###########################################################
load(file.path(data.dir,"nas","nas.RData"))
load(file.path(data.dir,"nas","fieldList.RData"))
load(file.path(data.dir,"top500","top500.RData"))
nsf.facilities.dir <- file.path(work.dir,"data","nsf","facilities")
nsf.facilities.file.path <- file.path(nsf.facilities.dir,"nsf_facilities_2005.txt")
nsf.facilities <- read.csv(nsf.facilities.file.path,
                           header=TRUE,sep="\t",stringsAsFactors=FALSE)


getAbstract <- function(journal.name){
  col.names <- c("PT","AU","BA","BE","GP","AF","BF","CA","TI","SO","SE","LA",
                 "DT","CT","CY","CL","SP","HO","DE","ID","AB","C1","RP","EM",
                 "RI","FU","FX","CR","NR","TC","Z9","PU","PI","PA","SN","BN",
                 "J9","JI","PD","PY","VL","IS","PN","SU","SI","MA","BP","EP",
                 "AR","DI","D2","PG","P2","WC","SC","GA","UT","--")
  raw.journal.dir <- file.path(data.dir,"chem_abstract","raw",journal.name)
  
  # directory that contains the merged data files
  clean.journal.dir <- file.path(data.dir,"chem_abstract","clean",journal.name)
  dir.create(clean.journal.dir,recursive=TRUE)
  file.list <- dir(raw.journal.dir)
  file.count <- length(file.list)
  
  setwd(raw.journal.dir)
  abstract.counts <- numeric(file.count)
  for (i in 1:file.count){
    file.name <- file.path(raw.journal.dir,file.list[i])
    tmp.content <- read.csv(file.name,header=FALSE,sep="\t",col.names=col.names,
                            skip=1,stringsAsFactors=FALSE)
    abstract.counts[i] <- nrow(tmp.content)
  }
  print(abstract.counts)
  abstract.counts
#   abstract.list <- character(sum(abstract.counts))
#   bIndex <- 1
#   for (i in 1:file.count){
#     file.name <- file.path(hpc.dir,file.list[i])
#     tmp.content <- read.csv(file.name,header=FALSE,sep="\t",col.names=col.names,
#                             skip=1,stringsAsFactors=FALSE)
#     eIndex <- bIndex + nrow(tmp.content) - 1
#     abstract.list[bIndex:eIndex] <- tmp.content$AB
#     bIndex <- eIndex + 1
#   }
# 
#   colnames(merged.output) <- col.names
#   clean.file <- file.path(clean.journal.dir,
#                           paste(journal.name,".csv",sep=""))
#   setwd(clean.journal.dir)
#   write.csv(merged.output,clean.file,sep="\t",col.names=col.names,
#             row.names=FALSE)
#   
#   setwd(work.dir)
#   merged.output
}

test.output <- getAbstract("J_COMPUT_CHEM")