#! C:\\Program Files\\R\\R-2.14.2\\bin\\Rscript.exe
options(digits=2)
#### load libraries and set up directory paths #################################
library(FEAR)

work.dir <- file.path("D:","Work","R","ciimpact")
#work.dir <- file.path("/home","lngo","Dropbox","R","ciimpact")
setwd(work.dir)

data.dir <- file.path(work.dir,"data","ncses","academicinstitutionprofiles")

#### load data files ###########################################################
load(file.path(work.dir,"data","nas","nas.RData"))

#### check and create subdirectories ###########################################

uniqueInstitutionList <- unique(input.file$Institution.Name.)

setwd(data.dir)
for (i in 1:length(uniqueInstitutionList)){
  if (!file.exists(uniqueInstitutionList[i])){
    dir.create(file.path(data.dir, uniqueInstitutionList[i]))    
  }  
}

dlist <- list.files(data.dir)
for (i in 1:length(dlist)){
  setwd(file.path(data.dir,dlist[i]))
  flist <- list.files(pattern = "zip")
  if (length(flist) > 0){
    
    for (j in 1:length(flist)){
      print(flist[j])
      unzip(flist[j])
    }
  }
  setwd(data.dir)
}